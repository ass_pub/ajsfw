/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { ServiceCtor } from "ajsfw/core/services/Service";
import { AjsFileReader } from "ajsfw/services/data/providers/FileDataProvider/AjsFileReader";
import { AjsFileReaderCached } from "ajsfw/services/data/providers/FileDataProvider/AjsFileReaderCached";
import { IIFileProvider } from "ajsfw/services/data/providers/FileDataProvider/FileProvider";
import { AjsLang } from "ajsfw/services/lang/AjsLang";
import { IILang } from "ajsfw/services/lang/Lang";
import { AjsNavigationService } from "ajsfw/services/navigation/AjsNavigationService";
import { IINavigationService } from "ajsfw/services/navigation/NavigationService";
import { AjsPackageLoader } from "ajsfw/services/packages/AjsPackageLoader";
import { IIPackageLoader } from "ajsfw/services/packages/PackageLoader";
import { AjsTemplateManager } from "ajsfw/services/templating/AjsTemplateManager";
import { AjsTranslatedTemplateManager } from "ajsfw/services/templating/AjsTranslatedTemplateManager";
import { IITemplateManager } from "ajsfw/services/templating/TemplateManager";
import { AjsUrlBinderService } from "ajsfw/services/urlbinding/AjsUrlBinderService";
import { IIUrlBinder } from "ajsfw/services/urlbinding/UrlBinder";
import { AjsServiceBuilder } from "./AjsServiceBuilder";
import { IIVirtualView } from "ajsfw/services/virtualview/VirtualView";
import { AjsVirtualView } from "ajsfw/services/virtualview/AjsVirtualView";
import { IIView } from "ajsfw/services/mvvm/view/View";
import { IIViewServiceManager } from "ajsfw/services/mvvm/view/ViewServiceManager";
import { AjsViewServiceManager } from "ajsfw/services/mvvm/view/AjsViewServiceManager";
import { IIViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { AjsViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/AjsViewModelServiceManager";
import { IIDomUpdater } from "ajsfw/services/dom/DomUpdater";
import { AjsDomUpdater } from "ajsfw/services/dom/AjsDomUpdater";

export class AjsAppServiceBuilder extends AjsServiceBuilder<AjsAppServiceBuilder> {

    protected _usingOffline = false;
    protected _usingPackages = false;
    protected _usingModules = false;
    protected _usingLang = false;
    protected _usingNavigation = false;
    protected _usingUrlBinding = false;
    protected _usingTemplating = false;
    protected _usingMvvm = false;

    public useDefaults(): AjsAppServiceBuilder {
        return this;
    }

    public useOffline(): AjsAppServiceBuilder {
        this._usingOffline = true;
        return this;
    }

    public usePackages(): AjsAppServiceBuilder {
        this._usingPackages = true;
        return this;
    }

    public useAppModules(): AjsAppServiceBuilder {
        this.usePackages();
        this._usingModules = true;
        return this;
    }

    public useLang(): AjsAppServiceBuilder {
        this._usingLang = true;
        return this;
    }

    public useNavigation(): AjsAppServiceBuilder {
        this._usingNavigation = true;
        return this;
    }

    public useUrlBinding(): AjsAppServiceBuilder {
        this._usingNavigation = true;
        this._usingUrlBinding = true;
        return this;
    }

    public useTemplating(): AjsAppServiceBuilder {
        this._usingTemplating = true;
        return this;
    }

    public useMvvm(): AjsAppServiceBuilder {
        this.useNavigation();
        this.useUrlBinding();
        this.useTemplating();
        this._usingMvvm = true;
        return this;
    }

    public async build(): Promise<void> {

        // #ifdef DEBUG
        let usings = "";
        usings += this._usingOffline ? "AsjOffline " : "";
        usings += this._usingPackages ? "AjsPackages " : "";
        usings += this._usingModules ? "AjsApplicationModules " : "";
        usings += this._usingLang ? "AjsLang " : "";
        usings += this._usingNavigation ? "AjsNavigation " : "";
        usings += this._usingUrlBinding ? "AjsUrlBinding " : "";
        usings += this._usingTemplating ? "AjsTemplating " : "";
        usings += this._usingMvvm ? "AjsMvvm " : "";
        log.info(1, this, "Configuring application services (", usings.trim() , ")");
        // #endif

        // This file reader can be used globally in the application to load resources from the server
        if (this._usingOffline) {
            this.container.addSingleton("ResourceFileProvider", <ServiceCtor>AjsFileReaderCached);
        } else {
            this.container.addSingleton("ResourceFileProvider", <ServiceCtor>AjsFileReader);
        }

        if (this._usingPackages) {
            this.container.addSingleton(IIPackageLoader, <ServiceCtor>AjsPackageLoader).mapDep(IIFileProvider, "ResourceFileProvider");
        }

        if (this._usingModules) {
        }

        if (this._usingLang) {
            this.container.addSingleton(IILang, <ServiceCtor>AjsLang).mapDep(IIFileProvider, "ResourceFileProvider");
        }

        if (this._usingNavigation) {
            this.container.addSingleton(IINavigationService, <ServiceCtor>AjsNavigationService);
        }

        if (this._usingTemplating) {
            if (this._usingLang) {
                this.container.addSingleton(IITemplateManager, <ServiceCtor>AjsTranslatedTemplateManager).mapDep(IIFileProvider, "ResourceFileProvider");
            } else {
                this.container.addSingleton(IITemplateManager, <ServiceCtor>AjsTemplateManager).mapDep(IIFileProvider, "ResourceFileProvider");
            }
        }

        if (this._usingMvvm) {
            this.container.addSingleton(IIDomUpdater, <ServiceCtor>AjsDomUpdater);
            this.container.addSingleton(IIVirtualView, <ServiceCtor>AjsVirtualView);
            this.container.addSingleton(IIViewServiceManager, <ServiceCtor>AjsViewServiceManager);
            this.container.addSingleton(IIViewModelServiceManager, <ServiceCtor>AjsViewModelServiceManager);
            await this.container.registerServiceManager(IIView, IIViewServiceManager);
            await this.container.registerServiceManager(IIViewModel, IIViewModelServiceManager);
        }

        if (this._usingUrlBinding) {
            this.container.addSingleton(IIUrlBinder, <ServiceCtor>AjsUrlBinderService);
            await this.container.registerPropertyInjector(IIUrlBinder);
        }

    }

}
