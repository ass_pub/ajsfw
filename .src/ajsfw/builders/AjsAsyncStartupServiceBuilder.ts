/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { ServiceCtor } from "ajsfw/core/services/Service";
import { AjsFileReader } from "ajsfw/services/data/providers/FileDataProvider/AjsFileReader";
import { SIFileReader } from "ajsfw/services/data/providers/FileDataProvider/DependencyIdentifiers";
import { AjsHttpDataProvider } from "ajsfw/services/data/providers/HttpDataProvider/AjsHttpDataProvider";
import { IIHttpDataProvider } from "ajsfw/services/data/providers/HttpDataProvider/HttpDataProvider";
import { DIAppConfigUri, IIAppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { AjsAppConfig } from "ajsfw/services/appstartup/async/AjsAppConfig";
import { AjsServiceBuilder } from "./AjsServiceBuilder";

export class AjsAsyncStartupServiceBuilder extends AjsServiceBuilder<AjsAsyncStartupServiceBuilder> {

    protected _usedAppConfig = false;

    public useDefaults(): AjsAsyncStartupServiceBuilder {
        return this;
    }

    public useAppConfig(configUri: string): AjsAsyncStartupServiceBuilder {
        this.container.
            registerSingletonInstance(DIAppConfigUri, configUri);

        this._usedAppConfig = true;
        return this;
    }

    public async build(): Promise<void> {

        // #ifdef DEBUG
        let usings = "";
        usings += this._usedAppConfig ? "AppConfig " : "";
        log.info(1, this, "Configuring async startup services (", usings.trim() , ")");
        // #endif

        if (this._usedAppConfig) {
            this.container.
                addSingleton(IIHttpDataProvider, <ServiceCtor>AjsHttpDataProvider).
                addSingleton(SIFileReader, <ServiceCtor>AjsFileReader).
                addSingleton(IIAppConfig, <ServiceCtor>AjsAppConfig);
        }

    }


}