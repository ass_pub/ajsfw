/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***********************************************************ß********************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { AjsOfflineStartup } from "ajsfw/services/appstartup/sync/AjsOfflineStartup";
import { AjsOnlineStartup } from "ajsfw/services/appstartup/sync/AjsOnlineStartup";
import { AjsServiceBuilder } from "./AjsServiceBuilder";

export class AjsStartupServiceBuilder extends AjsServiceBuilder<AjsStartupServiceBuilder> {

    protected _usedOnlineStartup = false;
    protected _usedOfflineStartup = false;

    public useDefaults(): AjsStartupServiceBuilder {
        this.
            useAjsOnlineStartup();
        return this;
    }

    public useAjsOnlineStartup(): AjsStartupServiceBuilder {
        this._usedOnlineStartup = true;
        return this;
    }

    public useAjsOfflineStartup(): AjsStartupServiceBuilder {
        this._usedOfflineStartup = true;
        return this;
    }

    public async build(): Promise<void> {
        // #ifdef DEBUG
        let usings = "";
        usings += this._usedOnlineStartup ? "AjsOnlineStartup " : "";
        usings += this._usedOfflineStartup ? "AjsOfflineStartup " : "";
        log.info(1, this, "Configuring startup services (", usings.trim() , ")");
        // #endif

        if (this._usedOfflineStartup) this.container.addTransient(AjsOfflineStartup);
        if (this._usedOnlineStartup) this.container.addTransient(AjsOnlineStartup);

    }

}
