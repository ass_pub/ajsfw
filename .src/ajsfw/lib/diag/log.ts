/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { anyToString } from "ajsfw/lib/utils/anyToString";
import { getBrowserType, BrowserType } from "ajsfw/lib/utils/browser";
import { ServiceDecoration } from "ajsfw/core/services/ServiceDecoration";
import { ServiceObjectDecorationsDic } from "ajsfw/services/dic/ServiceObjectDecorationsDic";

const browser: BrowserType = getBrowserType();

/** Function information stored with the log record */
export interface FunctionInfo {
    /** Name of the function */
    name: string;
    /** Function caller */
    caller: string;
    /** Caller source code */
    callerSource: string;
    /** Logger caller source */
    loggerCallerSource: string;
}


let logVerbosity = 9;

function fillSpacesRight(text: string, chars: number): string {
    if (text.length > chars) return text.substr(0, chars);
    while (text.length < chars) text += " ";
    return text;
}

function buildMessage(level: string, service: any, ...data: any[]): string {

    let styleChar;

    if (browser === BrowserType.InternetExplorer) {
        styleChar = "";
    } else {
        styleChar = "%c";
    }

    let out = styleChar + " ";
    out += fillSpacesRight(level, 11) + " " + styleChar + " ";

    if (typeof(service) === "object" && !Array.isArray(service)) {
        const id = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(service, { id: undefined });
        const idStr = id ? "#" + id : "";
        out += fillSpacesRight(anyToString(service.constructor) + idStr, 35) + " ";
    } else {
        out += fillSpacesRight(anyToString(service), 35) + " ";
    }

    for (const dout of data) {
        if (Array.isArray(dout)) {
            for (const din of dout) {
                const text = anyToString(din);
                const id = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(din, { id: undefined });
                const idStr = id ? "#" + id : "";
                if (text && text !== "") out += text + idStr;
                }
        } else {
            const text = anyToString(dout);
            const id = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(dout, { id: undefined });
            const idStr = id ? "#" + id : "";
            if (text && text !== "") out += text + idStr;
        }
    }

    return out  + " ";
}



function getFunctionInfo(): FunctionInfo {

    try {
        throw new Error("Error");
    } catch (e) {
        if (e.stack) {
            let functions: RegExpMatchArray = (<string>e.stack).match(/(at ).*\(.*\)/g);
            if (functions === null) {
                functions = (<string>e.stack).match(/.*\n/gm);
                if (functions && functions !== null) {
                    for (let i = 0; i < functions.length; i++) {
                        functions[i] = functions[i].substr(0, functions[i].length - 1);
                    }
                    return getNameAndCaller(functions);
                }
            }
            return getNameAndCaller(functions);
        }

    }

    return { name: "Unknown", caller: "Unknown", callerSource: "unknown", loggerCallerSource: "unknown" };
}

function getNameAndCaller(functions: RegExpMatchArray): FunctionInfo {

    if (functions === undefined || functions === null) {
        return { name: "Unknown", caller: "Unknown", callerSource: "unknown", loggerCallerSource: "unknown" };
    }

    if (functions.length === 0) {
        return { name: "Unknown", caller: "Unknown", callerSource: "unknown", loggerCallerSource: "unknown" };
    }

    const fi: FunctionInfo = <any>{};

    functions.shift();
    functions.shift();

    if (functions.length > 0) {
        if (functions[0].indexOf("at ") !== -1) {
            fi.name = functions[0].substring(3, functions[0].indexOf("(") - 1).trim();
            fi.loggerCallerSource = functions[0].substring(functions[0].indexOf("(") + 1, functions[0].length - 1);
        } else {
            fi.name = functions[0];
            fi.loggerCallerSource = functions[0].substr(functions[0].indexOf("@") + 1);
        }
    } else {
        fi.name = "unknown";
        fi.loggerCallerSource = "unknown";
    }

    functions.shift();

    if (functions.length > 0) {

        if (functions[0].indexOf("at ") !== -1) {
            fi.caller = functions[0].substring(3, functions[0].indexOf("(") - 1).trim();
            fi.callerSource = functions[0].substring(functions[0].indexOf("(") + 1, functions[0].length - 1);
        } else {
            fi.caller = functions[0];
            fi.callerSource = functions[0];
        }

    } else {
        fi.caller = "unknown";
    }

    return fi;
}


const commonStyle = "font-family: 'Courier New', Courier, monospace; font-size: 13px; font-weight: normal;";
const constructorStyle = commonStyle + "background: #ffffcc;";
const enterStyle = commonStyle + "background: #bbffbb";
const exitStyle = commonStyle + "background: #ddffdd";
const decoratorStyle =  commonStyle + "background: #fff5e6";
const infoStyle = commonStyle + "background: #f2f2f2";
const dumpStyle = commonStyle + "background: #ccefff";
const warningStyle = commonStyle + "color: #ff8000";
const errorStyle = commonStyle + "color: red";
const fatalStyle = commonStyle + "color: red; background: #ffe6e6";
const messageStyle = commonStyle + "background: #ffffff;";

export function empty(): void {
    try {
        console.log("");
    } catch (e) {
        // IE11 throws errors when console is closed, ignore
    }
}

export function constructor(logLevel: number, component: any, ...data: any[]): void {
    try {
        if (logLevel > logVerbosity) return;
        const message = buildMessage("CONSTRUCTOR", component, data);
        const fi = getFunctionInfo();
        console.groupCollapsed(message, constructorStyle, messageStyle);
        console.log("Logged from: " + fi.loggerCallerSource);
        console.log("Caller: " + fi.caller);
        console.log("Caller source: " + fi.callerSource);
        console.groupEnd();
    } catch (e) {
        // IE11 throws errors when console is closed, ignore
    }
}

export function enter(logLevel: number, component: any, ...data: any[]): void {
    try {
        if (logLevel > logVerbosity) return;
        const message = buildMessage("ENTER", component, data);
        const fi = getFunctionInfo();
        console.groupCollapsed(message, enterStyle , messageStyle);
        console.log("Logged from: " + fi.loggerCallerSource);
        console.log("Fn or method: " + fi.name);
        console.log("Caller: " + fi.caller);
        console.log("Caller source: " + fi.callerSource);
        console.groupEnd();
    } catch (e) {
        // IE11 throws errors when console is closed, ignore
    }
}

export function exit(logLevel: number, component: any, ...data: any[]): void {
    try {
        if (logLevel > logVerbosity) return;
        const message = buildMessage("EXIT", component, data);
        const fi = getFunctionInfo();
        console.groupCollapsed(message, exitStyle, messageStyle);
        console.log("Logged from: " + fi.loggerCallerSource);
        console.log("Fn or method: " + fi.name);
        console.groupEnd();
    } catch (e) {
        // IE11 throws errors when console is closed, ignore
    }
}

export function decorator(logLevel: number, component: any, ...data: any[]): void {
    try {
        if (logLevel > logVerbosity) return;
        const fi = getFunctionInfo();
        const message = buildMessage("DECORATOR", component, data);
        console.groupCollapsed(message, decoratorStyle, messageStyle);
        console.log("Logged from: " + fi.loggerCallerSource);
        console.groupEnd();
    } catch (e) {
        // IE11 throws errors when console is closed, ignore
    }
}

export function info(logLevel: number, component: any, ...data: any[]): void {
    try {
        if (logLevel > logVerbosity) return;
        const fi = getFunctionInfo();
        const message = buildMessage("INFO", component, data);
        console.groupCollapsed(message, infoStyle, messageStyle);
        console.log("Logged from: " + fi.loggerCallerSource);
        console.groupEnd();
    } catch (e) {
        // IE11 throws errors when console is closed, ignore
    }
}

export function dump(logLevel: number, component: any, ...data: any[]): void {
    try {
        if (logLevel > logVerbosity) return;
        const fi = getFunctionInfo();
        const message = buildMessage("DUMP", component);
        for (let i = 0; i < data.length; i++) if (typeof data[i] === "function") data[i] = anyToString(data[i]);
        console.groupCollapsed.apply(this, [ message, dumpStyle, messageStyle ].concat(data));

        if (browser === BrowserType.InternetExplorer) {
            console.log(data);
        }

        console.log("Logged from: " + fi.loggerCallerSource);
        console.groupEnd();
    } catch (e) {
        // IE11 throws errors when console is closed, ignore
    }
}

export function warning(component: any, ...data: any[]): void {
    try {
        const fi = getFunctionInfo();
        const message = buildMessage("WARNING", component, data);
        console.groupCollapsed(message, warningStyle, warningStyle);
        console.log("Logged from: " + fi.loggerCallerSource);
        console.groupEnd();
    } catch (e) {
        // IE11 throws errors when console is closed, ignore
    }
}

export function error(component: any, ...data: any[]): void {
    try {
        const fi = getFunctionInfo();
        const message = buildMessage("ERROR", component, data);
        console.groupCollapsed(message, errorStyle, errorStyle);
        console.log("Logged from: " + fi.loggerCallerSource);
        console.groupEnd();
    } catch (e) {
        // IE11 throws errors when console is closed, ignore
    }
}

export function fatal(component: any, ...data: any[]): void {
    try {
        const fi = getFunctionInfo();
        const message = buildMessage("FATAL", component, data);
        console.groupCollapsed(message, fatalStyle, commonStyle);
        console.log("Logged from: " + fi.loggerCallerSource);
        console.groupEnd();
    } catch (e) {
        // IE11 throws errors when console is closed, ignore
    }
}

export function setVerbosity(verbosity: number): void {
    logVerbosity = verbosity;
}