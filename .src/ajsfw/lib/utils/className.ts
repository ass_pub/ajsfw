/*******************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

export function className(obj: any): string {

    if (obj !== undefined && obj.name !== undefined) {
        // hack:because some chrome versions returns Empty for anonymous functions
        if (obj.name === "Empty") {
            return "";
        }
        return obj.name;
    }

    if (obj === undefined || obj.constructor === undefined || obj.constructor.toString === undefined) {
        return undefined;
    }

    if (obj.constructor.toString().trim().substr(0, 5) === "class") {
        return getES6ClassName(obj);
    }

    let arr: RegExpMatchArray = obj.constructor.toString().match(/function\s*(\w+)/);

    if (arr && arr.length === 2) {
        if (arr[1] === "Function") {
            if (obj.toString) {
                arr = obj.toString().match(/function\s*(\w+)/);
                if (arr && arr.length === 2) {
                    return arr[1];
                }
            }
        } else {
            return arr[1];
        }
    }

    return undefined;
}

function getES6ClassName(obj: any): string {

    if (obj && obj.name) {
        return obj.name;
    }

    if (obj === undefined || obj.constructor === undefined || obj.constructor.toString === undefined) {
        return undefined;
    }

    const objstr: string = obj.constructor.toString();

    if (objstr.trim().substr(0, 5) !== "class") {
        return getES6ClassName(obj);
    }

    const indexOfBracket: number = objstr.indexOf("{");
    const indexOfExtends: number = objstr.indexOf("extends");

    if (indexOfExtends !== -1 && indexOfExtends < indexOfBracket) {
        return objstr.substring(5, indexOfExtends - 1).trim();
    }

    return objstr.substring(5, indexOfBracket - 1).trim();
}
