/*******************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { className } from "./className";

export function anyToString(obj: any): string {

    switch (typeof(obj)) {
        case "string":
            return obj;
        case "number":
            return "" + obj;
        case "boolean":
            return obj ? "true" : "false";
        case "function":
            return className(obj);
        case "symbol":
            return obj.toString();
        case "undefined":
            return "undefined";
        case "object":
            if (Array.isArray(obj)) return "[" + arrayToString(obj) + "]";
            if (obj === null) return "null";
            return className(obj.constructor);
        default:
            return "Failed to convert object to string";
    }

}

export function arrayToString(arr: any[]): string {
    let out = "";
    for (const a of arr) out += anyToString(a) + ", ";
    return out.substr(0, out.length - 2);
}
