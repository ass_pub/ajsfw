/*******************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.
Thanks to Rob W @ stackowerflow (9847580) for his browser detection solution

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

export enum BrowserType {
    Chrome,
    Edge,
    Firefox,
    InternetExplorer,
    Opera,
    Safari,
    Other
}

declare const opr: any;
declare const InstallTrigger: any;
declare const safari: any;

export function getBrowserType(): BrowserType {

    const w: any = window;
    const d: any = document;

    // Opera 8.0+
    const isOpera = (!!w.opr && !!opr.addons) || !!w.opera || navigator.userAgent.indexOf(" OPR/") >= 0;

    // Firefox 1.0+
    const isFirefox = typeof InstallTrigger !== "undefined";

    // Safari 3.0+ "[object HTMLElementConstructor]"
    const isSafari = /constructor/i.test(w.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!w["safari"] || (typeof safari !== "undefined" && safari.pushNotification));

    // Internet Explorer 6-11
    const isIE = /*@cc_on!@*/false || !!d.documentMode;

    // Edge 20+
    const isEdge = !isIE && !!w.StyleMedia;

    // Chrome 1+
    const isChrome = !!w.chrome && !!w.chrome.webstore;

    // Blink engine detection
    // const isBlink = (isChrome || isOpera) && !!w.CSS;

    if (isOpera) return BrowserType.Opera;
    if (isFirefox) return BrowserType.Firefox;
    if (isSafari) return BrowserType.Safari;
    if (isIE) return BrowserType.InternetExplorer;
    if (isEdge) return BrowserType.Edge;
    if (isChrome) return BrowserType.Chrome;

    return BrowserType.Other;
}
