/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Listener } from "./Listener";
import { Notifier } from "./Notifier";

interface ListenerBinding<T> {
    listener: Listener<T>;
    binding: any;
}

export class AjsNotifier<T> implements Notifier<T> {

    /** List of subscribers */
    private __listenerBindings: ListenerBinding<T>[];

    /**
     * Instantiates the Notifier and subscribes listeners passed as parameter
     * @param listeners
     */
    public constructor(...listeners: ListenerBinding<T>[]) {
        this.__listenerBindings = [];
        for (let i = 0; i < listeners.length; i++) {
            this.__listenerBindings.push(listeners[i]);
        }
    }

    /**
     * Subscribes listener to obtain notifications passed through the current instance of Notifier
     * @param listener Listener to be subscribed
     */
    public subscribe(listener: Listener<T>, binding?: any): void {
        for (const listenerBinding of this.__listenerBindings) {
            if (listenerBinding.listener === listener && listenerBinding.binding === binding) return;
        }
        this.__listenerBindings.push({ listener, binding });
    }

    /**
     * Unsubscribes the listener from the current instance of the notifier
     * @param listener Listener to be subscribed
     */
    public unsubscribe(listener: Listener<T>, binding?: any): void {
        for (let i = 0; i < this.__listenerBindings.length; i++) {
            const listenerBinding = this.__listenerBindings[i];
            if (listenerBinding.listener === listener && listenerBinding.binding === binding) {
                this.__listenerBindings.splice(i, 1);
                return;
            }
        }
    }

    /**
     * Notifies registered subscribers the event occured
     * Subscribers can cancel propagation to other subscribers by returning false from listener function
     * @param sender Sender object identifier
     * @param data Data to be passed to subscribers
     */
    public notify(sender: any, data?: T): void {
        for (let i = 0; i < this.__listenerBindings.length; i++) {
            let result: boolean | void | Promise<void>;
            if (this.__listenerBindings[i].binding) {
                result = this.__listenerBindings[i].listener.call(this.__listenerBindings[i].binding, sender, data);
            } else {
                result = this.__listenerBindings[i].listener(sender, data);
            }
            if (result !== undefined && !result) {
                return;
            }
        }
    }

}
