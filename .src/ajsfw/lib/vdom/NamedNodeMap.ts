/*******************************************************************************
The MIT License (MIT)
Copyright (c)2016-2018 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Node } from "./Node";

/**
 * The NamedNodeMap interface represents a collection of #see [Attr]{Ajs.VDom.Attr} objects.
 * <p>
 * Objects inside a NamedNodeMap are not in any particular order, unlike NodeList, although they
 * may be accessed by an index as in an array.
 * </p>
 */
export class NamedNodeMap<T extends Node> {

    /**
     * Holds nodes stored in the named node map
     */
    private __nodes: { [name: string]: T };

    /**
     * Constructs the NamedNodeMap and prepares it for usage
     */
    constructor() {
        this.__nodes = {};
    }

    /**
     * Returns the amount of objects in the map.
     */
    public get length(): number {
        return Object.keys(this.__nodes).length;
    }

    /**
     * Returns a #see [Attr]{Ajs.VDom.Attr}, corresponding to the given name.
     * @param name Name of the #see [Attr]{Ajs.VDom.Attr}, to be returned
     */
    public getNamedItem(name: string): T {
        if (!(name in this.__nodes)) {
            return null;
        }

        return this.__nodes[name];
    }

    /**
     * Replaces, or adds, the #see [Attr]{Ajs.VDom.Attr}, identified in the map by the given name.
     * @param item Name of the #see [Attr]{Ajs.VDom.Attr}, whose value has to be set
     */
    public setNamedItem(item: T): T {
        this.__nodes[item.nodeName] = item;
        return item;
    }

    /**
     * Removes the #see [Attr]{Ajs.VDom.Attr}, identified by the given map.
     * @param item Name of the #see [Attr]{Ajs.VDom.Attr}, to be removed from the map
     */
    public removeNamedItem(item: T): void {
        if (!(item.nodeName in this.__nodes)) {
            return null;
        }

        delete this.__nodes[item.nodeName];
    }

    /**
     * Returns the #see [Attr]{Ajs.VDom.Attr} at the given index, or null if the index is higher or equal to the number of nodes.
     * @param index Index of the #see [Attr]{Ajs.VDom.Attr} to be returned
     */
    public item(index: number): T {
        const rv: T = this.__nodes[Object.keys(this.__nodes)[index]];
        return rv || null;
    }

}
