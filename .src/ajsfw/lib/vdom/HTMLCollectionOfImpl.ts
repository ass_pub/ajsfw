/*******************************************************************************
The MIT License (MIT)
Copyright (c)2016-2018 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { HTMLCollectionOf } from "./HTMLCollectionOf";
import { HTMLElement } from "./HTMLElement";

/**
 * Implementation of the HTMLElement collection. It is inherited from JavaScript Array.
 */
export class HTMLCollectionOfImpl extends Array<HTMLElement> implements HTMLCollectionOf<HTMLElement> {

    /**
     * Constructs the HTMLColletionOfImpl object
     * @param arrayLength
     */
    public constructor(arrayLength?: number) {
        if (arrayLength) {
            super(arrayLength);
        } else {
            super();
        }
    }

    /**
     * Returns the specific node at the given zero-based index into the list. Returns null if the index is out of range.
     * @param index
     */
    public item(index: number): HTMLElement {
        if (index < 0 || index > this.length - 1) {
            return null;
        }

        return this[index];
    }

    /**
     * Returns the specific node whose ID or, as a fallback, name matches the string specified by name.
     * <p>
     * Matching by name is only done as a last resort, only in HTML, and only if the referenced
     * element supports the name attribute. Returns null if no node exists by the given name.
     * </p>
     * @param name Value of the id or name attribute to be searched
     */
    public namedItem(name: string): HTMLElement {
        for (const e of this) {
            if (e.getAttribute("id") === name || e.getAttribute("name") === ("id")) {
                return e;
            }
        }
        return null;
    }

}
