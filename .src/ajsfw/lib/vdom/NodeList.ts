/*******************************************************************************
The MIT License (MIT)
Copyright (c)2016-2018 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Node } from "./Node";

/**
 * NodeList objects are collections (linked lists) of nodes such as those returned by i.e. Node.childNodes
 */
export interface NodeList {

    /** Holds the number of nodes in the list */
    readonly length: number;

    /**
     * Returns item from the collection at the specified index. It is "slow" access to the list item as
     * it is neccessary to iterate through the list to specified item before it can be returned. For iterating
     * the list it is recommended to obtain the first item in the list using the list.item(0) and then
     * using the item.nextSibling method.
     */
    item: (index: number) => Node;
}
