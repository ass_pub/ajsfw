/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { AsyncFunction } from "ajsfw/types";
import { RunningTaskDescriptor } from "./RunningTaskDescriptor";

const runningTasks: RunningTaskDescriptor[] = [];

export function runAwaitRunning(): MethodDecorator {

    return (target: any, propertyKey: string | symbol, descriptor: PropertyDescriptor): PropertyDescriptor => {

        const proxy: (...ars: any[]) => Promise<any> = function (...args: any[]): Promise<any> {
            return new Promise(
                (resolve: (value: any) => void, reject: (reason: any) => void) => {
                    runOrAwaitRunningResolver(descriptor.value, resolve, reject);
                }
            );
        };

        descriptor.value = proxy;
        return descriptor;

    };

}

function getRunningTask(taskId: AsyncFunction): RunningTaskDescriptor {

    for (let i = 0; i < runningTasks.length; i++) {
        if (runningTasks[i].taskId === taskId) return runningTasks[i];
    }

    return null;
}

function runOrAwaitRunningResolver(taskId: AsyncFunction, resolve: (value: any) => void, reject: (reason: any) => void, ...args: any[]): void {

    let runningTaskDescriptor: RunningTaskDescriptor = getRunningTask(taskId);

    if (runningTaskDescriptor === null) {
        runningTaskDescriptor = {
            taskId,
            runningTaskPromise: taskId.apply(this, args)
        };

        runningTasks.push(runningTaskDescriptor);

        runningTaskDescriptor.runningTaskPromise.then((value: any) => { runningTasks.splice(runningTasks.indexOf(runningTaskDescriptor), 1); });
        runningTaskDescriptor.runningTaskPromise.catch((reason: any) => { reject(reason); });
    }

    runningTaskDescriptor.runningTaskPromise.then((value: any) => { resolve(value); });
    runningTaskDescriptor.runningTaskPromise.catch((reason: any) => { reject(reason); });

}
