# Startup process

## Code organization (recommended folder structure)

The following directory structure is currently recommended (with high probability it will change in future once the custom bundler will be ready for use):

```
   ${workspaceroot}          currently this is also web root of the application served by HTTP server
          |--- .vscode
          |--- build
          |--- buildtools
          |--- dist
          |--- js
          |--- node_modules
          |--- res
          |--- src
          |    |--- modules
          |    |--- node_modules
          |    |    |--- ajsfw
          |    |--- <other app folders>
          |    |--- startup
          |    |    |--- App.ts
          |    |    |--- Startup.ts
          |    |--- index.ts
          |--- config.json
          |--- index.html
          |--- package.json
          |--- tsconfig.json
          |--- tslint.json


```

*Folder*                | *Description*
----------------------- | -------------
/.vscode                | visual studio code configuration for the current workspace
/build                  | folder to which the typescript files are built
/buildtools             | tools used by the VS code during the build process (see .vscode/tasks.json for details)
/dist                   | currently not used, will be recommended to be used when the future versions of the framework (including the bundler) will be released
/js                     | destination folder for final javascript bundles (each application module can be in separate bundle)
/node_modules           | 3rd party development tools (used in the build tool chain) and other 3rd party libraries to be bundled together with the application
/res                    | resources folder (stylesheets, images, languages, templates)
/src                    | source code of the application and currently also of the framework itself. the framework will be placed in the the node_modules folder in later versions
/src/modules            | application modules with custom folder structure
/src/node_modules       | currently, only the AjsFw source code is placed here (before it will be pushed to the NPM)
/src/startup            | application startup files
/src/startup/Startup.ts | class used to configure framework startup and provides information about application to be started
/src/startup/App.ts     | class used to configure the application services
/src/index.ts           | root module to be bundled it forces using of the AjsFw main module and the Startup class module
/config.json            | optional, application config to be loaded during the startup
/index.html             | single page application entry point

## Bundling

Currently the Browserify bundler is in use. It supports bundling of the common (application) package and accompanying packages (application modules).

For the application module it is important the AjsFw module and ./startup/Startup modules are imported by the root module (typically ./src/index.ts file) and
the index module is set as the root module of the bundle.

Application modules will be added soon but are not currently supported by the framework.

## Startup

> Please note the documentation for the startup will change soon once new planned features will be implemented

<!-- The Ajs Framework boot process starts when the browser loads the index.html page with the included application bundle: -->

If the recommended folder structure is in use, the application is properly bundled (the root module is /src/index.ts) and the /src/index.ts has the content as follows:

the Ajs Framework index.ts module code is started followed by the code of the ./app/Startup module.

Startup class decoration event occurs at the moment when the Startup module of the application is executed by the browser. This
event is executed by the @startup decorator used on the Startup class. Additionally, the startup class must be decorated using
the @serviceType(IIStartup) decorator. This will ensure the proper service manager (StartupServiceManager) will take care of
calling optional methods of the Startup class.

Once the Startup class decoration event occurs the ajsfw/core/main/main.ts/start function is called with the constructor of the
startup class as a parameter and instantiates the Startup class.

In order to fully understand the startup process the topics related to Dependency Injection, Services and Service Identifiers,
Service Managers and Service Builders should be read first.

Framework and application startup is done in 6 basic phases:

### 1. HTML loading and JavaScript execution

This is the standard phase performed by the browser when user navigates to the web site. Html code is loaded, parsed and all found resources
are started being downloaded in parallel. Once script resource is downloaded from the server it is executed immediately even if the rest of
resources is not downloaded yet. In case more scripts are provided and one depends on other it can crash with undefined exception as the
script depending on another can be downloaded, parsed and executed earlier. This is standard behavior how the browser is loading web page.

Applications using the Ajs Framework should be bundled in the way that the main application module is only the one script resource referred
in the index.html. It is only the one script resource which is downloaded, parsed and executed by the browser directly. Another script
resources should be managed by the framework, although this is not required.

~~~{.html caption="index.html"}
<!DOCTYPE html>
<html>
    <head>
        <script src="/js/index.js"></script>
    </head>

    <body>
    </body>
</html>
~~~

### 2. Bootstrap

Once the main application module is downloaded (it is supposed the main app module is properly bundled), parsed and executed by the browser
it is necessary to prepare framework for the startup. The root CommonJs module (typically /src/index.ts in the source code) has to import
the root ajs framework module and properly decorated startup class in the correct order (the framework must be imported prior the Startup class)

~~~{.typescript caption="/src/index.ts"}
import "ajsfw";
import "./app/Startup"
~~~

These two CommonJS modules are executed in the order they were defined. When the Ajs Framework root module executes it calls the
ajsfw/core/main/main() function which subscribes to the "Startup class decoration" event and finishes the execution.

If the application Startup class is properly decorated using the @startup decorator, execution of the Startup CommonJS module will invoke
the @startup decorator which fires the Startup class decoration event and starts the Startup phase of the framework.

In the following example, the @startup decorator is called with optional config object argument which is later passed to the Startup class
constructor. Config can be used to direct the startup class to configure the startup process differently for various conditions
(i.e. for different environments, i.e. test, prod, etc.).

~~~{.typescript caption="/src/startup/Startup.ts"}
import { startup } from "ajsfw/services/startup"
import { App } from "./App";

// example startup config interface
interface StartupConfig {
    environment: "test" | "runtime";
}

@startup({ config: "config });
@serviceType(IIStartup)
export class AppStartup implements Startup {

    private _config: StartupConfig;

    constructor(config: StartupConfig) {
        this._config = config;
    }

    public useApplication(): ApplciationCtor {
        return <ApplicationCtor>App;
    }
}

~~~

The config object can be also provided by the function, such as

~~~{.typescript caption="@startup config example"}
import { startup } from "ajsfw.services/startup"

interface StartupConfig {
    environment: "test" | "runtime";
}

@startup(getStartupConfig())
@serviceType(IIStartup);
export class AppStartup implements Startup {
    ...
}

function getStartupConfig(): StartupConfig {
    const config: StartupConfig = <any>{};

    // detect environment
    config.environment = window ? "runtime" : "test";

    return config;
}

~~~

The bootstrap procedure controlled by the ajsfw/core/main/main() function is supposed to perform configuration and initialization of
the root dependency injection container, default service manager, startup managers and startup services. All of these services are fully
configurable by the Startup class. If they are not changed the default AjsFw services are configured, initialized and used.

The following tasks are done in the main() function of the Ajs Framework in the mentioned order. If the Startup class implements the
"Customization method" this method is called to provide required service constructor, service builder or configure services using provided
service builder instead of using default ones. For more details see Startup interface in the reference guide

Task description                                                    | Customization method            | Defaults (service identifier, service impl. / data)   | Notes
------------------------------------------------------------------- | ------------------------------- | ----------------------------------------------------- | -------
Startup class instantiation                                         | -                               | Startup class decorated using @startup decorator      | -
Instantiation of the DI container                                   | useCustomRootDiContainer        | \<IIContainer>AjsContainer                            | -
Singleton addition (default service manager)                        | useCustomDefaultServiceManager  | \<IIServiceManager>AjsServiceManager                  | Manages * services
Singleton addition (startup service manager)                        | useCustomStartupServiceManager  | \<IIStartupServiceManager>AjsStartupServiceManager    | Manages \<IIStartupService> service type
Singleton addition (startup manager)                                | useCustomStartupManager         | \<IIStartupManager>AjsStartupManager                  | Manages \<IIStartup> service type
Manager registration (*, default service manager)                   | -                               | -                                                     | -
Manager registration (\<IIStartupService>, startup service manager) | -                               | -                                                     | -
Manager registration (\<IIStartup>, startup manager)                | -                               | -                                                     | -
Singleton instance registration (<IIStartup>, Startup instance)     | -                               | -                                                     | IIStartup stores instance internally for later use
Transient addition (Startup service builder)                        | useCustomStartupServiceBuilder  | \<StartupServiceBuildes>AjsStartupServiceBuilder      | -
Instantiating of the Startup service builder (Resolve used)         | -                               | -                                                     | -
Configuration of Startup Services                                   | configureStartupServices        | Singleton addition (<IIStartupService>)               | -
Instantiating of the Startup Service Manager (Resolve used)         | -                               | -                                                     | -
Service Manager.run method invocation                               | -                               | -                                                     | -

When the Startup service manager run method is called the manager instantiates and starts all Startup services configured using the Startup
Service Builder in the same order these services were configured. All of these services are called synchronously. By starting of the service
is meant calling of the init() method of the Startup Service class implementation. For reference check the source code of default Ajs
startup service implementations (i.e. ajsfw/services/startup/AjsOnlineStartup.ts or AjsOfflineStartup.ts).

Startup services typically adds listeners to browser events, such as *window.onload* or *window.applicationCache.cached* and performs some
tasks when these events occurs. Finally, they calls the StartupManager.run() method to continue the startup process. Please note, the
StartupManager.run can be called just once so first call should win (this note is important especially in case of custom implementation
of the Startup manager, the default implementation takes care of this by setting _started flag internally and rejects more calls to the run
method).

Till this point the script was executed synchronously without any interruption so any browser related registered listeners will be called
properly.

> In future, Service Worker Management will be added in order to be possible to provide Startup Services (i.e. for offline support) in modern way.

### 3. Startup - Part I

During the startup phase managed by the StartupManager, asynchronous startup services are configured, registered, resolved, invoked and
awaited. All asynchronous services are run in parallel so currently, no AsyncStartupService can depend on another or its results.

The following tasks are done by the StartupManager:

Task description                                                    | Customization method                | Defaults (service identifier, service impl. / data)          | Notes
------------------------------------------------------------------- | ----------------------------------- | ------------------------------------------------------------ | -------
Singleton addition (Async Startup Service Manager)                  | useCustomAsyncStartupServiceManager | \<IIAsyncStartupServiceManager>AjsAsyncStartupServiceManager |
Registration of Async Startup Service Manager to DIC                | -                                   | -                                                            | -
Transient addition (Async Startup Service Builder)                  | useCustomAsyncStartupServiceBuilder |
Instantiation of Async Startup Service Builder                      | -                                   |
Configuration of AsyncStartupServices                               | configureAsyncStartupServices       |
Starting the Async Startup Service Manager.run()

### 4. AsyncStartup

This step is optional and will be skipped most of the time. However, sometimes, asynchronous tasks has to be done prior the startup process
can continue. This can typically include loading of the initial application configuration file from the server in case the configuration is
not directly hardcoded and packed within the main application package. On other hand, AsyncServices can depend on another services which
must be configured properly, before resolving and starting of these services.

Run all
Await all AsycService

### 3. Startup - Part II

Task description                                                    | Customization method                | Defaults (service identifier, service impl. / data)          | Notes
------------------------------------------------------------------- | ----------------------------------- | ------------------------------------------------------------ | -------
wait for Async Startup Service Manager.run()                        | -                                   |
Singleton addition Application Service Manager                      | useCustomApplicationManager
Registration of Application Service Manager                         |
Calling of Startup.useApplication() to obtain Application Service   |
Transient addition of the Application Service
Instantiation of the Application Service
Instantiation of the Application Service Manager
Starting the Application Service Manager.run

### 5. Application service configuration

### 6. Application start

