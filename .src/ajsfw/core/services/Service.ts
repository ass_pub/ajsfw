/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { DependencyIdentifier } from "ajsfw/services/dic/DependencyIdentifier";
import { ServiceCtorDecorations } from "./ServiceCtorDecorations";
import { ServiceObjectDecorations } from "./ServiceObjectDecorations";

/**
 * Used mainly to identify the common service type (not for the injection purposes)
 */
export const IIService: DependencyIdentifier = [ "IIService" ];

/**
 * Indentifier can be used to force DiC to inject the service dependent on service being instantied
 */
export const IIDependentService: DependencyIdentifier = [ "IIDependentService" ];



/**
 * Common service constructor
 */
export interface ServiceCtor extends Function {
    new(...dependencies: any[]): Service;
    __ajs__?: ServiceCtorDecorations;
}

/**
 * Service interface
 * Methods contained within this interface are used by the DiC and AjsDefaultServiceManager during the service lifecycle
 */
export interface Service extends Object {
    // [property: string]: any;

    /**
     * Service object decorations (used by Ajs services to store information about the service for internal use)
     */
    __ajs__?: ServiceObjectDecorations;

    /**
     * Called from the service manager to initialize the service immediately after the service is instantiated
     * If service is using injected properties, these properties are not available at this time
     */
    init?: () => void;

    /**
     * Can be used to setup the service once all properties are injected. Called immediately when properties are injected
     * to the service
     * @param dependecies
     */
    setup?(): void;

    /**
     * Can be called by service managers and/or DiC to dispose all objects allocated internally and release all references in order to
     * prevent memory leaks when the service is destroyed and will not be used anymore
     */
    dispose?: () => void;

    /**
     * When Url binding is used the method is called when the property bound to url has been changed (the URL has changed and caused the
     * property change)
     */
    bindingsUpdated?: (properties: string[]) => void;

    /**
     * When the service is bound (does not matter if it is newly created or reused) the bound method is called
     */
    bound?: () => void;

}
