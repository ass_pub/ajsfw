/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { ServiceCtor, Service } from "./Service";
import { StringKeyValue } from "ajsfw/types";
import { ServiceCtorDecorations } from "./ServiceCtorDecorations";
import { ServiceObjectDecorations } from "./ServiceObjectDecorations";

// Following methods are supposed to be used by the framewrok only. These
// methods are used to decorate the object or the object constructor with
// the information required by particular services or functions of the framework

export class ServiceDecoration {

    /**
     * Returns the decorated value or default value if the decorated does not exist
     * @param serviceCtor Service constructor which property should be read
     * @param keyValue Key - property of the constructor to be read, value - the default value to be returned if the constructor is not decorated with the given property
     */
    public static getCtorDecoration<T extends ServiceCtorDecorations>(serviceCtor: ServiceCtor, keyValue: T): any {

        const keys = Object.keys(keyValue);
        if (keys.length === 0) return;

        const key = keys[0];
        const defaultValue = (<any>keyValue)[key];

        if (serviceCtor.__ajs__ === undefined) return defaultValue;
        if ((<any>serviceCtor.__ajs__)[key] === undefined) return defaultValue;

        return (<any>serviceCtor.__ajs__)[key];
    }

    /**
     * Sets the Service class (constructor) decoration properties
     * @param serviceCtor Service constructor to be used to set values
     * @param keyValue Object with properties to be set to the constructor's __ajs__ property
     */
    public static setCtorDecorations<T extends ServiceCtorDecorations>(serviceCtor: ServiceCtor, keyValue: T): void {

        const keys = Object.keys(keyValue);
        if (keys.length === 0) return;

        ServiceDecoration.decorateClassAjs(serviceCtor);

        for (const key of keys) {
            const value = (<StringKeyValue<any>>keyValue)[key];
            if (!serviceCtor.__ajs__.hasOwnProperty(key)) {
                Object.defineProperty(serviceCtor.__ajs__, key, { value });
            }
        }

    }

    /**
     * Sets the service object decoration properties
     * @param service Service object to be decorated
     * @param keyValue Object with properties to be set to the service object __ajs__ property
     */
    public static getObjectDecoration<T extends ServiceObjectDecorations>(service: Service, keyValue: T): any {

        const keys =  Object.keys(keyValue);
        if (keys.length === 0) return;

        const key = keys[0];
        const defaultValue = (<any>keyValue)[key];

        if (service.__ajs__ === undefined) return defaultValue;
        if ((<any>service.__ajs__)[key] === undefined) return defaultValue;

        return (<any>service.__ajs__)[key];
    }

    public static setObjectDecorations<T extends ServiceObjectDecorations>(service: Service, keyValue: T): void {

        const keys =  Object.keys(keyValue);
        if (keys.length === 0) return;

        ServiceDecoration.decorateObjectAjs(service);

        for (const key of keys) {
            const value = (<StringKeyValue<any>>keyValue)[key];
            if (!service.__ajs__.hasOwnProperty(key)) {
                Object.defineProperty(service.__ajs__, key, { value, writable: true });
            } else {
                (<any>service.__ajs__)[key] = value;
            }
        }
    }


    /**
     * Extends __ajs__ property from the prototype constructor or creates a new
     * if prototype constructor has not __ajs__ property defined
     * @param ctor Class to which constructor the __ajs__ property has to be added
     */
    private static decorateClassAjs(ctor: ServiceCtor): void {

        if (!ctor.hasOwnProperty("__ajs__")) {
            const parentCtor = Object.getPrototypeOf(ctor.prototype).constructor;
            const __ajs__ = Object.create(parentCtor.__ajs__ ? parentCtor.__ajs__ : Object);
            Object.defineProperty(
                ctor,
                "__ajs__",
                { value: __ajs__ }
            );
        }

    }

    /**
     * Creates __ajs__ object property for decorations if it does not exist already
     * @param service Service to which the __ajs__ property should be added
     */
    private static decorateObjectAjs(service: Service): void {

        if (!service.hasOwnProperty("__ajs__")) {
            Object.defineProperty(
                service,
                "__ajs__",
                { value: Object.create(Object) }
            );
        }

    }

}
