/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { StringKeyValue } from "ajsfw/types";
import { DependencyIdentifier } from "ajsfw/services/dic/DependencyIdentifier";

export const IIAjsfwStartupConfig: DependencyIdentifier = [ "IIAjsFwConfig" ];

/**
 * Ajs Framework Configuration
 * This interface should be inherited by application startup config interface and
 * the configuration should be provided during the application startup (by passing
 * thethe application and the ajs framework configs merged to single object to the
 * @startup attribute). If config is not passed, defaults will be used for ajsfw.
 */
export interface AjsfwStartupConfig extends StringKeyValue<any> {

    /**
     * Root URL of deployed application
     * (i.e. for /var/www/html/test & http://host.com/test this should be /test, otherwise the URL binding will not work properly)
     */
    rootPath: string;

}
