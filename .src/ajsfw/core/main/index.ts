/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

// TODO: Conditionally link the es6

// #if TARGET < es6
import "es6-promise/auto";
// #endif

// #ifdef DEBUG
import * as consts from "../consts";
import * as log from "ajsfw/lib/diag/log";
// #endif

// this must be reduced using the conditional compilation in order to get only modules required

import { AjsAppServiceBuilder } from "ajsfw/builders/AjsAppServiceBuilder";
import { AjsAsyncStartupServiceBuilder } from "ajsfw/builders/AjsAsyncStartupServiceBuilder";
import { AjsStartupServiceBuilder } from "ajsfw/builders/AjsStartupServiceBuilder";
import { SIAppServiceBuilder, SIAsyncStartupServiceBuilder, SIStartupServiceBuilder } from "ajsfw/builders/DependencyIdentifiers";
import { AjsDefaultServiceManager } from "ajsfw/services/dic/AjsDefaultServiceManager";
import { ServiceCtor } from "ajsfw/core/services/Service";
import { ServiceBuilder, ServiceBuilderCtor } from "ajsfw/core/services/ServiceBuilder";
import { IIServiceManager } from "ajsfw/core/services/ServiceManager";
import { Exception } from "ajsfw/lib/exceptions/Exception";
import { StackInfo } from "ajsfw/lib/exceptions/StackInfo";
import { Application, IIApplication } from "ajsfw/services/application/Application";
import { AjsContainerService } from "ajsfw/services/dic/AjsContainerService";
import { ContainerService, IIContainerService } from "ajsfw/services/dic/ContainerService";
import { ServiceDescriptor } from "ajsfw/services/dic/ServiceDescriptor";
import { AsyncStartupService, IIAsyncStartupService } from "ajsfw/services/appstartup/AsyncStartupService";
import { IIAppStartupService, AppStartupService } from "ajsfw/services/appstartup/AppStartupService";
import { startupClassDecorated } from "ajsfw/services/appstartup/startup.decorator";
import { AppStartupInfo } from "ajsfw/services/appstartup/AppStartupInfo";
import { IIStartupService } from "ajsfw/services/appstartup/StartupService";
import { CBIStartup } from "ajsfw/services/appstartup/sync/DependencyIdentifiers";
import * as exceptions from "./exceptions";
import { IIAjsfwStartupConfig } from "./AjsfwStartupConfig";

/**
 * Holds reference to Startup instance when created (used during various startup phases)
 */
let appStartup: AppStartupService;

/**
 * Holds reference to the root DI container when created (used during various startup phases)
 */
let dic: ContainerService;

/**
 * Default global exception handler
 * @param event ErrorEvent passed by the runtime VM
 * @param type Type of the error to be processed (error or unhandledrejection)
 */
function exception(event: ErrorEvent, type: "error" | "unhandledrejection"): void {

    let msg: string;
    let err: any;

    if (type === "error") {
        msg = "Unhandled exception";
        err = event.error;
    } else {
        msg = "Unhandled promise rejection";
        err = (<any>event).reason;
    }

    if (err instanceof Exception) {
        console.group("%c   " + msg + ": " + err.name + " (" + (err.message ? err.message.toString() : "[no message]") + ")   ", "color: red");
        let ch: StackInfo = err.stack;
        while (ch) {
            console.log("   at: " + ch.file + ":" + ch.line + ":" + ch.character);
            ch = ch.child;
        }
        console.groupEnd();
    } else {
        console.error(err);
    }

}

/**
 * STARTUP PHASE I.
 * When the scriprt is downloaded by the browser and executed the index.ts calls the main function
 * The main fuction registers @startup decoration event listener. Once the @startup decorator
 * is executed the STARTUP PHASE II (bootstrap) will be started.
 */
export async function main() {

    // #ifdef DEBUG
    log.info(1, "Ajs Framework", "Ajs Framework version ", consts.ajsFwVersion);
    log.info(1, "Ajs Framework", "Copyright (c)2018-2019 Atom Software Studios, Released under MIT license");
    log.empty();
    log.info(1, "main", "Ajs Framework Startup phase I. (standard main()) - Waiting for startup class to be decorated.");
    // #endif


    startupClassDecorated.subscribe(
        (sender: any, startupInfo: AppStartupInfo): boolean => {
            bootstrap(startupInfo);
            return false;
        }
    );

}

/**
 * STARTUP PHASE II. - Initial bootstrap
 * When the @starup decorator is executed the notification is sent to the subscribed listener (this function) with details about the startup
 * class and the configuration to be injected to it. Creates the root DIC and service manager. Configures and resolves all startup services
 * responsible for calling the startup callback once initial tasks are done by the browser (loading of the rest of the page or tasks
 * related to the ApplicationCache). The CBIStartup callback is set to the bootstrap Phase II (see bellow).
 * See ajsfw/services/startup/services/AjsOnlineStartup or AjsOfflineStartup for implementation details
 * In future releases, service workers could be also included.
 * @param startupInfo Infromation about the user defined startup class and the config to be passed to its constructor
 */
async function bootstrap(startupInfo: AppStartupInfo) {

    // #ifdef DEBUG
    log.enter(1, "main", "Ajs Framework bootstrap phase II. Bootstrap, startupInfo:");
    log.dump(1, "main", startupInfo);
    // #endif

    // the startup lifecycle starts here by creation of the Startup configuration class
    appStartup = new startupInfo.ctor(startupInfo.config);

    // global exception handler
    if (appStartup.useCustomGlobalErrorHandler) {
        window.addEventListener("error", appStartup.useCustomGlobalErrorHandler());
        window.addEventListener("unhandledrejection", appStartup.useCustomGlobalErrorHandler());
    } else {
        window.addEventListener("error", (ev: ErrorEvent) => {
            exception(ev, "error");
            ev.preventDefault();
            ev.cancelBubble = true;
        });
        window.addEventListener("unhandledrejection", (ev: ErrorEvent) => {
            exception(ev, "unhandledrejection");
            ev.preventDefault();
            ev.cancelBubble = true;
        });
    }

    // first of all it is necessary to create the DI container and register its instance to itself in order to be possible to inject it
    dic = appStartup.useCustomRootDiContainer ? appStartup.useCustomRootDiContainer() : new AjsContainerService(null);
    dic.registerSingletonInstance(IIContainerService, dic);

    // also, the startup config is registered under IIAjsfwStartupConfig key
    dic.registerSingletonInstance(IIAjsfwStartupConfig, startupInfo.config);

    // add default service manager (used to manage the service lifecycle for all service types which has no service manager registered)
    if (appStartup.useCustomDefaultServiceManager) {
        dic.addSingleton(IIServiceManager, appStartup.useCustomDefaultServiceManager());
    } else {
        dic.addSingleton(IIServiceManager, <ServiceCtor>AjsDefaultServiceManager);
    }

    // register added service manager
    await dic.registerServiceManager("*", IIServiceManager);

    // also, register Startup to DIC in order to be possible to use method injection
    await dic.registerSingletonInstance(IIAppStartupService, appStartup);

    // configure the startup service builder
    if (appStartup.useCustomStartupServiceBuilder) {
        dic.addTransient(SIStartupServiceBuilder, appStartup.useCustomStartupServiceBuilder());
    } else {
        dic.addTransient(SIStartupServiceBuilder, <ServiceBuilderCtor>AjsStartupServiceBuilder);
    }

    // resolve the startup service builder
    const ssb: ServiceBuilder = await dic.resolveService<ServiceBuilder>(SIStartupServiceBuilder);

    // configure startup services and service managers using startup service builder
    if (appStartup.configureStartupServices) {
        appStartup.configureStartupServices(ssb);
    } else {
        ssb.useDefaults();
    }

    ssb.build();

    // startup services are expected to call the startup callback identified by CBIStartup
    dic.registerSingletonInstance(CBIStartup, configureAndRunAsyncStartupServices);

    // get all startup services
    const services: ServiceDescriptor[] = dic.getServiceDescriptorsByType(IIStartupService);

    // and resolve them (the default service manager will call init on all resoved services)
    for (let i = 0; i < services.length; i++) await dic.resolveService(services[i].key);

    // #ifdef DEBUG
    log.exit(1, "main", "Ajs Framework bootstrap phase II. Bootstrap");
    log.empty();
    // #endif
}

/**
 * STARTUP PHASE III.
 * Called from Startup services registered and resolved in the configureAndRunStartupServices
 * During this phase, asynchronous services (better said, tasks) are configured, resolved and started in order to perform asynchronous
 * operation required for further startup process. This may include i.e. loading of the application configuration file from the server.
 * All configured asynchronous tasks are executed in parallel and, currently, can't depend each on other or on results of another
 * asynchronous startup service.
 */
async function configureAndRunAsyncStartupServices(): Promise<void> {

    // #ifdef DEBUG
    log.enter(1, "main", "Ajs Framework bootstrap phase III. Asynchronous startup services:");
    // #endif

    // helper to initialize and run async service
    async function startAsyncService(service: AsyncStartupService): Promise<void> {
        if (service.asyncInit) await service.asyncInit();
        if (service.asyncRun) await service.asyncRun();
    }

    // configure the async startup service builder
    if (appStartup.useCustomAsyncStartupServiceBuilder) {
        dic.addTransient(SIAsyncStartupServiceBuilder, appStartup.useCustomAsyncStartupServiceBuilder());
    } else {
        dic.addTransient(SIAsyncStartupServiceBuilder, <ServiceBuilderCtor>AjsAsyncStartupServiceBuilder);
    }

    // use the builder to configure async startup services
    if (dic.isTransientRegistered(SIAsyncStartupServiceBuilder)) {
        const aaisb: ServiceBuilder = await dic.resolveService<ServiceBuilder>(SIAsyncStartupServiceBuilder);
        if (appStartup.configureAsyncStartupServices) {
            appStartup.configureAsyncStartupServices(aaisb);
        } else {
            aaisb.useDefaults();
        }
        aaisb.build();
    }


    // get all async startup services
    const services: ServiceDescriptor[] = dic.getServiceDescriptorsByType(IIAsyncStartupService);

    // and resolve them (the default service manager will call init on all resoved services)
    const runningServices: Promise<void>[] = [];
    for (let i = 0; i < services.length; i++) {
        // call init method if exists (done by default service manager)
        const service: AsyncStartupService = await dic.resolveService<AsyncStartupService>(services[i].key);
        // call await asyncInit and await asyncRun, but in manner all starup services can run in paralel
        runningServices.push(startAsyncService(service));
    }

    // wait while all async services finishes their job
    await Promise.all(runningServices);

    // #ifdef DEBUG
    log.info(1, "main", "Async startup services initialization finished");
    log.info(1, "main", "Configuring application services");
    // #endif

    // configure the application service builder
    if (appStartup.useCustomServiceBuilder) {
        dic.addSingleton(SIAppServiceBuilder, appStartup.useCustomServiceBuilder());
    } else {
        dic.addSingleton(SIAppServiceBuilder, <ServiceBuilderCtor>AjsAppServiceBuilder);
    }

    // use the builder to configure application services
    const asb: ServiceBuilder = await dic.resolveService<ServiceBuilder>(SIAppServiceBuilder);
    if (appStartup.configureServices) {
        await appStartup.configureServices();
    } else {
        asb.useDefaults();
    }

    await asb.build();

    // #ifdef DEBUG
    log.exit(1, "main", "Bootstrap done. Starting the application...");
    log.empty();
    // #endif

    // check if the application instance has been registered
    if (dic.isSingletonRegistered(IIApplication)) {

        const app: Application = await dic.resolveService<Application>(IIApplication);

        if (appStartup.runApplication) {
            appStartup.runApplication(app);
            return;
        } else {
            if (app.run) app.run();
            return;
        }

    }

    if (appStartup.run) {
        appStartup.run();
        return;
    }

    throw new exceptions.StartupDoneButNothingToRun();

}
