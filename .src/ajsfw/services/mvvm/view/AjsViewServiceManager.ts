/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Service } from "ajsfw/core/services/Service";
import { ServiceDecoration } from "ajsfw/core/services/ServiceDecoration";
import { AjsDefaultServiceManager } from "ajsfw/services/dic/AjsDefaultServiceManager";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { ServiceDescriptor } from "ajsfw/services/dic/ServiceDescriptor";
import { IITemplateManager, TemplateManager } from "ajsfw/services/templating/TemplateManager";
import { ServiceCtorDecorationsMvvm } from "../ServiceCtorDecorationsMvvm";
import { ServiceObjectDecorationsMvvm } from "../ServiceObjectDecorationsMvvm";
import { ViewServiceManager } from "./ViewServiceManager";

@inject(IITemplateManager)
export class AjsViewServiceManager extends AjsDefaultServiceManager implements ViewServiceManager {

    protected _templateManager: TemplateManager;

    constructor(templateManager: TemplateManager) {
        super();
        this._templateManager = templateManager;
    }

    /**
     * Initiates resolving of the template using the template manager if template is in use
     * @param serviceDescriptor Descriptor of the service instantiated
     * @param instance Instance of the service (actually ViewModel) instantiated
     */
    public async serviceInstantied(serviceDescriptor: ServiceDescriptor, instance: Service): Promise<void> {

        const templateName = ServiceDecoration.getCtorDecoration<ServiceCtorDecorationsMvvm>(serviceDescriptor.ctor, { useTemplate: undefined });

        if (templateName) {
            const tplPromise = this._templateManager.getTemplate(templateName);
            ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsMvvm>(serviceDescriptor.instance, { templatePromise: tplPromise });
        }

        super.serviceInstantied(serviceDescriptor, instance);

    }

    /**
     * Called from DiC when the view is being disposed
     * @param instance View instance being disposed
     */
    public serviceDisposing(instance: Service): void {
        super.serviceDisposing(instance);
        ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsMvvm>(instance, { templatePromise: undefined });
    }
}
