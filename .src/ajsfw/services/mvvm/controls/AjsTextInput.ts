/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { AjsControl } from "./AjsControl";
import { ajsViewModel } from "../viewmodel/ajsViewModel.decorator";
import { AjsVDomEventData } from "ajsfw/services/virtualview/AjsVDomEventData";

@ajsViewModel()
export class AjsTextInput extends AjsControl {

    /**
     * Represents a value in the input type text field
     */
    protected _value: string;

    /**
     * Represents a help value (placeholder) displayed in empty text field
     */
    protected _help: string;

    /**
     * Returns the current value of the input text field
     */
    public get value(): string {
        return this._value;
    }

    /**
     * Sets the value of the input type text field and updates a view
     */
    public set value(value: string) {
        if (this._value !== value) {
            this._value = value;
            if (this._onChange) this._onChange.notify(this, null);
            this.updated();
        }
    }

    /**
     * Sets the help (placeholder) value for the input field
     */
    public set help(value: string) {
        if (this._help !== value) {
            this._help = value;
            this.updated();
        }
    }

    /**
     * Initializes the control
     */
    public init(): void {
        this._value = "";
        this._help = "";
    }

    /**
     * Should be bound to the HTML template (oninput, ontextinput)
     * @param data Event data passed from DOM
     */
    protected _change(data: AjsVDomEventData) {
        if (this._value !== data.value) {
            this._value = data.value;
            super._change(data);
            this._viewModelServiceManager.viewModelUpdated(this);
        }
    }

}