/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { AjsNotifier } from "../../../lib/events/AjsNotifier";
import { inject } from "../../dic/inject.decorator";
import { ViewModel } from "../viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "../viewmodel/ViewModelServiceManager";
import { AjsVDomEventData } from "../../virtualview/AjsVDomEventData";

@inject(IIViewModelServiceManager)
export class AjsControl implements ViewModel {

    public get onChange(): AjsNotifier<AjsVDomEventData> { return this._onChange = this._onChange || new AjsNotifier(); }
    public get onMouseDown(): AjsNotifier<AjsVDomEventData> { return this._onMouseDown = this._onMouseDown || new AjsNotifier(); }
    public get onMouseUp(): AjsNotifier<AjsVDomEventData> { return this._onMouseUp = this._onMouseUp || new AjsNotifier(); }
    public get onClick(): AjsNotifier<AjsVDomEventData> { return this._onClick = this._onClick || new AjsNotifier(); }
    public get onDblClick(): AjsNotifier<AjsVDomEventData> { return this._onDblClick = this._onDblClick || new AjsNotifier(); }
    public get onKeyDown(): AjsNotifier<AjsVDomEventData> { return this._onKeyDown = this._onKeyDown || new AjsNotifier(); }
    public get onKeyUp(): AjsNotifier<AjsVDomEventData> { return this._onKeyUp = this._onKeyUp || new AjsNotifier(); }
    public get onKeyPress(): AjsNotifier<AjsVDomEventData> { return this._onKeyPress = this._onKeyPress || new AjsNotifier(); }
    public get onDragDrop(): AjsNotifier<AjsVDomEventData> { return this._onDragDrop = this._onDragDrop || new AjsNotifier(); }
    public get onEnter(): AjsNotifier<void> { return this._onEnter = this._onEnter || new AjsNotifier(); }
    public get onExit(): AjsNotifier<void> { return this._onExit = this._onExit || new AjsNotifier(); }

    protected _onChange: AjsNotifier<AjsVDomEventData>;
    protected _onMouseDown: AjsNotifier<AjsVDomEventData>;
    protected _onMouseUp: AjsNotifier<AjsVDomEventData>;
    protected _onClick: AjsNotifier<AjsVDomEventData>;
    protected _onDblClick: AjsNotifier<AjsVDomEventData>;
    protected _onKeyDown: AjsNotifier<AjsVDomEventData>;
    protected _onKeyUp: AjsNotifier<AjsVDomEventData>;
    protected _onKeyPress: AjsNotifier<AjsVDomEventData>;
    protected _onDragDrop: AjsNotifier<AjsVDomEventData>;
    protected _onEnter: AjsNotifier<void>;
    protected _onExit: AjsNotifier<void>;

    protected _viewModelServiceManager: ViewModelServiceManager;

    constructor(viewModelServiceManager: ViewModelServiceManager) {
        this._viewModelServiceManager = viewModelServiceManager;
    }

    protected _change(data: AjsVDomEventData): void {
        if (this._onChange) this._onChange.notify(this, data);
    }

    protected _mouseDown(data: AjsVDomEventData): void {
        if (this._onMouseDown) this._onMouseDown.notify(this, data);
    }

    protected _mouseUp(data: AjsVDomEventData): void {
        if (this._onMouseUp) this._onMouseUp.notify(this, data);
    }

    protected _click(data: AjsVDomEventData): void {
        if (this._onClick) this._onClick.notify(this, data);
    }

    protected _dblClick(data: AjsVDomEventData): void {
        if (this._onDblClick) this._onDblClick.notify(this, data);
    }

    protected _keyDown(data: AjsVDomEventData): void {
        if (this._onKeyDown) this._onKeyDown.notify(this, data);
    }

    protected _keyUp(data: AjsVDomEventData): void {
        if (this._onKeyUp) this._onKeyUp.notify(this, data);
    }

    protected _keyPress(data: AjsVDomEventData): void {
        if (this._onKeyPress) this._onKeyPress.notify(this, data);
    }

    protected _dragDrop(data: AjsVDomEventData): void {
        if (this._onDragDrop) this._onDragDrop.notify(this, data);
    }

    protected _enter(data: AjsVDomEventData): void {
        if (this._onEnter) this.onEnter.notify(this);
    }

    protected _exit(data: AjsVDomEventData): void {
        if (this._onExit) this.onExit.notify(this);
    }

    /**
     * Sets cursor focus to the control
     */
    public focus(): void {
    }

    /**
     * Forces the control DOM to be updated during next dom update cycle
     */
    public updated(): void {
        this._viewModelServiceManager.viewModelUpdated(this);
    }

}