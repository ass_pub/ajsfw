/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { AjsControl } from "./AjsControl";
import { ajsViewModel } from "../viewmodel/ajsViewModel.decorator";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";

@ajsUseTemplate("ajsbutton")
@ajsViewModel()
export class AjsButton extends AjsControl {

    protected _disabled: boolean;
    public get enabled(): boolean { return this._disabled; }
    public set enabled(value: boolean) {
        if (value === !this._disabled) return;
        this._disabled = !value;
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    protected _label: string;
    public get label(): string { return this._label; }
    public set label(value: string) {
        if (value === this._label) return;
        this._label = value;
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    public init(): void {
        this._disabled = false;
        this._label = "";
    }

}