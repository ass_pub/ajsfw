/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { AjsControl } from "./AjsControl";
import { SelectItem } from "./viewstates/SelectItem";
import { ajsViewModel } from "../viewmodel/ajsViewModel.decorator";
import { AjsVDomEventData } from "../../virtualview/AjsVDomEventData";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";

@ajsUseTemplate("ajsselect")
@ajsViewModel()
export class AjsSelect extends AjsControl {

    /**
     * DOM element option should be bound to this property
     */
    protected _items: SelectItem[];

    /**
     * Holds currently selected item index
     * If multiple items are selected the first item is taken into account
     */
    protected _selectedIndex: number;

    /**
     * Holds list of selected item indexes
     */
    protected _selectedIndexes: number[];

    /**
     * Holds the currently selected item value.
     * If multiple items are selected the first one from the list is taken into account.
     */
    protected _value: string;

    /**
     * Called from view model manager to initialize the control
     */
    public init(): void {
        this._items = [];
        this._selectedIndex = -1;
        this._selectedIndexes = [];
    }

    /**
     * Called from view model manager or DI or UrlBinder when the control is being disposed
     */
    public dispose(): void {
        this._items = undefined;
    }

    /**
     * Returns array of items (when this array is manipulated the itemsUpdated method must be called)
     */
    public get items(): SelectItem[] {
        return this._items;
    }

    /**
     * Must be called when the item array was changed externally
     */
    public itemsUpdated(): void {

        if (this._items.length > 0) {
            this._selectedIndexes = [];
            for (let i = 0; i < this._items.length; i++) {
                if (this._items[i].selected) this._selectedIndexes.push(i);
            }
            if (this.selectedIndexes.length === 0) {
                this._items[0].selected = true;
                this._selectedIndexes = [ 0 ];
            }
            this._selectedIndex = this._selectedIndexes[0];
            this._value = this._items[this._selectedIndex].value;
        } else {
            this._selectedIndex = -1;
            this._selectedIndexes = [];
            this._value = undefined;
        }

    }

    /**
     * Returns currently selected item index
     */
    public get selectedIndex(): number {
        return this._selectedIndex;
    }

    public get value(): string {
        return this._value;
    }

    public set value(value: string) {

        this._selectedIndex = -1;
        this._selectedIndexes = [ ];
        this._value = undefined;

        for (let i = 0; i < this._items.length; i++) {
            if (this._items[i].value === value) {
                this._items[i].selected = true;
                this._selectedIndex = i;
                this._selectedIndexes = [ i ];
            } else {
                this._items[i].selected = false;
            }
        }

        if (!this._value && this.items.length > 0) {
            this._selectedIndex = 0;
            this._selectedIndexes = [ 0 ];
            this._value = this._items[0].value;
            this._items[0].selected = true;
        }

        this._viewModelServiceManager.viewModelUpdated(this);

    }

    /**
     * Sets the selected item by index
     */
    public set selectedIndex(index: number) {
        if (index < 0 || index > this._items.length - 1 || index === this._selectedIndex) return;
        this._selectedIndex = index;
        this._selectedIndexes = [ index ];
        this._value = this._items[index].value;
        this._updateItemsSelection();
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    /**
     * Returns array of indexes of selected items
     */
    public get selectedIndexes(): number[] {
        return this._selectedIndexes;
    }

    /**
     * Clears the list
     */
    public clear(): void {
        this._items = [];
        this._selectedIndex = -1;
        this._selectedIndexes = [];
        this._value = undefined;
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    /**
     * Bindable to the onchange DOM event
     * @param data Event data processed by the virtual view manager
     */
    protected _change(data: AjsVDomEventData): void {
        this._selectedIndex = data.selectedIndex || -1;
        this._selectedIndexes = data.selectedIndexes || [];
        if (this._selectedIndex !== -1 && this._selectedIndexes.length === 0) this._selectedIndexes = [ this._selectedIndex ];
        this._value = data.value;
        this._updateItemsSelection();
        super._change(data);
    }

    /**
     * Used internally to update select attribute of items in the list
     */
    private _updateItemsSelection(): void {
        for (let i = 0; i < this._items.length; i++) {
            this._items[i].selected = this._selectedIndexes.indexOf(i) !== -1;
        }
    }

}