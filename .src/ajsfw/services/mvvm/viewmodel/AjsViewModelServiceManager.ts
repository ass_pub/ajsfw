/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { inject } from "ajsfw/services/dic/inject.decorator";
import { ServiceDescriptor } from "ajsfw/services/dic/ServiceDescriptor";
import { Service, ServiceCtor } from "ajsfw/core/services/Service";
import { AjsDefaultServiceManager } from "ajsfw/services/dic/AjsDefaultServiceManager";
import { IIVirtualView, VirtualView } from "ajsfw/services/virtualview/VirtualView";
import { ViewModelServiceManager } from "./ViewModelServiceManager";
import { ViewModel } from "./ViewModel";
import { ServiceDecoration } from "ajsfw/core/services/ServiceDecoration";
import { ServiceCtorDecorationsMvvm } from "ajsfw/services/mvvm/ServiceCtorDecorationsMvvm";
import { ServiceObjectDecorationsDic } from "ajsfw/services/dic/ServiceObjectDecorationsDic";
import { ServiceObjectDecorationsMvvm } from "ajsfw/services/mvvm/ServiceObjectDecorationsMvvm";
import { ContainerService } from "ajsfw/services/dic/ContainerService";
import { View } from "../view/View";

@inject(IIVirtualView)
export class AjsViewModelServiceManager extends AjsDefaultServiceManager implements ViewModelServiceManager {

    protected _virtualView: VirtualView;

    protected _rootViewModelCtor: ServiceCtor;
    protected _rootViewModel: ViewModel;

    protected _updateDomScheduled: boolean;

    protected _updatedViewModels: ViewModel[];

    constructor(virtualView: VirtualView) {
        super();
        this._virtualView = virtualView;
        this._updateDomScheduled = false;
        this._updatedViewModels = [];
    }

    /**
     * For internal purposes of the ViewModel tree management the first instantiated service will be used as a root
     * Because ViewModels can be injected to the root it is necessary to remember the first VM
     * @param serviceDescriptor Descriptor of the service being instantiated
     */
    public serviceInstantiating(serviceDescriptor: ServiceDescriptor) {
        if (!this._rootViewModelCtor) this._rootViewModelCtor = serviceDescriptor.ctor;
        super.serviceInstantiating(serviceDescriptor);
    }

    /**
     * For internal purposes of the ViewModel tree management the first instantiated service will be used as a root
     * Remembered ViewModel's constructor is used to detect the first VM being instantiated
     * If view is in use the view is instantiated (or existing instance collected) using the same DiC as ViewModel
     * If the template is in use, resolving of it is initiated using the template manager
     * @param serviceDescriptor Descriptor of the service instantiated
     * @param instance Instance of the service (actually ViewModel) instantiated
     */
    public async serviceInstantied(serviceDescriptor: ServiceDescriptor, instance: Service): Promise<void> {

        if (!this._rootViewModel && instance instanceof this._rootViewModelCtor) {
            this._rootViewModel = <ViewModel>instance;
        }

        const viewKey: any = ServiceDecoration.getCtorDecoration<ServiceCtorDecorationsMvvm>(serviceDescriptor.ctor, { useView: undefined });

        if (viewKey) {
            const dic: ContainerService = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(instance, { dic: undefined });
            const view = await dic.resolveService(viewKey, instance);
            ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsMvvm>(instance, { view });
        }

        super.serviceInstantied(serviceDescriptor, instance);

    }

    /**
     * When any view model is disposed it is neccessary to check if it is a root View Model and eventually release references to it too
     * Additionally, if view was used its instance is also disposed
     * If the template was used with the view model the reference to the template is also released
     * @param instance Instance of the view model being disposed
     */
    public serviceDisposing(instance: Service): void {

        super.serviceDisposing(instance);

        const view: View = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsMvvm>(instance, { view: undefined });

        if (view) {
            const dic: ContainerService = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(instance, { dic: undefined });
            dic.disposeServiceInstance(view);
            ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsMvvm>(instance, { view: undefined });
        }

        ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsMvvm>(instance, { templatePromise: undefined });

        if (this._rootViewModel === instance) {
            this._rootViewModelCtor === undefined;
            this._rootViewModel === undefined;
        }

        this._scheduleDomUpdate();
    }

    /**
     * Called from the ViewModel when the view model state will change
     * @param viewModel View model reporting its state has been updated
     */
    public viewModelUpdated(viewModel: ViewModel): void {
        if (this._updatedViewModels.indexOf(viewModel) !== -1) return;
        this._updatedViewModels.push(viewModel);
        this._scheduleDomUpdate();
    }

    /**
     * Creates a VieModel and sets its parent to the provided value. View model must be registered in the DiC.
     * @param parent Parent view model creating the component
     * @param childKey DiC registered view model key
     */
    public async createChildViewModel(parent: ViewModel, childDiCKey: any): Promise<ViewModel> {
        const dic: ContainerService = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(parent, { dic: undefined });
        return dic.resolveService<ViewModel>(childDiCKey, parent);
    }

    /**
     * Disposes a children view model of the given view model created in another way than by URL binding
     * @param child Child view model (and its ViewComponent tree) to be released
     */
    public disposeChildViewModel(parent: ViewModel, child: ViewModel): undefined {
        return undefined;
    }

    /**
     * Schedules the DOM update for next tick (when all currently running JS tasks are done)
     */
    protected _scheduleDomUpdate(): void {
        if (this._updateDomScheduled) return;
        this._updateDomScheduled = true;

        requestAnimationFrame(() => {
            this._updateDomScheduled = false;
            this._updateDom();
        });

        /*
        setTimeout(() => {
            this._updateDomScheduled = false;
            this._updateDom();
        }, 0);
        */
    }

    /**
     * Performs the DOM update of all services previously called the viewModelUpdated method
     */
    protected async _updateDom(): Promise<void> {

        // if there is root view model in the update queue, update the root view model and throw out other changes
        for (const vm of this._updatedViewModels) {
            if (vm === this._rootViewModel) {
                await this._virtualView.fullRender(this._rootViewModel);
                this._updatedViewModels = [];
                return;
            }
        }

        // otherwise render just updated view models and do it only once for each in the queue
        while (this._updatedViewModels.length > 0) {
            const vm = this._updatedViewModels.shift();
            await this._virtualView.partialRender(vm);
        }

    }


}
