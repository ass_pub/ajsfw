/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Service, ServiceCtor } from "ajsfw/core/services/Service";
import { DependencyIdentifier } from "ajsfw/services/dic/DependencyIdentifier";
import { ViewModel } from "./ViewModel";

export const IIViewModelServiceManager: DependencyIdentifier = [ "IIViewModelServiceManager" ];

export interface ViewModelServiceManagerCtor extends ServiceCtor {
    new(...dependecies: any[]): ViewModelServiceManager;
}

export interface ViewModelServiceManager {

    /**
     * Should be called from view model when it's state changes when DOM update is required
     * @param viewModel ViewModel which state has been changed
     */
    viewModelUpdated(viewModel: ViewModel): void;

    /**
     * Creates a VieModel and sets its parent to the provided value
     * @param parent Parent view model creating the component
     * @param childKey DiC registered view model key
     */
    createChildViewModel(parent: ViewModel, childKey: any): Promise<ViewModel>;

    /**
     * Disposes a children view model of the given view model created in another way than by URL binding
     * @param child Child view model (and its ViewComponent tree) to be released
     */
    disposeChildViewModel(parent: ViewModel, child: ViewModel): undefined;

}
