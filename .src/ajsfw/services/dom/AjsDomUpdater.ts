/*******************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { eventAttributes } from "./eventAttributes";
import { DomUpdater } from "./DomUpdater";

import { Document as vdomDocument } from "ajsfw/lib/vdom/Document";
import { Node as vdomNode } from "ajsfw/lib/vdom/Node";
import { Element as vdomElement } from "ajsfw/lib/vdom/Element";

export class AjsDomUpdater implements DomUpdater {

    public fullUpdate(shadowDom: vdomDocument): void {
        this._updateNodeTree(shadowDom.body, document.body);
    }

    public partialUpdate(shadowDom: vdomDocument): void {

        if (shadowDom.body.firstChild.nodeType === vdomDocument.ELEMENT_NODE) {

            const classAttr = (<vdomElement>shadowDom.body.firstChild).attributes.getNamedItem("class");
            const tmp = (/ajsService[0-9].*/g).exec(classAttr.nodeValue);

            if (!Array.isArray(tmp) || tmp.length === 0) return;

            const serviceId = tmp[0];
            const elements = document.getElementsByClassName(serviceId);

            if (elements.length === 0) return;
            const firstElement = elements.item(0);
            const lastElement = elements.item(elements.length - 1);

            const parentNode = elements.item(0).parentNode;
            let firstIndex: number;
            let LastIndex: number;

            for (let i = 0, l = parentNode.childNodes.length; i < l; i++) {
                if (parentNode.childNodes.item(i) === firstElement) firstIndex = i;
                if (parentNode.childNodes.item(i) === lastElement) {
                    LastIndex = i;
                    break;
                }
            }

            this._partialUpdateNodeTree(shadowDom.body, parentNode, firstIndex, LastIndex);

        }

    }

    private _updateNodeTree(src: vdomNode, dst: Node): void {

        for (let i = 0; i < src.childNodes.length; i++) {

            const srcNode = src.childNodes.item(i);

            if (srcNode.nodeType !== vdomNode.ELEMENT_NODE && srcNode.nodeType !== vdomNode.TEXT_NODE) {
                continue;
            }

            if (i >= dst.childNodes.length) {
                this._appendNode(srcNode, dst);
                continue;
            }

            const dstNode = dst.childNodes.item(i);

            if (srcNode.nodeType !== dstNode.nodeType) {
                this._replaceNode(srcNode, dstNode);
                continue;
            }

            if (srcNode.nodeType === vdomNode.TEXT_NODE) {
                this._updateNode(srcNode, dstNode);
                continue;
            }

            if (srcNode.nodeName !== dstNode.nodeName) {
                this._replaceNode(srcNode, dstNode);
                continue;
            }

            this._updateNode(srcNode, dstNode);

        }

        // skip child nodes removal in some special cases
        if (dst instanceof HTMLTextAreaElement) return;

        while (dst.childNodes.length > src.childNodes.length) {
            const node = dst.childNodes.item(src.childNodes.length);
            node.parentNode.removeChild(node);
        }

    }
    private _partialUpdateNodeTree(src: vdomNode, dst: Node, firstIndex: number, lastIndex: number): void {

        for (let i = 0; i < src.childNodes.length; i++) {

            const srcNode = src.childNodes.item(i);

            if (srcNode.nodeType !== vdomNode.ELEMENT_NODE && srcNode.nodeType !== vdomNode.TEXT_NODE) {
                continue;
            }

            const index = firstIndex + i;

            if (index > lastIndex) {
                if (index + 1 > dst.childNodes.length - 1) {
                    this._appendNode(srcNode, dst);
                } else {
                    this._insertNode(srcNode, dst, index + 1);
                }
            }

            const dstNode = dst.childNodes.item(index);

            if (srcNode.nodeType !== dstNode.nodeType) {
                this._replaceNode(srcNode, dstNode);
                continue;
            }

            if (srcNode.nodeType === vdomNode.TEXT_NODE) {
                this._updateNode(srcNode, dstNode);
                continue;
            }

            if (srcNode.nodeName !== dstNode.nodeName) {
                this._replaceNode(srcNode, dstNode);
                continue;
            }

            this._updateNode(srcNode, dstNode);

        }

        // remove nodes until lastIndex

    }

    private _createNode(node: vdomNode): Node {

        if (node.nodeType === vdomNode.ELEMENT_NODE) {
            const srcElement: vdomElement = node as vdomElement;
            const element: Element = document.createElement(node.nodeName);

            this._updateElementAttributes(srcElement, element);

            if (srcElement.hasOwnProperty("customData")) {
                (<any>element)["customData"] = srcElement.customData;
            }

            return element;
        }

        if (node.nodeType === vdomNode.TEXT_NODE) {
            return document.createTextNode(this._encodeText(node.textContent));
        }

        return null;
    }

    private _appendNode(srcNode: vdomNode, dst: Node): void {

        const node: Node = this._createNode(srcNode);

        if (node !== null) {
            dst.appendChild(node);
        }

        this._updateNodeTree(srcNode, node);

    }

    private _insertNode(srcNode: vdomNode, dst: Node, index: number): void {
        const node: Node = this._createNode(srcNode);

        if (node !== null) {
            const nodeAfter = dst.childNodes.item(index);
            nodeAfter.parentNode.insertBefore(node, nodeAfter);
        }

        this._updateNodeTree(srcNode, node);
    }

    private _replaceNode(srcNode: vdomNode, dstNode: Node): void {

        const node: Node = this._createNode(srcNode);

        if (node !== null) {
            dstNode.parentNode.replaceChild(node, dstNode);
        }

        this._updateNodeTree(srcNode, node);

    }

    private _updateNode(srcNode: vdomNode, dstNode: Node): void {

        if (srcNode.nodeType === vdomNode.TEXT_NODE) {
            const enc = this._encodeText(srcNode.nodeValue);
            if (dstNode.parentNode.nodeName === "TEXTAREA") {
                (<any>dstNode.parentNode).value = enc;
            } else {
                if (dstNode.nodeValue !== enc) dstNode.nodeValue = enc;
            }
            return;
        }

        if (srcNode.nodeType === vdomNode.ELEMENT_NODE) {

            const srcElement: vdomElement = srcNode as vdomElement;
            const dstElement: Element = dstNode as Element;

            if (srcElement.hasOwnProperty("customData")) {
                (<any>dstElement)["customData"] = srcElement.customData;
            }

            this._updateElementAttributes(srcElement, dstElement);
            this._updatePropsFromAttrs(srcElement, dstElement);

            this._updateNodeTree(srcNode, dstNode);

            return;
        }

    }

    private _updateElementAttributes(srcElement: vdomElement, dstElement: Element): void {

        // Non-existing attribute
        const attrsToRemove: string[] = [];

        for (let i = 0; i < dstElement.attributes.length; i++) {
            if (!srcElement.hasAttribute(dstElement.attributes.item(i).nodeName)) {
                attrsToRemove.push(dstElement.attributes.item(i).nodeName);
            }
        }

        for (let i = 0; i < attrsToRemove.length; i++) {
            dstElement.removeAttribute(attrsToRemove[i]);
        }

        // Non-existing event listeners
        const listenersToRemove: any[] = [];

        if ((<any>dstElement)._ajsListeners) {

            // find listeners not matching src element attributes
            for (const listener of (<any>dstElement)._ajsListeners) {
                if (!srcElement.hasAttribute(listener.event)) {
                    dstElement.removeEventListener(listener.event, listener.handler);
                    listenersToRemove.push(listener);
                }
            }

            // remove listeners from _ajsListeners collection
            for (const listener of listenersToRemove) {
                const i = (<any>dstElement)._ajsListeners.indexOf(listener);
                (<any>dstElement)._ajsListeners.splice(i, 1);
            }

        }

        // attribute addition or update

        for (let i = 0; i < srcElement.attributes.length; i++) {

            // because of some special cases it is necessary to set the value of the element object instead of
            // changing the attribute itself (it is mainly the text area)

            if (srcElement.attributes.item(i).nodeName === "value") {

                // the file accepts just empty string so if the value is empty, clear selected files, otherwise skip this step
                if (dstElement instanceof HTMLInputElement && dstElement.type === "file") {
                    if (srcElement.attributes.item(i).nodeValue) dstElement.value = "";
                    continue;
                }

                // the value is set only in case the dst element has the value property and it is different than the value to be set
                if (dstElement !== undefined && (<any>dstElement).value !== srcElement.attributes.item(i).nodeValue) {
                    (<any>dstElement).value = srcElement.attributes.item(i).nodeValue;
                }

                continue;
            }

            // attribute update

            if (dstElement.hasAttribute(srcElement.attributes.item(i).nodeName)) {

                if (eventAttributes.indexOf(srcElement.attributes.item(i).nodeName) === -1) {
                    if (dstElement.getAttribute(srcElement.attributes.item(i).nodeName) !== srcElement.attributes.item(i).nodeValue) {
                       dstElement.setAttribute(srcElement.attributes.item(i).nodeName, srcElement.attributes.item(i).nodeValue);
                    }
                }

            // attribute addition / event listener addition - update is not currently necessary as it will definitely point to the same handler

            } else {

                // event listeners
                if (eventAttributes.indexOf(srcElement.attributes.item(i).nodeName) !== -1) {

                    const evtName = srcElement.attributes.item(i).nodeName.substr(2);

                    // check if the listener was added or not
                    let exist = false;
                    if ((<any>dstElement)._ajsListeners) {
                        for (const listener of (<any>dstElement)._ajsListeners) {
                            if (listener.event === evtName) {
                                exist = true;
                                break;
                            }
                        }
                    }

                    // if not, add it
                    if (!exist) {
                        dstElement.addEventListener(
                            evtName,
                            srcElement.attributes.item(i).customData
                        );

                        (<any>dstElement)._ajsListeners = (<any>dstElement)._ajsListeners || [];
                        (<any>dstElement)._ajsListeners.push({
                            event: evtName,
                            handler: srcElement.attributes.item(i).customData
                        });
                    }

                // attributes
                } else {
                    dstElement.setAttribute(srcElement.attributes.item(i).nodeName, srcElement.attributes.item(i).nodeValue);
                }

            }
        }

    }

    private _updatePropsFromAttrs(srcElement: vdomElement, dstElement: Element): void {

        if (dstElement instanceof HTMLOptionElement) {
            dstElement.selected = srcElement.hasAttribute("selected");
        }

    }

    private _encodeText(text: string): string {
        let r = text;
        r = r.replace(/\&nbsp\;/gm, "\u00A0");
        r = r.replace(/\&lt\;/gm, "\u003C");
        r = r.replace(/\&gt\;/gm, "\u003E");
        return r;
    }

}