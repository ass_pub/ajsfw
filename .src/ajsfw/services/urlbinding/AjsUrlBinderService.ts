/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { AjsAppServiceBuilder } from "ajsfw/builders/AjsAppServiceBuilder";
import { SIAppServiceBuilder } from "ajsfw/builders/DependencyIdentifiers";
import { Service } from "ajsfw/core/services/Service";
import { ServiceBuilder, ServiceBuilderCtor } from "ajsfw/core/services/ServiceBuilder";
import { ApplicationModule, ApplicationModuleCtor } from "ajsfw/services/appmodules/ApplicationModule";
import { ApplicationModuleDescriptor } from "ajsfw/services/appmodules/ApplicationModuleDescriptor";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { IINavigationService, NavigationService } from "ajsfw/services/navigation/NavigationService";
import { NavigationNotificationData } from "ajsfw/services/navigation/NavigationNotificationData";
import { Listener } from "ajsfw/lib/events/Listener";
import { IIPackageLoader, PackageLoader } from "ajsfw/services/packages/PackageLoader";
import { UrlBinder } from "./UrlBinder";
import { UrlBindingInfo } from "./UrlBindingInfo";
import { AjsfwStartupConfig, IIAjsfwStartupConfig } from "ajsfw/core/main/AjsfwStartupConfig";

import * as exceptions from "./exceptions";
import { StringKeyValue } from "ajsfw/types";
import { ServiceDecoration } from "ajsfw/core/services/ServiceDecoration";
import { ServiceObjectDecorationsBindings } from "./ServiceObjectDecorationsBindings";
import { ServiceObjectDecorationsDic } from "ajsfw/services/dic/ServiceObjectDecorationsDic";
import { ContainerService } from "ajsfw/services/dic/ContainerService";
import { ServiceProtoDecorationsBindings } from "./ServiceProtoDecorationsBindings";

interface ServiceBindingInfo {
    path: UrlBindingInfo[];
    hash: UrlBindingInfo[];
    search: UrlBindingInfo[];
}

@inject(IINavigationService, IIPackageLoader, IIAjsfwStartupConfig)
export class AjsUrlBinderService implements UrlBinder {

    private _navigation: NavigationService;
    private _packageLoader: PackageLoader;
    private _startupConfig: AjsfwStartupConfig;

    private _rootService: Service;

    private _boundPath: string;

    private _navigatedBound: Listener<NavigationNotificationData>;

    constructor(navigation: NavigationService, packageLoader: PackageLoader, startupConfig: AjsfwStartupConfig) {
        this._navigation = navigation;
        this._packageLoader = packageLoader;
        this._startupConfig = startupConfig;
        this._boundPath = this._startupConfig.rootPath;
        this._navigatedBound = this._navigated.bind(this);
    }

    public init(): void {
        this._navigation.navigatedNotifier.subscribe(this._navigatedBound);
    }

    public async injectProperties(service: Service): Promise<void> {

        const bindings = this._getServiceBindings(service);

        if (bindings.path || bindings.hash || bindings.search) {
            if (!this._rootService) this._rootService = service;
            return this._bind(service, bindings);
        }

    }

    private _navigated(sender: any, nav: NavigationNotificationData): void {

        // #ifdef DEBUG
        log.empty();
        log.info(1, this, "Navigated: ", window.location.pathname, window.location.hash, window.location.search);
        // #endif

        this._boundPath = this._startupConfig.rootPath;

        const bindings = this._getServiceBindings(this._rootService);
        if (this._rootService) this._bind(this._rootService, bindings);

    }

    private _getServiceBindings(service: Service): ServiceBindingInfo {
        const proto = Object.getPrototypeOf(service);
        return {
            hash: ServiceDecoration.getObjectDecoration<ServiceProtoDecorationsBindings>(proto, { urlHashBindings: undefined }),
            path: ServiceDecoration.getObjectDecoration<ServiceProtoDecorationsBindings>(proto, { urlPathBindings: undefined }),
            search: ServiceDecoration.getObjectDecoration<ServiceProtoDecorationsBindings>(proto, { urlSearchBindings: undefined })
        };
    }

    private async _bind(service: Service, bindings: ServiceBindingInfo): Promise<void> {
        if (bindings.path) await this._bindPath(service, bindings);
        if (bindings.hash) await this._bindHash(service, bindings);
        if (bindings.search) await this._bindSearch(service, bindings);
    }

    private async _bindPath(service: Service, bindings: ServiceBindingInfo): Promise<void> {

        if (!bindings.path || bindings.path.length === 0) {
            // #ifdef DEBUG
            log.warning(this, "No properties to bind (404). Service: ", service);
            // #endif
            return;
        }

        // find all unique properties and prepare info if the property was bound or not
        const uniqueProps: string[] = [];
        for (let i = 0; i < bindings.path.length; i++) {
            if (uniqueProps.indexOf(bindings.path[i].property) === -1) {
                uniqueProps.push(bindings.path[i].property);
            }
        }

        let serviceBound = false;
        const boundProperties: string[] = [];

        // for each unique property build defined path bindings
        for (let i = 0; i < uniqueProps.length; i++) {

            // create a separate list of binding info in reverse order
            const bindingsInfoFiltered: UrlBindingInfo[] = [];
            for (let j = 0; j < bindings.path.length; j++) {
                if (bindings.path[j].property === uniqueProps[i]) bindingsInfoFiltered.splice(0, 0, bindings.path[j]);
            }

            // and bind property to the path if the path is matching
            const boundResult = await this._doPathBinding(service, bindingsInfoFiltered, boundProperties, bindings);
            serviceBound = serviceBound || boundResult;

        }

        if (service.bindingsUpdated && boundProperties.length > 0) {
            service.bindingsUpdated(boundProperties);
        }

        // if no service was bound to any of property, report no rule match (404)
        if (!serviceBound) {
            // #ifdef DEBUG
            log.warning(this, "No binding rule mathced (404) on service ", service.constructor);
            // #endif
        }

    }

    private async _doPathBinding(service: Service, bindingsInfo: UrlBindingInfo[], bound: string[], bindings: ServiceBindingInfo): Promise<boolean> {

        const path: string = location.pathname.substr(this._boundPath.length);

        // #ifdef DEBUG
        log.info(1, this, "Trying to bind path '", path, "' to ", service.constructor, ".", bindingsInfo[0].property, " (Previously bound path: '", this._boundPath, "')");
        // #endif

        let optional = false;
        const previousValue = (<any>service)[bindingsInfo[0].property];

        for (let i = 0; i < bindingsInfo.length; i++) {

            const tokenizedRule: string[] = this._tokenizeRule(bindingsInfo[i].rule);
            const matchedPath: string = this._matchRule(tokenizedRule, path);

            optional = optional || tokenizedRule.indexOf("??") === 0;

            if (matchedPath === undefined) continue;

            // #ifdef DEBUG
            log.info(1, this, "Binding rule '", bindingsInfo[i].rule, "' for property '", bindingsInfo[i].property, "' matches to '", matchedPath, "'. Using dep/pkg (", bindingsInfo[i].dependencyOrPackage, ")");
            // #endif

            if (!bindingsInfo[i].dependencyOrPackage) {
                (<any>service)[bindingsInfo[i].property] = matchedPath;

                // #ifdef DEBUG
                log.info(1, this, "Property bound: ", service.constructor, ".", bindingsInfo[i].property, " = '", matchedPath, "'");
                // #endif

                if (previousValue !== (<any>service)[bindingsInfo[i].property]) bound.push(bindingsInfo[i].property);
                return false;
            }

            this._boundPath += matchedPath !== "" ? "/" + matchedPath : "";

            const dic: ContainerService = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(service, { dic: undefined });
            const dep: Service = await dic.resolveService(bindingsInfo[i].dependencyOrPackage, service);

            if ((<ApplicationModuleDescriptor>dep).appModuleUrl) {
                const pkgName = (<ApplicationModuleDescriptor>dep).appModuleUrl;
                // #ifdef DEBUG
                log.info(1, this, "Binding package to service property: ", service.constructor , ".", bindingsInfo[i].property, " <= ", pkgName, " (matched path part: '", matchedPath, "')");
                // #endif
                await this._loadAndBind(pkgName, service, bindingsInfo[i], bindings);
            } else {
                // #ifdef DEBUG
                log.info(1, this, "Binding service to service property: ", service.constructor, ".", bindingsInfo[i].property, " <= ", dep.constructor, " (matched path part: '", matchedPath, "')");
                // #endif
                await this._bindService(service, bindingsInfo[i], dep);
            }

            if (previousValue !== (<any>service)[bindingsInfo[i].property]) bound.push(bindingsInfo[i].property);

            return true;
        }

        if (bindingsInfo.length > 0) {
            if ([bindingsInfo[0].property]) {
                this._disposeTree((<any>service)[bindingsInfo[0].property]);
                (<any>service)[bindingsInfo[0].property] = undefined;
            }
        }

        if (previousValue !== (<any>service)[bindingsInfo[0].property]) bound.push(bindingsInfo[0].property);

        // Optional only if the rest of the path is empty
        optional = optional && path === "";

        // #ifdef DEBUG
        log.info(1, this, "Not bound, optional = ", optional);
        // #endif

        return optional;
    }

    private async _bindService(service: Service, bindingInfo: UrlBindingInfo, dependency: Service): Promise<void> {
        if ((<any>service)[bindingInfo.property] !== dependency) {

            if ((<any>service)[bindingInfo.property]) this._disposeTree((<any>service)[bindingInfo.property]);

            (<any>service)[bindingInfo.property] = dependency;
            if (dependency.bound) dependency.bound();

            const depBindings = this._getServiceBindings(dependency);
            if (depBindings) await this._bindSearch(dependency, depBindings);

            // #ifdef DEBUG
            log.info(1, this, "Service bound: ", service.constructor, ".", bindingInfo.property, " <= ", dependency.constructor);
            // #endif
        }
        else {
            // #ifdef DEBUG
            log.info(1, this, "Reusing previous service binding: ", service.constructor, ".", bindingInfo.property, " = ", dependency.constructor);
            // #endif

            if ((<any>service)[bindingInfo.property].bound) (<any>service)[bindingInfo.property].bound();
            const bindings = this._getServiceBindings((<any>service)[bindingInfo.property]);
            this._bind((<any>service)[bindingInfo.property], bindings);
        }
    }

    private async _loadAndBind(pkgName: string, service: Service, bindingInfo: UrlBindingInfo, bindings: ServiceBindingInfo): Promise<void> {

        // check if the package to load is different than previous
        if ((<any>service)[bindingInfo.property]) {

            const pkg: string = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsBindings>((<any>service)[bindingInfo.property], { package: undefined });

            if (pkg === pkgName) {
                // #ifdef DEBUG
                log.info(1, this, "Reusing the loaded package '", pkgName, "'");
                // #endif
                this._bind((<any>service)[bindingInfo.property], bindings);
                return;
            }

            // #ifdef DEBUG
            log.info(1, this, "Disposing package services");
            // #endif

            const appModule: ApplicationModule = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsBindings>(service, { module: undefined });

            // dispose container and all services it manages (done automatically inside of it)
            if (appModule) {
                const dic = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(appModule, { dic: undefined });
                dic.disposeServiceInstance(appModule);
            }

        }

        // #ifdef DEBUG
        log.info(1, this, "Loading application module package '", pkgName, "'");
        // #endif

        // Recognize the same or different package and eventually dispose the tree
        // For each new loaded application module the child dic is used
        let dic: ContainerService = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(service, { dic: undefined });
        dic = dic.createChildDic();

        // load package and get main module id
        const mainModuleId = await this._packageLoader.load(pkgName);
        log.info(1, this, "Package loaded: ", pkgName);

        // default export of the main package module must be an ApplicationModule
        const appModuleCtor: ApplicationModuleCtor = (await import(mainModuleId)).default;

        // #ifdef DEBUG
        log.info(1, this, "Package application module: ", appModuleCtor);
        // #endif

        // add the main application module and resolve it
        dic.addSingleton(appModuleCtor);
        const appModule = await dic.resolveService<ApplicationModule>(appModuleCtor);

        // use custom service builder
        if (appModule.useCustomServiceBuilder) {
            dic.addSingleton(SIAppServiceBuilder, appModule.useCustomServiceBuilder());
        } else {
            dic.addSingleton(SIAppServiceBuilder, <ServiceBuilderCtor>AjsAppServiceBuilder);
        }

        // configure application module services
        const asb: ServiceBuilder = await dic.resolveService<ServiceBuilder>(SIAppServiceBuilder);
        if (appModule.configureServices) {
            await appModule.configureServices();
        } else {
            await asb.useDefaults();
        }

        // build application module services
        await asb.build();

        // resolve the application module default service (previously configured using the configureServices)
        if (appModule.defaultServiceKey) {
            const key = appModule.defaultServiceKey();

            // #ifdef DEBUG
            log.info(1, this, "Application module default service: ", key);
            // #endif

            if (!key) throw new exceptions.MissingApplicationModuleDefaultServiceKey();
            const dep = await <Service>dic.resolveService(key, service);

            // store the package name (for tree freeing)
            ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsBindings>(dep, {
                package: pkgName,
                module: appModule
            });

            // bind
            (<any>service)[bindingInfo.property] = dep;
        } else {
            throw new exceptions.MissingApplicationModuleDefaultServiceKey();
        }

    }

    private _disposeTree(service: Service): void {

        if (!service) return;
        if (typeof(service) !== "object") return;

        const bindings = this._getServiceBindings(service);

        // #ifdef DEBUG
        log.info(1, this, "Disposing ", service);
        // #endif

        if (bindings.path) {
            for (let i = 0; i < bindings.path.length; i++) {
                this._disposeTree((<any>service)[bindings.path[i].property]);
                (<any>service)[bindings.path[i].property] = undefined;
            }
        }

        if (bindings.hash) {
            for (let i = 0; i < bindings.hash.length; i++) {
                this._disposeTree((<any>service)[bindings.hash[i].property]);
                (<any>service)[bindings.hash[i].property] = undefined;
            }
        }

        if (bindings.search) {
            for (let i = 0; i < bindings.search.length; i++) {
                this._disposeTree((<any>service)[bindings.search[i].property]);
                (<any>service)[bindings.search[i].property] = undefined;
            }
        }

        const dic: ContainerService = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(service, { dic: undefined });

        if (dic) {
            dic.disposeServiceInstance(service);
        }
    }

    private _tokenizeRule(rule: string): string[] {
        const tokens: string[] = [];

        let pos = 0;
        let currentToken = "";
        let tagOpened = false;
        do {

            if (!tagOpened && rule.substr(pos, 2) === "**") {
                if (currentToken !== "") tokens.push(currentToken);
                currentToken = "";
                tokens.push("**");
                break;
            }

            if (!tagOpened && rule.substr(pos, 2) === "??") {
                if (currentToken !== "") tokens.push(currentToken);
                currentToken = "";
                tokens.push("??");
                pos += 2;
                continue;
            }

            if (!tagOpened && rule[pos] === "{") {
                if (currentToken !== "") tokens.push(currentToken);
                currentToken = "{";
                tagOpened = true;
                pos++;
                continue;
            }

            if (tagOpened && rule[pos] === "}") {
                tokens.push(currentToken + "}");
                currentToken = "";
                tagOpened = false;
                pos++;
                continue;
            }

            if (pos < rule.length) {
                currentToken += rule[pos];
                pos++;
                continue;
            }

        } while (pos < rule.length);

        if (currentToken !== "") tokens.push(currentToken);

        return tokens;
    }

    private async _bindHash(service: Service, bindings: ServiceBindingInfo): Promise<void> {

        if (!bindings.hash || bindings.hash.length === 0) return;
        for (let i = 0; i < bindings.hash.length; i++) {
            // console.log(bindings[i].property, bindings[i].rule, anyToString(bindings[i].dependency));
        }

    }

    private async _bindSearch(service: Service, bindings: ServiceBindingInfo): Promise<void> {

        if (!bindings.search || bindings.search.length === 0) return;

        const fixSearch = location.search.length > 0 && location.search[0] === "?" ? location.search.substr(1) : location.search;
        const searchArgs: string[] = fixSearch !== "" ? fixSearch.split("&") : [];
        const search: StringKeyValue<any> = {};

        for (const s of searchArgs) {

            if (s.indexOf("=") !== -1) {
                const kv = s.split("=");
                search[kv[0]] = kv[1];
            } else {
                search[s] = true;
            }
        }

        const boundProperties = [];
        for (let i = 0; i < bindings.search.length; i++) {
            (<any>service)[bindings.search[i].property] = search[bindings.search[i].searchKey];
            boundProperties.push(bindings.search[i].property);
        }

        if (boundProperties.length > 0 && service.bindingsUpdated) service.bindingsUpdated(boundProperties);

    }

    private _matchRule(ruleTokens: string[], path: string): string | undefined {

        if (ruleTokens.length === 1 && ruleTokens[0] === "**") return "";

        let toMatch = "(^/?)";
        let groupCounter = 1;
        let lastGroup = -1;

        for (let i = 0; i < ruleTokens.length; i++) {
            if (ruleTokens[i].indexOf("{string") === 0) {
                const toAdd = this._prepareStringMatch(ruleTokens[i]);
                if (groupCounter > 1 && toMatch[toMatch.length - 1] === ")") {
                    toMatch = toMatch.substr(0, toMatch.length - 1) + toAdd + ")";
                } else {
                    toMatch += toAdd;
                }
                for (const char of ruleTokens[i]) if (char === "(") groupCounter++;
                groupCounter++;
            } else if (ruleTokens[i].indexOf("{number") === 0) {
                const toAdd = this._prepareNumberMatch(ruleTokens[i]);
                if (groupCounter > 1 && toMatch[toMatch.length - 1] === ")") {
                    toMatch = toMatch.substr(0, toMatch.length - 1) + toAdd + ")";
                } else {
                    toMatch += toAdd;
                }
                for (const char of ruleTokens[i]) if (char === "(") groupCounter++;
                groupCounter++;
            } else if (ruleTokens[i] === "**") {
                if (lastGroup === -1) lastGroup = groupCounter;
                toMatch += "($|/.*)";
                break;
            } else if (ruleTokens[i] === "??") {
                if (lastGroup === -1) lastGroup = groupCounter;
                continue;
            } else {
                for (const char of ruleTokens[i]) if (char === "(") groupCounter++;
                toMatch += "(" + ruleTokens[i] + ")";
                groupCounter++;
            }
        }

        const rx = new RegExp(toMatch);
        const mx = path.match(rx);

        if (!mx) return undefined;

        let result = "";
        for (let i = 2; i < mx.length; i++) {
            if (mx[i]) result += mx[i];
            if (i >= lastGroup) break;
        }

        return result;

    }

    private _prepareStringMatch(rule: string): string {
        if (rule.indexOf(":") === -1) throw new exceptions.MissingStringEvaluationArgument();
        const helper = rule.substring(rule.indexOf(":") + 1, rule.indexOf("}"));
        if (helper === "lang") {
            return "([a-z][a-z]-[A-Z][A-Z])";
        } else {
            const rulefix: string = "(" + helper + ")";
            if (rule[rule.indexOf(":") - 1] === "?") {
                return rulefix + "?";
            }
            return rulefix;
        }
    }

    private _prepareNumberMatch(rule: string): string {
        // full float regexp (for url probably too much)
        const rulefix = "([-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?)";

        if (rule.indexOf("?") === rule.indexOf("}") - 1) {
            return rulefix + "?";
        }

        return rulefix;
    }

}