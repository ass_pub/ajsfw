# Url binding

Usually, the router is used to manage this stuff in common server-side or client-side frameworks.

<p>
Ajs Framework introduces concept of so called "Url binding" which allows developers to develop various application components independently
without need of knowing the internal structure and URL mappings of "children" components and without need of usage of routers or "children"
router component tree. Declaration of "routes" - url bindings - is moved directly to objects (usually application, application modules and
view models) so it is always joined with the given component even if the component tree is completely redesigned. Url bindings are processed
by the Url binder which can be considered as the similar component to the router as it takes care of similar tasks (creation and releasing
of services based on the URL) and binding of the service instance properties to the given properties of the URL. For this purposes the Ajs
Framework is using property injection pattern. The Url Binder service is implemented in the AjsUrlBinder class. This class is implementing
the PropertyInjector interface so it can be used as an extension of the dependency injection container. Each property to be bound to the
URL must be decorated using the @urlBind() decorator. This decorator stores information to the object prototype under the
__ajs__.url&lt;type&gt;Bindings__ properties (see bellow). These properties are supposed to be used by the Ajs Framework internally only and
should not be modified by the application.
</p>

## Basics

<p>
First of all, lets take a look on the structure of the URL. Everybody knows it, just to be sure the terminology used is clear and dummies
understands how the URL is composed and used by various applications, mainly the web browsers and web servers:
</p>

Part     | Example                 | Description
-------- | ----------------------- | ------------------------------------------------------------------------------------------------
Protocol | https:// ftp:// file:// | specifies the transfer protocol to be used to transfer the data between the client and server
Hostname | google.com, 1.2.3.4     | specifies the DNS name of the host or its IP address. It is the remote server asked for the resource
Port     | :80 :443 :8080 :8443    | usually the TCP port on which the remote service is listening for incoming connections on the given server
Path     | /index.html /a/1/b/2    | uniquely identifies the resource on the server to be transferred to the client
Search   | ?a=1&b=2c=3             | key/value "container" used to closer define the resource to be returned from the server (usually filter)
Hash     | #abcd                   | Usually not transferred to the server at all. By default it is used for the navigation to anchors inside of the HTML document

<p>
During the URL binding procedure (and using the default AjsUrlBinder), only the Path, Search and Hash parts of the URL can be used. Protocol,
hostname and port is not used in the binding process at all and if it is required inside of the application for whatever reason it must be
handled manually by the programmer. The Path and Hash parts of the URL can be used for property value binding or the Service tree creation.
The Search part of the URL can be used just for property value binding. Bindings can be defined for ?any? custom class property using the
TypeScript decorators @urlBindingPath, @urlBindingHash and @urlBindingSearch. These decorators are executed when the class is constructed
and stores the information defined by these decorators to the class prototype object properties __ajs__.urlPathBindings,
__ajs__.urlHashBindings and __ajs__.urlSearchBindings respectively.
</p>

<p>
When the class is instantiated using the DiC and the AjsUrlBinder is properly registered to the DiC as a property injector the AjsUrlBinder
(and basically all other registered property injectors as well) is executed by the DiC to inject property values to the created object.
</p>

<p>
AjsUrlBinder checks if there are mentioned "internal" properties defined within the object and if so it loads definitions for property
injections. Finally, it parses the URL and according to rules and the current URL state it can:
</p>

- load and initialize the application module (see application modules for details)
- build the service tree according the current URL path (this is usually the ApplicationModule / ViewModel service tree but it can be tree
  of any Service objects)
- fills created object properties with values taken from the URL
- notify the DiC (furthermore given service managers and services) when the service is not needed anymore and can be released
  (including all its resources)

<p>
AjsUrlBinder tries to reuse loaded application modules and existing objects whenever possible. For best performance, this requires correct
configuration of service scopes in the dependency injection container.
</p>

<p>
Properties are injected once the object is constructed (so after the class constructor is called) but before init method of the object is
called (this is the default behavior when default service managers are in use).
</p>

<p>
Services to be instantiated when the particular URL is reached are instanced automatically using the same DiC used to create the service
itself.
</p>

<p>
Services not needed anymore are disposed automatically. The DiC and furthermore given registered service manager is called so the service
instance references can be released by the framework and the Dispose method can be called by the service manager.
</p>

## Url path binding

<p>
The decorator @urlBindPath can be used to bind the data or services to the object property. The decorator is applied when the class is
declared and it stores the information about bindings in the __ajs__.urlPathBindings prototype property.
</p>

```
@urlBindPath(rule: string, service?: Service)
```

<p>
rule arguments is string value describing the URL matching rule. When the rule matches the given URL part the URL part is mapped to the
object property according to the following conditions:
</p>

- If service argument is not defined the value matching the rule is bound to the decorated property
- If the service is defined and matched the service instance is created
- If no rule matches the service and ?? (see bellow) is not specified the ?? 404 is thrown ??

## Url processing by the AjsUrlBinder

<p>
Url path is parsed when the object is created and the injector (binder) is called. The binder internally remembers the part of the path
already bound to properties where services previously created were injected. For further reading the path already bound to resolved services
is referred as a "bound path". Basically, when the binder is called for the first object using the path binding the bound path is an empty
string. When the @urlBindPath(rule, service) is used and the rule is matching the part of the rest of the url which is not already bound the
bound path is extended with all matching groups of the regular expression built internally, except the matching group 1 which is internally
defined as (^\/?) - start of the (rest) url where the path delimiter (the slash tag) can be optionally present. The rest of matching groups
are used to bind the part of the patch currently matching the rule. Although it is usually not necessary, for advanced matching techniques
the rule can contain so called non-capturing group specified in the rule: (^:...) which is used to match the path but does not affect the
bound path remembered internally. Please note the service-less bindings (@urlBindPath(rule)) does not affect the bound path at all and
just binds the matching part of the path directly to the object property.
</p>

### Rule definitions:

<p>
Rule is standard regular expression but because there is specific processing performed by the AjsUrlBinder it is necessary to be careful
when specifying rules.
</p>

Following helpers can be used:

Token               | Internally replaced with regular expression | Description
------------------- | ------------------------------------------- | ----------------
{string:lang}       | [a-z][a-z]-[A-Z][A-Z]                       | Language tag according to RFC5646 (only language region pair is supported, UN region codes are not supported)
{string:\<regexp\>} | regexp                                      | Part of the path must match the string defined by the regexp
{number}            | ([-+]?[0-9]*\,?[0-9]+(?:[eE][-+]?[0-9]+)?)  | Floating number regular expression (the floating number must use , (comma) as a separator of a floating part)
{number:>=0}        | ([+]?[0-9]*\,?[0-9]+(?:[eE][-+]?[0-9]+)?)   | Floating number regular expression (the floating number must use , (comma) as a separator of a floating part)
{number:int}        | ([-+]?[0-9]*)                               | Any integer number (in range of JavaScript float)
{number:int>=0}     | ([+]?[0-9]*)                                | Integer number equal or greater to 0 (in range of JavaScript float)
**                  | ($\|/.*)                                    | The rest of the path must be end of the path or next path segment (/) followed by anything but is not included to match
??                  |                                             | The rest of the path must match the expression following double asterisk but is no included to match

<sup>
** The rest of the rule is ignored<br>
?? If used in the beginning of the expression the binding will be considered as optional. Once used, all rules of the binding are considered as optional<br>
!! Can be used anywhere in the rule and does not affect the rule itself. If another rules are applied
</sup>

#### Rule cheat sheet

```
@urlBindPath("{string?:[a-z][a-z]-[A-Z][A-Z]}**", Layout)
```

```
```


## Url hash binding

<p>
It is recommended to use the url path binding instead of the hash binding. However, hash binding can be used in case there is no possibility
to perform url rewriting on the server side or there is no access to the server configuration at all. If hash bindings are in use there is
high probability it will not be possible to use hashes for the purposes they were originally designed for (navigation to anchors inside of
the HTML page).
</p>

```
@urlBindHash(rule: string, service?: Service)
```

<p>
Arguments of the decorator are the same as in case of the path binding as well as the functionality. Hash bindings works in the same way as
path bindings but the path part of the URL is not taken into account at all. In the url the hash part starts with the hash tag followed by
the resource path in the same format it would be normally specified in the URL path part.
</p>

```
https://www.coolapp.io:443/index.html#/library/123/book/12/chapter/1
```

## Url search binding

Search bindings are defined using the following decorator

```
@urlBindSearch(key: string)
```

<p>
The key argument specifies the name of the variable in the URL search part. If the key/value pair is not contained within the search part
of the url the undefined value will be set to the property. If there are more occurrences of the same key in the url the array of values
is set to the property filled with values in the same order as these values appear in the search part of the url.
</p>

```
class CoolApp {
    @urbBindSearch("coolStuff")
    public coolStuff: string | string[];

    public init(): void {
        console.log(this.coolStuff);
    }
}

https://coolapp.io will produce "undefined" output
https://coolapp.io/?coolStuff=ajs will produce "ajs" output
https://coolapp.io/?coolStuff=ajs&cooStuff=bjs&coolStuff=cjs will produce [ "ajs", "bjs", "cjs" ] output

```

### Few more words about the URL composition

Standard "URL's" vs REST like URL's

---
Written by Frantisek Novak
Copyright (c) 2018 Atom Software Studios