/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Service, ServiceCtor } from "ajsfw/core/services/Service";
import { ServiceBuilderCtor } from "ajsfw/core/services/ServiceBuilder";
import { DependencyIdentifier } from "ajsfw/services/dic/DependencyIdentifier";

export const IIApplicationModule: DependencyIdentifier = [ "IIApplicationModule" ];

export interface ApplicationModuleCtor extends ServiceCtor {
    new(...dependencies: any[]): ApplicationModule;
}

export interface ApplicationModule extends Service {

    /**
     * Returns custom implementation of the service builder responsible for configuration of application services
     */
    useCustomServiceBuilder?(): ServiceBuilderCtor;

    /**
     * Can be used by the application to configure application services
     * If application does not implement the method the default AjsApplicationManager will be used
     * As it is usually required to inject dependencies to the method and dependency resolving is asynchronous the method must be asynchronous too
     */
    configureServices?(...dependencies: any[]): Promise<void>;

    /**
     * Returns the default application module service constructor key (usually the root module viewmodel) used when the default service was configured to the DiC.
     */
    defaultServiceKey(): DependencyIdentifier | ServiceCtor;

}