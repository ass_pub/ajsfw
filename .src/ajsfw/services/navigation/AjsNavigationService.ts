/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { NavigationService } from "./NavigationService";
import { AjsNotifier } from "ajsfw/lib/events/AjsNotifier";
import { Notifier } from "ajsfw/lib/events/Notifier";
import { NavigationNotificationData } from "./NavigationNotificationData";

export class AjsNavigationService implements NavigationService {

    private _origPushState: (data: any, title?: string, url?: string | null) => void;
    private _origReplaceState: (data: any, title?: string, url?: string | null) => void;
    private _lastUrl: string;

    private _navigatedHandler: EventListener;

    private _navigatedNotifier: Notifier<NavigationNotificationData>;
    public get navigatedNotifier(): Notifier<NavigationNotificationData> { return this._navigatedNotifier; }

    constructor() {
        this._lastUrl = "";
        this._navigatedNotifier = new AjsNotifier<NavigationNotificationData>();
        this._navigatedHandler = this._navigated.bind(this);
    }

    public init(): void {

        window.addEventListener("pushstate", this._navigatedHandler);
        window.addEventListener("replacestate", this._navigatedHandler);
        window.addEventListener("popstate", this._navigatedHandler);
        window.addEventListener("hashchange", this._navigatedHandler);

        this._lastUrl = location.href;

        const pushStateEvent: Event = document.createEvent("Event");
        pushStateEvent.initEvent("pushstate", true, true);

        const replaceStateEvent: Event = document.createEvent("Event");
        pushStateEvent.initEvent("replacestate", true, true);

        this._origPushState = history.pushState;
        this._origReplaceState = history.replaceState;

        history.pushState = (data: any, title?: string, url?: string | null) => {
            this._origPushState.apply(history, [ data, title, url]);
            window.dispatchEvent(pushStateEvent);
        };

        history.replaceState = (data: any, title?: string, url?: string | null) => {
            this._origReplaceState.apply(history, [ data, title, url]);
            window.dispatchEvent(replaceStateEvent);
        };

    }

    public dispose(): void {
        window.removeEventListener("pushstate", this._navigatedHandler);
        window.removeEventListener("replacestate", this._navigatedHandler);
        window.removeEventListener("popstate", this._navigatedHandler);
        window.removeEventListener("hashchange", this._navigatedHandler);

        this._navigatedNotifier = undefined;
    }

    public navigate(url: string): void {
        if (window.location.href !== url && window.location.href !== window.location.origin + url) {
            this._lastUrl = url;
            window.history.pushState({}, "", url);
        }
    }

    private _navigated(event: Event): void {

        this._navigatedNotifier.notify(this, {
            protocol: location.protocol,
            host: location.host,
            port: location.port,
            pathname: location.pathname,
            search: location.search,
            hash: location.hash
        });

    }


}
