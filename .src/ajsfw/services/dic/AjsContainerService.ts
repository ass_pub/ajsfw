/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { IIService, Service, ServiceCtor, IIDependentService } from "ajsfw/core/services/Service";
import { ServiceManager } from "ajsfw/core/services/ServiceManager";
import { ServiceScope } from "ajsfw/core/services/ServiceScope";
import { serviceType } from "./serviceType.decorator";
import { PropertyInjector } from "ajsfw/services/dic/PropertyInjector";
import { AjsServiceConfig } from "./AjsServiceConfig";
import { ContainerService, IIContainerService } from "./ContainerService";
import { DependencyIdentifier } from "./DependencyIdentifier";
import * as exceptions from "./exceptions";
import { MapDep } from "./MapDep";
import { MapDeps } from "./MapDeps";
import { ServiceDescriptor } from "./ServiceDescriptor";
import { ServiceManagerInfo } from "./ServiceManagerInfo";
import { ServiceCtorDecorationsDic } from "ajsfw/services/dic/ServiceCtorDecorationsDic";
import { ServiceDecoration } from "ajsfw/core/services/ServiceDecoration";
import { ServiceObjectDecorationsDic } from "ajsfw/services/dic/ServiceObjectDecorationsDic";

@serviceType(IIContainerService)
export class AjsContainerService implements ContainerService {

    private _uniqueId: number;

    private _parentContainer: ContainerService;
    public get parentContainer(): ContainerService { return this._parentContainer; }

    private _defaultServiceManager: ServiceManager;
    public get defaultServiceManager(): ServiceManager { return this._defaultServiceManager; }

    private _propertyInjectors: PropertyInjector[];
    private _serviceManagers: ServiceManagerInfo[];
    private _services: ServiceDescriptor[];

    private _disposingService: Service;
    private _disposing: boolean;

    constructor(parentContainer: ContainerService) {

        // #ifdef DEBUG
        log.constructor(1, this, "Parent container: ", parentContainer);
        // #endif

        this._parentContainer = parentContainer;
        this._propertyInjectors = [];
        this._serviceManagers = [];
        this._services = [];
        this._disposing = false;

        if (!parentContainer) this._uniqueId = 0;
    }

    public addSingleton(keyOrCtor: any|ServiceCtor, ctor?: ServiceCtor): MapDep & MapDeps {
        const skey: any = keyOrCtor;
        const sctor: ServiceCtor = ctor ? ctor :  keyOrCtor;
        const sd: ServiceDescriptor = this._addService(ServiceScope.Singleton, skey, sctor);
        return new AjsServiceConfig(this, sd);
    }

    public addTransient(keyOrCtor: any|ServiceCtor, ctor?: ServiceCtor): MapDep & MapDeps {
        const skey: any = keyOrCtor;
        const sctor: ServiceCtor = ctor ? ctor :  keyOrCtor;
        const sd: ServiceDescriptor = this._addService(ServiceScope.Transient, skey, sctor);
        return new AjsServiceConfig(this, sd);
    }

    public addServiceDescriptor(sd: ServiceDescriptor): ContainerService {
        this._services.push(sd);
        return this;
    }

    public isSingletonRegistered(key: any | ServiceCtor): boolean {
        const sd: ServiceDescriptor = this._getServiceDescriptorByKey(key);
        return sd !== undefined && sd.scope === ServiceScope.Singleton;
    }

    public isTransientRegistered(key: any | ServiceCtor): boolean {
        const sd: ServiceDescriptor = this._getServiceDescriptorByKey(key);
        return sd !== undefined && sd.scope === ServiceScope.Transient;
    }

    public async registerPropertyInjector(propertyInjectorIdentifier: DependencyIdentifier): Promise<ContainerService> {

        // #ifdef DEBUG
        log.info(1, this, "Registering injector: ", propertyInjectorIdentifier);
        // #endif

        const propertyInjector: PropertyInjector = await this.resolveService<PropertyInjector>(propertyInjectorIdentifier);
        if (this._propertyInjectors.indexOf(propertyInjector) !== -1) throw new exceptions.PropertyInjectorRegisteredAlready();
        this._propertyInjectors.push(propertyInjector);
        return this;
    }

    public async registerServiceManager(serviceType: "*" | DependencyIdentifier, serviceManagerIdentifier: DependencyIdentifier): Promise<ContainerService> {

        // #ifdef DEBUG
        log.info(1, this, "Registering service manager. Service type: ", serviceType, ", manager identifier: ", serviceManagerIdentifier);
        // #endif

        if (serviceType === "*") {
            if (this._defaultServiceManager) throw new exceptions.DefaultServiceManagerRegisteredAlready();
            this._defaultServiceManager = await this.resolveService(serviceManagerIdentifier);
            for (let i = 0; i < this._services.length; i++) {
                const sm: ServiceManager = this._getServiceManagerByType(this._services[i].type);
                if (sm === this._defaultServiceManager) this._notifyServiceRegistered(this._services[i]);
            }
            return;
        }

        const sm: ServiceManager = this._getServiceManagerByType(serviceType);
        if (sm && sm !== this._defaultServiceManager) throw new exceptions.ServiceMangerForGivenServiceTypeRegisteredAlready();

        const serviceManager: ServiceManager = await this.resolveService(serviceManagerIdentifier);
        this._serviceManagers.push({ serviceManagerIdentifier, serviceType, serviceManager });

        return this;
    }

    public async registerSingletonInstance(key: any, instance: Service): Promise<ContainerService> {

        // #ifdef DEBUG
        log.info(1, this, "Registering singleton instance, key: ", key, ", instance: <", (typeof instance), ">", instance);
        // #endif

        let sd: ServiceDescriptor = this._getServiceDescriptorByKey(key);
        if (!sd) {
            if (typeof instance === "function") {
                sd = this._addService(ServiceScope.Singleton, key, <ServiceCtor>instance);
            } else {
                sd = this._addService(ServiceScope.Singleton, key, <ServiceCtor>instance.constructor);
            }
        }
        if (sd.instance) throw new exceptions.SingletonInstanceCreatedOrRegisteredAlready();

        sd.instance = instance;

        if (typeof(instance) === "object") {
            ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsDic>(instance, { dic: this });
            ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsDic>(instance, { id: this._getUniqueId() });
            await this._notifyServiceInstantied(sd, sd.instance);
        }

        return this;
    }

    public getServiceDescriptorsByType(serviceType: DependencyIdentifier): ServiceDescriptor[] {

        let services: ServiceDescriptor[];

        if (this._parentContainer) {
            services = this._parentContainer.getServiceDescriptorsByType(serviceType);
        } else {
            services = [];
        }

        for (let i = 0; i < this._services.length; i++) {
            if (this._services[i].type === serviceType) services.push(this._services[i]);
        }

        return services;
    }


    public async resolveService<T>(key: any, dependent?: Service): Promise<T> {

        // #ifdef DEBUG
        log.info(1, this, "Resolving service by key: ", key);
        // #endif

        const sd: ServiceDescriptor = this._getServiceDescriptorByKey(key);
        if (!sd && !this._parentContainer) throw new exceptions.UnableToResolveServiceWithGivenKey(key.toString());
        if (!sd && this._parentContainer) return this._parentContainer.resolveService<T>(key, dependent);
        const service = await this._resolveLocal<T>(sd, dependent);

        if (!this._disposingService) {
            this._disposingService = service;
            // #ifdef DEBUG
            log.info(1, this, "Container will be disposed with ", service);
            // #endif
        }

        // #ifdef DEBUG
        log.info(1, this, "Resolved service: ", service, " by ", key);
        // #endif
        return service;
    }

    public disposeServiceInstance(instance: Service): void {

        // if the service is the disposing one, remove the DiC
        if (this._disposingService === instance && !this._disposing) {
            // #ifdef DEBUG
            log.info(1, this, "Dosposing of service '", instance, "' will dispose the DiC too.");
            // #endif
            this.dispose();
            return;
        }

        // #ifdef DEBUG
        log.info(1, this, "Disposing service instance: ", instance);
        // #endif

        // if the transient service is disposing the sd ill be undefined
        const sd: ServiceDescriptor = this._getServiceDescriptorByInstance(instance, false);

        // can be disposed only if ServiceScope is Singleton or if the current DiC is disposing
        if (!sd || sd.scope === ServiceScope.Transient || this._disposing) {

            let smFound = false;

            // dispose immediately using stored service manager
            const sm: ServiceManager = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(instance, { serviceManager: undefined });
            if (sm) {
                smFound = true;
                sm.serviceDisposing(instance);
            }

            // dispose by appropriate service manager related to given service type
            if (!smFound) {
                for (const sm of this._serviceManagers) {
                    if (sm.serviceType === ServiceDecoration.getCtorDecoration<ServiceCtorDecorationsDic>(sd.ctor, { serviceType: undefined })) {
                        smFound = true;
                        sm.serviceManager.serviceDisposing(instance);
                    }
                }
            }

            // dispose by default service manager
            if (!smFound) {
                if (this._defaultServiceManager) {
                    this._defaultServiceManager.serviceDisposing(instance);
                } else {
                    if (instance.dispose) instance.dispose();
                }
            }

        } else {
            // #ifdef DEBUG
            log.info(1, this, "Singleton instance '", instance, "' can't be released until it is disposing service of the given DiC or DiC is disposed already");
            // #endif
        }

    }

    public createChildDic<T>(): ContainerService {

        // #ifdef DEBUG
        log.info(1, this, "Creating child container of ", this);
        // #endif

        const dic = new AjsContainerService(this);
        ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsDic>(dic, { id: this._getUniqueId() });
        dic._defaultServiceManager = this._defaultServiceManager;
        dic.registerSingletonInstance(IIContainerService, <Service>dic);
        for (const smi of this._serviceManagers) dic._serviceManagers.push(smi);
        for (const pi of this._propertyInjectors) dic._propertyInjectors.push(pi);
        return dic;
    }

    public dispose(): void {

        // #ifdef DEBUG
        log.info(1, this, "Disposing DiC: ", this);
        // #endif

        this._disposing = true;

        // release all local services (local managers and injectors should be disposed too)
        for (let i = this._services.length - 1; i >= 0; i--) {
            if (this._services[i].instance && this._services[i].instance !== this) this.disposeServiceInstance(this._services[i].instance);
        }

        /*
        // release only local service managers
        // parent containers copies managers from their prents so not necessary to walk the whole tree
        for (const sm of this._serviceManagers) {
            if ((<AjsContainerService>this._parentContainer)._serviceManagers.indexOf(sm) !== -1) continue;
            if (sm.serviceManager && sm.serviceManager.dispose) sm.serviceManager.dispose();
        }

        // release only local property injectors
        // parent containers copies injectors from their prents so not necessary to walk the whole tree
        for (const pi of this._propertyInjectors) {
            if ((<AjsContainerService>this._parentContainer)._propertyInjectors.indexOf(pi) !== -1) continue;
            if (pi.dispose) pi.dispose();
        }

        if (this._defaultServiceManager.dispose) {
            this._defaultServiceManager.dispose();
        }
        */

        this._defaultServiceManager = undefined;
        this._disposingService = undefined;
        this._parentContainer = undefined;
        this._propertyInjectors = [];
        this._serviceManagers = [];
        this._services = [];
    }

    private _getUniqueId(): number {
        if (this._parentContainer) return (<AjsContainerService>this._parentContainer)._getUniqueId();
        this._uniqueId++;
        return this._uniqueId - 1;
    }

    private _addService(scope: ServiceScope, key: any, ctor: ServiceCtor): ServiceDescriptor {

        let sd: ServiceDescriptor = this._getServiceDescriptorByKey(key);
        if (sd) throw new exceptions.ServiceWithGivenKeyAddedAlready();

        sd = {
            key,
            ctor,
            scope,
            deps: ServiceDecoration.getCtorDecoration<ServiceCtorDecorationsDic>(ctor, { inject: [] }),
            type: ServiceDecoration.getCtorDecoration<ServiceCtorDecorationsDic>(ctor, { serviceType: IIService }),
            mapDep: [],
            mapDeps: undefined,
            instance: undefined
        };

        this._services.push(sd);
        this._notifyServiceRegistered(sd);

        return sd;

    }

    private _getServiceDescriptorByKey(key: any, localOnly = true): ServiceDescriptor {

        for (const sd of this._services) {
            if (sd.key === key) {
                return sd;
            }
        }

        if (this._parentContainer && !localOnly) {
            return (<AjsContainerService>this._parentContainer)._getServiceDescriptorByKey(key);
        }

    }

    private _getServiceDescriptorByInstance(instance: Service, localOnly = true): ServiceDescriptor {

        for (const sd of this._services) {
            if (sd.instance === instance) {
                return sd;
            }
        }

        if (this._parentContainer && !localOnly) {
            return (<AjsContainerService>this._parentContainer)._getServiceDescriptorByInstance(instance);
        }

    }

    private _getServiceManagerByType(serviceType: DependencyIdentifier): ServiceManager {

        for (let i = 0; i < this._serviceManagers.length; i++) {
            if (this._serviceManagers[i].serviceType === serviceType) return this._serviceManagers[i].serviceManager;
        }

        return this._defaultServiceManager;

    }

    private _notifyServiceRegistered(sd: ServiceDescriptor): void {
        const sm: ServiceManager = this._getServiceManagerByType(sd.type);
        if (sm && sm.serviceRegistered) sm.serviceRegistered(sd);
    }

    private _notifyServiceInstantiating(sd: ServiceDescriptor): void {
        const sm: ServiceManager = this._getServiceManagerByType(sd.type);
        if (sm && sm.serviceInstantiating) sm.serviceInstantiating(sd);
    }

    private async _notifyServiceInstantied(sd: ServiceDescriptor, instance?: Service): Promise<void> {

        const sm: ServiceManager = this._getServiceManagerByType(sd.type);
        if (sm && sm.serviceInstantied) await sm.serviceInstantied(sd, instance);

        for (let i = 0; i < this._propertyInjectors.length; i++) {
            await this._propertyInjectors[i].injectProperties(instance);
        }

        if (sm && sm.servicePropertiesInjected) sm.servicePropertiesInjected(sd, instance);

    }

    private async _resolveLocal<T>(sd: ServiceDescriptor, dependent?: Service): Promise<T> {

        if (sd.scope === ServiceScope.Singleton && sd.instance) return <T>sd.instance;

        this._notifyServiceInstantiating(sd);

        if (sd.scope === ServiceScope.Singleton && sd.deps.length === 0) {
            sd.instance = new sd.ctor();
            ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsDic>(sd.instance, {
                id: this._getUniqueId(),
                dic: this
            });
            await this._notifyServiceInstantied(sd, sd.instance);
            return <T>sd.instance;
        }

        if (sd.scope === ServiceScope.Transient && sd.deps.length === 0) {
            const instance: T = <T>new sd.ctor();
            ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsDic>(instance, {
                id: this._getUniqueId(),
                dic: this
            });
            await this._notifyServiceInstantied(sd, instance);
            return instance;
        }

        const deps: any[] = await this._resolveDeps(sd, dependent);

        if (sd.scope === ServiceScope.Singleton) {
            sd.instance = new (sd.ctor.bind.apply(sd.ctor, [ null ].concat(deps)));
            ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsDic>(sd.instance, {
                id: this._getUniqueId(),
                dic: this
            });
            await this._notifyServiceInstantied(sd, sd.instance);
            return <T>sd.instance;
        }

        if (sd.scope === ServiceScope.Transient) {
            const instance: Service = new (sd.ctor.bind.apply(sd.ctor, [ null ].concat(deps)));
            ServiceDecoration.setObjectDecorations<ServiceObjectDecorationsDic>(instance, {
                id: this._getUniqueId(),
                dic: this
            });
            await this._notifyServiceInstantied(sd, instance);
            return <T>instance;
        }

    }

    private async _resolveDeps(sd: ServiceDescriptor, dependent?: Service): Promise<any[]> {

        const resolvedDeps: any[] = [];
        const mappedDeps: any[] = this._mapDeps(sd);

        for (let i = 0; i < mappedDeps.length; i++) {
            if (mappedDeps[i] === IIDependentService && dependent) {
                resolvedDeps.push(dependent);
            } else {
                resolvedDeps.push(await this.resolveService(mappedDeps[i], dependent));
            }
        }

        return resolvedDeps;

    }

    private _mapDeps(sd: ServiceDescriptor): any[] {
        if (sd.mapDeps) return sd.mapDeps;

        const map: any[] = [];

        for (let i = 0; i < sd.deps.length; i++) {
            let remapped = false;
            for (let j = 0; j < sd.mapDep.length; j++) {
                if (sd.deps[i] === sd.mapDep[j].injected) {
                    map.push(sd.mapDep[j].registered);
                    remapped = true;
                    break;
                }
            }
            if (!remapped) map.push(sd.deps[i]);
        }

        return map;
    }

}
