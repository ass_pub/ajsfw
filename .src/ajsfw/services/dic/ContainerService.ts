/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Service, ServiceCtor } from "ajsfw/core/services/Service";
import { ServiceManager } from "ajsfw/core/services/ServiceManager";
import { ServiceDescriptor } from "ajsfw/services/dic/ServiceDescriptor";
import { DependencyIdentifier } from "./DependencyIdentifier";
import { MapDep } from "./MapDep";
import { MapDeps } from "./MapDeps";

export const IIContainerService: DependencyIdentifier = [ "IIContainerService" ];

export interface ContainerService extends Service {

    readonly parentContainer: ContainerService;
    readonly defaultServiceManager: ServiceManager;

    addSingleton(keyOrCtor: any | ServiceCtor, ctor?: ServiceCtor): MapDep & MapDeps;
    addTransient(keyOrCtor: any | ServiceCtor, ctor?: ServiceCtor): MapDep & MapDeps;

    isSingletonRegistered(key: any | ServiceCtor): boolean;
    isTransientRegistered(key: any | ServiceCtor): boolean;

    registerPropertyInjector(propertyInjectorIdentifier: DependencyIdentifier): Promise<ContainerService>;
    registerServiceManager(serviceType: "*"|DependencyIdentifier|DependencyIdentifier[], serviceManagerIdentifier: DependencyIdentifier): Promise<ContainerService>;

    registerSingletonInstance(keyOrInstance: any|ServiceCtor, instance?: Service | any): Promise<ContainerService>;

    getServiceDescriptorsByType(serviceType: DependencyIdentifier): ServiceDescriptor[];

    resolveService<T>(key: any, dependant?: Service): Promise<T>;
    disposeServiceInstance(service: Service): void;

    createChildDic<T>(): ContainerService;
    dispose(): void;

}
