/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Service, ServiceCtor } from "ajsfw/core/services/Service";
import { ServiceManager } from "ajsfw/core/services/ServiceManager";
import { ContainerService } from "./ContainerService";
import { DependencyIdentifier } from "./DependencyIdentifier";
import { MapDep } from "./MapDep";
import { MapDeps } from "./MapDeps";
import { ServiceDescriptor } from "./ServiceDescriptor";

export class AjsServiceConfig implements MapDep, MapDeps {

    private _dic: ContainerService;
    private _serviceDescriptor: ServiceDescriptor;

    constructor(dic: ContainerService, serviceDescriptor: ServiceDescriptor) {
        this._dic = dic;
        this._serviceDescriptor = serviceDescriptor;
    }

    public mapDep(injected: any, registered: any): MapDep {
        this._serviceDescriptor.mapDep.push({ injected, registered });
        return this;
    }

    public mapDeps(registered: any[]): ContainerService {
        this._serviceDescriptor.mapDeps = registered;
        return this._dic;
    }

    /**
     * Proxy container properties & methods
     */

    public get parentContainer(): ContainerService { return this._dic.parentContainer; }
    public get defaultServiceManager(): ServiceManager { return this._dic.defaultServiceManager; }

    public addSingleton(keyOrCtor: any|ServiceCtor, ctor?: ServiceCtor): MapDep & MapDeps {
        return this._dic.addSingleton(keyOrCtor, ctor);
    }

    public addTransient(keyOrCtor: any|ServiceCtor, ctor?: ServiceCtor): MapDep & MapDeps {
        return this._dic.addTransient(keyOrCtor, ctor);
    }

    public isSingletonRegistered(key: any | ServiceCtor): boolean {
        return this._dic.isSingletonRegistered(key);
    }

    public isTransientRegistered(key: any | ServiceCtor): boolean {
        return this._dic.isTransientRegistered(key);
    }

    public registerPropertyInjector(propertyInjectorIdentifier: DependencyIdentifier): Promise<ContainerService> {
        return this._dic.registerPropertyInjector(propertyInjectorIdentifier);
    }

    public registerServiceManager(serviceType: "*"|DependencyIdentifier|DependencyIdentifier[], serviceManagerIdentifier: DependencyIdentifier): Promise<ContainerService> {
        return this._dic.registerServiceManager(serviceType, serviceManagerIdentifier);
    }

    public registerSingletonInstance(keyOrCtor: any|ServiceCtor, instance?: Service): Promise<ContainerService> {
        return this._dic.registerSingletonInstance(keyOrCtor, instance);
    }

    public getServiceDescriptorsByType(serviceType: DependencyIdentifier): ServiceDescriptor[] {
        return this._dic.getServiceDescriptorsByType(serviceType);
    }

    public async resolveService<T>(key: any): Promise<T> {
        return this._dic.resolveService<T>(key);
    }

    public disposeServiceInstance(service: Service): void {
        this._dic.disposeServiceInstance(service);
    }

    public createChildDic<T>(): ContainerService {
        return this._dic.createChildDic();
    }

    public dispose(): void {
        this._dic.dispose();
    }

}