/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { serviceType } from "ajsfw/services/dic/serviceType.decorator";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { IIStartupService, StartupService } from "../StartupService";
import * as exceptions from "./ajsOfflineStartupExceptions";
import { CBIStartup } from "./DependencyIdentifiers";

@inject(CBIStartup)
@serviceType(IIStartupService)
export class AjsOfflineStartup implements StartupService {

    private _startupCallback: EventListener;

    private _checkingListener: EventListener;
    private _downloadingListener: EventListener;
    private _progressListener: EventListener;
    private _cachedListener: EventListener;
    private _noUpdateListener: EventListener;
    private _updateReadyListener: EventListener;
    private _errorListener: EventListener;

    constructor (startupCallback: EventListener) {
        // #ifdef #DEBUG
        log.constructor(1, this, "Using offline startup. Startup callback: ", startupCallback);
        log.warning(this, "Not fully implemented yet");
        // #endif
        this._startupCallback = startupCallback;
    }

    public init() {

        if (!window.applicationCache) throw new exceptions.ApplicationCacheNotSupported();

        this._checkingListener = (event: Event) => { this._checking(event); };
        this._downloadingListener = (event: Event) => { this._downloading(event); };
        this._progressListener = (event: Event) => { this._progress(event); };
        this._cachedListener = (event: Event) => { this._cached(event); };
        this._noUpdateListener = (event: Event) => { this._noUpdate(event); };
        this._updateReadyListener = (event: Event) => { this._updateReady(event); };
        this._errorListener = (event: Event) => { this._error(event); };

        window.applicationCache.addEventListener("checking", this._checkingListener);
        window.applicationCache.addEventListener("downloading", this._downloadingListener);
        window.applicationCache.addEventListener("progress", this._progressListener);
        window.applicationCache.addEventListener("cached", this._cachedListener);
        window.applicationCache.addEventListener("noupdate", this._noUpdateListener);
        window.applicationCache.addEventListener("updateready", this._updateReadyListener);
        window.applicationCache.addEventListener("error", this._errorListener);
    }

    public dispose() {
        window.applicationCache.removeEventListener("checking", this._checkingListener);
        window.applicationCache.removeEventListener("downloading", this._downloadingListener);
        window.applicationCache.removeEventListener("progress", this._progressListener);
        window.applicationCache.removeEventListener("cached", this._cachedListener);
        window.applicationCache.removeEventListener("noupdate", this._noUpdateListener);
        window.applicationCache.removeEventListener("updateready", this._updateReadyListener);
        window.applicationCache.removeEventListener("error", this._errorListener);
    }

    protected _checking(event: Event): void {
        console.log("Offline support: checking", event);
    }

    protected _downloading(event: Event): void {
        console.log("Offline support: downloading", event);
    }

    protected _progress(event: Event): void {
        console.log("Offline support: progress", event);
    }

    protected _cached(event: Event): void {
        console.log("Offline support: cached", event);
    }

    protected _noUpdate(event: Event): void {
        console.log("Offline support: nooupdate", event);
    }

    protected _updateReady(event: Event): void {
        console.log("Offline support: updateready", event);
    }

    protected _error(event: Event): void {
        console.log("Offline support: error", event);
    }

}