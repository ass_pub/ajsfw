/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { serviceType } from "ajsfw/services/dic/serviceType.decorator";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { IIStartupService, StartupService } from "../StartupService";
import { CBIStartup } from "./DependencyIdentifiers";

@inject(CBIStartup)
@serviceType(IIStartupService)
export class AjsOnlineStartup implements StartupService {

    private _startupCallback: EventListener;

    constructor (startupCallback: EventListener) {
        // #ifdef #DEBUG
        log.constructor(1, this, "Using online startup. Startup callback: ", startupCallback);
        // #endif

        this._startupCallback = startupCallback;
    }

    public init() {
        window.addEventListener("load", (e: Event) => {
            // #ifdef #DEBUG
            log.info(1, this, "window.onload event handled. Calling configured callback.");
            // #endif

            this._startupCallback(e);
        });
    }

    public dispose() {
        window.removeEventListener("load", this._startupCallback);
    }

}
