/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { ServiceCtor } from "ajsfw/core/services/Service";
import { AjsNotifier } from "ajsfw/lib/events/AjsNotifier";
import { IIAppStartupService } from "./AppStartupService";
import { AppStartupInfo } from "./AppStartupInfo";
import { AjsfwStartupConfig } from "ajsfw/core/main/AjsfwStartupConfig";
import { ServiceDecoration } from "../../core/services/ServiceDecoration";
import { ServiceCtorDecorationsDic } from "ajsfw/services/dic/ServiceCtorDecorationsDic";

export let startupClassDecorated: AjsNotifier<AppStartupInfo> = new AjsNotifier();

export function startup(configuration?: AjsfwStartupConfig): ClassDecorator {

    return function (target: any): void {

        // #ifdef DEBUG
        log.decorator(1, "startup", "Target: ", target, ", Configuration:");
        // #endif

        // default ajfw configuration
        const config = configuration || {
            rootPath: ""
        };

        // #ifdef DEBUG
        log.dump(1, "startup", configuration);
        // #endif

        ServiceDecoration.setCtorDecorations<ServiceCtorDecorationsDic>(target, { serviceType: IIAppStartupService });

        const startupInfo: AppStartupInfo = {
            ctor: target as any,
            config
        };

        startupClassDecorated.notify(null, startupInfo);

    };

}

export function dispose() {
    startupClassDecorated = undefined;
}
