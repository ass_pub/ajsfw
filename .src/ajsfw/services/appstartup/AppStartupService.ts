/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Service, ServiceCtor } from "ajsfw/core/services/Service";
import { ServiceBuilder, ServiceBuilderCtor } from "ajsfw/core/services/ServiceBuilder";
import { Application } from "ajsfw/services/application/Application";
import { ContainerService } from "ajsfw/services/dic/ContainerService";
import { DependencyIdentifier } from "ajsfw/services/dic/DependencyIdentifier";

export const IIAppStartupService: DependencyIdentifier = [ "IIAppStartupService" ];

export interface AppStartupService extends Service {

    /**
     * Returns the error event handler to be used as default one
     */
    useCustomGlobalErrorHandler?(): OnErrorEventHandler;

    /**
     * Constructs and returns the root di container to be used further in the application
     * If the method is not implemented by the application the default AjsContainer will created by the ajsfw/main/main.ts
     */
    useCustomRootDiContainer?(...dependencies: any[]): ContainerService;

    /**
     * Returns custom implementation of default service manager (service manager to manage lifecycle of all services with no assigned specific manager)
     * If the method is not implemented by the application the AjsServiceManager will be used
     */
    useCustomDefaultServiceManager?(...dependencies: any[]): ServiceCtor;

    /**
     * Returns custom implementation of the startup service builder responsible for creation of startup services used to control the application startup.
     * If the method is not implemented by the application class the default AjsStartupServiceBuilder will be used
     */
    useCustomStartupServiceBuilder? (...dependencies: any[]): ServiceBuilderCtor;

    /**
     * Using the service builder configures startup services used to control the application startup
     * If the metod is not implemented by the application the used startup service builder defaults will be used
     */
    configureStartupServices?(serviceBuilder: ServiceBuilder): void;

    /**
     * Returns custom implementation of the service builder responsible for creation of the AsyncInitService manager and services used to
     * perform asynchronous application initialization tasks (i.e. loading application configuration from the server) prior the application
     * services are configured.
     * If the method is not implemented by the application the defult AjsAsyncServiceBuilder is used in next step
     */
    useCustomAsyncStartupServiceBuilder?(...dependencies: any[]): ServiceBuilderCtor;

    /**
     * Can be used by the application to configure async services used to initialize application asynchronously prior the application services
     * are built.
     * If application does not implement this method no async services are created and executed before the application services are configured
     */
    configureAsyncStartupServices?(asyncInitServiceBuilder: ServiceBuilder): void;

    /**
     * Returns custom implementation of the service builder responsible for configuration of application services
     */
    useCustomServiceBuilder?(): ServiceBuilderCtor;

    /**
     * Can be used by the application to configure application services
     * If application does not implement the method the default AjsApplicationManager will be used
     * As it is usually required to inject dependencies to the method and dependency resolving is asynchronous the method must be asynchronous too
     */
    configureServices?(...dependencies: any[]): Promise<void>;

    /**
     * Start application
     */
    runApplication?(application: Application): void;

    /**
     * Start whatever is defined inside of the startup.run (this method will not be called if application is defined or ruApplication used)
     */
    run?(...dependencies: any[]): void;

}