/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { DependencyIdentifier } from "ajsfw/services/dic/DependencyIdentifier";

/**
 * Identifier of the string url to be injected to the application configuration loader
 * The built-in configuration of the application should contain the URL of the config file to be loaded
 * The value of the application configuration url stored in the config should be copied to the DIC under this key
 * in order to be possible to use the AjsAppConfigLoader to load the configuration file
 */
export const DIAppConfigUri: DependencyIdentifier = [ "DIAppConfigUri" ];

/**
 * Identifier of the application configuration
 * When the application configuration is loaded the configuration should be stored to to the DIC under this key
 */
export const DIAppConfig: DependencyIdentifier = [ "DIAppConfig" ];

/**
 * Identifier of the application configuration loader
 * As application configuration loader is the AsyncStartupServide the interface is not specifically declared
 */
export const IIAppConfig: DependencyIdentifier = [ "IIAppConfig" ];

/**
 * Interface to be used to collect the configuration data
 */
export interface AppConfig {
    data: any;
}