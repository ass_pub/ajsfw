/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { serviceType } from "ajsfw/services/dic/serviceType.decorator";
import { SIFileReader } from "ajsfw/services/data/providers/FileDataProvider/DependencyIdentifiers";
import { FileProvider } from "ajsfw/services/data/providers/FileDataProvider/FileProvider";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { AsyncStartupService, IIAsyncStartupService } from "../AsyncStartupService";
import { IIContainerService, ContainerService } from "ajsfw/services/dic/ContainerService";
import { DIAppConfigUri, DIAppConfig, AppConfig } from "./AppConfig";

@inject(IIContainerService, SIFileReader, DIAppConfigUri)
@serviceType(IIAsyncStartupService)
export class AjsAppConfig implements AsyncStartupService, AppConfig {

    private _container: ContainerService;
    private _fileReader: FileProvider;
    private _uri: string;

    private _data: any;
    public get data(): any { return this._data; }

    constructor(container: ContainerService, fileReader: FileProvider, uri: string) {
        this._container = container;
        this._fileReader = fileReader;
        this._uri = uri;
    }

    public async asyncRun(): Promise<void> {
        const cfg: string | Int8Array = await this._fileReader.asyncRead(this._uri);
        if (typeof cfg === "string") {
            this._data = JSON.parse(<string>cfg);
            this._container.registerSingletonInstance(DIAppConfig, this._data);
        } else {
            this._data = {};
        }
    }

}