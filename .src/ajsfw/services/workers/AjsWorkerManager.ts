/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { Service } from "ajsfw/core/services/Service";

import { CallPromiseMap } from "./CallPromiseMap";
import { ServiceInstanceInfo } from "./ServiceInstanceInfo";
import { Call } from "./wcp/Call";
import { Exception } from "./wcp/Exception";
import { Message } from "./wcp/Message";
import { Notify } from "./wcp/Notify";
import { Return } from "./wcp/Return";
import { WorkerInfoMap } from "./WorkerInfoMap";
import { className } from "ajsfw/lib/utils/className";

import * as exceptions from "./exceptions";

// this is necessary to get the AjsWorker module included to the package
import "./worker/AjsWorkerBootstrap";
import "./worker/AjsWorker";
import { ServiceDecoration } from "ajsfw/core/services/ServiceDecoration";
import { ServiceObjectDecorationsDic } from "ajsfw/services/dic/ServiceObjectDecorationsDic";

/**
 * To avoid need of importing the wcp/enums this is placed directly here
 */
enum MessageType {
    Call = 0,
    Return = 1,
    Notify = 2,
    Exception = 3
}

/**
 *
 */
export class AjsWorkerManager implements Service {

    /** Holds information about existing workers */
    private _workerInfoMap: WorkerInfoMap;
    /** Holds information about known service classes */
    private _serviceClassInfos: ServiceClassInfo[];
    /** Holds information about services instanced in the main worker thread */
    private _serviceInstances: ServiceInstanceInfo[];
    /** Last call Id made to the given worker */
    private _callId: number;
    /** Call promises made to the given worker */
    private _callPromiseMap: CallPromiseMap;

    constructor() {

        // #ifdef DEBUG
        log.constructor(1, this);
        // #endif

        this._workerInfoMap = {};
        this._callId = 0;
        this._callPromiseMap = {};

        this._serviceClassInfos = [{
            moduleId: "ajsfw/workers/AjsWorkerManager",
            className: className(this),
            methods: this.__getClassMethods(AjsWorkerManager)
        }];

        const id = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(this, { id: undefined });

        this._serviceInstances = [{
            moduleId: "ajsfw/workers/AjsWorkerManager",
            className: className(this),
            instanceId: id,
            instance: this
        }];

    }

    public init(): void {
    }

    /**
     * Creates a worker and initializes it
     * @param id Id of the worker to be created
     */
    public async createWorker(workerId: string): Promise<void> {

        // #ifdef DEBUG
        log.enter(1, this, "Creating worker id #", workerId);
        // #endif

        // prevent usage of the "main" as id of the worker
        if (workerId === "main") {
            throw new exceptions.MainIsReservedWorkerIdException();
        }

        // just return if the worker with the given id exists already
        if (this._workerInfoMap.hasOwnProperty(workerId)) {
            return;
        }

        // get the worker code and prepare it for creating the worker
        const moduleCode = (<any>window)._ajs.modules["./node_modules/ajsfw/services/workers/worker/AjsWorkerBootstrap"];

        // convert worker code to blob
        let blob: Blob;
        try {
            blob = new Blob([ moduleCode ], { type: "application/javascript" });
        } catch (e) {
            const bbClass = (<any>window).BlobBuilder || (<any>window).MSBlobBuilder || (<any>window).WebKitBlobBuilder || (<any>window).MozBlobBuilder;
            const bb: MSBlobBuilder = new bbClass();
            bb.append(moduleCode);
            blob = bb.getBlob();
        }

        // create a worker code as url response
        const url = URL.createObjectURL(blob);

        // store the worker info and create a worker
        this._workerInfoMap[workerId] = {
            worker: new Worker(url),
            callId: 0,
            callPromiseMap: {},
            modules: [
                "./node_modules/ajsfw/services/workers/worker/AjsWorker"
            ],
            serviceInstances: [],
            ajsWorkerInstance: null
        };

        // setup the worker listener
        this._workerInfoMap[workerId].worker.onmessage = (e: MessageEvent) => {
            this.__processMessage(e.data);
        };

        const moduleId = "ajsfw/workers/AjsWorker";
        const className = "AjsWorker";
        const instanceId = "ajsWorker";

        // intialize the AjsWorker with the given id and grab its methods
        const methods: string[] = await this.__call(this, workerId + "." + instanceId, "init", workerId);

        // register module/class methods
        this.__registerServiceClassMethods(moduleId, className, methods);

        // create ajsWorker proxy (service instance)
        const instance: any = this.__createServiceProxy(workerId, instanceId, methods);

        // register worker service to the instance table
        this.__registerWorkerService(workerId, moduleId, className, instanceId, instance);

        // store worker control instance to info
        this._workerInfoMap[workerId].ajsWorkerInstance = instance;

        // #ifdef DEBUG
        log.exit(1, this, workerId);
        // #endif
    }

    /**
     * Store info about the module class methods to the class info table (in order to be possible to create a proxy)
     * @param serviceClassName Name of the class which methods should be registered
     * @param serviceClassMethods List of methods
     */
    private __registerServiceClassMethods(moduleId: string, className: string, classMethods: string[]): void {

        if (this.__getClassInfo(moduleId, className) === null) {
            this._serviceClassInfos.push({
                moduleId,
                className,
                methods: classMethods
            });
        }

    }

    /**
     * Registers a service instance instanced in the particular worker
     * @param workerId Id of the worker where the class is instanced
     * @param moduleId Module where the class id declared
     * @param className Name of the class of from which the instance is created
     * @param instanceId Id of the class instance (unique per worker)
     * @param instance Instance of the class or instance proxy
     */
    private __registerWorkerService(workerId: string, moduleId: string, className: string, instanceId: number, instance: any): void {

        const isii: ServiceInstanceInfo = {
            moduleId,
            className,
            instanceId,
            instance
        };

        if (workerId === "main") {
            this._serviceInstances.push(isii);
        } else {
            this._workerInfoMap[workerId].serviceInstances.push(isii);
        }
    }

    /**
     * Processes a message recieved from the worker managed object
     * @param wcpMessage Worker Control Protocol message to be processed
     */
    private __processMessage(wcpMessage: Message): void {

        // check if the destination is in the object table
        const dest: string[] = wcpMessage.destination.split(".");

        // if destination is not main "worker", route message to the target worker (just resend it)
        if (dest[0] !== "main") {
            this.__postMessage(wcpMessage);
            return;
        }

        // if destination is main worker, check if the object required exists
        const destObj = this.__findDestinationObject(dest[1]);

        if ((wcpMessage.msgType === MessageType.Call || wcpMessage.msgType === MessageType.Notify) && destObj === null) {
            this.__exception(wcpMessage, new Error("Invalid destination object '" + wcpMessage.destination + "'"));
        }

        // process the wcp message according to its type
        switch (wcpMessage.msgType) {
            // call of object method located in the worker
            case MessageType.Call:
                this.__processWcpCall(destObj, wcpMessage);
                break;
            // notification (call of object method located in the worker without waiting for the return value)
            case MessageType.Notify:
                this.__processWcpNotify(destObj, wcpMessage);
                break;
            // return from a call made using the wcp
            case MessageType.Return:
                this.__processWcpReturn(wcpMessage);
                break;
            // exception occured during remote call so process it
            case MessageType.Exception:
                this.__processWcpException(wcpMessage);
                break;
            default:
                break;
        }

    }

    /**
     * Executes worker managet object method with given params and returns a value using the WCP
     * @param target Target object which method has to be called
     * @param message WCP message with the call data
     */
    private async __processWcpCall(target: any, message: Message): Promise<void> {

        const callData: Call = <Call>message.data;

        if (typeof (target[callData.method]) !== "function") {
            this.__exception(message, new Error("Invalid method call: '" + message.destination + "." + callData.method + "'"));
        }

        try {
            const rv: any = await target[callData.method].apply(target, callData.args);
            this.__return(message, rv);
        } catch (e) {
            this.__exception(message, e);
        }

    }

    /**
     * Calls a local method without returning the value over the WCP
     * @param target Target object which method has to be called
     * @param message WCP message with the call data
     */
    private async __processWcpNotify(target: any, message: Message): Promise<void> {

        const notifyData: Notify = <Notify>message.data;

        if (typeof (target[notifyData.method]) !== "function") {
            this.__exception(message, new Error("Invalid method call: '" + message.destination + "." + notifyData.method + "'"));
        }

        target[notifyData.method].apply(target, notifyData.args);

        try {
            const rv: any = await target[notifyData.method].apply(target, notifyData.args);
        } catch (e) {
            this.__exception(message, e);
        }
    }

    /**
     * Processess the return value from the remote worker managed object method call
     * @param message WCP message containing the retrun data
     */
    private async __processWcpReturn(message: Message): Promise<void> {

        const retData: Return = <Return>message.data;

        // resolve the call promise
        if (retData.callId !== undefined && this._callPromiseMap.hasOwnProperty(retData.callId.toString())) {
            this._callPromiseMap[retData.callId].resolve(retData.data);
            delete this._callPromiseMap[retData.callId];
            return;
        }
    }

    /**
     *
     * @param target
     * @param message
     */
    private async __processWcpException(message: Message): Promise<void> {

        const exceptionData: Exception = <Exception>message.data;

        // reject the call promise if the exception has be thrown during the method call
        if (exceptionData.callId !== undefined && this._callPromiseMap.hasOwnProperty(exceptionData.callId.toString())) {
            this._callPromiseMap[exceptionData.callId].reject(exceptionData.data);
            delete this._callPromiseMap[exceptionData.callId];
            return;
        }

        // report exception when the call ID does not exist in the call table
        if (exceptionData.callId !== undefined) {
            // #ifdef DEBUG
            log.error(this, "Invalid call ID: ", exceptionData.callId, message);
            // #endif
        }

        // otherwise just print our the exception to the console
        log.error(this, "Exception occured during the remote method call: ", exceptionData.data);
        return;

    }

    /**
     * Informs the caller the exception hac occured during a call or notification processing
     * @param srcMessage Message being processed
     * @param data Exception data (can be typeof Error, Exception or any other seriazable data object)
     */
    private __exception(srcMessage: Message, data: any): void {

        const dta: Exception = { errorType: typeof (data) };

        if (srcMessage.msgType === MessageType.Call) {
            dta.callId = (<Call>srcMessage.data).callId;
        }

        if (data instanceof Error) {
            dta.data = data.message;
        } else {
            dta.data = data;
        }

        const msg: Message = {
            source: srcMessage.destination,
            destination: srcMessage.source,
            msgType: MessageType.Exception,
            data: dta
        };

        this.__postMessage(msg);
    }

    /**
     * Finds id of source instance performing a call
     * @param obj Object performing a call
     */
    private __findSourceId(obj: any): number {
        for (const si of this._serviceInstances) {
            if (si.instance === obj) {
                return si.instanceId;
            }
        }
        return null;
    }

    /**
     * Sends a message with a call return value
     * @param srcCall Message being processed
     * @param data Data to be returned
     */
    private __return(srcCall: Message, data?: any): void {

        const dta: Return = {
            callId: (<Call>srcCall.data).callId
        };

        if (data !== undefined) {
            dta.data = data;
        }

        const msg: Message = {
            source: srcCall.destination,
            destination: srcCall.source,
            msgType: MessageType.Return,
            data: dta
        };

        this.__postMessage(msg);

    }

    /**
     * Calls a remote worker managed object method while a return value is expected
     * @param src Source object of the call
     * @param destination Destination ID of the object to be called
     * @param method Mehod of the object to be called
     * @param args Arguments to be passed to the method
     */
    private __call(src: any, destination: string, method: string, ...args: any[]): Promise<any> {

        const srcName: string = "main." + this.__findSourceId(src);

        // prepare call data
        const callData: Call = {
            callId: this._callId++,
            method,
            args
        };

        // prepare message to be sent
        const msg: Message = {
            source: srcName,
            destination,
            msgType: MessageType.Call,
            data: callData
        };

        // prepare the call promise and store it locally to the call promise map
        const callPromise = new Promise<any>(
            (resolve: (data: any) => void, reject: (reason: any) => void) => {
                this._callPromiseMap[callData.callId] = {
                    resolve,
                    reject
                };
            }
        );

        // post the message
        this.__postMessage(msg);

        // return the call promise
        return callPromise;

    }

    /**
     * Calls a remote worker managed object method while a return value is not expected
     * @param src Source object of the call
     * @param destination Destination ID of the object to be called
     * @param method Mehod of the object to be called
     * @param args Arguments to be passed to the method
     */
    private __notify(src: any, destination: string, method: string, ...args: any[]): void {

        // get id of the source object
        const srcId: number = this.__findSourceId(src);

        // prepare call data
        const callData: Call = {
            callId: this._callId++,
            method,
            args
        };

        // prepare message to be sent
        const msg: Message = {
            sourceWorker,
            source: srcId,
            destination,
            msgType: MessageType.Call,
            data: callData
        };

        // post the message
        this.__postMessage(msg);

    }

    /**
     * Sends a message to the worker which redirects it to the appropriate destination object
     * @param message Message to be sent
     */
    private __postMessage(message: Message) {

        // get worker ID to which the message has to be sent
        const dest: string[] = message.destination.split(".");

        // get the worker from the map
        const worker: Worker = this._workerInfoMap[dest[0]].worker;

        // post the message
        worker.postMessage(message);

    }

    /**
     * Returns an destination object of main worker specified by destObjId
     * @param destObjId ID of the target object
     */
    private __findDestinationObject(destObjId: number): any {

        for (const sii of this._serviceInstances) {
            if (sii.instanceId === destObjId) {
                return sii.instance;
            }

        }
        return null;

    }

    /**
     * Returns Service class info for the module and class name specified
     * @param moduleId Id of the module where the class is defined
     * @param className Name of the class about which the info has to be returned
     */
    private __getClassInfo(moduleId: string, className: string): ServiceClassInfo {

        for (const sci of this._serviceClassInfos) {
            if (sci.moduleId === moduleId && sci.className === className) {
                return sci;
            }
        }

        return null;

    }

    /**
     * Extracts method names from the class (and its prototype chain). The module in which the class is declared must be deployed locally (in main "worker")
     * @param ctor Constructor of the class which method names should be extracted
     */
    private __getClassMethods(ctor: new (...args: any[]) => any): string[] {

        if (!(ctor instanceof Function)) {
            throw new exceptions.PassedTypeIsNotFunctionException();
        }

        const methods: string[] = [];
        let current: any = ctor;

        while (current.prototype !== undefined) {

            for (const k in current.prototype) {

                if (!current.prototype.hasOwnProperty(k)) {
                    continue;
                }

                if (typeof (current.prototype[k]) === "function") {
                    methods.push(k);
                }

            }

            current = Object.getPrototypeOf(current);
        }

        return methods;
    }

    /**
     * Creates a service proxy object taking care of calling "remote" service over messaging
     * @param methods Methods to be added to the service proxy
     */
    private __createServiceProxy(targetWorkerId: string, targetInstanceId: string,  methods: string[]): any {

        const ajsWorkerManager: AjsWorkerManager = this;

        class ServiceProxy {

            private __destination: string;
            private __callMethod: (...args: any[]) => any;

            constructor(targetWorkerId: string, targetInstanceId: string, callMethod: (...args: any[]) => any, methods: string[]) {

                this.__destination = targetWorkerId + "." + targetInstanceId;
                this.__callMethod = callMethod;

                for (const m of methods) {

                    (<any>this)[m] = function (...args: any[]): Promise<any> {
                        return this.__callMethod.apply(ajsWorkerManager, [null, this.__destination, m].concat(args));
                    };

                }

            }

        }

        return new ServiceProxy(targetWorkerId, targetInstanceId, this.__call, methods);

    }

}