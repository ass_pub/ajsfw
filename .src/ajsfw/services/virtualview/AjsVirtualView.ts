/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Attr as VDomAttr } from "ajsfw/lib/vdom/Attr";
import { Document as VDomDocument } from "ajsfw/lib/vdom/Document";
import { Element as VDomElement } from "ajsfw/lib/vdom/Element";
import { Node as VDomNode } from "ajsfw/lib/vdom/Node";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { eventAttributes } from "ajsfw/services/dom/eventAttributes";
import { Template } from "ajsfw/services/templating/Template";
import { IITemplateManager, TemplateManager } from "ajsfw/services/templating/TemplateManager";
import { IIDomUpdater, DomUpdater } from "ajsfw/services/dom/DomUpdater";
import { AjsChildElementStateInfo } from "./AjsChildElementStateInfo";
import { AjsVDomEventData } from "./AjsVDomEventData";
import { VirtualView } from "./VirtualView";
import { ServiceCtor } from "ajsfw/core/services/Service";
import { ServiceDecoration } from "ajsfw/core/services/ServiceDecoration";
import { ServiceCtorDecorationsDic } from "ajsfw/services/dic/ServiceCtorDecorationsDic";

import * as exceptions from "./exceptions";
import { View } from "ajsfw/services/mvvm/view/View";
import { ServiceObjectDecorationsMvvm } from "ajsfw/services/mvvm/ServiceObjectDecorationsMvvm";
import { ServiceCtorDecorationsMvvm } from "ajsfw/services/mvvm/ServiceCtorDecorationsMvvm";
import { ServiceObjectDecorations } from "ajsfw/core/services/ServiceObjectDecorations";
import { ServiceObjectDecorationsDic } from "ajsfw/services/dic/ServiceObjectDecorationsDic";

interface CssClassUpdateInfo {
    name: string;
    delete: boolean;
    classes: string[];
    currentTemplateLevel: number;
}

interface ViewStateInfo {
    viewState: any;
    propertyName?: string;
    propertyIndex?: number;
    cssClassUpdateInfo: CssClassUpdateInfo[];
}

const emptyViewState = {};

@inject(IITemplateManager, IIDomUpdater)
export class AjsVirtualView implements VirtualView {

    protected _templateManager: TemplateManager;
    protected _domUpdater: DomUpdater;

    constructor(templateManager: TemplateManager, domUpdater: DomUpdater) {
        this._templateManager = templateManager;
        this._domUpdater = domUpdater;
    }

    public async fullRender(rootViewState: any): Promise<void> {
        const vdom: VDomDocument = new VDomDocument();
        await this._render(vdom.body, rootViewState, { viewState: rootViewState, cssClassUpdateInfo: [] });
        this._domUpdater.fullUpdate(vdom);
    }

    public async partialRender(viewState: any): Promise<void> {
        const vdom: VDomDocument = new VDomDocument();
        await this._render(vdom.body, viewState, { viewState, cssClassUpdateInfo: [] });
        this._domUpdater.partialUpdate(vdom);
    }

    protected _processDomEvent(eventName: string,  event: Event): boolean {

        const target: Node = <Node>event.target;
        const currentTarget: Node = <Node>event.currentTarget;
        const data: AjsVDomEventData = Object.create((<any>target)["customData"]);

        data.eventType = event.type;

        if (target instanceof HTMLTableCellElement) {
            data.column = (<any>target)["customData"].viewModelPropertyIndex;
            data.row = (<any>target.parentElement)["customData"].viewModelPropertyIndex;
        }

        if (target instanceof HTMLTableRowElement) {
            data.row = (<any>target)["customData"].viewModelPropertyIndex;
        }

        if ((<DragEvent>event).dataTransfer) data.dataTransfer = (<DragEvent>event).dataTransfer;

        if (target instanceof HTMLAnchorElement) data.href = target.href;

        if (target instanceof HTMLTextAreaElement) {
            if (target.value !== undefined) data.value = target.value;
        }

        if (target instanceof HTMLInputElement) {

            if (target.hasAttribute("type")) {
                switch (target.getAttribute("type")) {
                    case "checkbox":
                        data.checked = target.checked;
                        break;
                    case "radio":
                        data.checked = target.checked;
                        break;
                    case "file":
                        data.files = target.files;
                        break;
                    default:
                        if (target.value !== undefined) data.value = target.value;
                }
            } else {
                if (target.value !== undefined) data.value = target.value;
            }

        }

        if (target instanceof HTMLSelectElement) {

            if (target.hasAttribute("multiple")) {
                data.selectedIndexes = [];
                for (let i = 0; i < target.selectedOptions.length; i++) {
                    data.selectedIndexes.push(target.selectedOptions.item(i).index);
                }
            } else {
                data.value = target.value;
                data.selectedIndex = target.selectedIndex;
            }

        }

        data.cancelBubble = false;
        data.preventDefault = false;

        const view: View = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsMvvm>(data.viewModel, { view: undefined });
        if (view && (<any>view)[eventName]) {
            (<any>view)[eventName](data);
        } else if (data.viewModel && (<any>data.viewModel)[eventName]) {
            (<any>data.viewModel)[eventName](data);
        }

        if (data.cancelBubble) event.cancelBubble = true;
        if (data.preventDefault) event.preventDefault();

        return data.preventDefault || false;

    }

    protected _isService(obj: any): boolean {
        if (obj === undefined) return false;
        return ServiceDecoration.getCtorDecoration<ServiceCtorDecorationsDic>(obj.constructor, { serviceType: undefined }) !== undefined;
    }

    protected async _render(target: VDomElement, viewState: any, viewStateInfo: ViewStateInfo): Promise<void> {

        let templateName: string;
        let template: Template;

        const view: View = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsMvvm>(viewStateInfo.viewState, { view: undefined });

        if (view) {
            templateName = ServiceDecoration.getCtorDecoration<ServiceCtorDecorationsMvvm>(<ServiceCtor>view.constructor, { useTemplate: undefined });
        } else {
            templateName = ServiceDecoration.getCtorDecoration<ServiceCtorDecorationsMvvm>(<ServiceCtor>viewStateInfo.viewState.constructor, { useTemplate: undefined });
        }

        if (templateName) {
            template = await this._templateManager.getTemplate(templateName);
        } else {
            throw new exceptions.UnableToRenderServiceWithoutTemplate();
        }

        await this._renderTemplate(template.dom, target, viewState, viewStateInfo);

    }

    protected async _renderTemplate(source: VDomElement, target: VDomElement, viewState: any, viewStateInfo: ViewStateInfo): Promise<void> {

        const serviceId: number = ServiceDecoration.getObjectDecoration<ServiceObjectDecorationsDic>(viewState, { id: undefined });

        for (let i = 0; i < source.childNodes.length; i++) {
            await this._renderNode(source.childNodes.item(i), target, viewState, viewStateInfo, serviceId);
        }

    }

    private async _renderContainer(target: VDomElement, viewState: any, viewStateInfo: ViewStateInfo): Promise<void> {

        if (!this._isService(viewState)) return;

        if (viewState instanceof Array) {
            for (let i = 0; i < viewState.length; i++) {
                const vmi: ViewStateInfo = {
                    viewState,
                    propertyIndex: i,
                    cssClassUpdateInfo: []
                };
                await this._render(target, viewState[i], viewStateInfo);
            }
        } else {
            const vmi: ViewStateInfo = { viewState, cssClassUpdateInfo: [] };
            await this._render(target, viewState, vmi);
        }

    }

    private async _renderNode(source: VDomNode, target: VDomElement, viewState: any, viewStateInfo: ViewStateInfo, serviceId: number = undefined): Promise<void> {

        if (source.nodeType === VDomNode.ELEMENT_NODE) {
            await this._renderElementNode(<VDomElement>source, target, viewState, viewStateInfo, serviceId);
        }

        if (source.nodeType === VDomNode.TEXT_NODE) {
            const textContent = this._processTextContent(source.textContent, viewState);
            const node: VDomNode = target.ownerDocument.createTextNode(textContent);
            target.appendChild(node);
        }

    }

    private _getElementViewStateInfo(source: VDomElement, viewState: any, viewStateInfo: ViewStateInfo): AjsChildElementStateInfo {

        const chvsi: AjsChildElementStateInfo = <AjsChildElementStateInfo>{};

        if (source.hasAttribute("data-source")) {
            const key: string = source.getAttribute("data-source");
            if (key === "[]") {
                chvsi.viewState = viewState;
                chvsi.childViewState = viewStateInfo.viewState;
                chvsi.propertyName = viewStateInfo.propertyName;
            } else {
                chvsi.viewState = viewState[key] || emptyViewState;
                if (this._isService(chvsi.viewState)) {
                    chvsi.childViewState = chvsi.viewState;
                    chvsi.propertyName = undefined;
                } else {
                    chvsi.childViewState = viewStateInfo.viewState;
                    chvsi.propertyName = key;
                }
            }
        } else {
            chvsi.viewState = viewState;
            chvsi.childViewState = viewStateInfo.viewState;
            chvsi.propertyName = viewStateInfo.propertyName;
        }

        return chvsi;

    }

    /**
     * In case the template has to be rendered the tag referenced it is checked for the "class" attribute.
     * If the attribute is present the css class update information is parsed and stored for element rendering.
     * @param source Element referencing the template to be used
     * @param cssClassUpdateInfo Previo
     */
    private _updateCssClassInfo(source: VDomElement, cssClassUpdateInfo: CssClassUpdateInfo[]): CssClassUpdateInfo[] {
        if (!source.hasAttribute("class")) return cssClassUpdateInfo;

        // copy previous info to the new array
        const info: CssClassUpdateInfo[] = cssClassUpdateInfo.slice(0);

        // parse class removal and additions - class update format is:
        // refClassName[level]:class1,class2,class3
        // refClassName - name of the class inside of the template to be optionally deleted and to which other classes will be added
        // level        - maximum number of subordinate templates where the class removal / additions are applicable (default 99999)
        // class1..3    - class names to be added to the tag where the refClassName is found
        // !!! Level is not in use at this time so the removal / additions are applied to the whole tree of templated bellow the current one !!!

        // get all referenced classes including arguments and the data
        const classes = source.getAttribute("class").split(" ");

        // walthrough all classes to be processed and parse it
        for (const c of classes) {

            const classInfo = c.split(":");

            // get class name
            const start = classInfo[0].indexOf("!") === 0 ? 0 : 1;
            const count = classInfo[0].indexOf("[");
            const len = count === -1 ? classInfo[0].length - start : count;
            const className = classInfo[0].substr(start + Number(classInfo[0].indexOf("!") === 0), len);

            // get info if the ref class name should be deleted
            const classDelete = classInfo[0].indexOf("!") === 0;

            // get list of classes to be added
            const classesToAdd = classInfo[1].split(",");

            // get level to which the class updates are applicable
            const level: number = count !== -1 ? Number(classInfo[0].substr(count + 1, len - count - 1)) : 99999;

            // build the update info
            const ui: CssClassUpdateInfo = {
                name: className,
                delete: classDelete,
                classes: classesToAdd,
                currentTemplateLevel: level
            };

            info.push(ui);
        }


        return info;
    }

    private async _renderElementNode(source: VDomElement, target: VDomElement, viewState: any, viewStateInfo: ViewStateInfo, serviceId: number): Promise<void> {

        const tagName = source.nodeName.toLowerCase();
        const key: string = source.getAttribute("data-source");
        const evsi = this._getElementViewStateInfo(source, viewState, viewStateInfo);

        if (evsi.viewState instanceof Array) {
            for (let i = 0; i < evsi.viewState.length; i++) {

                const vmi: ViewStateInfo = {
                    viewState: evsi.childViewState,
                    propertyName: evsi.propertyName,
                    propertyIndex: i,
                    cssClassUpdateInfo: viewStateInfo.cssClassUpdateInfo
                };

                if (tagName === "container") {
                    await this._renderContainer(target, evsi.viewState[i], vmi);
                } else if (key && viewState.hasOwnProperty(key) && this._isService(viewState[key])) {
                    this._render(target, evsi.viewState[i], vmi);
                } else {
                    let template: Template;
                    try {
                        template = await this._templateManager.getTemplate(tagName);
                    } catch (e) { }

                    if (template) {
                        if (source.hasAttribute("class")) vmi.cssClassUpdateInfo = this._updateCssClassInfo(source, vmi.cssClassUpdateInfo);
                        await this._renderTemplate(template.dom, target, evsi.viewState[i], vmi);
                    } else {
                        await this._renderElement(source, target, evsi.viewState[i], vmi, serviceId);
                    }
                }

            }
        } else {
            const vmi: ViewStateInfo = {
                viewState: evsi.childViewState,
                propertyName: evsi.propertyName,
                cssClassUpdateInfo: viewStateInfo.cssClassUpdateInfo
            };

            if (tagName === "container") {
                await this._renderContainer(target, evsi.viewState, vmi);
            } else if (evsi.viewState !== emptyViewState) {

                let template: Template;
                try {
                    template = await this._templateManager.getTemplate(tagName);
                } catch (e) { }

                if (template) {
                    if (source.hasAttribute("class")) vmi.cssClassUpdateInfo = this._updateCssClassInfo(source, vmi.cssClassUpdateInfo);
                    await this._renderTemplate(template.dom, target, evsi.viewState, vmi);
                } else  {
                    await this._renderElement(source, target, evsi.viewState, vmi, serviceId);
                }

            } else {

                let template: Template;
                try {
                    template = await this._templateManager.getTemplate(tagName);
                } catch (e) { }

                if (template && !key) {
                    if (source.hasAttribute("class")) vmi.cssClassUpdateInfo = this._updateCssClassInfo(source, vmi.cssClassUpdateInfo);
                    await this._renderTemplate(template.dom, target, evsi.viewState, vmi);
                } else if (!template) {
                    await this._renderElement(source, target, evsi.viewState, vmi, serviceId);
                }

            }

        }

    }

    private async _renderElement(source: VDomElement, target: VDomNode, viewState: any, viewStateInfo: ViewStateInfo, serviceId: number = undefined): Promise<void> {

        const element = target.ownerDocument.createElement(source.nodeName.toLowerCase());

        const eventData: AjsVDomEventData = {
            view: this,
            viewModel: viewStateInfo.viewState,
            viewModelPropertyName: viewStateInfo.propertyName,
            viewModelPropertyIndex: viewStateInfo.propertyIndex
        };

        element.customData = eventData;

        this._renderElementAttributes(source, element, viewState);

        if (viewStateInfo.cssClassUpdateInfo.length > 0 && element.hasAttribute("class")) {
            const classesIn = element.getAttribute("class").split(" ");
            let classesOut = "";

            for (const classIn of classesIn) {
                let found = false;
                for (const cUI of viewStateInfo.cssClassUpdateInfo) {
                    if (cUI.name === classIn) {
                        if (!cUI.delete) classesOut += classIn + " ";
                        for (const cToAdd of cUI.classes) classesOut += cToAdd + " ";
                        found = true;
                        break;
                    }
                }
                if (!found) classesOut += classIn + " ";
            }
            element.setAttribute("class", classesOut.trim());
        }

        if (serviceId) {
            const classAttr: VDomAttr = element.attributes.getNamedItem("class") || new VDomAttr("class", element.ownerDocument);
            classAttr.nodeValue = classAttr.nodeValue ? classAttr.nodeValue + " ajsService" + serviceId : "ajsService" + serviceId;
            element.attributes.setNamedItem(classAttr);
        }

        target.appendChild(element);

        for (let i = 0; i < source.childNodes.length; i++) {
            await this._renderNode(source.childNodes.item(i), element, viewState, viewStateInfo);
        }

    }

    private _renderElementAttributes(source: VDomElement, target: VDomElement, viewState: any): void {

        for (let i = 0; i < source.attributes.length; i++) {

            const src: VDomAttr = source.attributes.item(i);
            if (src.nodeName === "data-source") continue;

            let dst: VDomAttr = target.ownerDocument.createAttribute(src.nodeName);

            if (eventAttributes.indexOf(src.nodeName) !== -1) {

                dst.customData = (event: Event) => { this._processDomEvent(src.nodeValue, event); };

            } else {

                const value: string = this._processTextContent(src.nodeValue, viewState);
                if (value === undefined) continue;

                switch (src.nodeName) {
                    case "selected":
                        if (value !== "true") dst = null;
                        break;
                    case "checked":
                        if (value !== "true") dst = null;
                        break;
                    case "disabled":
                        if (value !== "true") dst = null;
                        break;
                    default:
                        dst.nodeValue = value;
                }

            }

            if (dst !== null) target.attributes.setNamedItem(dst);

        }

    }

    private _processTextContent(text: string, viewState: any): string {

        const toReplace: string[] = [];
        let textContent: string = text;

        const rx: RegExp = /\{.*\}/g;
        let rxea: RegExpExecArray;

        while (rxea = rx.exec(textContent)) {
            toReplace.push(rxea[0].substr(1, rxea[0].length - 2));
        }

        toReplace.push("{}");

        for (let i = 0; i < toReplace.length; i++) {
            if (viewState[toReplace[i]] !== undefined) {
                let replacement = viewState[toReplace[i]];
                if (typeof replacement === "boolean") replacement = replacement ? "true" : "false";
                textContent = textContent.replace("{" + toReplace[i] + "}", replacement);
            }
            if (toReplace[i] === "") {
                textContent = textContent.replace("{}", viewState);
            }
        }

        return textContent;

    }


}
