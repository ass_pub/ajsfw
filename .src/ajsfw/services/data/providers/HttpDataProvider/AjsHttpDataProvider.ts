/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { StringKeyValue } from "ajsfw/types";
import { EXMLHttpRequest } from "./EXMLHttpRequest";
import { HttpDataProvider } from "./HttpDataProvider";
import { HttpMethod } from "./HttpMethod";
import { HttpResponse } from "./HttpResponse";

export class AjsHttpDataProvider implements HttpDataProvider {

    private _xhr: XMLHttpRequest;
    private _headers: StringKeyValue<string>;

    public init(): void {
        this._xhr = new XMLHttpRequest();
        this._headers = { };
    }

    public async asyncRead(uri: string): Promise<string | Int8Array> {
        const resp: HttpResponse = await this._asyncRequest(uri, HttpMethod.GET, this._headers);
        return resp.data;
    }

    public async asyncCreate(uri: string, data: string | Int8Array, contentType = "application/x-www-form-urlencoded"): Promise<string | Int8Array> {
        const response = await this._asyncRequest(uri, HttpMethod.POST, this._prepareHeaders(contentType), data);
        return response.data;
    }

    public async asyncUpdate(uri: string, data: string | Int8Array, contentType = "application/x-www-form-urlencoded"): Promise<string | Int8Array> {
        const response = await this._asyncRequest(uri, HttpMethod.PUT, this._prepareHeaders(contentType), data);
        return response.data;
    }

    public async asyncPatch(uri: string, data: string | Int8Array, contentType = "application/x-www-form-urlencoded"): Promise<string | Int8Array> {
        const response = await this._asyncRequest(uri, HttpMethod.PATCH, this._prepareHeaders(contentType), data);
        return response.data;
    }

    public async asyncDelete(uri: string): Promise<string | Int8Array> {
        const response = await this._asyncRequest(uri, HttpMethod.DELETE, this._headers);
        return response.data;
    }

    public async asyncRequest(url: string, method: HttpMethod, headers: StringKeyValue<string> = { }, data?: string | Int8Array): Promise<HttpResponse> {
        return this._asyncRequest(url, method, headers, data);
    }

    private _prepareHeaders(contentType: string): StringKeyValue<string> {
        const headers: StringKeyValue<string> = {};
        for (const key in this._headers) if (key !== "Content-Type") headers[key] = this._headers[key];
        headers["Content-Type"] = contentType;
        return headers;
    }

    private _asyncRequest(url: string, method: HttpMethod, headers: StringKeyValue<string> = { }, data?: string | Int8Array): Promise<HttpResponse> {
        // #ifdef DEBUG
        log.info(1, this, "HTTP Request: '", url, "', method: ", method, ", data: ", data ? data : "");
        // #endif
        return new Promise<HttpResponse>((resolve, reject) => {
            this._resolveRequest(resolve, reject, url, method, headers, data);
        });
    }

    private _resolveRequest(resolve: (result: HttpResponse) => void, reject: (reason: any) => void, url: string, method: HttpMethod, headers: StringKeyValue <string>, data: any): void {

        const xhr: EXMLHttpRequest = new XMLHttpRequest();

        xhr.url = url;
        xhr.onload = (e: Event) => { this._load(e); };
        xhr.onerror = (e: Event) => { this._error(e); };

        xhr.open(HttpMethod[method], url, true);
        xhr.withCredentials = true;
        xhr.promise = { resolve, reject };

        let hdrName;
        for (hdrName in headers); {
            xhr.setRequestHeader(hdrName, headers[hdrName]);
        }

        xhr.send(data);

    }

    private _load(e: Event): void {

        const xhr: EXMLHttpRequest = e.target as EXMLHttpRequest;

        // #ifdef DEBUG
        log.info(1, this, "HTTP Response: ", xhr.status, " - ", xhr.statusText, " for ", xhr.url);
        // #endif

        if (xhr.status !== 200) {
            xhr.promise.reject("HTTP Request failed with HTTP code " + xhr.status);
        } else {
            xhr.promise.resolve({
                data: xhr.responseText,
                statusCode: xhr.status,
                statusText: xhr.statusText,
                url: xhr.url,
            });
        }

    }

    private _error(e: Event): void {
        const xhr: EXMLHttpRequest = e.target as EXMLHttpRequest;
        xhr.promise.reject("HTTP Request failed with unknown error.");
    }

}
