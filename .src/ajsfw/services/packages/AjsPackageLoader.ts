/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { FileProvider, IIFileProvider } from "ajsfw/services/data/providers/FileDataProvider/FileProvider";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { PackageLoader } from "./PackageLoader";

/**
 * Ajs code package loader used to load package and register its modules to the global _ajs collection
 * The load unction returns id of the main package contained within the module.
 * If the package is used with standard components the main module of the package must have the default
 * export of the class implementing the ApplicationModule interface.
 */
@inject(IIFileProvider)
export class AjsPackageLoader implements PackageLoader {

    private _fileProvider: FileProvider;

    constructor(fileReader: FileProvider) {
        // #ifdef DEBUG
        log.constructor(1, this, fileReader);
        // #endif
        this._fileProvider = fileReader;
    }

    /**
     * Loads the Ajs code package and returns the id of the package main module
     * @param packageUrl Url of the package to be loaded
     */
    public async load(packageUrl: string): Promise<string> {
        // #ifdef DEBUG
        log.info(1, this, "Loading package: '", packageUrl);
        // #endif
        const packageCode: string = <string>await this._fileProvider.asyncRead(packageUrl);

        try {
            const result = eval.call(null, packageCode);
            return result;
        } catch (e) {
            log.error(this, e);
            throw e;
        }

    }

}
