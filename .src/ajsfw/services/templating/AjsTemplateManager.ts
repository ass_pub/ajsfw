/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Document as vdomDocument } from "ajsfw/lib/vdom/Document";
import { Element as vdomElement } from "ajsfw/lib/vdom/Element";
import { Node as vdomNode } from "ajsfw/lib/vdom/Node";
import { FileProvider, IIFileProvider } from "ajsfw/services/data/providers/FileDataProvider/FileProvider";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { StringKeyValue } from "ajsfw/types";
import { AjsTemplate } from "./AjsTemplate";
import * as exceptions from "./exceptions";
import { Template } from "./Template";
import { TemplateManager } from "./TemplateManager";

@inject(IIFileProvider)
export class AjsTemplateManager implements TemplateManager {

    private _fileProvider: FileProvider;
    private _languageCode: string;
    private _loadings: StringKeyValue<Promise<void>>;

    private _templatesByName: StringKeyValue<Template>;
    public get templatesByName(): StringKeyValue<Template> { return this.templatesByName; }

    constructor(fileProvider: FileProvider) {
        this._fileProvider = fileProvider;
        this._loadings = {};
        this._templatesByName = {};
    }

    public setLanguage(languageCode: string): void {
        this._languageCode = languageCode;
    }

    public async getTemplate(name: string): Promise<Template> {

        const loadings: Promise<void>[] = [];
        for (const k in this._loadings) loadings.push(this._loadings[k]);

        await Promise.all(loadings);

        if (this._templatesByName.hasOwnProperty(name + "." + this._languageCode)) {
            return this._templatesByName[name + "." + this._languageCode];
        } else {
            if (this._templatesByName.hasOwnProperty(name)) {
                return this._templatesByName[name];
            } else {
                return Promise.reject(new exceptions.TemplateNotFound(name));
            }
        }

    }

    public load(url: string | string[]): void {

        if (typeof(url) === "string") {
            if (this._loadings.hasOwnProperty(url)) return;
            this._loadings[url] = this._loadTemplatesFile(url);
        }

        if (Array.isArray(url)) {
            for (const u of url) {
                this.load(u);
            }
        }
    }

    private async _loadTemplatesFile(url: string): Promise<void> {

        const tplsFile: string = <string>await this._fileProvider.asyncRead(url);
        const templates: vdomDocument = new vdomDocument();
        templates.body.innerHTML = tplsFile;
        this._createTemplatesFromFile(url, templates);

    }

    private _createTemplatesFromFile(fileName: string, templates: vdomDocument): void {

        for (let i = 0; i < templates.body.childNodes.length; i++) {
            const node: vdomNode = templates.body.childNodes.item(i);

            if (node.nodeType === vdomNode.ELEMENT_NODE &&
                node.nodeName.toLowerCase() === "template" &&
                (node as vdomElement).hasAttribute("id")
            ) {
                const templateName = (node as vdomElement).getAttribute("id");
                if (templateName === "") {
                    throw new exceptions.TemplateIdMustBeDefined();
                }

                if (this._templatesByName.hasOwnProperty(templateName)) {
                    throw new exceptions.TemplateAlreadyExists("'" + templateName + "' (" + fileName + ")");
                }

                const template: Template = new AjsTemplate(
                    fileName,
                    templateName,
                    node as vdomElement
                );

                this._templatesByName[templateName] = template;

            }
        }

    }

}