/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { FileProvider, IIFileProvider } from "ajsfw/services/data/providers/FileDataProvider/FileProvider";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { IILang, Lang } from "ajsfw/services/lang/Lang";
import { StringKeyValue } from "ajsfw/types";
import { AjsTemplateManager } from "./AjsTemplateManager";
import { Template } from "./Template";
import { Document as vdomDocument } from "ajsfw/lib/vdom/Document";

@inject(IIFileProvider, IILang)
export class AjsTranslatedTemplateManager extends AjsTemplateManager {

    protected _lang: Lang;
    protected _langDocs: StringKeyValue<vdomDocument>;
    protected _translatedTemplates: StringKeyValue<StringKeyValue<Template>>;

    constructor(fileProvider: FileProvider, lang: Lang) {
        super(fileProvider);
        this._lang = lang;
        this._langDocs = {};
        this._translatedTemplates = {};
    }

    public async getTemplate(name: string): Promise<Template> {

        if (this._translatedTemplates.hasOwnProperty(this._lang.code) &&
            this._translatedTemplates[this._lang.code].hasOwnProperty(name)) {
            return this._translatedTemplates[this._lang.code][name];
        }

        const template: Template = await super.getTemplate(name);
        return this._translateTemplate(template);

    }

    protected _translateTemplate(template: Template): Template {

        if (!this._translatedTemplates.hasOwnProperty(this._lang.code)) {
            this._translatedTemplates[this._lang.code] = {};
            this._langDocs[this._lang.code] = new vdomDocument();
        }


        const idAttr = this._langDocs[this._lang.code].createAttribute("id");
        const translatedTemplateElement = this._langDocs[this._lang.code].createElement("template");
        idAttr.nodeValue = template.name;
        translatedTemplateElement.attributes.setNamedItem(idAttr);

        const translatedTemplate: Template = {
            name: template.name,
            fileName: template.fileName,
            dom: translatedTemplateElement
        };

        this._translatedTemplates[this._lang.code][template.name] = translatedTemplate;

        const html = this._translateHTML(template.name, template.dom.innerHTML);
        translatedTemplate.dom.innerHTML = html;

        return translatedTemplate;
    }

    protected _translateHTML(templateName: string, html: string): string {

        const re = /\{\[.*?\]\}/g;
        const trasnlatedHtml = html.replace(re, (matchm, g1, g2): string => {
            return this._lang.translate("templates." + templateName + "." + matchm.substring(2, matchm.length - 2));
        });

        return trasnlatedHtml;
    }

}