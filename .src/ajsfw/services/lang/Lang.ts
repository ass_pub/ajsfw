/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Service } from "ajsfw/core/services/Service";
import { DependencyIdentifier } from "ajsfw/services/dic/DependencyIdentifier";
import { LangDescriptor } from "./LangDescriptor";
import { LangRef } from "./LangRef";
import { Notifier } from "ajsfw/lib/events/Notifier";

export const IILang: DependencyIdentifier = [ "IILang" ];

export interface Lang extends Service {

    /** Current language code (the code selected to be used for translations) */
    code: string;
    /** Returns list of loaded language files */
    getLangs(): Promise<LangDescriptor[]>;
    /** Loads a language file or language files */
    load(url: string | string[]): void;
    /** Sets the translation to be used */
    use(code: string): Promise<void>;
    /** Translates the given key from currently selected translations with given arguments */
    translate(translation: string, ...args: (string|number|LangRef)[]): string;
    /** Can be used to register component to be notified when the language is changed */
    langChangedNotifier: Notifier<string>;

}
