/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Notifier } from "ajsfw/lib/events/Notifier";
import { AjsNotifier } from "ajsfw/lib/events/AjsNotifier";
import { FileProvider, IIFileProvider } from "ajsfw/services/data/providers/FileDataProvider/FileProvider";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { Lang } from "./Lang";
import { LangDescriptor } from "./LangDescriptor";
import { LangFile } from "./LangFile";
import { LangRef } from "./LangRef";
import * as exceptions from "./exceptions";

@inject(IIFileProvider)
export class AjsLang implements Lang {

    /** File provider used to load language files */
    protected _fileProvider: FileProvider;

    /** Stores promises of translations being loaded or loaded already */
    protected _langFiles: Promise<LangFile>[];

    /** Stores the language code in use */
    protected _code: string;

    /** Stores current language translation object */
    protected _langFile: LangFile;

    /** Notifies subscribers if the language is changed */
    protected _langChangedNotifier: Notifier<string>;
    public get langChangedNotifier(): Notifier<string> { return this._langChangedNotifier; }

    /** Returns currently used language code */
    public get code(): string { return this._code; }
    public set code(value: string) { this.use(value); }

    constructor (fileProvider: FileProvider) {
        this._fileProvider = fileProvider;
        this._langFiles = [];
        this._langChangedNotifier = new AjsNotifier<string>();
    }

    public dispose() {
        this._fileProvider = undefined;
        this._langFiles = undefined;
        this._langChangedNotifier = undefined;
    }

    /**
     * Initiates loading of language file or files specified
     */
    public load(url: string | string[]): void {

        if (typeof url === "string") {
            return this._load(url);
        }

        if (Array.isArray(url)) {
            for (const u of url) this.load(u);
        }

    }

    /**
     * Returns list of loaded language files
     * If any language file is being loaded the use method will wait for it
     */
    public async getLangs(): Promise<LangDescriptor[]> {

        const langFiles: LangFile[] = await Promise.all(this._langFiles);

        const lds: LangDescriptor[] = [];

        for (const k of langFiles) {
            lds.push({
                code: k.lang.code,
                label: k.lang.label,
            });
        }

        return lds;
    }

    /**
     * Sets the language to be used for translation
     * If any language file is being loaded the use method will wait for it
     */
    public async use(code: string): Promise<void> {

        const langFiles: LangFile[] = await Promise.all(this._langFiles);

        for (const lf of langFiles) {
            if (code === lf.lang.code) {
                this._code = code;
                this._langFile = lf;
                this._langChangedNotifier.notify(this, code);
                return;
            }
        }
        throw new exceptions.FailedToSetLanguageByCode(code);
    }

    /**
     * Formats the string (usually taken from Lang.using["key"]))
     * The string in a language file can contain named reference to args collection or directly to the translation table:
     * "numberNames": [ "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" ],
     * "numberName": "Name of number '${value}' is $[numberNames, ${value}]"
     * lang.translate(lang.using.numberName, { value: 3 })
     *
     * If the LangRef is passed to the translate function as an argument it can reffer directly to the translation in the file:
     * "numberName": "Name of number '${value}' is ${ref}"
     * lang.translate(lang.using.numberName, { value: 3, ref: { path: "numberNames", index: 3 }})
     */
    public translate(translation: string | any, ...args: (string|number|LangRef)[]): string {

        if (!this._langFile) return "Language not selected";

        let t: any;

        if (typeof translation === "string") {
            const keys = translation.split(".");
            t = this._langFile;
            for (const key of keys) {
                if (t.hasOwnProperty(key)) {
                    t = t[key];
                } else {
                    return "{[" + translation + "]}";
                }
            }
            t = this._applyArgs.apply(this, [ t ].concat(args));
        } else {
            t = translation;
        }

        return t;

    }

    protected _applyArgs(text: string, ...args: (string|number|LangRef)[]): string {
        let rv = text;
        for (let i = 0; i < args.length; i++) {
            const rx = new RegExp("\\{" + i + "\\}", "gm");
            rv = rv.replace(rx, args[i].toString());
        }
        return rv;
    }

    /** Parses a translation JSON file */
    protected _parse(translations: string): LangFile {
        try {
            const lang: LangFile = JSON.parse(translations);
            if (!lang.lang) throw "Missing lang section";
            if (!lang.lang.code) throw "Missing language code";
            if (!lang.lang.label) throw "Missing language label";
            return lang;
        } catch (e) {
            throw new exceptions.FailedToParseLanguageFile(e);
        }
    }

    /** Loads and parses the language file */
    protected _load(url: string): void {

        const promise = new Promise<LangFile>(

            async (resolve: (value: LangFile) => void, reject: (reason: any) => void) => {
                try {
                    const contents = <string>await this._fileProvider.asyncRead(url);
                    const langFile: LangFile = this._parse(contents);
                    if (!this.code) this.use(langFile.lang.code);
                    resolve(langFile);
                } catch (e) {
                    reject(new exceptions.LanguageFileLoadingFailed(url, e));
                }
            }

        );

        this._langFiles.push(promise);


    }

}
