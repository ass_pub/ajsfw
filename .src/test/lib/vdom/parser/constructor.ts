import "jest";
import { DomParser as vdomDomParser } from "ajsfw/lib/vdom/DomParser";
import { Document as vdomDocument } from "ajsfw/lib/vdom/Document";

// import * as browserenv from "browser-env";

describe("Parser construction and created document contents tests", () => {

   test("Parameterless constructor", () => {
      const parser = new vdomDomParser();

      expect(parser).toBeDefined();
      expect(parser).toBeInstanceOf(vdomDomParser);

      expect(parser.document).toBeDefined();
      expect(parser.document).toBeInstanceOf(vdomDocument);

      expect(parser.parse).toBeDefined();
      expect(typeof parser.parse).toBe("function");
   });

   test("Parametrized constructor", () => {

      const document = new vdomDocument();
      const parser = new vdomDomParser(document);

      expect(parser).toBeDefined();
      expect(parser).toBeInstanceOf(vdomDomParser);

      expect(parser.document).toBeInstanceOf(vdomDocument);
      expect(parser.document).toEqual(document);

   });

});
