/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { IIDependentService } from "ajsfw/core/services/Service";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { IILang, Lang } from "ajsfw/services/lang/Lang";
import { AjsVDomEventData } from "ajsfw/services/virtualview/AjsVDomEventData";
import { Param } from "dto/Profile";
import { FormField } from "./FormField";

@ajsUseTemplate("formpasswordverification")
@ajsViewModel(
    IIDependentService,
    IIViewModelServiceManager,
    IILang
)
export class FormPasswordVerification implements FormField {

    public paramDTO: Param;
    public label: string;
    public help: string;
    public error1: string;
    public error2: string;
    public type: "text" | "password";

    public get value(): string { return this._value1; }
    public set value(value: string) {
        this._value1 = value;
        this.validate();
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    public get value2(): string { return this._value2; }
    public set value2(value: string) {
        this._value2 = value;
        this.validate();
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    protected _parent: ViewModel;
    protected _viewModelServiceManager: ViewModelServiceManager;
    protected _lang: Lang;

    protected _value1: string;
    protected _value2: string;

    constructor(
        parent: ViewModel,
        viewModelServiceManager: ViewModelServiceManager,
        lang: Lang
    ) {
        this._parent = parent;
        this._viewModelServiceManager = viewModelServiceManager;
        this._lang = lang;
    }

    public onChange1(data: AjsVDomEventData) {
        if (this._value1 === data.value && data.eventType !== "blur") return;
        this.value = data.value;
    }

    public onChange2(data: AjsVDomEventData) {
        if (this._value2 === data.value && data.eventType !== "blur") return;
        this.value2 = data.value;
    }

    public validate(): boolean {

        let result = true;

        if (this._value1 === "") {
            if (this.paramDTO.mandatory === "TRUE") {
                this._setError1(this._lang.translate("formFields.mandatory"));
                result = false;
            } else {
                this._setError1("");
            }
        }

        if (!this.paramDTO.filter) {
            this._setError1("");
        } else {
            const rx = new RegExp(this.paramDTO.filter);
            if (rx.test(this._value1)) {
                this._setError1("");
            } else {
                result = false;
            }
        }

        if (!result) {
            if (this.paramDTO.langParamError) {
                this._setError1(this.paramDTO.langParamError[this._lang.code]);
            } else {
                this._setError1(this._lang.translate("formFields.invalidInput"));
            }
        }

        if (this._value2 !== this._value1) {
            this._setError2(this._lang.translate("formFields.passwordsDoesNotMatch"));
            result = false;
        } else {
            this._setError2("");
        }

        return result;
    }

    protected _setError1(error: string): void {
        if (this.error1 !== error) {
            if (error === "") {
                this.error1 = "&nbsp;";
            } else {
                this.error1 = error;
            }
            this._viewModelServiceManager.viewModelUpdated(this);
        }
    }

    protected _setError2(error: string): void {
        if (this.error2 !== error) {
            if (error === "") {
                this.error2 = "&nbsp;";
            } else {
                this.error2 = error;
            }
            this._viewModelServiceManager.viewModelUpdated(this);
        }
    }

}