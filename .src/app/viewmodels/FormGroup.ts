/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { IIDependentService } from "ajsfw/core/services/Service";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { FormField } from "./FormField";
import { Group } from "../dto/Profile";

@ajsUseTemplate("formfieldgroup")
@ajsViewModel(IIDependentService)
export class FormGroup implements ViewModel {

    public label: string;
    public formFields: FormField[];
    public groupDTO: Group;

    private _parent: ViewModel;

    constructor(parent: ViewModel) {
        this._parent = parent;
    }

}