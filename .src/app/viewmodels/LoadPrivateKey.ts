/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { AjsPasswordInput } from "ajsfw/services/mvvm/controls/AjsPasswordInput";
import { ExternallyResolvablePromise, createExternallyResolvablePromise } from "ajsfw/lib/utils/externallyResolvablePromise";
import { AjsVDomEventData } from "ajsfw/services/virtualview/AjsVDomEventData";
import { IILang, Lang } from "ajsfw/services/lang/Lang";
import { IIAppConfig, AppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { CafeConfig } from "application/CafeConfig";
import { AjsTextArea } from "ajsfw/services/mvvm/controls/AjsTextArea";
import { CryptoServices } from "appservices/cryptoservices/CryptoServices";
import { ErrorMessage } from "./ErrorMessage";

@ajsUseTemplate("loadprivatekey")
@ajsViewModel(
    IIViewModelServiceManager,
    IILang,
    IIAppConfig,
    ErrorMessage,
    CryptoServices)
export class LoadPrivateKey implements ViewModel {

    protected _visible: boolean;

    protected _privateKey: AjsTextArea;
    protected _privateKeyFileName: string;
    protected _password: AjsPasswordInput;
    protected _okDisabled: boolean;
    protected _privateKeyFileCleanup: boolean;
    protected _error: string;

    protected _viewModelServiceManager: ViewModelServiceManager;
    protected _lang: Lang;
    protected _config: CafeConfig;
    protected _errorMessage: ErrorMessage;
    protected _cryptoServices: CryptoServices;

    protected _getPrivateKey: ExternallyResolvablePromise<{ key: string, password: string}>;

    protected _crt: string;

    constructor(
        viewModelServiceManager: ViewModelServiceManager,
        lang: Lang,
        config: AppConfig,
        errorMessage: ErrorMessage,
        cryptoServices: CryptoServices
    ) {
        // #ifdef DEBUG
        log.constructor(1, this);
        // #endif

        this._viewModelServiceManager = viewModelServiceManager;
        this._lang = lang;
        this._config = config.data;
        this._errorMessage = errorMessage;
        this._cryptoServices = cryptoServices;
    }

    public async init(): Promise<void> {
        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._visible = false;
        this._okDisabled = true;

        this._privateKey = <AjsTextArea>await this._viewModelServiceManager.createChildViewModel(this, AjsTextArea);
        this._privateKey.onChange.subscribe(this._privateKeyChanged, this);
        this._privateKey.onDragDrop.subscribe(this._privateKeyDragDrop, this);
        this._password = <AjsPasswordInput>await this._viewModelServiceManager.createChildViewModel(this, AjsPasswordInput);
        this._password.onChange.subscribe(this._passwordChanged, this);
        this._privateKeyFileCleanup = true;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif
    }

    public dispose(): void {

        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._privateKey.onChange.unsubscribe(this._privateKeyChanged, this);
        this._privateKey.onDragDrop.unsubscribe(this._privateKeyDragDrop, this);
        this._privateKey = <AjsTextArea>this._viewModelServiceManager.disposeChildViewModel(this, this._privateKey);
        this._password.onChange.unsubscribe(this._passwordChanged, this);
        this._password = <AjsPasswordInput>this._viewModelServiceManager.disposeChildViewModel(this, this._password);
        this._viewModelServiceManager = undefined;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif

    }

    public getPrivateKey(crt: string): Promise<{ key: string, password: string}> {
        this._crt = crt;
        this._visible = true;
        this._cleanPrivateKey();
        this._getPrivateKey = createExternallyResolvablePromise<{ key: string, password: string}>();
        this._viewModelServiceManager.viewModelUpdated(this);
        this._privateKeyFileName = this._lang.translate("loadPrivateKey.noPrivateKeySelected");
        return this._getPrivateKey;
    }

    protected _onOkClick(data: AjsVDomEventData): void {

        const pwd = this._password.value;
        const pk = this._privateKey.value;

        let privateKey;
        try {
            privateKey = this._cryptoServices.decryptAndCheckPrivateKey(this._crt, pk, pwd);
        } catch (e) {
            this._error = this._lang.translate("loadPrivateKey.invalidPwdOrKey");
            this._viewModelServiceManager.viewModelUpdated(this);
            return;
        }

        if (privateKey === undefined) {
            this._error = this._lang.translate("loadPrivateKey.privateKeyNotMatch"); "Private key does not match selected certificate";
            this._viewModelServiceManager.viewModelUpdated(this);
            return;
        }

        if (privateKey === null) {
            this._error = this._lang.translate("loadPrivateKey.failedToDecryptPrivateKey"); "Failed to decrypt private key";
            this._viewModelServiceManager.viewModelUpdated(this);
            return;
        }

        this._hide();
        this._getPrivateKey.resolve({key: privateKey, password: pwd});

    }

    protected _onCancelClick(data: AjsVDomEventData): void {
        this._hide();
        this._getPrivateKey.resolve(undefined);
    }

    protected _hide(): void {
        this._cleanPrivateKey();
        this._visible = false;
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    protected _privateKeyDragDrop(sender: ViewModel, data: AjsVDomEventData): void {
        this._fileDraggedDropped(data);
    }

    protected _fileDraggedDropped(data: AjsVDomEventData): void {

        if (data.eventType === "drop") {
            if (!data.dataTransfer || !data.dataTransfer.files || data.dataTransfer.files.length === 0) return;
            this._loadPrivateKeyFile(data.dataTransfer.files.item(0));
        } else {
            this._viewModelServiceManager.viewModelUpdated(this);
        }

        data.cancelBubble = true;
        data.preventDefault = true;
    }

    protected _fileSelected(data: AjsVDomEventData): void {
        if (data.files.length === 0) {
            this._viewModelServiceManager.viewModelUpdated(this);
            return;
        }
        this._loadPrivateKeyFile(data.files.item(0));
    }

    protected _cleanPrivateKey(): void {
        this._privateKey.value = "";
        this._password.value = "";
        this._error = "&nbsp;";
        this._okDisabled = true;
    }

    protected async _loadPrivateKeyFile(file: File): Promise<void> {

        this._cleanPrivateKey();

        const reader = new FileReader();
        reader.onload = () => {
            try {
                this._privateKeyFileName = file.name;
                this._privateKey.value = <string>reader.result;
            } catch (e) {
                this._cleanPrivateKey();
                this._privateKeyFileName = this._lang.translate("loadPrivateKey.loadingError");
            }
            this._viewModelServiceManager.viewModelUpdated(this);
        };
        reader.readAsText(file);

    }

    protected _privateKeyChanged(data: AjsVDomEventData): void {
        this._validate();
    }

    protected _passwordChanged(data: AjsVDomEventData): void {
        this._validate();
    }

    protected _validate(): void {
        const old = this._okDisabled;
        this._okDisabled = this._password.value === "" || this._privateKey.value === "";
        if (old !== this._okDisabled) this._viewModelServiceManager.viewModelUpdated(this);
    }

}