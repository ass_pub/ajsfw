/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { IIDependentService } from "ajsfw/core/services/Service";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { SelectItem } from "viewstates/SelectItem";
import { AjsVDomEventData } from "ajsfw/services/virtualview/AjsVDomEventData";
import { FormField } from "./FormField";
import { AjsSelect } from "ajsfw/services/mvvm/controls/AjsSelect";

@ajsUseTemplate("formselect")
@ajsViewModel(IIDependentService, AjsSelect)
export class FormSelect implements FormField {

    public paramDTO: any;
    public label: string;
    public help: string;
    public select: AjsSelect;
    public error: string;
    public value: string;

    private _parent: ViewModel;

    constructor(parent: ViewModel, select: AjsSelect) {
        this._parent = parent;
        this.select = select;
    }

}