/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { IIDependentService } from "ajsfw/core/services/Service";
import { IIAppConfig, AppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { CafeConfig } from "application/CafeConfig";
import { Link } from "viewstates/Link";
import { IILang, Lang } from "ajsfw/services/lang/Lang";
import { Listener } from "ajsfw/lib/events/Listener";

@ajsUseTemplate("cacertscrls")
@ajsViewModel(
    IIDependentService,
    IIViewModelServiceManager,
    IIAppConfig,
    IILang
)
export class CaCertsCrls implements ViewModel {

    public certificates: Link[];
    public crls: Link[];

    protected _parentViewModel: ViewModel;
    protected _viewModelServiceManager: ViewModelServiceManager;
    protected _config: CafeConfig;
    protected _lang: Lang;
    protected _langChangedListener: Listener<string>;

    constructor(
        parentViewModel: ViewModel,
        viewModelServiceManager: ViewModelServiceManager,
        config: AppConfig,
        lang: Lang
    ) {

        // #ifdef DEBUG
        log.constructor(1, this);
        // #endif

        this._parentViewModel = parentViewModel;
        this._viewModelServiceManager = viewModelServiceManager;
        this._config = config.data;
        this._lang = lang;

        this._langChangedListener = this._langChanged.bind(this);
        this._lang.langChangedNotifier.subscribe(this._langChangedListener);
    }

    public init(): void {

        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._update();

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif

    }

    public dispose(): void {

        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this.certificates = undefined;
        this.crls = undefined;
        this._parentViewModel = undefined;
        this._viewModelServiceManager = undefined;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif

    }

    /**
     * Called when the language is changed
     * @param sender AjsLang object
     * @param code Language code currently in use
     */
    protected _langChanged(sender: any, code: string): void {
        this._update();
    }

    protected _update(): void {

        this.certificates = [];
        this.crls = [];

        for (const cert of this._config.ca.certUrls) {
            this.certificates.push({
                label: this._lang.translate("caCertsCrls.certificateOfCa", cert.label),
                url: cert.url
            });
        }

        for (const crl of this._config.ca.crlUrls) {
            this.crls.push({
                label: this._lang.translate("caCertsCrls.latestCaCrl", crl.label),
                url: crl.url
            });
        }

    }

}
