/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { AjsPasswordInput } from "ajsfw/services/mvvm/controls/AjsPasswordInput";
import { ExternallyResolvablePromise, createExternallyResolvablePromise } from "ajsfw/lib/utils/externallyResolvablePromise";
import { AjsVDomEventData } from "ajsfw/services/virtualview/AjsVDomEventData";
import { IILang, Lang } from "ajsfw/services/lang/Lang";
import { IIAppConfig, AppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { CafeConfig } from "application/CafeConfig";

@ajsUseTemplate("privatekeypassword")
@ajsViewModel(IIViewModelServiceManager, IILang, IIAppConfig)
export class PrivateKeyPassword implements ViewModel {

    protected _visible: boolean;

    protected _error1: string;
    protected _password: AjsPasswordInput;

    protected _error2: string;
    protected _passwordVerify: AjsPasswordInput;

    protected _okDisabled: boolean;

    protected _viewModelServiceManager: ViewModelServiceManager;
    protected _lang: Lang;
    protected _config: CafeConfig;

    protected _getPassword: ExternallyResolvablePromise<string>;

    constructor(viewModelServiceManager: ViewModelServiceManager, lang: Lang, config: AppConfig) {
        // #ifdef DEBUG
        log.constructor(1, this);
        // #endif

        this._viewModelServiceManager = viewModelServiceManager;
        this._lang = lang;
        this._config = config.data;
    }

    public async init(): Promise<void> {
        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._visible = false;
        this._error1 = "&nbsp;";
        this._error2 = "&nbsp;";
        this._okDisabled = true;

        this._password = <AjsPasswordInput>await this._viewModelServiceManager.createChildViewModel(this, AjsPasswordInput);
        this._password.onChange.subscribe(this._validate, this);

        this._passwordVerify = <AjsPasswordInput>await this._viewModelServiceManager.createChildViewModel(this, AjsPasswordInput);
        this._passwordVerify.onChange.subscribe(this._validate, this);

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif
    }

    public dispose(): void {

        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._password = <AjsPasswordInput>this._viewModelServiceManager.disposeChildViewModel(this, this._password);
        this._passwordVerify = <AjsPasswordInput>this._viewModelServiceManager.disposeChildViewModel(this, this._passwordVerify);
        this._viewModelServiceManager = undefined;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif

    }

    public getPassword(): Promise<string> {
        this._visible = true;
        this._getPassword = createExternallyResolvablePromise<string>();
        this._viewModelServiceManager.viewModelUpdated(this);
        return this._getPassword;
    }

    protected _onOkClick(data: AjsVDomEventData): void {
        const pwd = this._password.value;
        this._hide();
        this._getPassword.resolve(pwd);
    }

    protected _onCancelClick(data: AjsVDomEventData): void {
        this._hide();
        this._getPassword.resolve(undefined);
    }

    protected _hide(): void {
        this._password.value = "";
        this._passwordVerify.value = "";
        this._error1 = "";
        this._error2 = "";
        this._visible = false;
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    protected _validate(): boolean {

        let result = true;

        const prx = new RegExp(this._config.passwordComplexity);

        if (!prx.test(this._password.value)) {
            this._error1 = this._lang.translate("savePrivateKeyPassword.complexity");
            result = false;
        } else {
            this._error1 = "&nbsp;";
        }

        if (this._passwordVerify.value !== this._password.value) {
            this._error2 = this._lang.translate("savePrivateKeyPassword.notMatch");
            result = false;
        } else {
            this._error2 = "&nbsp;";
        }

        this._okDisabled = !result;
        this._viewModelServiceManager.viewModelUpdated(this);

        return result;

    }

}