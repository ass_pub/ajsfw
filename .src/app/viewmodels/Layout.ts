/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { IIDependentService } from "ajsfw/core/services/Service";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { urlBindPath } from "ajsfw/services/urlbinding/urlBindPath.decorator";
import { ErrorMessage } from "./ErrorMessage";
import { Loading } from "./Loading";
import { CaCertsCrls } from "./CaCertsCrls";
import { Navigation } from "./Navigation";
import { NewRequest } from "./NewRequest";
import { PendingRequests } from "./PendingRequests";
import { PrivateKeyPassword } from "./PrivateKeyPassword";
import { LoadPrivateKey } from "./LoadPrivateKey";

@ajsUseTemplate("layout")
@ajsViewModel(
    IIDependentService,
    IIViewModelServiceManager,
    Loading,
    ErrorMessage,
    PrivateKeyPassword,
    LoadPrivateKey
)
export class Layout implements ViewModel {

    protected _parentViewModel: ViewModel;
    protected _viewModelServiceManager: ViewModelServiceManager;

    protected _loading: Loading;
    protected _errorMessage: ErrorMessage;
    protected _privateKeyPassword: PrivateKeyPassword;
    protected _loadPrivateKey: LoadPrivateKey;

    protected _menuVisible: boolean;
    protected _navigation: Navigation;

    @urlBindPath("$|NewRequest$", NewRequest)
    @urlBindPath("PendingRequests$", PendingRequests)
    @urlBindPath("CaCertsCrls$", CaCertsCrls)
    protected _body: ViewModel;

    constructor(
        parentViewModel: ViewModel,
        viewModelServiceManager: ViewModelServiceManager,
        loading: Loading,
        error: ErrorMessage,
        pkPwd: PrivateKeyPassword,
        loadPrivateKey: LoadPrivateKey
    ) {

        // #ifdef DEBUG
        log.constructor(1, this);
        // #endif

        this._parentViewModel = parentViewModel;
        this._viewModelServiceManager = viewModelServiceManager;
        this._loading = loading;
        this._errorMessage = error;
        this._privateKeyPassword = pkPwd;
        this._loadPrivateKey = loadPrivateKey;
    }

    public async init(): Promise<void> {
        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._menuVisible = false;
        this._navigation = <Navigation>await this._viewModelServiceManager.createChildViewModel(this, Navigation);

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif
    }

    public bindingsUpdated(properties: string[]) {
        // #ifdef DEBUG
        log.info(1, this, "Bindings updated: ", properties);
        for (const p of properties) {
            log.info(1, this, "   this." + p + " = ", (<any>this)[p]);
        }
        // #endif

        this._viewModelServiceManager.viewModelUpdated(this);
    }

    public dispose(): void {

        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._viewModelServiceManager.disposeChildViewModel(this, this._navigation);
        this._navigation = undefined;

        this._parentViewModel = undefined;
        this._viewModelServiceManager = undefined;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif

    }

    public hideMenu() {
        this._menuVisible = false;
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    protected _menuClick() {
        this._menuVisible = !this._menuVisible;
        this._viewModelServiceManager.viewModelUpdated(this);
    }

}