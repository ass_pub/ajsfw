/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { Param } from "../dto/Profile";

/**
 * Represents a dynamic form field contained in the FormGroup
 * This can be FormInput, FormSelect or FormPasswordVerification
 */
export interface FormField extends ViewModel {

    /**
     * Holds reference to the parameter defined by the profile
     */
    paramDTO: Param;

    /**
     * Field value
     */
    value: string;

    /**
     * Validates the field and returns the result of the validation
     */
    validate?(): boolean;

}