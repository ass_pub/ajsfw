/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";

@ajsUseTemplate("loading")
@ajsViewModel(IIViewModelServiceManager)
export class Loading implements ViewModel {

    protected _visible: boolean;
    protected _message: string;

    protected _showedTimes: number;

    protected _viewModelServiceManager: ViewModelServiceManager;

    constructor(viewModelServiceManager: ViewModelServiceManager) {
        // #ifdef DEBUG
        log.constructor(1, this);
        // #endif

        this._viewModelServiceManager = viewModelServiceManager;
    }

    public async init(): Promise<void> {
        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._visible = false;
        this._message = "";
        this._showedTimes = 0;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif
    }

    public show(message: string): void {
        this._showedTimes++;
        this._visible = true;
        this._message = message;
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    public hide(): void {
        this._showedTimes--;
        if (this._showedTimes <= 0) {
            this._showedTimes = 0;
            this._visible = false;
            this._viewModelServiceManager.viewModelUpdated(this);
        }
    }

    public dispose(): void {

        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._viewModelServiceManager = undefined;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif

    }

}