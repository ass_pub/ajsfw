/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { IIDependentService } from "ajsfw/core/services/Service";
import { AppConfig, IIAppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { IILang, Lang } from "ajsfw/services/lang/Lang";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { AjsSelect } from "ajsfw/services/mvvm/controls/AjsSelect";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { AjsVDomEventData } from "ajsfw/services/virtualview/AjsVDomEventData";
import { StringKeyValue } from "ajsfw/types";
import { CafeConfig } from "application/CafeConfig";
import { Ca } from "appservices/ca/Ca";
import { ProfileManager } from "appservices/ca/ProfileManager";
import { CryptoServices } from "appservices/cryptoservices/CryptoServices";
import { CsrServerResponse } from "dto/CsrServerResponse";
import { Group, Param } from "dto/Profile";
import { ErrorMessage } from "./ErrorMessage";
import { FormGroup } from "./FormGroup";
import { FormInput } from "./FormInput";
import { FormPasswordVerification } from "./FormPasswordVerification";
import { FormSelect } from "./FormSelect";
import { Loading } from "./Loading";
import { PrivateKeyPassword } from "./PrivateKeyPassword";
import { saveFile } from "ajsfw/lib/utils/saveFile";
import { RequestTracker } from "appservices/ca/RequestTracker";
import { IINavigationService, NavigationService } from "ajsfw/services/navigation/NavigationService";
import { Settings } from "appservices/user/Settings";
import { FormField } from "./FormField";

interface CsrInfo {
    date?: number;
    profileName?: string;
    commonName?: string;
    privateKeyPem: string;
    publicKeyPem: string;
    csrPem: string;
    winCryptoApi?: boolean;
    serverCryptoApi?: boolean;
}

@ajsUseTemplate("newrequest")
@ajsViewModel(
    IIDependentService,
    IIViewModelServiceManager,
    IIAppConfig,
    IILang,
    IINavigationService,
    Loading,
    ErrorMessage,
    PrivateKeyPassword,
    Ca,
    ProfileManager,
    CryptoServices,
    RequestTracker,
    Settings
)

export class NewRequest implements ViewModel {

    // View state properties and children view models

    protected _profile: AjsSelect;
    protected _method: AjsSelect;
    protected _keyType: AjsSelect;
    protected _hashAlg: AjsSelect;
    protected _csp: AjsSelect;

    protected _csrPreparationMethodHidden: boolean;
    protected _csrPreparationMethodSelectHidden: boolean;
    protected _csrPreparationMethodLoadHidden: boolean;
    protected _csrPreparationMethodGenerateHidden: boolean;
    protected _csrPreparationCspHidden: boolean;

    protected _csrFile: File[];
    protected _csrFileCleanup: boolean;
    protected _csrFileName: string;
    protected _csrLoadingError: string;
    protected _csrContent: string;
    protected _csrContentError: string;

    protected _formGroups: FormGroup[];

    protected _submitHidden: boolean;

    // Internal properties

    protected _initialized: boolean;
    protected _csrFileLoaded: boolean;

    protected _csps: StringKeyValue<number>;

    protected _cnFieldReference: FormInput;
    protected _dns0FieldReference: FormInput;

    // Injected dependencies

    protected _parentViewModel: ViewModel;
    protected _viewModelServiceManager: ViewModelServiceManager;
    protected _config: CafeConfig;
    protected _lang: Lang;
    protected _navigation: NavigationService;

    protected _loading: Loading;
    protected _errorMessage: ErrorMessage;
    protected _privateKeyPassword: PrivateKeyPassword;

    protected _ca: Ca;
    protected _profileManager: ProfileManager;
    protected _cryptoServices: CryptoServices;
    protected _requestTracker: RequestTracker;
    protected _userSettings: Settings;

    // Constructor

    constructor(
        parentViewModel: ViewModel,
        viewModelServiceManager: ViewModelServiceManager,
        config: AppConfig,
        lang: Lang,
        navigation: NavigationService,
        loading: Loading,
        errorMessage: ErrorMessage,
        passwordDialog: PrivateKeyPassword,
        ca: Ca,
        profileManager: ProfileManager,
        cryptoServices: CryptoServices,
        requestTracker: RequestTracker,
        settings: Settings
    ) {
        this._initialized = false;

        // #ifdef DEBUG
        log.constructor(1, this);
        // #endif

        this._parentViewModel = parentViewModel;
        this._viewModelServiceManager = viewModelServiceManager;
        this._config = config.data;
        this._lang = lang;
        this._navigation = navigation;
        this._errorMessage = errorMessage;
        this._privateKeyPassword = passwordDialog;
        this._loading = loading;

        this._ca = ca;
        this._profileManager = profileManager;
        this._cryptoServices = cryptoServices;
        this._requestTracker = requestTracker;
        this._userSettings = settings;
    }

    // Public methods

    public init(): void {
        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._init();

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif
    }

    public dispose(): void {
        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._cnFieldReference = undefined;
        this._dns0FieldReference = undefined;

        this._lang.langChangedNotifier.unsubscribe(this._langChanged);
        this._profile.onChange.unsubscribe(this._profileChanged, this);
        this._profile = this._viewModelServiceManager.disposeChildViewModel(this, this._profile);
        this._method.onChange.unsubscribe(this._methodChanged, this);
        this._method = this._viewModelServiceManager.disposeChildViewModel(this, this._method);
        this._keyType = this._viewModelServiceManager.disposeChildViewModel(this, this._keyType);
        this._hashAlg = this._viewModelServiceManager.disposeChildViewModel(this, this._hashAlg);
        this._csp = this._viewModelServiceManager.disposeChildViewModel(this, this._csp);

        this._lang = undefined;
        this._profile = undefined;
        this._parentViewModel = undefined;
        this._viewModelServiceManager = undefined;

        this._initialized = true;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif
    }

    /**
     * Initializes the NewRequest ViewModel
     */
    protected async _init(): Promise<void> {

        this._csrFileCleanup = true;
        this._csrContentError = "";
        this._csrFileLoaded = false;

        this._lang.langChangedNotifier.subscribe(this._langChanged, this);

        this._profile = <AjsSelect>await this._viewModelServiceManager.createChildViewModel(this, AjsSelect);
        this._profile.onChange.subscribe(this._profileChanged, this);

        this._method = <AjsSelect>await this._viewModelServiceManager.createChildViewModel(this, AjsSelect);
        this._method.onChange.subscribe(this._methodChanged, this);

        this._keyType = <AjsSelect>await this._viewModelServiceManager.createChildViewModel(this, AjsSelect);
        this._hashAlg = <AjsSelect>await this._viewModelServiceManager.createChildViewModel(this, AjsSelect);
        this._csp = <AjsSelect>await this._viewModelServiceManager.createChildViewModel(this, AjsSelect);

        await this._loadProfiles();

        this._initialized = true;

        this._update();
    }

    // event listeners

    /**
     * Called when user selects one of the profile options
     * @param data Ajs Event data
     */
    protected _profileChanged(data: AjsVDomEventData): void {

        this._cnFieldReference = undefined;
        this._dns0FieldReference = undefined;

        this._profileManager.currentProfileId = data.value;
        this._cleanCsrFile();
        this._generateForm();
        this._populateMethods();
        this._viewModelServiceManager.viewModelUpdated(this);

    }

    /**
     * Called when user selesct one of available request creation method
     * @param data Ajs Event data
     */
    protected _methodChanged(data: AjsVDomEventData): void {

        this._csrPreparationMethodLoadHidden = this._method.value !== "file";
        this._csrPreparationMethodGenerateHidden = this._method.value === "file";
        this._csrPreparationCspHidden = this._method.value !== "cryptoapi";

        if (this._method.value !== "file") this._populateKeyHash();

        if (this._method.value === "cryptoapi") this._populateCsps();

        this._viewModelServiceManager.viewModelUpdated(this);

    }

    /**
     * Called when the CSR file (input type file) is changed
     * @param data Ajs Event Data
     */
    protected _csrFileSelected(data: AjsVDomEventData): void {
        if (data.files.length === 0) return;
        this._loadCsrFile(data.files.item(0));
    }

    /**
     * Called when content of the CSR (text area) changes
     * @param data Ajs Event Data
     */
    protected async _csrContentChanged(data: AjsVDomEventData): Promise<void> {

        this._csrContent = data.value;
        let pkcs10: any;

        try {
            this._csrContentError = "";
            if (this._csrContent !== "") {
                pkcs10 = this._cryptoServices.csrPemToPkcs10(this._csrContent);
            } else {
                this._viewModelServiceManager.viewModelUpdated(this);
                return;
            }
        } catch (e) {
            this._csrContentError = this._lang.translate("newRequest.csrContentError");
        }

        // clean the form by regenerating it
        // await this._generateForm();

        this._loadFormWithPKCS10Data(pkcs10);
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    /**
     * Called when one of drag & drop events occur
     * @param data Ajs Event Data
     */
    protected _csrFileDraggedDropped(data: AjsVDomEventData): void {

        if (data.eventType === "drop" && this._method.value === "file") {
            if (!data.dataTransfer || !data.dataTransfer.files || data.dataTransfer.files.length === 0) return;
            this._loadCsrFile(data.dataTransfer.files.item(0));
        }

        data.cancelBubble = true;
        data.preventDefault = true;
    }

    /**
     * Called when submit button is clicked
     * @param data Ajs Event Data
     */
    protected _submit(data: AjsVDomEventData): void {

        data.cancelBubble = true;
        data.preventDefault = true;

        const isIE11 = !!(<any>window).MSInputMethodContext && !!(<any>document).documentMode;

        if (this._profileManager.currentProfile.request &&
            this._profileManager.currentProfile.request.keystores.length === 1 &&
            this._profileManager.currentProfile.request.keystores[0] === "csp" && !isIE11) {
                this._errorMessage.show(
                    this._lang.translate("newRequest.submitErrorNotIECaption"),
                    this._lang.translate("newRequest.submitErrorNotIE")
                );
                return;
            }

        if (!this._validateForm()) {
            this._errorMessage.show(
                this._lang.translate("newRequest.submitErrorsCaption"),
                this._lang.translate("newRequest.submitErrors")
            );
            return;
        }

        this._sendCertificateRequest();

    }

    /**
     * Called when the language is changed
     * @param sender AjsLang object
     * @param code Language code currently in use
     */
    protected _langChanged(sender: any, code: string): void {

        if (!this._initialized) return;

        this._update(true);

        // just to translate errors
        this._validateForm();
    }

    /**
     * Loads a csr file from the local filesystem based on the input or drag & drop event
     * @param file File object (from input type file or drag & drop event) specyfying the file to be loaded
     */
    protected async _loadCsrFile(file: File): Promise<void> {

        const reader = new FileReader();
        reader.onload = () => {
            try {
                // this is just to check if the file is OK
                this._csrFileCleanup = true;
                this._cryptoServices.csrPemToPkcs10(<string>reader.result);
                this._csrFileLoaded = true;
                this._csrLoadingError = "";
            } catch (e) {
                this._cleanCsrFile();
                this._csrLoadingError = this._lang.translate("newRequest.csrLoadingError");
            }

            this._csrContentChanged(<any>{ value: reader.result });
        };
        this._csrFileName = file.name;
        reader.readAsText(file);

    }

    /**
     * Called internally to update view model state
     * @param update If update is not passed the complete form is refreshed, otherwise only properties related to language are updated
     */
    protected _update(update = false): void {
        if (!this._initialized) return;
        this._populateProfiles();
        this._populateMethods(update);
        this._generateForm(update);
    }

    /**
     * Uses the profile manager to loads list of profiles from the server
     */
    protected async _loadProfiles(): Promise<void> {
        this._cleanCsrFile();
        this._loading.show(this._lang.translate("newRequest.loadingProfiles"));
        await this._profileManager.loadProfiles();
        this._loading.hide();
    }

    /**
     * Cleans the content of the CSR file
     */
    protected _cleanCsrFile(): void {
        this._csrFile = undefined;
        this._csrFileName = this._lang.translate("newRequest.csrNotLoaded");
        this._csrLoadingError = "";
        this._csrContent = "";
        this._csrFileLoaded = false;
    }

    /**
     * Populates the list of profiles
     */
     protected _populateProfiles(): void {

        this._profile.clear();
        for (const profile of this._profileManager.profiles) {
            this._profile.items.push({
                selected: profile.profileId === this._profileManager.currentProfileId,
                value: profile.profileId,
                label: profile.langProfileName[this._lang.code]
            });
        }

        this._submitHidden = this._profile.items.length === 0;

        this._profile.updated();

    }

    /**
     * Populates the list of methods for certificate / private key creation
     * Furthermore, populates key generation fields if necessary
     */
    protected _populateMethods(update = false): void {

        const methodSelectedIndex = update ? this._method.selectedIndex : 0;

        this._method.clear();

        let methodsCount = 0;

        if (this._profileManager.currentProfile && this._profileManager.currentProfile.request) {
            for (const ks of this._profileManager.currentProfile.request.keystores) {
                if (ks === "unmanaged") { this._addUnmanaged(); methodsCount++; }
                if (ks === "pfx") { this._addPfx(); methodsCount++; }
                if (ks === "csp") { if (this._addCryptoApi()) methodsCount++; }
            }
            this._method.itemsUpdated();
            this._method.selectedIndex = methodSelectedIndex;
        }

        if (this._method.value !== "file" && methodsCount > 0) this._populateKeyHash(update);
        if (this._method.value === "cryptoapi") this._populateCsps(update);

        if (methodsCount > 1) this._csrPreparationMethodSelectHidden = false;
        if (methodsCount > 0) this._csrPreparationMethodHidden = false;

        this._csrPreparationMethodHidden = methodsCount === 0;
        this._csrPreparationMethodSelectHidden = methodsCount < 2;
        this._csrPreparationMethodLoadHidden = this._method.value !== "file";
        this._csrPreparationMethodGenerateHidden = this._method.value === "file";
        this._csrPreparationCspHidden = this._method.value !== "cryptoapi";

    }

    /**
     * Adds the "unmanaged" option to the methods selection box if the method is valid for the profile
     */
    protected _addUnmanaged(): void {
        this._method.items.push({
            selected: false,
            value: "file",
            label: this._lang.translate("newRequest.methodFile")
        });
        this._csrPreparationMethodLoadHidden = false;
    }

    /**
     * Adds the "pfx" option to the methods selection box if the method is valid for the profile
     */
    protected _addPfx(): void {
        this._method.items.push({
            selected: this._method.value === "browser",
            value: "browser",
            label: this._lang.translate("newRequest.methodBrowser")
        });
    }

    /**
     * Adds the "csp" option to the methods selection box if the method is valid for the profile
     */
    protected _addCryptoApi(): boolean {
        try {
            this._csps = this._csps || this._cryptoServices.getCSPs();
            const isIE11 = !!(<any>window).MSInputMethodContext && !!(<any>document).documentMode;
            if (!isIE11 || this._profileManager.currentProfile.request.keystores.indexOf("csp") === -1) return false;
            this._method.items.push({
                selected: false,
                value: "cryptoapi",
                label: this._lang.translate("newRequest.methodCryptoApi")
            });
            return true;
        } catch (e) {
            return false;
        }
    }

    /**
     * Populates hashes and key types select according to the selected profile
     * @param update If update is set to true, the previous selection is kept
     */
    protected _populateKeyHash(update = false): void {

        const keySelIndex = update ? this._keyType.selectedIndex : 0;
        const hashSelIndex = update ? this._hashAlg.selectedIndex : 0;

        this._keyType.clear();
        this._hashAlg.clear();

        for (const k of this._profileManager.currentProfile.request.keys) {
            this._keyType.items.push({ selected: false, value: k, label: this._config.keyMappings[k].label });
        }

        this._keyType.itemsUpdated();
        this._keyType.selectedIndex = keySelIndex;

        for (const h of this._profileManager.currentProfile.request.hashAlgorithms) {
            this._hashAlg.items.push({ selected: false, value: h, label: this._config.hashMappings[h].label });
        }

        this._hashAlg.itemsUpdated();
        this._hashAlg.selectedIndex = hashSelIndex;

    }

    /**
     * Populates list of CSPs based on the current system available CSPs and CSPs defined in currently selected profile
     * @param update If update is set to true, the previous selection is kept
     */
    protected _populateCsps(update = false): void {
        const selIndex = update ? this._csp.selectedIndex : 0;
        this._csp.clear();
        for (const csp of this._profileManager.currentProfile.request.csps) {
            if (this._csps.hasOwnProperty(csp)) {
                this._csp.items.push({ selected: this._csp.value === csp, value: csp, label: csp });
            }
        }
        this._csp.selectedIndex = selIndex;
    }

    /**
     * Generates the dynamic form based on currently selected profile
     * @param update If update is false the complete form is generated, otherwise only language related properties are updated
     */
    protected async _generateForm(update = false): Promise<void> {

        if (!this._profileManager.currentProfile) {
            // #ifdef DEBUG
            log.warning(this, "No profile selected, unable to generate the form!");
            return;
            // #endif
        }

        await this._generateFormGroups(update);

        if (!update && this._cnFieldReference && this._dns0FieldReference) {
            this._cnFieldReference.changed.subscribe(() => {
                this._dns0FieldReference.value = this._cnFieldReference.value;
                this._dns0FieldReference.validate();
            });
        }

        this._viewModelServiceManager.viewModelUpdated(this);
    }

    /**
     * Generates form groups
     * @param update If update is false the complete form is generated, otherwise only language related properties are updated
     */
    protected async _generateFormGroups(update: boolean): Promise<void> {

        if (!update) this._formGroups = [];

        for (const paramGroup of this._profileManager.currentProfile.groups) {

            if (paramGroup.type === "explicit") {

                let group: FormGroup;

                if (update) {
                    for (const existingGroup of this._formGroups) {
                        if (existingGroup.groupDTO === paramGroup) group = existingGroup;
                    }
                } else {
                    group = <FormGroup>await this._viewModelServiceManager.createChildViewModel(this, FormGroup);
                    group.groupDTO = paramGroup;
                    this._formGroups.push(group);
                }

                group.label = paramGroup.langGroupName[this._lang.code];

                await this._generateGroupFields(paramGroup, group, update);
            }

        }

    }

    /**
     * Generates fields of the parameter group based on the currently selected profile
     * @param paramGroup Parameter group from the profile
     * @param formGroup Form group created by the _generateForm method
     * @param update If update is false the complete form is generated, otherwise only language related properties are updated
     */
    protected async _generateGroupFields(paramGroup: Group, formGroup: FormGroup, update: boolean): Promise<void> {

        if (!update) formGroup.formFields = [];

        for (const param of paramGroup.params) {

            let cachedData: StringKeyValue<string> = {};

            if (paramGroup.cacheable === "TRUE") {
                cachedData = this._userSettings.settings.newRequestCachableGroups[paramGroup.id] || {};
            }

            switch (param.type) {
                case "text": // text and password are same fields (templates) with a different type
                case "password":
                    await this._generateFormText(cachedData[param.id], param, formGroup, update);
                    break;

                case "select":
                    await this._generateFormSelect(cachedData[param.id], param, formGroup, update);
                    break;

                case "password_verification":
                    await this._generateFormPasswordVerification(param, formGroup, update);
                    break;

            }

        }

    }

    /**
     * Generates or updates select with options according to the param passed
     * @param param Profile parameter containing select options and field settings
     * @param formGroup Group to which the select will be added
     * @param update If update is set to true the select is not created but reused and its help, label and error messages are updated
     */
    protected async _generateFormSelect(cachedData: string, param: Param, formGroup: FormGroup, update = false): Promise<void> {

        let select: FormSelect;

        if (update) {
            for (const existingSelect of formGroup.formFields) {
                if ((<FormSelect>existingSelect).paramDTO === param) select = <FormSelect>existingSelect;
            }
        } else {
            select = <FormSelect>await this._viewModelServiceManager.createChildViewModel(this, FormSelect);
            select.paramDTO = param;
            formGroup.formFields.push(select);
            select.value = param.values.length > 0 ? param.values[0] : "";

            for (const value of param.values) {
                select.select.items.push({
                    selected: cachedData ? cachedData === value : select.value === value,
                    label: value,
                    value
                });
            }
        }

        select.label = param.langParamName ? param.langParamName[this._lang.code] + (param.mandatory ? " *" : "") : "";
        select.help = param.langParamHelp ? param.langParamHelp[this._lang.code] || "" : "";
        select.error = param.langParamError ? param.langParamError[this._lang.code] || "&nbsp;" : "&nbsp;";

    }

    /**
     * Generates or updates input (text/password) with options according to the param passed
     * @param param Profile parameter containing the input field settings (profile.type decides if text or passsword field will be shown)
     * @param formGroup Group to which the select will be added
     * @param update If update is set to true the input is not created but reused and its help, label and error messages are updated
     */
    protected async _generateFormText(cachedData: string, param: Param, formGroup: FormGroup, update = false): Promise<void> {

        let input: FormInput;

        if (update) {
            for (const existingInput of formGroup.formFields) {
                if ((<FormInput>existingInput).paramDTO === param) input = <FormInput>existingInput;
            }
        } else {
            input = <FormInput>await this._viewModelServiceManager.createChildViewModel(this, FormInput);
            input.paramDTO = param;
            input.type = param.type === "password" ? "password" : "text";
            input.value = cachedData && param.type !== "password" ? cachedData : "";
            // clean error - it will be shown later
            input.error = "&nbsp;";
            formGroup.formFields.push(input);

            if (param.id === "cn") this._cnFieldReference = input;
            if (param.id === "dns0") this._dns0FieldReference = input;
        }

        input.label = param.langParamName ? param.langParamName[this._lang.code] + (param.mandatory === "TRUE" ? " *" : "") : "";
        input.help = param.langParamHelp ? param.langParamHelp[this._lang.code] || "" : "";
    }

    /**
     * Generates or updates password verification field (two password fields)
     * @param param Profile parameter containing the password verification field settings
     * @param formGroup Group to which the password verification field will be added
     * @param update If update is set to true the PVF created but reused and its help, label and error messages are updated
     */
    protected async _generateFormPasswordVerification(param: Param, formGroup: FormGroup, update = false): Promise<void> {

        let pwdVerification: FormPasswordVerification;

        if (update) {
            for (const existingPwdVerification of formGroup.formFields) {
                if (existingPwdVerification.paramDTO === param) pwdVerification = <any>existingPwdVerification;
            }
        } else {
            pwdVerification = <FormPasswordVerification>await this._viewModelServiceManager.createChildViewModel(this, FormPasswordVerification);
            pwdVerification.paramDTO = param;
            pwdVerification.type = param.type === "password" ? "password" : "text";
            pwdVerification.value = "";
            pwdVerification.value2 = "";
            // clean error - it will be shown later
            pwdVerification.error1 = "&nbsp;";
            pwdVerification.error2 = "&nbsp;";
            formGroup.formFields.push(pwdVerification);
        }

        pwdVerification.label = param.langParamName ? param.langParamName[this._lang.code] + (param.mandatory === "TRUE" ? " *" : "") : "";
        pwdVerification.help = param.langParamHelp ? param.langParamHelp[this._lang.code] || "" : "";
    }

    /**
     * Loads a form with the PKCS#10 data loaded from the file
     * @param pkcs10 PKCS#10 object in node-forge format
     */
    protected _loadFormWithPKCS10Data(pkcs10: any): void {

        const cn = this._loadFormWithPKCS10attributes(pkcs10);
        this._loadFormWithPKCS10extensions(pkcs10, cn);

    }

    /**
     * Loads a form with the PKCS#10 attributes
     * @param pkcs10 PKCS#10 object in node-forge format
     * @returns common name if contained withing the request, otherwise undefined
     */
    protected _loadFormWithPKCS10attributes(pkcs10: any): string {

        let cn: string;

        for (const attr of pkcs10.subject.attributes) {
            const remapped = this._mapPKCS10AttrName(attr.name);
            if (remapped === "cn") cn = attr.value;
            const field = this._findField(remapped);
            if (field) field.value = attr.value;
        }

        return cn;

    }

    /**
     * Remaps node-forge DN to server-required DN
     * @param name
     */
    protected _mapPKCS10AttrName(name: string): string {

        for (const key in this._config.distinguishedNameMappings) {
            if (this._config.distinguishedNameMappings[key] === name) return key;
        }
        return name;

    }

    /**
     * Loads a form with the PKCS#10 extensions
     * @param pkcs10 PKCS#10 object in node-forge format
     * @param cn Common name (used for dns0)
     */
    protected _loadFormWithPKCS10extensions(pkcs10: any, cn: string): void {

        let dnsCounter = 0;

        for (const attr of pkcs10.attributes) {

            if (attr.name !== "extensionRequest") continue;

            for (const extension of attr.extensions) {
                if (extension.name !== "subjectAltName") continue;

                for (const altName of extension.altNames) {
                    // DNS
                    if (altName.type === 2) {
                        const field = this._findField("dns" + dnsCounter);
                        if (field) {
                            if (altName.value === cn && dnsCounter > 0) {
                                const dns0 = this._findField("dns0");
                                if (dns0) {
                                    field.value = dns0.value;
                                    dns0.value = altName.value;
                                }
                            } else {
                                field.value = altName.value;
                            }
                        }
                        dnsCounter++;
                    }
                }
            }
        }

        if (dnsCounter === 0 && cn) {
            const dns0 = this._findField("dns0");
            if (dns0) dns0.value = cn;
        }

        this._viewModelServiceManager.viewModelUpdated(this);

    }

    /**
     * Tries to locate a field in the locally replerented param group (form group) and param (form field) tree
     * @param id Id of the field (paramId from the params)
     */
    protected _findField(id: string): FormField {
        if (!this._formGroups) return;

        for (const group of this._formGroups) {
            for (const field of group.formFields) {
                if (field.paramDTO.id === id) return field;
            }
        }
    }

    /**
     * Validates the whole form and returns true if the form is valid according the current profile
     */
    protected _validateForm(): boolean {

        let valid = true;

        for (const group of this._formGroups) {
            for (const field of group.formFields) {
                if (field.validate) {
                    const fieldValid = field.validate();
                    valid = valid && fieldValid;
                }
            }
        }

        if (!valid) {
            this._viewModelServiceManager.viewModelUpdated(this);
        }

        return valid;
    }

    /**
     * Collects the generated form data for request submitting
     */
    protected _collectGeneratedFormData(): StringKeyValue<string> {

        const args: StringKeyValue<string> = {};
        let updateCache = false;

        // collect form values
        for (const group of this._formGroups) {

            let cachedData: StringKeyValue<string> = {};

            if (group.groupDTO.cacheable === "TRUE") {
                this._userSettings.settings.newRequestCachableGroups = this._userSettings.settings.newRequestCachableGroups || {};
                cachedData = this._userSettings.settings.newRequestCachableGroups[group.groupDTO.id] || {};
            }

            for (const field of group.formFields) {
                 args[field.paramDTO.id] = field.value;

                 if (group.groupDTO.cacheable === "TRUE") {
                     cachedData[field.paramDTO.id] = field.value;
                     updateCache = true;
                 }
            }

            if (group.groupDTO.cacheable === "TRUE") {
                // TODO: Rework to group ID when available
                this._userSettings.settings.newRequestCachableGroups[group.groupDTO.id] = cachedData;
            }

        }

        if (updateCache) {
            this._userSettings.updateSettings();
        }

        return args;
    }

    /**
     * Collects form data, builds certification request and sends it to server
     */
    protected async _sendCertificateRequest(): Promise<void> {

        const formData = this._collectGeneratedFormData();
        const date = new Date(Date.now());
        const profileName = this._profileManager.currentProfile.langProfileName[this._lang.code];
        const cn = formData.cn ? formData.cn : this._config.ca.commonName + " - " + (formData.cn || profileName) + " - " + date.toLocaleDateString() + " " + date.toLocaleTimeString();

        let response: string;
        let csrInfo: CsrInfo;

        switch (this._method.value) {
            case "file":
                csrInfo = <any>{};
                formData["pkcs10"] = this._csrContent;
                break;
            case "browser":
                csrInfo = await this._pkcs10KeyPairUsingWebCrypto(cn);
                formData["pkcs10"] = csrInfo.csrPem;
                break;
            case "cryptoapi":
                csrInfo = <any>{};
                csrInfo.winCryptoApi = true;
                formData["pkcs10"] = await this._pkcs10UsingWinApi(cn);
                break;
            default:
                csrInfo = <any>{};
                csrInfo.serverCryptoApi = true;
                break;
        }

        if (csrInfo) {
            csrInfo.commonName = cn;
            csrInfo.date = date.getTime();
            csrInfo.profileName = profileName;
        }

        try {
            this._loading.show(this._lang.translate("newRequest.serverRequest"));
            response = await this._ca.sendRequest(formData);
            // tslint:disable-next-line:quotemark
            // response = ' { "requestStatus": "pending", "requestNumber": "' + prompt("request no") + '" } ';
        } catch (e) {
            this._loading.hide();
            this._errorMessage.show(
                this._lang.translate("newRequest.serverRequestFailed"),
                e.toString()
            );
            return;
        }

        this._loading.hide();
        this._processCertificateSignRequestResponse(response, csrInfo);

    }

    /**
     * Generates the pkcs#10 CSR while keys are generated using the Web Crypto API and CSR signed using the node-forge library
     * Public and private keys are returned for further operations
     */
    protected async _pkcs10KeyPairUsingWebCrypto(cn: string): Promise<CsrInfo> {

        // create key pair according to entered parameters
        const key = this._config.keyMappings[this._keyType.value].web;
        const hash = this._config.hashMappings[this._hashAlg.value].web;
        this._loading.show(this._lang.translate("newRequest.generatingKeyPair"));
        const keyPair = await this._cryptoServices.generateKeyPair(key, hash);
        this._loading.hide();

        // create dummy PKCS10 and sign it using the private key
        const pkcs10 = await this._cryptoServices.pkcs10(cn, keyPair.privatePem, keyPair.publicPem);

        return {
            privateKeyPem: keyPair.privatePem,
            publicKeyPem: keyPair.publicPem,
            csrPem: pkcs10
        };
    }

    /**
     * Generates the pkcs#10 CSR while keys are generated using Windows Crypto API and also the CSR is generated using it
     * Private key is usually kept in secure storage (according to CSP) and is not returned
     */
    protected async _pkcs10UsingWinApi(cn: string): Promise<string> {
        const key = this._config.keyMappings[this._keyType.value].win;
        const hash = this._config.hashMappings[this._hashAlg.value].win;
        const exportable = this._profileManager.currentProfile.request.exportable;
        const protection = this._profileManager.currentProfile.request.keyProtection;

        this._loading.show(this._lang.translate("newRequest.generatingCSR"));

        try {
            const pkcs10 = await this._cryptoServices.pkcs10IE11(cn, this._csp.value, key, hash, exportable, protection);
            this._loading.hide();
            return pkcs10;
        } catch (e) {
            this._loading.hide();
            this._errorMessage.show(this._lang.translate("newRequest.generatingCSRErrorWinAPI"), e);
            throw e;
        }
    }

    /**
     * Processes the CA CSR signing response and based on result and previous settings:
     * - allows importing of private key and exporting PFX of completed request (when key was generated using Web Crypto)
     * - allows saving of the signed response
     * - stores information about sent CSR to localStorage (pending, fail)
     * - shows error when error occured during CSR request
     * @param response
     */
    protected async _processCertificateSignRequestResponse(response: string, csrInfo: CsrInfo): Promise<void> {

        let respObject: CsrServerResponse;

        try {
            respObject = JSON.parse(response);
        } catch (e) {
            this._errorMessage.show(
                this._lang.translate("newRequest.unexpectedServerResponse"),
                response ? response.substr(0, 30) + "..." : ""
            );
            return;
        }

        switch (respObject.requestStatus) {
            case "complete":
                this._processCompleted(respObject, csrInfo);
                break;

            case "pending":
                this._processPending(respObject, csrInfo);
                break;

            case "failed":
                this._errorMessage.show(
                    this._lang.translate("newRequest.certificateRequestFailed"),
                    respObject.errorReason
                );
                return;
        }

    }

    protected async _processCompleted(respObject: CsrServerResponse, csrInfo: CsrInfo): Promise<void> {

        const date = new Date(csrInfo.date);
        const certChain: string[] = [];

        if (csrInfo.privateKeyPem) {
            const password = await this._privateKeyPassword.getPassword();

            if (!password) {
                this._errorMessage.show(
                    this._lang.translate("newRequest.error"),
                    this._lang.translate("newRequest.privateKeyNotSaved"),
                );
                return;
            }

            const p12 = this._cryptoServices.createPfx(respObject.certificate, certChain, csrInfo.privateKeyPem, password);
            saveFile(csrInfo.commonName + ".pfx", p12, "application/x-pkcs12");
        }

        for (const c of respObject.chain) certChain.push(c.certificate);

        /*
        TODO: Let user select the format of the certificate
        if (csrInfo.winCryptoApi) {
            const p7b = this._cryptoServices.createP7B(respObject.certificate, certChain);
            saveFile(csrInfo.commonName + ".p7b", p7b, "application/x-pkcs7-certificates");
        }
        */

        if (csrInfo.winCryptoApi) {
            saveFile(csrInfo.commonName + ".cer", respObject.certificate, "application/x-x509-user-cert");
        }

        if (csrInfo.serverCryptoApi) {
            saveFile(csrInfo.commonName + ".pfx", respObject.pkcs12, "application/x-pkcs12");
        }

        this._requestTracker.addTdri({
            requestId: respObject.requestNumber,
            commonName: csrInfo.commonName,
            privateKey: false,
            profileName: csrInfo.profileName,
            certificate: respObject.certificate,
            chain: certChain,
            requestDate:  date.toLocaleDateString() + " " + date.toLocaleTimeString(),
            status: "Completed"
        });

        this._profileChanged(<any>{ value: this._profileManager.currentProfileId });

        this._navigation.navigate("./PendingRequests?id=" + respObject.requestNumber);

    }

    protected async _processPending(respObject: CsrServerResponse, csrInfo: CsrInfo): Promise<void> {

        if (csrInfo.privateKeyPem) {
            if (!await this._savePrivateKey(csrInfo.commonName, csrInfo.privateKeyPem)) return;
        }

        const date = new Date(csrInfo.date);

        this._requestTracker.addTdri({
            requestId: respObject.requestNumber,
            commonName: csrInfo.commonName,
            privateKey: csrInfo.privateKeyPem !== undefined,
            profileName: csrInfo.profileName,
            certificate: undefined,
            chain: [],
            requestDate:  date.toLocaleDateString() + " " + date.toLocaleTimeString(),
            status: "Pending"
        });

        this._profileChanged(<any>{ value: this._profileManager.currentProfileId });
        this._navigation.navigate("./PendingRequests?id=" + respObject.requestNumber);

    }

    protected async _savePrivateKey(cn: string, privateKeyPem: string): Promise<boolean> {
        const password = await this._privateKeyPassword.getPassword();

        if (!password) {
            this._errorMessage.show(
                this._lang.translate("newRequest.error"),
                this._lang.translate("newRequest.privateKeyNotSaved"),
            );
            return false;
        }

        const encryptedKey = this._cryptoServices.encryptPrivateKey(privateKeyPem, password);
        saveFile(cn + ".key", encryptedKey, "application/pkcs8");

        return true;
    }

}
