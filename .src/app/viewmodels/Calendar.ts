/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { AjsVDomEventData } from "ajsfw/services/virtualview/AjsVDomEventData";

@ajsUseTemplate("calendar")
@ajsViewModel(IIViewModelServiceManager)
export class Calendar implements ViewModel {

    // List of months to be used - This can be translated and passed by app followed by call to update to render the translated month
    public months: string[];

    // List of short day names to be used - This can be translated and passed by app followed by call to update to render the translated month
    public days: string[];

    // First day of a week
    public fistDayOfWeek: number;

    public get date(): Date { return this._date; }
    public set date(value: Date) { this._setDate(value); }

    // date choosen from the Calendar widget or externally set
    protected _date: Date;

    // Year data to be rendered
    protected _year: number;
    // Month data to be rendered
    protected _month: string;
    // Day data to be rendered
    protected _dayData: string[][];

    protected _viewModelServiceManager: ViewModelServiceManager;

    constructor(viewModelServiceManager: ViewModelServiceManager) {
        // #ifdef DEBUG
        log.constructor(1, this);
        // #endif

        this._viewModelServiceManager = viewModelServiceManager;

        // this.months = [ "January", "February", "March", "April", "May", "June", "July", "August", "October", "November", "December" ];
        // this.days = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
        this.months = [ "Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec" ];
        this.days = [ "Ne", "Po", "Út", "St", "Čt", "Pá", "So"];
        this.fistDayOfWeek = 1;
    }

    public async init(): Promise<void> {
        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._date = new Date();
        this._populateCalendar();

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif
    }

    public dispose(): void {

        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._viewModelServiceManager = undefined;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif

    }

    protected _onYearMinusClick(event: AjsVDomEventData): void {
        this._date.setUTCFullYear(this._date.getUTCFullYear() - 1);
        this._populateCalendar();
    }

    protected _onYearPlusClick(event: AjsVDomEventData): void {
        this._date.setUTCFullYear(this._date.getUTCFullYear() + 1);
        this._populateCalendar();
    }

    protected _onMonthMinusClick(event: AjsVDomEventData): void {
        this._date.setUTCMonth(this._date.getUTCMonth() - 1);
        this._populateCalendar();
    }

    protected _onMonthPlusClick(event: AjsVDomEventData): void {
        this._date.setUTCMonth(this._date.getUTCMonth() + 1);
        this._populateCalendar();
    }

    protected _onDateClick(event: AjsVDomEventData): void {
        console.log(event);
    }

    protected _setDate(value: Date): void {
        this._date = value;
        this._populateCalendar();
    }

    protected _getDate(): number {
        return 0;
    }

    protected _populateCalendar(): void {

        this._computeElements();

        const firstDay = new Date(this._date.getTime());
        const lastDay = new Date(this._date.getTime());
        firstDay.setDate(1);
        lastDay.setUTCMonth(lastDay.getUTCMonth() + 1);
        lastDay.setDate(0);


        this._dayData = [];

        let i = 0;
        let day = this.fistDayOfWeek;

        let days: string[] = [];

        while (i < 7) {
            if (day > 6) day = 0;
            days.push(this.days[day]);
            i++;
            day++;
        }
        this._dayData.push(days);

        const ld = lastDay.getUTCDate();
        let d = 1;
        let col = 0;

        days = [];

        let empty = firstDay.getDay() - this.fistDayOfWeek;
        if (empty < 0) empty = 7 + empty;

        for (let i = 0; i < empty; i++) {
            days.push("");
            col++;
        }

        while (d <= ld) {

            days.push(d.toString());
            d++;

            col++;

            if (col > 6) {
                col = 0;
                this._dayData.push(days);
                days = [];
            }

        }
        if (days.length > 0) this._dayData.push(days);

        this._viewModelServiceManager.viewModelUpdated(this);
    }

    protected _computeElements(): void {
        this._year = this._date.getUTCFullYear();
        this._month = this.months[this._date.getUTCMonth()];
    }


}