/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { IIDependentService } from "ajsfw/core/services/Service";
import { saveFile } from "ajsfw/lib/utils/saveFile";
import { IILang, Lang } from "ajsfw/services/lang/Lang";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { AjsVDomEventData } from "ajsfw/services/virtualview/AjsVDomEventData";
import { urlBindSearch } from "ajsfw/services/urlbinding/urlBindSearch.decorator";
import { RequestTracker } from "appservices/ca/RequestTracker";
import { CryptoServices } from "appservices/cryptoservices/CryptoServices";
import { ListView } from "./ListView";
import { LoadPrivateKey } from "./LoadPrivateKey";
import { ErrorMessage } from "./ErrorMessage";
import { AjsButton } from "ajsfw/services/mvvm/controls/AjsButton";

@ajsUseTemplate("pendingrequests")
@ajsViewModel(
    IIDependentService,
    IIViewModelServiceManager,
    IILang,
    ErrorMessage,
    LoadPrivateKey,
    RequestTracker,
    CryptoServices
)
export class PendingRequests implements ViewModel {

    @urlBindSearch("id")
    protected _id: number;

    protected _requestIdAdd: string;

    protected _tcrDeleteButton: AjsButton;
    protected _tcrDownloadButton: AjsButton;
    protected _tcrRefreshButton: AjsButton;
    protected _acrAddButton: AjsButton;
    protected _acrDeleteButton: AjsButton;
    protected _acrDownloadButton: AjsButton;
    protected _acrRefreshButton: AjsButton;

    protected _thisDeviceRequests: ListView;
    protected _anotherDeviceRequests: ListView;

    protected _parentViewModel: ViewModel;
    protected _viewModelServiceManager: ViewModelServiceManager;
    protected _lang: Lang;
    protected _erroroMessage: ErrorMessage;
    protected _requestTracker: RequestTracker;
    protected _cryptoServices: CryptoServices;

    protected _loadPrivateKey: LoadPrivateKey;

    protected _initialized: boolean;


    constructor(
        parentViewModel: ViewModel,
        viewModelServiceManager: ViewModelServiceManager,
        lang: Lang,
        errorMessage: ErrorMessage,
        loadPrivateKey: LoadPrivateKey,
        requestTracker: RequestTracker,
        cryptoServices: CryptoServices
    ) {

        // #ifdef DEBUG
        log.constructor(1, this);
        // #endif

        this._parentViewModel = parentViewModel;
        this._viewModelServiceManager = viewModelServiceManager;
        this._lang = lang;
        this._erroroMessage = errorMessage;
        this._loadPrivateKey = loadPrivateKey;
        this._requestTracker = requestTracker;
        this._cryptoServices = cryptoServices;

        this._lang.langChangedNotifier.subscribe(this._langChanged, this);
        this._requestTracker.statusChangeNotifier.subscribe(this._statusChanged, this);

        this._initialized = false;

    }

    public init(): void {
        // #ifdef DEBUG
        log.enter(1, this);
        this._init();
        log.exit(1, this);
        // #endif
    }

    public bindingsUpdated(props: string[]): void {
        if (this._initialized) this._update();
    }

    public bound(): void {
        if (this._initialized) this._update();
    }

    public dispose(): void {

        // #ifdef DEBUG
        log.enter(1, this);
        // #endif


        this._tcrDeleteButton.onClick.unsubscribe(this._onTcrDeleteClick, this);
        this._tcrDownloadButton.onClick.unsubscribe(this._onTcrDownloadClick, this);
        this._tcrRefreshButton.onClick.unsubscribe(this._onTcrRefreshClick, this);
        this._acrAddButton.onClick.unsubscribe(this._onAcrAddClick, this);
        this._acrDeleteButton.onClick.unsubscribe(this._onAcrDeleteClick, this);
        this._acrDownloadButton.onClick.unsubscribe(this._onAcrDownloadClick, this);
        this._acrRefreshButton.onClick.unsubscribe(this._onAcrRefreshClick, this);

        this._tcrDeleteButton = this._viewModelServiceManager.disposeChildViewModel(this, this._tcrDeleteButton);
        this._tcrDownloadButton = this._viewModelServiceManager.disposeChildViewModel(this, this._tcrDeleteButton);
        this._tcrRefreshButton = this._viewModelServiceManager.disposeChildViewModel(this, this._tcrDeleteButton);
        this._acrAddButton = this._viewModelServiceManager.disposeChildViewModel(this, this._acrAddButton);
        this._acrDeleteButton = this._viewModelServiceManager.disposeChildViewModel(this, this._tcrDeleteButton);
        this._acrDownloadButton = this._viewModelServiceManager.disposeChildViewModel(this, this._tcrDeleteButton);
        this._acrRefreshButton = this._viewModelServiceManager.disposeChildViewModel(this, this._tcrDeleteButton);

        this._requestTracker.statusChangeNotifier.unsubscribe(this._statusChanged, this);
        this._thisDeviceRequests.onSelectionChanged = undefined;
        this._thisDeviceRequests = this._viewModelServiceManager.disposeChildViewModel(this, this._thisDeviceRequests);
        this._anotherDeviceRequests.onSelectionChanged = undefined;
        this._anotherDeviceRequests = this._viewModelServiceManager.disposeChildViewModel(this, this._anotherDeviceRequests);
        this._lang.langChangedNotifier.unsubscribe(this._langChanged);
        this._lang = undefined;
        this._parentViewModel = undefined;
        this._viewModelServiceManager = undefined;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif

    }

    protected async _init(): Promise<void> {

        this._requestIdAdd = "";

        this._tcrDeleteButton = <AjsButton>await this._viewModelServiceManager.createChildViewModel(this, AjsButton);
        this._tcrDownloadButton = <AjsButton>await this._viewModelServiceManager.createChildViewModel(this, AjsButton);
        this._tcrRefreshButton = <AjsButton>await this._viewModelServiceManager.createChildViewModel(this, AjsButton);
        this._acrAddButton = <AjsButton>await this._viewModelServiceManager.createChildViewModel(this, AjsButton);
        this._acrDeleteButton = <AjsButton>await this._viewModelServiceManager.createChildViewModel(this, AjsButton);
        this._acrDownloadButton = <AjsButton>await this._viewModelServiceManager.createChildViewModel(this, AjsButton);
        this._acrRefreshButton = <AjsButton>await this._viewModelServiceManager.createChildViewModel(this, AjsButton);

        this._tcrDeleteButton.onClick.subscribe(this._onTcrDeleteClick, this);
        this._tcrDownloadButton.onClick.subscribe(this._onTcrDownloadClick, this);
        this._tcrRefreshButton.onClick.subscribe(this._onTcrRefreshClick, this);
        this._acrAddButton.onClick.subscribe(this._onAcrAddClick, this);
        this._acrDeleteButton.onClick.subscribe(this._onAcrDeleteClick, this);
        this._acrDownloadButton.onClick.subscribe(this._onAcrDownloadClick, this);
        this._acrRefreshButton.onClick.subscribe(this._onAcrRefreshClick, this);

        this._tcrDeleteButton.enabled = false;
        this._tcrDownloadButton.enabled = false;
        this._tcrRefreshButton.enabled = false;
        this._acrDeleteButton.enabled = false;
        this._acrDownloadButton.enabled = false;
        this._acrRefreshButton.enabled = false;

        this._thisDeviceRequests = <ListView> await this._viewModelServiceManager.createChildViewModel(this, ListView);
        this._thisDeviceRequests.onSelectionChanged = this._thisDeviceRequestsSelectionChanged.bind(this);
        this._anotherDeviceRequests = <ListView> await this._viewModelServiceManager.createChildViewModel(this, ListView);
        this._anotherDeviceRequests.onSelectionChanged = this._anotherDeviceRequestsSelectionChanged.bind(this);

        this._initialized = true;

        this._update();
    }

    protected _langChanged(sender: any, code: string): void {
        this._update();
    }

    protected _update(): void {

        const tIndex = this._thisDeviceRequests.selectedItemIndex;
        const aIndex = this._anotherDeviceRequests.selectedItemIndex;

        this._thisDeviceRequests.clearHeader();
        this._thisDeviceRequests.clearItems();
        this._anotherDeviceRequests.clearHeader();
        this._anotherDeviceRequests.clearItems();

        this._thisDeviceRequests.
            addHeaderItem(this._lang.translate("pendingRequests.requestId"), "center", "center").
            addHeaderItem(this._lang.translate("pendingRequests.requestDate"), "center", "center").
            addHeaderItem(this._lang.translate("pendingRequests.profileName"), "center", "center").
            addHeaderItem(this._lang.translate("pendingRequests.commonName"), "center", "center").
            addHeaderItem(this._lang.translate("pendingRequests.status"), "center", "center").
            addHeaderItem("PKCS#8", "center", "center");

        for (let i = 0; i < this._requestTracker.tdriLength; i++) {
            const tr = this._requestTracker.getTdri(i);
            let status: string;
            switch (tr.status) {
                case "Completed": status = this._lang.translate("pendingRequests.statusComplete"); break;
                case "Failed": status = this._lang.translate("pendingRequests.statusFailed"); break;
                case "Pending": status = this._lang.translate("pendingRequests.statusPending"); break;
                case "Error": status = this._lang.translate("pendingRequests.statusError"); break;
                default: status = ""; break;
            }
            const selected = this._id ? tr.requestId === this._id : tIndex === i;
            this._thisDeviceRequests.addItem(
                [
                    tr.requestId.toString(),
                    tr.requestDate,
                    tr.profileName,
                    tr.commonName,
                    status,
                    tr.privateKey ? "\uD83D\uDD11" : "&nbsp;"
                ],
                selected,
                true);
        }

        this._id = undefined;

        this._anotherDeviceRequests.
            addHeaderItem(this._lang.translate("pendingRequests.requestId"), "center", "center").
            addHeaderItem(this._lang.translate("pendingRequests.status"), "center", "center");

        for (let i = 0; i < this._requestTracker.adriLength; i++) {
            const tr = this._requestTracker.getAdri(i);
            let status: string;
            switch (tr.status) {
                case "Completed": status = this._lang.translate("pendingRequests.statusComplete"); break;
                case "Failed": status = this._lang.translate("pendingRequests.statusFailed"); break;
                case "Pending": status = this._lang.translate("pendingRequests.statusPending"); break;
                case "Error": status = this._lang.translate("pendingRequests.statusError"); break;
                default: status = ""; break;
            }
            const selected = aIndex === i;
            this._anotherDeviceRequests.addItem([ tr.requestId.toString(), status ], selected, true);
        }

        this._tcrDeleteButton.label = this._lang.translate("pendingRequests.delete");
        this._tcrDownloadButton.label = this._lang.translate("pendingRequests.download");
        this._tcrRefreshButton.label = this._lang.translate("pendingRequests.refresh");
        this._acrAddButton.label = this._lang.translate("pendingRequests.add");
        this._acrDeleteButton.label = this._lang.translate("pendingRequests.delete");
        this._acrDownloadButton.label = this._lang.translate("pendingRequests.download");
        this._acrRefreshButton.label = this._lang.translate("pendingRequests.refresh");

        this._updateTcrButtons();
        this._updateAcrButtons();

    }

    protected _updateTcrButtons(): void {

        const index = this._thisDeviceRequests.selectedItemIndex;

        if (index === -1) {
            this._tcrDeleteButton.enabled = false;
            this._tcrDownloadButton.enabled = false;
            this._tcrRefreshButton.enabled = false;
        } else {
            const cri = this._requestTracker.getTdri(index);
            this._tcrDeleteButton.enabled = true;
            this._tcrRefreshButton.enabled = cri.status === "Pending" || cri.status === "Error";
            this._tcrDownloadButton.enabled = cri.status === "Completed";
        }

    }

    protected _updateAcrButtons(): void {

        const index = this._anotherDeviceRequests.selectedItemIndex;

        if (index === -1) {
            this._acrDeleteButton.enabled = false;
            this._acrDownloadButton.enabled = false;
            this._acrRefreshButton.enabled = false;
        } else {
            const ari = this._requestTracker.getAdri(index);
            this._acrDeleteButton.enabled = true;
            this._acrRefreshButton.enabled = ari.status === "Pending" || ari.status === "Error";
            this._acrDownloadButton.enabled = ari.status === "Completed";
        }

    }

    protected _thisDeviceRequestsSelectionChanged(index: number): void {
        this._updateTcrButtons();
    }

    protected _anotherDeviceRequestsSelectionChanged(index: number): void {
        this._updateAcrButtons();
    }

    protected _onRequestIdAddChange(data: AjsVDomEventData): void {
        this._requestIdAdd = data.value;
    }

    protected _statusChanged(id: number): void {
        this._update();
    }

    protected _onAcrAddClick(sender: any, data: AjsVDomEventData): void {

        const id = parseInt(this._requestIdAdd);
        if (isNaN(id)) return;

        for (let i = 0; i < this._requestTracker.adriLength; i++) {
            const acr = this._requestTracker.getAdri(i);
            if (acr.requestId === id) return;
        }

        this._requestTracker.addAdri({ requestId: id, status: "Pending" });
        this._requestTracker.adriRefreshStatus(id);

        this._requestIdAdd = "";

        this._update();

    }

    protected _onTcrRefreshClick(sender: any, data: AjsVDomEventData): void {

        const index = this._thisDeviceRequests.selectedItemIndex;
        if (index === -1) return;

        const id = this._requestTracker.getTdri(index).requestId;
        this._requestTracker.tdriRefreshStatus(id);

        this._update();
    }

    protected _onAcrRefreshClick(sender: any, data: AjsVDomEventData): void {

        const index = this._anotherDeviceRequests.selectedItemIndex;
        if (index === -1) return;

        const id = this._requestTracker.getAdri(index).requestId;
        this._requestTracker.adriRefreshStatus(id);

        this._update();
    }

    protected _onTcrDeleteClick(sender: any, data: AjsVDomEventData): void {
        const index = this._thisDeviceRequests.selectedItemIndex;
        if (index === -1) return;
        this._requestTracker.delTdri(index);
        this._update();
    }

    protected _onAcrDeleteClick(sender: any, data: AjsVDomEventData): void {
        const index = this._anotherDeviceRequests.selectedItemIndex;
        if (index === -1) return;
        this._requestTracker.delAdri(index);
        this._update();
    }

    protected _onAcrDownloadClick(sender: any, data: AjsVDomEventData): void {

        const index = this._anotherDeviceRequests.selectedItemIndex;
        if (index === -1) return;

        const ri = this._requestTracker.getAdri(index);
        if (ri.status !== "Completed") return;

        /*
        TODO: Let user choose the certificate format
        const p7b = this._cryptoServices.createP7B(ri.certificate, ri.chain);
        saveFile("certificate.p7b", p7b, "application/x-pkcs7-certificates");
        */

        saveFile("certificate.cer", ri.certificate, "application/x-x509-ca-cert");

    }

    protected async _onTcrDownloadClick(sender: any, data: AjsVDomEventData): Promise<void> {

        const index = this._thisDeviceRequests.selectedItemIndex;
        if (index === -1) return;

        const ri = this._requestTracker.getTdri(index);
        if (ri.status !== "Completed") return;

        if (ri.privateKey) {
            const privateKeyPwd = await this._loadPrivateKey.getPrivateKey(ri.certificate);
            if (!privateKeyPwd) return;
            console.log(privateKeyPwd);
            const pfx = this._cryptoServices.createPfx(ri.certificate, ri.chain, privateKeyPwd.key, privateKeyPwd.password);
            saveFile(ri.commonName + ".pfx", pfx, "application/x-pkcs12");
        } else {
            saveFile(ri.commonName + ".cer", ri.certificate, "application/x-x509-ca-cert");
        }

    }

}
