/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { IIDependentService } from "ajsfw/core/services/Service";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { AjsVDomEventData } from "ajsfw/services/virtualview/AjsVDomEventData";

type align = "justify" | "left" | "center" | "right";

interface ListViewHeaderItem {
    label: string;
    halign: align;
    dalign: align;
}

interface ListViewItem {
    selected: boolean;
    data: ListViewItemData[];
}

interface ListViewItemData {
    align: align;
    label: string;
    loading: boolean;
}

@ajsUseTemplate("list")
@ajsViewModel(
    IIDependentService,
    IIViewModelServiceManager
)

export class ListView implements ViewModel {

    public get selectedItemIndex(): number { return this._selectedItemIndex; }

    public onSelectionChanged: (index: number) => void;

    protected _header: ListViewHeaderItem[];
    protected _items: ListViewItem[];

    protected _selectedItemIndex: number;

    protected _parent: ViewModel;
    protected _viewModelServiceManager: ViewModelServiceManager;

    constructor(
        parentViewModel: ViewModel,
        viewModelServiceManager: ViewModelServiceManager,
    ) {
        this._parent = parentViewModel;
        this._viewModelServiceManager = viewModelServiceManager;
    }

    public init(): void {
        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._header = [];
        this._items = [];
        this._selectedItemIndex = -1;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif
    }

    public clearHeader(): ListView {
        this._header = [];
        this._viewModelServiceManager.viewModelUpdated(this);
        return this;
    }

    public clearItems(): ListView {
        this._items = [];
        this._selectedItemIndex = -1;
        this._viewModelServiceManager.viewModelUpdated(this);
        return this;
    }

    public addHeaderItem(label: string, halign: align, dalign: align): ListView {
        this._header.push({ label, halign, dalign});
        this._viewModelServiceManager.viewModelUpdated(this);
        return this;
    }

    public updateHeader(index: number, label: string, halign: align, dalign: align): ListView {
        if (index < 0 && index >= this._header.length) return this;
        this._header[index] = { label, halign, dalign };
        this.updateColumnAlign(index, dalign);
        this._viewModelServiceManager.viewModelUpdated(this);
        return this;
    }

    public updateHeaderLabel(index: number, label: string): ListView {
        if (index < 0 && index >= this._header.length) return this;
        this._header[index].label = label;
        this._viewModelServiceManager.viewModelUpdated(this);
        return this;
    }

    public updateHeaderAlign(index: number, align: align): ListView {
        if (index < 0 && index >= this._header.length) return this;
        this._header[index].halign = align;
        this._viewModelServiceManager.viewModelUpdated(this);
        return this;
    }

    public updateColumnAlign(index: number, align: align): ListView {
        if (index < 0 && index >= this._header.length) return this;

        this._header[index].dalign = align;

        for (const item of this._items) {
            if (index < item.data.length - 1) {
                item.data[index].align = align;
            }
        }

        this._viewModelServiceManager.viewModelUpdated(this);
        return this;
    }

    public deleteHeaderItem(index: number): ListView {
        if (index < 0 && index >= this._header.length) return this;
        this._header.splice(index, 1);
        this._viewModelServiceManager.viewModelUpdated(this);
        return this;
    }

    public addItem(columns: string[], selected = false, loading: boolean = undefined): ListView {

        const data: ListViewItemData[] = [];

        for (let i = 0; i < columns.length; i++) {
            if (i > this._header.length) break;
            data.push({ align: this._header[i].dalign, label: columns[i], loading });
        }

        this._items.push({ selected, data });
        if (selected) this._selectedItemIndex = this._items.length - 1;

        this._viewModelServiceManager.viewModelUpdated(this);

        return this;
    }

    public deleteItem(index: number): ListView {
        if (index > this._items.length - 1) return this;
        this._items.splice(index, 1);
        this._viewModelServiceManager.viewModelUpdated(this);
        return this;
    }

    public updateItemLabel(index: number, column: number, label: string): ListView {
        if (index > this._items.length - 1) return this;
        if (column > this._items[index].data.length - 1) return this;
        this._items[index].data[column].label = label;
        this._viewModelServiceManager.viewModelUpdated(this);
        return this;
    }

    public selectItem(index: number): ListView {
        if (index > this._items.length - 1) {
            this._selectedItemIndex = -1;
            return this;
        }

        for (const item of this._items) item.selected = false;
        this._items[index].selected = true;
        this._selectedItemIndex = index;
        this._viewModelServiceManager.viewModelUpdated(this);
        return this;
    }

    protected _onItemClick(data: AjsVDomEventData): void {

        if (!this._items[data.row].selected) {
            for (const item of this._items) item.selected = false;
            this._items[data.row].selected = true;
            this._selectedItemIndex = data.row;
            this._viewModelServiceManager.viewModelUpdated(this);
            if (this.onSelectionChanged) this.onSelectionChanged(data.row);
        }

    }


}