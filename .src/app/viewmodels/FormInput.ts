/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { IIDependentService } from "ajsfw/core/services/Service";
import { AjsNotifier } from "ajsfw/lib/events/AjsNotifier";
import { IILang, Lang } from "ajsfw/services/lang/Lang";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { AjsVDomEventData } from "ajsfw/services/virtualview/AjsVDomEventData";
import { Param } from "dto/Profile";
import { FormField } from "./FormField";

@ajsUseTemplate("forminput")
@ajsViewModel(
    IIDependentService,
    IIViewModelServiceManager,
    IILang
)
export class FormInput implements FormField {

    public paramDTO: Param;
    public label: string;
    public help: string;
    public error: string;
    public type: "text" | "password";

    public get value(): string { return this._value; }
    public set value(value: string) {
        this._value = value;
        this.validate();
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    protected _changed: AjsNotifier<void>;
    public get changed(): AjsNotifier<void> { return this._changed; }

    protected _parent: ViewModel;
    protected _viewModelServiceManager: ViewModelServiceManager;
    protected _lang: Lang;
    protected _value: string;

    constructor(
        parent: ViewModel,
        viewModelServiceManager: ViewModelServiceManager,
        lang: Lang
    ) {
        this._parent = parent;
        this._viewModelServiceManager = viewModelServiceManager;
        this._lang = lang;
        this._changed = new AjsNotifier();
    }

    public onChange(data: AjsVDomEventData) {
        if (this._value === data.value && data.eventType !== "blur") return;
        this.value = data.value;
        this._changed.notify(this);
    }

    public validate(): boolean {

        if (this._value === "") {
            if (this.paramDTO.mandatory === "TRUE") {
                this._setError(this._lang.translate("formFields.mandatory"));
                return false;
            } else {
                this._setError("");
                return true;
            }
        }

        if (!this.paramDTO.filter) {
            this._setError("");
            return true;
        }

        const rx = new RegExp(this.paramDTO.filter);
        if (rx.test(this._value)) {
            this._setError("");
            return true;
        }

        if (this.paramDTO.langParamError) {
            this._setError(this.paramDTO.langParamError[this._lang.code]);
        } else {
            this._setError(this._lang.translate("formFields.invalidInput"));
        }

    }

    protected _setError(error: string): void {
        if (this.error !== error) {
            if (error === "") {
                this.error = "&nbsp;";
            } else {
                this.error = error;
            }
            this._viewModelServiceManager.viewModelUpdated(this);
        }
    }

}