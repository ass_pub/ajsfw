/* *****************************************************************************
CSOB Certification Authority User Interface
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { AjsfwStartupConfig, IIAjsfwStartupConfig } from "ajsfw/core/main/AjsfwStartupConfig";
import { IIDependentService } from "ajsfw/core/services/Service";
import { AppConfig, IIAppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { IILang, Lang } from "ajsfw/services/lang/Lang";
import { ajsUseTemplate } from "ajsfw/services/mvvm/ajsUseTemplate.decorator";
import { AjsSelect } from "ajsfw/services/mvvm/controls/AjsSelect";
import { ajsViewModel } from "ajsfw/services/mvvm/viewmodel/ajsViewModel.decorator";
import { ViewModel } from "ajsfw/services/mvvm/viewmodel/ViewModel";
import { IIViewModelServiceManager, ViewModelServiceManager } from "ajsfw/services/mvvm/viewmodel/ViewModelServiceManager";
import { AjsVDomEventData } from "ajsfw/services/virtualview/AjsVDomEventData";
import { IINavigationService, NavigationService } from "ajsfw/services/navigation/NavigationService";
import { CafeConfig } from "application/CafeConfig";
import { NavigationItem } from "viewstates/NavigationItem";
import { Layout } from "./Layout";

@ajsUseTemplate("navigation")
@ajsViewModel(
    IIDependentService,
    IIViewModelServiceManager,
    IIAjsfwStartupConfig,
    IIAppConfig,
    IINavigationService,
    IILang
)
export class Navigation implements ViewModel {

    // bindable properties / controls
    protected _label: string;
    protected _server: string;
    protected _navigationItems: NavigationItem[];
    protected _languages: AjsSelect;

    // services
    protected _parentViewModel: ViewModel;
    protected _viewModelServiceManager: ViewModelServiceManager;
    protected _startupConfig: AjsfwStartupConfig;
    protected _appConfig: CafeConfig;
    protected _navigation: NavigationService;
    protected _lang: Lang;

    protected _initialized: boolean;

    constructor(
        parentViewModel: ViewModel,
        viewModelServiceManager: ViewModelServiceManager,
        startupConfig: AjsfwStartupConfig,
        appConfig: AppConfig,
        navigation: NavigationService,
        lang: Lang
    ) {

        // #ifdef DEBUG
        log.constructor(1, this);
        // #endif

        this._parentViewModel = parentViewModel;
        this._viewModelServiceManager = viewModelServiceManager;
        this._startupConfig = startupConfig;
        this._appConfig = appConfig.data;
        this._navigation = navigation;
        this._lang = lang;
        this._initialized = false;

    }

    public init(): void {
        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._init();

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif

    }

    public dispose(): void {
        // #ifdef DEBUG
        log.enter(1, this);
        // #endif

        this._navigation.navigatedNotifier.unsubscribe(this._navigated, this);
        this._lang.langChangedNotifier.unsubscribe(this._langChanged, this);
        this._languages.onChange.unsubscribe(this._languageChanged, this);
        this._languages = this._viewModelServiceManager.disposeChildViewModel(this, this._languages);
        this._parentViewModel = undefined;
        this._viewModelServiceManager = undefined;
        this._startupConfig = undefined;
        this._appConfig = undefined;
        this._navigation = undefined;
        this._lang = undefined;

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif
    }

    protected async _init(): Promise<void> {
        this._label = "ČSOB CA";
        this._server = this._appConfig.ca.commonName;

        this._languages = <AjsSelect>await this._viewModelServiceManager.createChildViewModel(this, AjsSelect);

        this._navigation.navigatedNotifier.subscribe(this._navigated, this);
        this._languages.onChange.subscribe(this._languageChanged, this);
        this._lang.langChangedNotifier.subscribe(this._langChanged, this);

        this._initialized = true;
        this._update();
    }

    public onLinkClick(data: AjsVDomEventData): void {
        (<Layout>(this._parentViewModel)).hideMenu();
        this._navigation.navigate(data.href);
        this._update();
        data.cancelBubble = true;
        data.preventDefault = true;
    }

    protected _hideMenu(): void {
        (<Layout>(this._parentViewModel)).hideMenu();
    }

    protected _languageChanged(sender: ViewModel, data: AjsVDomEventData): void {
        this._lang.use(data.value);
    }

    protected _langChanged(sender: any, code: string): void {
        this._update();
    }

    protected _navigated(sender: any): void {
        if (this._initialized) this._update();
    }

    protected async _update(): Promise<void> {
        if (!this._initialized) return;
        await this._populateNavigationItems();
        await this._populateLanguages();
        this._viewModelServiceManager.viewModelUpdated(this);
    }

    protected _populateNavigationItems(): void {

        const isNewRequest = location.pathname.indexOf("/NewRequest") === this._startupConfig.rootPath.length ||
                             location.pathname === this._startupConfig.rootPath ||
                             location.pathname === this._startupConfig.rootPath + "/";
        const isPendingRequests = location.pathname.indexOf("/PendingRequests") === this._startupConfig.rootPath.length;
        const isCaCertsCrls = location.pathname.indexOf("/CaCertsCrls") === this._startupConfig.rootPath.length;

        this._navigationItems = [
            { current: isNewRequest, link: "./NewRequest", label: this._lang.translate("navigation.newRequest") },
            { current: isPendingRequests, link: "./PendingRequests", label: this._lang.translate("navigation.previousRequests") },
            { current: isCaCertsCrls, link: "./CaCertsCrls", label: this._lang.translate("navigation.caCertAndCrl") }
        ];

    }

    protected async _populateLanguages(): Promise<void> {

        const langs = await this._lang.getLangs();

        this._languages.items.length = 0;

        for (const lang of langs) {
            this._languages.items.push({
                selected: lang.code === this._lang.code,
                value: lang.code,
                label: lang.label
            });
        }

        this._languages.itemsUpdated();

    }

}