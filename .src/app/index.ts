/*! ****************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
This software is using Ajs Framework distributed under the MIT license.
*******************************************************************************/

// Use the framework
// Forces the framework module to be loaded and executed automatically.
// Framework initializes the Notifier which notifies when the application class
// gets registered (decorated) and executes the framework driven application
// lifecycle.
import "ajsfw";

// Use the application startup
// forces the application startup module to be loaded and executed. The
// startup class is decorated using the @startup decorator. The decorator, when
// executed, notifies the framework about the startup class to be used.
// For notification purpose the Notifier component is used. The framework starts
// the standard startup procedure afterwards.
import "appstartup/AppStartup";

import "application/Cafe";
