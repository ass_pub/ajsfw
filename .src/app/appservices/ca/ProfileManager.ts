/* *****************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { Service } from "ajsfw/core/services/Service";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { Profile } from "dto/Profile";
import { FileProvider } from "ajsfw/services/data/providers/FileDataProvider/FileProvider";
import { IIAppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { CafeConfig } from "application/CafeConfig";
import { AppConfig } from "ajsfw/services/appstartup/async/AppConfig";

@inject("FileProvider", IIAppConfig)
export class ProfileManager implements Service {

    private _currentProfileId: string;
    public get currentProfileId(): string { return this._currentProfileId; }
    public set currentProfileId(profileId: string) { this._setCurrentProfile(profileId); }

    private _currentProfile: Profile;
    public get currentProfile(): Profile { return this._currentProfile; }

    private _profiles: Profile[];
    public get profiles(): Profile[] { return this._profiles; }

    private _fileProvider: FileProvider;
    private _config: CafeConfig;

    constructor(fileProvider: FileProvider, config: AppConfig) {
        this._fileProvider = fileProvider;
        this._config = <CafeConfig>config.data;
    }

    public init(): void {
        this._profiles = [];
    }

    public dispose(): void {
        this._currentProfile = undefined;
        this._profiles = undefined;
        this._config = undefined;
    }

    public async loadProfiles(): Promise<void> {

        const priofilePromises: Promise<string>[] = [];

        for (const profileUrl of this._config.profileUrls) {
            priofilePromises.push(<any>this._fileProvider.asyncRead(profileUrl));
        }

        const profiles: string[] = await Promise.all(priofilePromises.map(p => p.catch(() => undefined)));

        for (const profile of profiles) {

            if (!profile) continue;

            const profiles: Profile[] = JSON.parse(profile);

            for (const profileDTO of profiles) {

                if (this._profileIndexById(profileDTO.profileId) === -1) {

                    this._profiles.push(profileDTO);

                    if (!this._currentProfile) {
                        this._currentProfile = profileDTO;
                        this._currentProfileId = profileDTO.profileId;
                    }
                }

                // #ifdef DEBUG
                else {
                    log.warning(this, "Profile", profileDTO.profileId, " exists already");
                }
                // #endif
            }
        }

    }

    private _setCurrentProfile(profileId: string): void {
        const profile = this._profileById(profileId);
        if (profile) {
            this._currentProfile = profile;
            this._currentProfileId = profileId;
        }
        // #ifdef DEBUG
        else {
            log.warning(this, "Profile '", profileId, "' not found.");
        }
        // #endif
    }

    private _profileIndexById(id: string): number {
        for (let i = 0; i < this._profiles.length; i++) {
            if (this._profiles[i].profileId === id) return i;
        }
        return -1;
    }

    private _profileById(id: string): Profile {
        for (const profile of this._profiles) {
            if (profile.profileId === id) return profile;
        }
        return undefined;
    }


}