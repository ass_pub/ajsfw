/* *****************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

import { Service } from "ajsfw/core/services/Service";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { FileProvider } from "ajsfw/services/data/providers/FileDataProvider/FileProvider";
import { StringKeyValue } from "ajsfw/types";
import { ProfileManager } from "./ProfileManager";

@inject("FileProvider", ProfileManager)
export class Ca implements Service {

    protected _fileProvider: FileProvider;
    protected _profileManager: ProfileManager;

    constructor(fileProvider: FileProvider, profileManager: ProfileManager) {
        this._fileProvider = fileProvider;
        this._profileManager = profileManager;
    }

    public async sendRequest(formData: StringKeyValue<string>): Promise<string> {

        const requestData = await this._buildRequestData(formData);

        let postData = "";
        for (const key in requestData) {
            postData += key + "=" + encodeURIComponent(requestData[key]) + "&";
        }
        postData = postData.substr(0, postData.length - 1);

        let url;
        if (requestData["token"]) url = "/cap/api/requestKRB.php"; else url = "/cap/api/request.php";

        return <string>await this._fileProvider.asyncCreate(url, postData);

    }

    public parseRequestResponse(response: string): void {
    }

    /**
     * Builds CSR data based on currently selected profile and information filled to the form
     * @param formData Data collected from the form
     */
    protected async _buildRequestData(formData: StringKeyValue<string>): Promise<StringKeyValue<string>> {

        const requestData: StringKeyValue<string> = {};

        const profile = this._profileManager.currentProfile;

        for (const group of profile.groups) {
            if (group.type !== "implicit") continue;

            for (const param of group.params) {

                switch (param.type) {

                    case "profile":
                        requestData[param.id] = profile.profileId;
                        break;

                    case "request":
                        requestData[param.id] = formData["pkcs10"];
                        break;

                    case "token":
                        requestData[param.id] = await this._getCSRFToken();
                        break;

                    case "backup_key":
                        requestData[param.id] = "TRUE";
                        break;
                }

            }

        }

        for (const key in formData) {
            if (key !== "pkcs10" && formData[key]) {
                requestData[key] = formData[key];
            }
        }

        return requestData;

    }

    /**
     * Recieves CSRF token from the CA server API
     */
    protected async _getCSRFToken(): Promise<string> {

        const response = <string>await this._fileProvider.asyncRead("/cap/api/tokenKRB.php");
        const tokenResponse = JSON.parse(response);
        return tokenResponse.token;

    }


}