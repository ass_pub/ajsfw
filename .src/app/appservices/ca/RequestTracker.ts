/* *****************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

import { Service } from "ajsfw/core/services/Service";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { FileProvider } from "ajsfw/services/data/providers/FileDataProvider/FileProvider";
import { IIAppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { CafeConfig } from "application/CafeConfig";
import { AppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { ThisDeviceRequestInfo } from "./ThisDeviceRequestInfo";
import { AnotherDeviceRequestInfo } from "./AnotherDeviceRequestInfo";
import { LocalStorageNotSupported } from "./exceptions";
import { AjsNotifier } from "ajsfw/lib/events/AjsNotifier";

interface Chain {
    sCN: string;
    iCN: string;
    certificate: string;
}

interface RetrieveResponse {
    requestStatus: "complete" | "pending" | "failed";
    requestNumber: number;
    certificate: string;
    chain: Chain[];
}

@inject("FileProvider", IIAppConfig)
export class RequestTracker implements Service {

    protected _thisDeviceRequestInfo: ThisDeviceRequestInfo[];
    protected _anotherDeviceRequestInfo: AnotherDeviceRequestInfo[];

    protected _fileProvider: FileProvider;
    protected _config: CafeConfig;

    protected _statusChangeNotifier: AjsNotifier<number>;
    public get statusChangeNotifier(): AjsNotifier<number> { return this._statusChangeNotifier; }

    constructor(fileProvider: FileProvider, config: AppConfig) {
        this._fileProvider = fileProvider;
        this._config = <CafeConfig>config.data;
        this._statusChangeNotifier = new AjsNotifier();
    }

    public init(): void {
        this._loadLocalStorageData();
        this._updatePending();
    }

    public dispose(): void {
        this._statusChangeNotifier = undefined;
        this._thisDeviceRequestInfo = undefined;
        this._anotherDeviceRequestInfo = undefined;
        this._config = undefined;
    }

    public addTdri(ri: ThisDeviceRequestInfo): void {
        this._thisDeviceRequestInfo.push(ri);
        this._saveLocalStorageData();
    }

    public setTdri(index: number, tdri: ThisDeviceRequestInfo): void {
        if (index < 0 || index > this._thisDeviceRequestInfo.length - 1) return;
        this._thisDeviceRequestInfo[index] = tdri;
        this._saveLocalStorageData();
    }

    public delTdri(index: number): void {
        if (index < 0 || index > this._thisDeviceRequestInfo.length - 1) return;
        this._thisDeviceRequestInfo.splice(index, 1);
        this._saveLocalStorageData();
    }

    public getTdri(index: number): ThisDeviceRequestInfo {
        if (index < 0 || index > this._thisDeviceRequestInfo.length - 1) return;
        return this._thisDeviceRequestInfo[index];
    }

    public get tdriLength(): number {
        return this._thisDeviceRequestInfo.length;
    }

    public tdriRefreshStatus(id: number): void {
        for (const ri of this._thisDeviceRequestInfo) {
            if (ri.requestId === id && (ri.status === "Pending" || ri.status === "Error")) {
                ri.status = "Checking";
                this._updateRequestFromServer(ri);
                break;
            }
        }
    }

    public addAdri(ri: AnotherDeviceRequestInfo): void {
        this._anotherDeviceRequestInfo.push(ri);
        this._saveLocalStorageData();
    }

    public setAdri(index: number, adri: AnotherDeviceRequestInfo): void {
        if (index < 0 || index > this._anotherDeviceRequestInfo.length - 1) return;
        this._anotherDeviceRequestInfo[index] = adri;
        this._saveLocalStorageData();
    }

    public delAdri(index: number): void {
        if (index < 0 || index > this._anotherDeviceRequestInfo.length - 1) return;
        this._anotherDeviceRequestInfo.splice(index, 1);
        this._saveLocalStorageData();
    }

    public getAdri(index: number): AnotherDeviceRequestInfo {
        if (index < 0 || index > this._anotherDeviceRequestInfo.length - 1) return;
        return this._anotherDeviceRequestInfo[index];
    }

    public get adriLength(): number {
        return this._anotherDeviceRequestInfo.length;
    }

    public adriRefreshStatus(id: number): void {
        for (const ri of this._anotherDeviceRequestInfo) {
            if (ri.requestId === id && (ri.status === "Pending" || ri.status === "Error")) {
                ri.status = "Checking";
                this._updateRequestFromServer(ri);
                break;
            }
        }
    }

    protected _loadLocalStorageData(): void {
        if (!localStorage) throw new LocalStorageNotSupported();

        const lsData = localStorage.getItem(this._config.requestTrackingLocalStorageKey);

        if (!lsData) {
            this._thisDeviceRequestInfo = [];
            this._anotherDeviceRequestInfo = [];
            this._saveLocalStorageData();
            return;
        }

        const data = JSON.parse(lsData);

        this._thisDeviceRequestInfo = data.tdri;
        this._anotherDeviceRequestInfo = data.adri;
    }

    protected _saveLocalStorageData(): void {
        const lsData = JSON.stringify({ tdri: this._thisDeviceRequestInfo, adri: this._anotherDeviceRequestInfo });
        localStorage.setItem(this._config.requestTrackingLocalStorageKey, lsData);
    }

    protected _updatePending(): void {
        for (const ri of this._thisDeviceRequestInfo) {
            if (ri.status === "Pending") {
                ri.status = "Checking";
                this._updateRequestFromServer(ri);
            }
        }

        for (const ri of this._anotherDeviceRequestInfo) {
            if (ri.status === "Pending") {
                ri.status = "Checking";
                this._updateRequestFromServer(ri);
            }
        }
    }

    protected async _updateRequestFromServer(ri: ThisDeviceRequestInfo | AnotherDeviceRequestInfo): Promise<void> {

        try {
            const response = <string>await this._fileProvider.asyncRead("/cap/api/retrieve.php?requestNumber=" + ri.requestId);
            const rresp: RetrieveResponse = JSON.parse(response);
            switch (rresp.requestStatus) {
                case "complete": ri.status = "Completed"; break;
                case "failed": ri.status = "Failed"; break;
                case "pending": ri.status = "Pending"; break;
                default: ri.status = "Error";
            }

            if (rresp.requestStatus === "complete" && rresp.certificate) {
                ri.certificate = rresp.certificate;
                if (rresp.chain) {
                    ri.chain = [];
                    for (const c of rresp.chain) ri.chain.push(c.certificate);
                } else {
                    rresp.chain = undefined;
                }
            }

            this._saveLocalStorageData();

        } catch (e) {
            console.log(e);
            ri.status = "Error";
        }

        this._statusChangeNotifier.notify(ri.requestId);

    }


}