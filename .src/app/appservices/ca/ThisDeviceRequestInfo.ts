/* *****************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

export interface ThisDeviceRequestInfo {
    requestId: number;
    requestDate: string;
    profileName: string;
    commonName: string;
    privateKey?: boolean;
    certificate?: string;
    chain?: string[];
    status: "Pending" | "Completed" | "Failed" | "Checking" | "Error";
}
