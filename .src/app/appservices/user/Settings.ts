/* *****************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

import { Service } from "ajsfw/core/services/Service";
import { UserSettings } from "./UserSettings";
import * as exceptions from "./exceptions";
import { DIAppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { CafeConfig } from "application/CafeConfig";

@inject(DIAppConfig)
export class Settings implements Service {

    private _config: CafeConfig;
    private _settings: UserSettings;
    public get settings(): UserSettings { return this._settings; }

    constructor(config: CafeConfig) {
        this._config = config;
    }

    public init(): void {
        if (!localStorage) throw new exceptions.BrowserNotSupportedException();

        const data = localStorage.getItem(this._config.userSettingsLocalStorageKey);

        if (data) {
            this._settings = JSON.parse(data);
            this._settings.language = this._settings.language || this._config.defaultLanguage;
            this._settings.newRequestCachableGroups = this._settings.newRequestCachableGroups || {};
        } else {
            this._settings = this._defaultSettings();
            this.updateSettings();
        }

    }

    public updateSettings() {
        const data = JSON.stringify(this._settings);
        localStorage.setItem(this._config.userSettingsLocalStorageKey, data);
    }

    private _defaultSettings(): UserSettings {
        return {
            language: this._config.defaultLanguage,
            newRequestCachableGroups: {}
        };
    }

}
