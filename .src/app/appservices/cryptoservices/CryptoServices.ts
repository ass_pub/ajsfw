/* *****************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

interface ActiveXObject {
    new (s: string): any;
}

declare function require(id: string): any;
declare var ActiveXObject: ActiveXObject;

import { Service } from "ajsfw/core/services/Service";
import { StringKeyValue } from "ajsfw/types";
import { asn1, pki, util } from "node-forge";
import { CspInformation, CspInformations, X509PrivateKeyExportFlags, X500NameFlags, X509KeySpec, ObjectIdGroupId, ObjectIdPublicKeyFlags, AlgorithmFlags, X509PrivateKeyProtection } from "./CryptoApi";
import { KeyPair } from "./KeyPair";

const pkcs7 = require("node-forge").pkcs7;
const pkcs12 = require("node-forge").pkcs12;


export class CryptoServices implements Service {

    /**
     * Converts PEM Base64 to node-forge PKCS#10 object
     * @param data PEM Base64 encoded certificate request
     */
    public csrPemToPkcs10(data: string): any {
        // workaround for bug in the certificationRequestFromPem function
        let udata: string = data.replace("BEGIN NEW CERTIFICATE REQUEST", "BEGIN CERTIFICATE REQUEST");
        udata = udata.replace("END NEW CERTIFICATE REQUEST", "END CERTIFICATE REQUEST");

        return (<any>pki).certificationRequestFromPem(udata, true, true);
    }

    /**
     * Creates PKCS#7b PEM Base64 endoded string from certificates encoded in PEM Base64 passed to the function
     * @param certificate PEM Base64 certificate
     * @param chain List of PEM Base64 certificates representing the CA chain
     */
    public createP7B(certificate: string, chain?: string[]): string {
        const p7 = pkcs7.createSignedData();
        p7.addCertificate(certificate);

        if (chain) {
            for (const cert of chain) p7.addCertificate(cert);
        }

        return(pkcs7.messageToPem(p7));
    }

    /**
     * Generates the key pair according to parameters passed
     * @param keyInfo Information abou the key generation algorithm and key parameters
     * @param hashAlgorithm Web Crypto requires this to generate keys
     */
    public async generateKeyPair(keyInfo: { algorithm: string; length: number; exponent: string }, hashAlgorithm: string): Promise<KeyPair> {

        const exponent = new Uint8Array((keyInfo.exponent.length / 2));
        for (let i = 0; i < exponent.length; i++) exponent[i] = parseInt(keyInfo.exponent.substr(i * 2, 2), 16);

        if ((<any>window).msCrypto) {
            return this._generateKeyPairIE11(keyInfo.algorithm, keyInfo.length, exponent, hashAlgorithm);
        } else {
            return this._generateKeyPairOthers(keyInfo.algorithm, keyInfo.length, exponent, hashAlgorithm);
        }

    }

    /**
     * Encrypts private key using AES256 and returns it in PEM format
     * @param privateKeyPem Unencrypted private key in PEM format
     * @param password Password to be used to encrypt the private key
     */
    public encryptPrivateKey(privateKeyPem: string, password: string): string {
        const privateKey = pki.privateKeyFromPem(privateKeyPem);
        const rsaPrivateKey = (<any>pki).privateKeyToAsn1(privateKey);
        const privateKeyInfo = (<any>pki).wrapRsaPrivateKey(rsaPrivateKey);
        const encryptedKeyInfo =  (<any>pki).encryptPrivateKeyInfo(privateKeyInfo, password, { algorithm: "aes256"});
        return (<any>pki).encryptedPrivateKeyToPem(encryptedKeyInfo);
    }

    /**
     * Decrypts the private key and checks if it matches the certificate signing request passed
     * @param certPem Private key signed certificate signing request in PEM format
     * @param privateKeyPem Private key in pem format
     * @param password Password used to encrypt the private key
     * @returns Decrypted private key in PEM format or exception is thrown
     */
    public decryptAndCheckPrivateKey(certPem: string, privateKeyPem: string, password: string): string {
        try {
            const privateKey = (<any>pki).decryptRsaPrivateKey(privateKeyPem, password);
            const cert = pki.certificateFromPem(certPem);

            if (privateKey.n.data.length !== (<any>cert.publicKey).n.data.length) return undefined;
            for (let i = 0; i < privateKey.n.data.length; i++) {
                if (privateKey.n.data[i] !== (<any>cert.publicKey).n.data[i]) return undefined;
            }

            return pki.privateKeyToPem(privateKey);
        } catch (e) {
            throw e;
        }
    }

    /**
     * Creates standard pfx (p12) file
     * @param certPem Signed certificate in PEM format
     * @param certChainPem Certificate authority chain certificates in PEM format
     * @param privKeyPem Private key in PEM format
     * @param password Password for PFX encryption
     */
    public createPfx(certPem: string, certChainPem: string[], privKeyPem: string, password: string): Int8Array {
        const privKey = pki.privateKeyFromPem(privKeyPem);
        const certs: string[] = [ certPem ].concat(certChainPem);
        const p12Asn1 = pkcs12.toPkcs12Asn1(privKey, certs, password, { algorithm: "3des" });
        const p12Der = asn1.toDer(p12Asn1).getBytes();

        const buf = new Int8Array(p12Der.length);
        for (let i = 0; i < p12Der.length; i++) {
            buf[i] = p12Der.charCodeAt(i);
        }

        return buf;
    }



    /**
     * Creates a PKCS#10 request (using the Web Crypto API)
     * @param cn Common name to be used in the csr
     * @param privateKey Private key in PEM PKCS8 format
     * @param publicKey Public key in PEM SPKI format
     * @param hashAlgorithm Certificate request fingerprint algorithm
     */
    public async pkcs10(cn: string, privateKey: string, publicKey: string): Promise<string> {

        const forgePrivateKey = pki.privateKeyFromPem(privateKey);
        const forgePublicKey = pki.publicKeyFromPem(publicKey);

        const csr = (<any>pki).createCertificationRequest();
        csr.publicKey = forgePublicKey;
        csr.setSubject([{ name: "commonName", value: cn }]);
        csr.sign(forgePrivateKey);

        return (<any>pki).certificationRequestToPem(csr);
    }

    /**
     * Creates a PKCS#10 request (using the Windows Crypto API)
     * @param cn Common name to be used in the csr
     * @param cspName Name of the CSP to be used for key generation / storage
     * @param key Key algorithm (in Windows CryptoAPI format) / length (1024/2048/4096) pair
     * @param hashAlgorithm Name of the hash algorithm to be used (in Windows CryptoAPI format)
     * @param exportable Specifies if the key is exportable from the Windows CryptoAPI storage
     * @param protection Flags specyfying protection of the key in the Windows CryptoAPI storage
     */
    public async pkcs10IE11(
        cn: string,
        cspName: string,
        key: { algorithm: string; length: number },
        hashAlgorithm: string,
        exportable: "FALSE" | "TRUE",
        protection: "XCN_NCRYPT_UI_NO_PROTECTION_FLAG" | "XCN_NCRYPT_UI_PROTECT_KEY_FLAG" | "XCN_NCRYPT_UI_FORCE_HIGH_PROTECTION_FLAG"
    ): Promise<string> {

        function generate(resolve: (value: string) => void, reject: (value?: any) => void) {

            try {
                const csp: CspInformation = new ActiveXObject("X509Enrollment.CCspInformation");
                const csps: CspInformations = new ActiveXObject("X509Enrollment.CCspInformations");
                const privKey = new ActiveXObject("X509Enrollment.CX509PrivateKey");
                const dn = new ActiveXObject("X509Enrollment.CX500DistinguishedName");
                const request = new ActiveXObject("X509Enrollment.CX509CertificateRequestPkcs10");
                const enroll = new ActiveXObject("X509Enrollment.CX509Enrollment");
                const privKeyAlg = new ActiveXObject("X509Enrollment.CObjectId");
                const hashAlg = new ActiveXObject("X509Enrollment.CObjectId");

                dn.Encode("CN=" + cn, X500NameFlags.XCN_CERT_NAME_STR_NONE);

                csp.InitializeFromName(cspName);
                csps.Add(csp);

                privKeyAlg.InitializeFromAlgorithmName(
                    ObjectIdGroupId.XCN_CRYPT_PUBKEY_ALG_OID_GROUP_ID,
                    ObjectIdPublicKeyFlags.XCN_CRYPT_OID_INFO_PUBKEY_ANY,
                    AlgorithmFlags.AlgorithmFlagsNone,
                    key.algorithm
                );

                privKey.MachineContext = false;
                // privKey.CspInformations = csps;
                // privKey.Algorithm = privKeyAlg; // This does not work because of the unknown reason (SmartCard slot selection is shown)
                privKey.ProviderName = csp.Name;
                privKey.ProviderType = csp.Type;
                privKey.Length = key.length;
                privKey.KeySpec = X509KeySpec.XCN_AT_SIGNATURE; // Sign
                privKey.ExportPolicy = exportable === "TRUE" ? X509PrivateKeyExportFlags.XCN_NCRYPT_ALLOW_EXPORT_FLAG : X509PrivateKeyExportFlags.XCN_NCRYPT_ALLOW_EXPORT_NONE;
                privKey.KeyProtection = X509PrivateKeyProtection[protection];
                privKey.Create();

                hashAlg.InitializeFromAlgorithmName(
                    ObjectIdGroupId.XCN_CRYPT_HASH_ALG_OID_GROUP_ID,
                    ObjectIdPublicKeyFlags.XCN_CRYPT_OID_INFO_PUBKEY_ANY,
                    AlgorithmFlags.AlgorithmFlagsNone,
                    hashAlgorithm
                );

                request.InitializeFromPrivateKey(1, privKey, "");
                request.Subject = dn;
                request.HashAlgorithm = hashAlg;

                enroll.InitializeFromRequest(request);
                const pkcs10 = enroll.CreateRequest(3);

                resolve(pkcs10);
            } catch (e) {
                reject(e);
            }

        }

        return new Promise<string>(
            (resolve: (value: string) => void, reject: (value?: any) => void) => {
                // this is just because of ui to be able to show the loading dialog
                setTimeout(() => {
                    generate(resolve, reject);
                }, 50);
            }
        );

    }

    /**
     * Returns Windows Crypto Service Providers names
     */
    public getCSPs(): StringKeyValue<number> {

        const csps: StringKeyValue<number> = {};
        const cspInformations: CspInformations = new ActiveXObject("X509Enrollment.CCspInformations");
        cspInformations.AddAvailableCsps();
        const count = cspInformations.Count;
        for (let i = 0 ; i < count; i++) {
            const csp = cspInformations.ItemByIndex(i);
            csps[csp.Name] = i;
        }
        return csps;
    }

    /**
     * Returns printable string filled with information from the certificate signing request
     * @param pkcs10 Node-forge PKCS#10 object from which the information should be extracted
     */
    public pkcs10ToString(pkcs10: any): string {

        console.log(pkcs10);

        const bb = new util.ByteStringBuffer(pkcs10.certificationRequestInfo.value[2].value[0].value[0].value);
        const pkOid = asn1.derToOid(bb);
        const keySize = pkcs10.certificationRequestInfo.value[2].value[1].value[0].value[0].value.length * 8 - 8;

        let csrContent = "CSR Version: " + pkcs10.version + "\n\n";

        csrContent += "Subject:\n\n";
        for (const attr of pkcs10.subject.attributes) {
            csrContent += "   " + attr.name + ": " + attr.value + "\n";
        }
        csrContent += "\n";

        csrContent += "Subject public key:\n\n";
        csrContent += "   Algorithm: " + pki.oids[pkOid] + " (" + pkOid + ")\n";
        csrContent += "   Key size: " + keySize + "\n\n";

        csrContent += "Extensions:\n\n";
        for (const attr of pkcs10.attributes) {
            if (attr.name !== "extensionRequest") continue;
            for (const extension of attr.extensions) {
                switch (extension.name) {
                    case "basicConstraints":
                        csrContent += "   Basic constraints";
                        csrContent += extension.critical ? " (Critical):\n" : ":\n";
                        extension.cA ? csrContent += "      CA\n" : null;
                        extension.pathLenConstraint ? csrContent += "      Path length = " + extension.pathLenConstraint + "\n" : null;
                        break;
                    case "keyUsage":
                        csrContent += "   Key usage: ";
                        csrContent += extension.critical ? " (Critical):\n" : ":\n";
                        extension.digitalSignature ? csrContent += "      Digital Signature\n" : null;
                        extension.nonRepudiation ? csrContent += "      Non Repudiation\n" : null;
                        extension.keyEncipherment ? csrContent += "      Key Encipherment\n" : null;
                        extension.dataEncipherment ? csrContent += "      Data Encipherment\n" : null;
                        extension.keyAgreement ? csrContent += "      Key Agreement\n" : null;
                        extension.cRLSign ? csrContent += "      CRL Sign\n" : null;
                        extension.keyCertSign ? csrContent += "      Key Cert Sign\n" : null;
                        extension.encipherOnly ? csrContent += "      Encipher Only\n" : null;
                        extension.decipherOnly ? csrContent += "      Decipher Only\n" : null;
                        break;
                    case "extKeyUsage":
                        csrContent += "   Extended key usage: ";
                        csrContent += extension.critical ? " (Critical):\n" : ":\n";
                        extension.serverAuth ? csrContent += "      Server Authentication\n" : null;
                        extension.clientAuth ? csrContent += "      Client Authentication\n" : null;
                        extension.codeSigning ? csrContent += "      Code Signing\n" : null;
                        break;
                    case "subjectAltName":
                        csrContent += "   Subject alternative name";
                        csrContent += extension.critical ? " (Critical):\n" : ":\n";
                        for (const altName of extension.altNames) {
                            switch (altName.type) {
                                case 0:
                                   csrContent += "      otherName: (unprintable)\n";
                                   break;
                                case 1:
                                   csrContent += "      rfc822Name: " + altName.value + "\n";
                                   break;
                                case 2:
                                   csrContent += "      dNSName: "  + altName.value + "\n";
                                   break;
                                case 3:
                                   csrContent += "      x400Address: "  + altName.value + "\n";
                                   break;
                                case 4:
                                   csrContent += "      directoryName: "  + altName.value + "\n";
                                   break;
                                case 5:
                                   csrContent += "      ediPartyName: "  + altName.value + "\n";
                                   break;
                                case 6:
                                   csrContent += "      uniformResourceIdentifier: "  + altName.value + "\n";
                                   break;
                                case 7:
                                   csrContent += "      iPAddress: "  + altName.ip + "\n";
                                   break;
                                case 8:
                                   csrContent += "      registeredID: "  + altName.value + "\n";
                                   break;
                            }
                        }
                        break;

                }
                csrContent += "\n";
            }

            csrContent += "Signature:\n\n";
            csrContent += "   Algorithm: " + pki.oids[pkcs10.signatureOid] + " (" + pkcs10.signatureOid + ")\n";
        }

        return csrContent;

    }

    /**
     * Generates the public/private key pair accoring to passed arguments using the IE11 Web Crypto API
     * @param algorithm Key arlgorithm (must match one of the IE11 Web Crypto API asymetric key algorithm names)
     * @param length Key length
     * @param exponent Key exponent
     * @param hashAlg Hash algorithm (must match one of the IE11 Web Crypto API hash algorithm names)
     */
    protected async _generateKeyPairIE11(algorithm: string, length: number, exponent: Uint8Array, hashAlg: string): Promise<KeyPair> {


        return new Promise<KeyPair>(
            (resolve: (value: KeyPair) => void, reject: (reason?: any) => void) => {

                const crypto = (<any>window).msCrypto;

                const cryptoOp = crypto.subtle.generateKey(
                    {
                        name: algorithm,
                        modulusLength: length,
                        publicExponent: exponent,
                        hash: { name: hashAlg }
                    },
                    true,
                    ["sign", "verify"]
                );

                cryptoOp.oncomplete = async() => {
                    try {
                        const ckp_priv = await this._exportKeyIE11(cryptoOp.result.privateKey, "pkcs8");
                        const ckp_pub = await this._exportKeyIE11(cryptoOp.result.publicKey, "spki");
                        resolve({
                            privatePem: this._binKeyToPEM("PRIVATE", ckp_priv),
                            publicPem: this._binKeyToPEM("PUBLIC", ckp_pub)
                        });
                    } catch (e) {
                        reject(e);
                    }
                };

                cryptoOp.onerror = (e: any) => {
                    reject(e);
                };

            }
        );

    }

    /**
     * Exports the key generated using the IE11 Web Crypto API
     * @param cryptoKey Key (private or public) generated using the IE11 Web Crypto API
     * @param format Name of format in which the key should be exported (must match the Web Crypto API key export format)
     */
    protected async _exportKeyIE11(cryptoKey: any, format: string): Promise<ArrayBuffer> {

        return new Promise<ArrayBuffer>(
            (resolve: (value: ArrayBuffer) => void, reject: (reason?: any) => void) => {

                const crypto = (<any>window).msCrypto;
                const cryptoOp = crypto.subtle.exportKey(format, cryptoKey);

                cryptoOp.oncomplete = () => {
                    resolve(cryptoOp.result);
                };

                cryptoOp.onerror = (e: any) => {
                    reject(e);
                };

            }
        );

    }

    /**
     * Generates the public/private key pair accoring to passed arguments using the Web Crypto API
     * @param algorithm Key arlgorithm (must match one of the Web Crypto API asymetric key algorithm names)
     * @param length Key length
     * @param exponent Key exponent
     * @param hashAlg Hash algorithm (must match one of the Web Crypto API hash algorithm names)
     */
    protected async _generateKeyPairOthers(algorithm: string, length: number, exponent: Uint8Array, hashAlg: string): Promise<KeyPair> {

        const ckp: CryptoKeyPair = <CryptoKeyPair>await crypto.subtle.generateKey(
            {
                name: algorithm,
                modulusLength: length,
                publicExponent: exponent,
                hash: { name: hashAlg }
            },
            true,
            ["sign", "verify"]

        );

        const ckp_priv = await crypto.subtle.exportKey("pkcs8", ckp.privateKey);
        const ckp_pub = await crypto.subtle.exportKey("spki", ckp.publicKey);

        return {
            privatePem: this._binKeyToPEM("PRIVATE", ckp_priv),
            publicPem: this._binKeyToPEM("PUBLIC", ckp_pub)
        };

    }

    /**
     * Converts public, private or encrypted private key from binary ASN.1 to Base64 encoded PEM format
     * @param type Type of the PEM to be created (not checked with the format of the key data passed)
     * @param keyData Data of the public, private or encrypted private key (usually from the Web Crypto API)
     */
    protected _binKeyToPEM(type: "PRIVATE" | "ENCRYPTED PRIVATE" | "PUBLIC", keyData: ArrayBuffer): string {

        let rv = "";
        const b64 = btoa(String.fromCharCode.apply(null, new Uint8Array(keyData)));

        rv += "-----BEGIN " + type + " KEY-----\n";
        for (let i = 0; i < b64.length; i++) {
            if (i % 64 === 0 && i !== 0) rv += "\n";
            rv += b64[i];
        }
        if (rv[rv.length - 1] !== "\n") rv += "\n";

        rv += "-----END " + type + " KEY-----\n";

        return rv;
    }


}

