/* *****************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

import { StringKeyValue } from "ajsfw/types";

export type HashAlgorithm = "sha256" | "sha512";
export type KeyPerotection = "XCN_NCRYPT_UI_NO_PROTECTION_FLAG" | "XCN_NCRYPT_UI_PROTECT_KEY_FLAG" | "XCN_NCRYPT_UI_FORCE_HIGH_PROTECTION_FLAG";

export interface Param {
    id: string;
    langParamName: StringKeyValue<string>;
    type: "profile" | "request" | "token" | "text" | "password" | "select" | "password_verification" | "backup_key";
    filter: string;
    mandatory: "TRUE" | "FALSE";
    values: string[];
    langParamHelp: StringKeyValue<string>;
    langParamError: StringKeyValue<string>;
}

export interface Group {
    id: string;
    type: "implicit" | "explicit";
    langGroupName: StringKeyValue<string>;
    params: Param[];
    cacheable: "TRUE" | "FALSE";
}

export interface Request {
    type: "pkcs10";
    exportable: "TRUE" | "FALSE";
    hashAlgorithms: HashAlgorithm[];
    keyProtection: KeyPerotection;
    keys: string[];
    keystores: ("csp" | "pfx" | "unmanaged")[];
    csps: string[];

}

export interface Profile {
    profileId: string;
    langProfileName: StringKeyValue<string>;
    groups: Group[];
    request: Request;
}