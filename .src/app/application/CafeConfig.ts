/* *****************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

export interface CafeConfig {

    /** Stores name of the localStorage key used to store the user preferences locally in the browser */
    userSettingsLocalStorageKey: string;

    /** Key under which the request tracking is stored in the local storage */
    requestTrackingLocalStorageKey: string;

    /** Stores the default language (used when the user preferences were not initialized) */
    defaultLanguage: string;

    /** Stores regular expression to evaluate validity of password */
    passwordComplexity: string;

    /** Certificate authority configuration options */
    ca: {
        /** certificate authority common name */
        commonName: string,

        /** certificate authority certificate urls (whole chain) */
        certUrls: {
            label: string,
            url: string
        }[],

        /** certificate authority certificate revocation list urls */
        crlUrls: {
            label: string,
            url: string
        }[]

    };

    /** Information about profile files available on the server */
    profileUrls: string[];

    distinguishedNameMappings: {
        [key: string]: string;
    };

    /** Key type name mappings for WebCrypto / WinCrypto */
    keyMappings: {
        [profileKeyName: string]: {
            label: string;
            win: { algorithm: string; length: number };
            web: { algorithm: string; length: number; exponent: string };
        };
    };

    /** Hash algorithm name mappings for WebCrypto / WinCrypto */
    hashMappings: {
        [profileHashName: string]: {
            label: string;
            win: string;
            web: string;
        }
    };

}
