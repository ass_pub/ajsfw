/* *****************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

// #ifdef DEBUG
import * as log from "ajsfw/lib/diag/log";
// #endif

import { Application } from "ajsfw/services/application/Application";
import { application } from "ajsfw/services/application/application.decorator";
import { DIAppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { IILang, Lang } from "ajsfw/services/lang/Lang";
import { IITemplateManager, TemplateManager } from "ajsfw/services/templating/TemplateManager";
import { urlBindPath } from "ajsfw/services/urlbinding/urlBindPath.decorator";
import { Settings } from "appservices/user/Settings";
import { Layout } from "viewmodels/Layout";
import { CafeConfig } from "./CafeConfig";

@application(DIAppConfig, IILang, IITemplateManager, Settings)
export class Cafe implements Application {

    private _templateManager: TemplateManager;
    private _lang: Lang;

    @urlBindPath("**", Layout)
    protected _layout: Layout;

    @urlBindPath("{string:[a-z][a-z]-[A-Z][A-Z]}")
    public lang: string;

    constructor(config: CafeConfig, lang: Lang, templateManager: TemplateManager) {
        // #ifdef DEBUG
        log.constructor(1, this);
        // #endif

        this._lang = lang;
        this._templateManager = templateManager;

        document.title = config.ca.commonName;
    }

    public init(): void {

        // #ifdef DEBUG
        log.enter(1, this, "Initializing...");
        // #endif

        this._lang.load([
            "./res/lang/cs-CZ.json",
            "./res/lang/en-US.json"
        ]);

        this._templateManager.load([
            "./res/tpl/ajscontrols.html",
            "./res/tpl/layout.html",
            "./res/tpl/navigation.html",
            "./res/tpl/dialogs.html",
            "./res/tpl/list.html",
            "./res/tpl/cacertcrls.html",
            "./res/tpl/newrequest.html",
            "./res/tpl/pendingrequests.html"
        ]);

        // #ifdef DEBUG
        log.exit(1, this);
        // #endif
    }

}

export default Cafe;