/*******************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

import { AjsAppServiceBuilder } from "ajsfw/builders/AjsAppServiceBuilder";
import { AjsAsyncStartupServiceBuilder } from "ajsfw/builders/AjsAsyncStartupServiceBuilder";
import { AjsStartupServiceBuilder } from "ajsfw/builders/AjsStartupServiceBuilder";
import { SIAppServiceBuilder } from "ajsfw/builders/DependencyIdentifiers";
import { AjsDefaultServiceManager } from "ajsfw/services/dic/AjsDefaultServiceManager";
import { ServiceCtor } from "ajsfw/core/services/Service";
import { ServiceBuilderCtor } from "ajsfw/core/services/ServiceBuilder";
import { IIApplication } from "ajsfw/services/application/Application";
import { AppStartupService } from "ajsfw/services/appstartup/AppStartupService";
import { IIAppConfig } from "ajsfw/services/appstartup/async/AppConfig";
import { startup } from "ajsfw/services/appstartup/startup.decorator";
import { AjsFileReader } from "ajsfw/services/data/providers/FileDataProvider/AjsFileReader";
import { IIFileProvider } from "ajsfw/services/data/providers/FileDataProvider/FileProvider";
import { AjsContainerService } from "ajsfw/services/dic/AjsContainerService";
import { ContainerService, IIContainerService } from "ajsfw/services/dic/ContainerService";
import { inject } from "ajsfw/services/dic/inject.decorator";
import { AjsPasswordInput } from "ajsfw/services/mvvm/controls/AjsPasswordInput";
import { AjsSelect } from "ajsfw/services/mvvm/controls/AjsSelect";
import { Cafe } from "application/Cafe";
import { CafeConfig } from "application/CafeConfig";
import { Ca } from "appservices/ca/Ca";
import { ProfileManager } from "appservices/ca/ProfileManager";
import { RequestTracker } from "appservices/ca/RequestTracker";
import { CryptoServices } from "appservices/cryptoservices/CryptoServices";
import { Settings } from "appservices/user/Settings";
import { CaCertsCrls } from "viewmodels/CaCertsCrls";
import { ErrorMessage } from "viewmodels/ErrorMessage";
import { FormGroup } from "viewmodels/FormGroup";
import { FormInput } from "viewmodels/FormInput";
import { FormPasswordVerification } from "viewmodels/FormPasswordVerification";
import { FormSelect } from "viewmodels/FormSelect";
import { Layout } from "viewmodels/Layout";
import { ListView } from "viewmodels/ListView";
import { Loading } from "viewmodels/Loading";
import { Navigation } from "viewmodels/Navigation";
import { NewRequest } from "viewmodels/NewRequest";
import { PendingRequests } from "viewmodels/PendingRequests";
import { PrivateKeyPassword } from "viewmodels/PrivateKeyPassword";
import { AppStartupConfig } from "./AppStartupConfig";
import { getStartupConfig } from "./environment";
import { LoadPrivateKey } from "viewmodels/LoadPrivateKey";
import { AjsTextArea } from "ajsfw/services/mvvm/controls/AjsTextArea";
import { AjsButton } from "ajsfw/services/mvvm/controls/AjsButton";

@startup(getStartupConfig())
export class AppStartup implements AppStartupService {

    private _config: AppStartupConfig;

    constructor(config: AppStartupConfig) {
        this._config = config;
    }

    public useCustomRootDiContainer(): ContainerService {
        return new AjsContainerService(null);
    }

    public useCustomDefaultServiceManager(): ServiceCtor {
        return <ServiceCtor>AjsDefaultServiceManager;
    }

    public useCustomStartupServiceBuilder(): ServiceBuilderCtor {
        return <ServiceBuilderCtor>AjsStartupServiceBuilder;
    }

    public configureStartupServices(startupServiceBuilder: AjsStartupServiceBuilder): void {
        startupServiceBuilder.
            useAjsOnlineStartup().
            useAjsOfflineStartup();
    }

    public useCustomAsyncStartupServiceBuilder(): ServiceBuilderCtor {
        return <ServiceBuilderCtor>AjsAsyncStartupServiceBuilder;
    }

    public configureAsyncStartupServices(asyncStartupServiceBuilder: AjsAsyncStartupServiceBuilder): void {
        asyncStartupServiceBuilder.
            useAppConfig(this._config.configUri);
    }

    public useCustomServiceBuilder(): ServiceBuilderCtor {
        return <ServiceBuilderCtor>AjsAppServiceBuilder;
    }

    @inject(IIContainerService, SIAppServiceBuilder, IIAppConfig)
    public async configureServices(container: ContainerService, serviceBuilder: AjsAppServiceBuilder, config: CafeConfig): Promise<void> {

        serviceBuilder.
            useDefaults().
            useLang().
            useAppModules().
            useUrlBinding().
            useNavigation().
            useTemplating().
            useMvvm();

        container.
            addSingleton(IIApplication, <ServiceCtor>Cafe).
            addSingleton(Settings).

            addSingleton("FileProvider", AjsFileReader).mapDep(IIFileProvider, "FileProvider").
            addSingleton(CryptoServices).
            addSingleton(ProfileManager).
            addSingleton(Ca).
            addSingleton(RequestTracker).

            // view models (reusable, same data for each instance)
            addSingleton(Loading).
            addSingleton(ErrorMessage).
            addSingleton(PrivateKeyPassword).
            addSingleton(LoadPrivateKey).
            addSingleton(Layout).
            addSingleton(Navigation).
            addSingleton(NewRequest).
            addSingleton(PendingRequests).
            addSingleton(CaCertsCrls).

            // view models (non reusable - different data for each instance)
            addTransient(FormGroup).
            addTransient(FormSelect).
            addTransient(FormInput).
            addTransient(FormPasswordVerification).
            addTransient(ListView).

            // controls (view models - always different data for each instance)
            // addTransient(AjsFile).
            addTransient(AjsButton).
            addTransient(AjsSelect).
            addTransient(AjsTextArea).
            addTransient(AjsPasswordInput)
            ;

    }

}
