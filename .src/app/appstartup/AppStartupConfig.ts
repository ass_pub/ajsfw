/*******************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

import { AjsfwStartupConfig } from "ajsfw/core/main/AjsfwStartupConfig";

export interface AppStartupConfig extends AjsfwStartupConfig {
    environment: string;
    configUri: string;
}
