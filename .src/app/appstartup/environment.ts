/*******************************************************************************
CSOB Certification Authority Front End
Copyright (c)2018-2019 CSOB, a.s.

It is prohibited to distribute this software or any part of it in any form
(binary, source or resources) without previous agreement of ČSOB, a.s.
*******************************************************************************/

import { AppStartupConfig } from "./AppStartupConfig";
import { AjsfwStartupConfig } from "ajsfw/core/main/AjsfwStartupConfig";

const environment: "Test" | "Debug" | "Production" = "Debug";

export function getStartupConfig(): AjsfwStartupConfig {

    switch (environment) {
        case "Test":
            return getTestConfig();
        case "Debug":
            return getDebugConfig();
        case "Production":
            return getProductionConfig();
    }

}

function getCommonConfig(): AppStartupConfig {
    return {
        rootPath: "/",
        environment: undefined,
        configUri: "./config.json"
    };
}

function getTestConfig(): AppStartupConfig {
    const config = getCommonConfig();
    config.environment = "Test";
    return config;
}

function getDebugConfig(): AppStartupConfig {
    const config = getCommonConfig();
    config.environment = "Debug";
    return config;
}

function getProductionConfig(): AppStartupConfig {
    const config = getCommonConfig();
    config.environment = "Production";
    return config;
}
