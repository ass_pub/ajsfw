/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as colors from "colors";

import api from "../api";

const usage =
`   ${colors.cyan("abuild-sass")}

      Standars SASS -> Transpilles SASS to CSS


      - configurable per project by sass.config.<config>.json

`;

module.exports = class ABuildSass extends api.ABuildToolSuper<object> {

   public static readonly versions: string[] = [
      "abuild-sass 1.0.0",
      "sass x"
   ];

   public static readonly usage = usage;

   protected _clean(): boolean {

      try {

         /*
         if (fs.existsSync(this._config.projectTgtPath)) {
            this._api.Diag.info(this.constructor.name, `Cleaning '${this._config.projectOutPath}'`);
            this._rmDirRecursive(this._config.projectTgtPath);
         }
         */

      } catch (e) {

         api.diag.warn(this.constructor.name, `Failed to clean: ${e}`);
         return false;

      }

      return true;
   }

};
