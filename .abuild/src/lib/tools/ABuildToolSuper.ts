/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as path from "path";
import * as diag from "../diag";

import { Project } from "../project";
import { ABuildTool } from "./ABuildTool";

export class ABuildToolSuper<TConfig> implements ABuildTool {

   private __initialized: boolean;

   protected _project: Project;
   protected _config: TConfig;

   constructor(project: Project, config: TConfig) {
      this._config = config;
      this._project = project;
      this.__initialized = false;
   }

   public init(): boolean {
      if (this.__initialized) return true;
      this._configure();
      this._diagConfig();
      this.__initialized = true;
      return true;
   }

   public clean(): boolean {
      if (!this.__initialized) return false;
      return this._clean();
   }

   public build(buildConfig: string): boolean {
      if (!this.__initialized) return false;
      return this._build(buildConfig);
   }

   public watch(buildConfig: string): boolean {
      if (!this.__initialized) return false;
      return this._watch(buildConfig);
   }

   protected _configure(): void {
   }

   protected _clean(): boolean {
      return false;
   }

   protected _build(buildConfig: string): boolean {
      return false;
   }

   protected _watch(buildConfig: string): boolean {
      return false;
   }

   protected _patchPath(pathToPatch: string): string {

      let result = pathToPatch;

      let rx: RegExp;

      for (const k in this._config) {
         rx = new RegExp(`<${k}>`, "g");
         result = result.replace(rx, (<any>this._config)[k]);
      }

      rx = new RegExp(`<solutionPath>`, "g");
      result = result.replace(rx, this._project.solution.path);

      rx = new RegExp(`<projectName>`, "g");
      result = result.replace(rx, this._project.name);

      rx = new RegExp(`<srcDirName>`, "g");
      result = result.replace(rx, this._project.solution.srcDirName);

      rx = new RegExp(`<buildDirName>`, "g");
      result = result.replace(rx, this._project.solution.buildDirName);

      return path.normalize(result);
   }

   /**
    * Removes contents of the directory recursively
    * If removeTopLevelDir is set to true the top level directory is also removed
    * @param dirName  Name of the directory its contens has to be deleted
    * @param removeTopLevelDir Indicates the top level directory should be also removed
    */
   protected _rmDirRecursive(dirName: string, removeTopLevelDir = false) {

      // diag.info(this.constructor.name, `Deleting content of directory ${dirName}`);

      const files: string[] = fs.readdirSync(dirName);

      for (const f of files) {

         const fn = path.join(dirName, f);
         const stat = fs.statSync(fn);

         if (stat.isDirectory()) {
            this._rmDirRecursive(fn, true);
         }

         if (stat.isFile()) {
            // diag.info(this.constructor.name, `   Deleting file ${f}`);
            fs.unlinkSync(fn);
         }

      }
      if (removeTopLevelDir) {
         // diag.info(this.constructor.name, `   Deleting directory ${dirName}`);
         fs.rmdirSync(dirName);
      }

   }

   protected _diagConfig() {
      for (const k in this._config) {
         diag.info(this.constructor.name, 2, `      ${(k + ":                         ").substring(0, 25)} '${this._config[k]}'`);
      }
   }


}