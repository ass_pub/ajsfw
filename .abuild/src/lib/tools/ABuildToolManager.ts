/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as path from "path";
import * as diag from "../diag";

import api from "../../api";

import { Project } from "../project";

import { ABuildToolsInfo } from "./ABuildToolsInfo";
import { ABuildTool } from "./ABuildTool";

/**
 * Searches for build tools, loads tools modules, holds list of found managers and implements functionality
 * for build tool instance creation.
 *
 * Following paths are used for tool module resolution:
 *    - ABuild installation folder
 *       - node_modules folder up to root folder on the device starting from ABuild installation folder
 *    - Current working directory
 *       - node_modules folder up to root folder on the device starting from ABuild installation folder
 *    - <NodeJS installation path>/node_modules (global modules on Windows)
 *    - $HOME/.node_modules
 *    - $HOME/.node_libraries
 *    - $HOMEDRIVE\$HOMEPATH\.node_modules
 *    - $HOMEDRIVE\$HOMEPATH\.node_libraries
 */

export class ABuildToolManager {

   private __initialized: boolean;

   /**
    * Holds information about all tool modules and constructors
    */
   private __tools: ABuildToolsInfo;

   /**
    * Holds information about all tool modules and constructors
    */
   public get tools(): ABuildToolsInfo { return this.__tools; }


   constructor() {
      this.__initialized = false;
      this.__tools = {};
   }


   /**
    * Searches for abuild modules on defined search paths and loads discovered modules
    */
   public init() {

      const paths = this.__getSearchPaths();
      const toolPaths = this.__searchTools(paths);

      this.__loadTools(toolPaths);

      this.__initialized = true;

   }


   /**
    * Returns configured instance of the given tool name. If instantiating or initialization fails returns null
    * @param toolName Tool name to be instantiated
    * @param config  Tool configuration
    */
   public createTool(toolName: string, project: Project, config: object): ABuildTool {

      if (!this.__initialized) {
         diag.error(this.constructor.name, `Tool manager must be initialized before calling createTool method!`);
         return null;
      }


      if (!this.__tools.hasOwnProperty(toolName)) {
         diag.error(this.constructor.name, `Can not create tool '${toolName}'. Tool not loaded!`);
         return null;
      }

      let tool: ABuildTool;

      try {
         tool = new this.__tools[toolName].ctor(project, config);
         tool.init();
      } catch (e) {
         diag.error(this.constructor.name, `Failed to instantiate or initialize tool '${toolName}': ${e}`);
         tool = null;
      }

      return tool;

   }


   /**
    * Returns loaded list of tool names
    */
   public getToolList(): string[] {

      if (!this.__initialized) {
         diag.error(this.constructor.name, `Tool manager must be initialized before calling createTool method!`);
         return null;
      }

      return Object.getOwnPropertyNames(this.__tools);

   }


   /**
    * Prepares and returns paths to be searched for ABuild tools
    */
   private __getSearchPaths(): string[] {

      let p: string;

      // Prepare list of paths where to look for tool modules
      const abuildPath = path.resolve(__dirname, "../../");

      // ABuild installation path and node_modules under it
      // current working directory and node_modules under it
      const paths = [
         abuildPath,
         path.join(abuildPath, "node_modules"),
         process.cwd(),
         path.join(process.cwd(), "node_modules")
      ];

      // node_modules up to root folder starting from ABuild/../
      p = abuildPath;
      while (p !== path.parse(p).root) {
         p = path.resolve(p, "../");
         paths.push(path.join(p, "node_modules"));
      }

      // node_modules up to root folder starting from cwd()/../
      p = process.cwd();
      while (p !== path.parse(p).root) {
         p = path.resolve(p, "../");
         paths.push(path.join(p, "node_modules"));
      }

      // node installation path
      p = path.dirname(process.argv[0]);
      paths.push(path.join(p, "node_modules"));

      // $HOME/.node_modules
      // $HOME/.node_libraries
      if (process.env.hasOwnProperty("HOME")) {
         paths.push(path.join(process.env["HOME"], ".node_modules"));
         paths.push(path.join(process.env["HOME"], ".node_libraries"));
      }

      // $HOMEDRIVE\$HOMEPATH\.node_modules
      // $HOMEDRIVE\$HOMEPATH\.node_libraries
      if (process.env.hasOwnProperty("HOMEDRIVE") && process.env.hasOwnProperty("HOMEPATH")) {
         p = path.join(process.env["HOMEDRIVE"], process.env["HOMEPATH"]);
         paths.push(path.join(p, ".node_modules"));
         paths.push(path.join(p, ".node_libraries"));
      }

      // $NODE_PATH
      if (process.env.hasOwnProperty("NODE_PATH")) {
         p = process.env["NODE_PATH"];
         let np: string[];
         if (p.indexOf(";")) {
            np = p.split(";");
         } else if (p.indexOf(",")) {
            np = p.split(",");
         } else {
            np = [ p ];
         }
         for (p of np) {
            if (path.isAbsolute(p)) {
               paths.push(p);
               if (p.indexOf("node_modules") === -1) {
                  paths.push(path.join(p, "node_modules"));
               }
            }
         }
      }


      diag.info(this.constructor.name, 2, `Following paths will be searched for build tools:`);
      for (const p of paths) {
         diag.info(this.constructor.name, 2, `   ${p}`);
      }

      return paths;

   }


   /**
    * Searches for ABuild tools on passed paths and returns paths to found modules
    * @param searchPaths Paths to be searched for ABuild tools
    */
   private __searchTools(searchPaths: string[]): string[] {

      diag.info(this.constructor.name, 1, "Searching for build tools...");

      const toolPaths: string[] = [];

      for (const p of searchPaths) {

         if (!fs.existsSync(p)) continue;
         if (!fs.statSync(p).isDirectory()) continue;

         const files = fs.readdirSync(p);

         for (const f of files) {

            const dp = path.join(p, f);
            const stat = fs.statSync(dp);

            if (!stat.isDirectory()) continue;
            if (!f.startsWith("abuild-")) continue;

            toolPaths.push(dp);
         }

      }

      return toolPaths;

   }


   /**
    * Loads ABuild tools form paths passed
    * @param toolPaths Paths where ABuild tools should be located
    */
   private __loadTools(toolPaths: string[]): void {

      for (const toolPath of toolPaths) {
         this.__loadTool(toolPath);
      }

   }


   /**
    * Loads a ABuild tool
    * @param toolPath Path where a ABuild tool is located
    */
   private __loadTool(toolPath: string): void {

      const toolName = path.basename(toolPath).substr(7);

      if (this.__tools.hasOwnProperty(toolName)) {
         diag.warn(this.constructor.name, `Tool '${toolName}' found at '${path}' can not override tool loaded from '${this.__tools[toolName].path}'`);
         return;
      }

      try {
         const ctor = require(toolPath);

         this.__tools[toolName] = {
            path: toolPath,
            ctor
         };

         diag.info(this.constructor.name, 1, `Tool 'abuild-${toolName}' found and loaded`);

      } catch (e) {
         diag.warn(this.constructor.name, `Failed to load tool module '${toolName}': ${e}`);
      }

   }



}