/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

/**
 * Replaces comments in the JSON string with spaces
 * @param json json string to stip comments from
 */
export function stripComments(json: string): string {

   enum State { default, sqstring, dqstring, slcomment, mlcomment }

   let i = 0;
   let state: State = State.default;
   let result = "";

   while (i < json.length) {

      switch (state) {
         case State.sqstring:
            result += json[i];
            if (json[i] === "'") state = State.default;
            break;
         case State.dqstring:
            result += json[i];
            if (json[i] === "\"") state = State.default;
            break;

         case State.slcomment:
            if (json[i] === "\r" || json[i] === "\n") {
               result += json[i];
               state = State.default;
            } else {
               result += " ";
            }
            break;

         case State.mlcomment:
            if (json.substr(i, 2) === "*/") {
               result += "  ";
               state = State.default;
               i++;
            } else {
               if (json[i] === "\n" || json[i] === "\r") {
                  result += json[i];
               } else {
                  result += " ";
               }
            }
            break;

         default:
            if (json[i] === "\"") {
               result += json[i];
               state = State.dqstring;
            } else

            if (json[i] === "'") {
               result += json[i];
               state = State.sqstring;
            } else

            if (json.substr(i, 2) === "//") {
               state = State.slcomment;
               result += "  ";
               i++;
            } else

            if (json.substr(i, 2) === "/*") {
               state = State.mlcomment;
               result += "  ";
               i++;
            } else {
               result += json[i];
            }

            break;
      }

      i++;
   }

   return result;

}

/**
 * Finds JSON row, column and near the error reported in form of number of character in the buffer passed to JSON.parse
 * @param scc Loaded JSON file in the buffer
 * @param msg Error message thrown by JSON.parse
 */
export function errorInfo(scc: Buffer, msg: string): string {

   if (msg.indexOf("JSON at position") === -1) return;

   const pos = parseInt(msg.substr(msg.lastIndexOf(" ") + 1));
   const cfgl = scc.toString();
   let row = 1;
   let col = 1;
   let rowstr = "";
   let i;

   for (i = 0; i <= pos; i++) {

      if (cfgl[i] === "\n") {
         row++;
         col = 1;
         rowstr = "";
      } else {
         rowstr += cfgl[i];
         col++;
      }

   }

   while (i < cfgl.length) {
      if (cfgl[i] === "\r" || cfgl[i] === "\n") break;
      rowstr += cfgl[i];
      i++;
   }

   return `JSON Error at [${row}:${col - 1}] '${rowstr}'`;
}

