/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as colors from "colors";

const usg = colors.grey(
`${colors.white("Info:")}

Utility to run various build tools in sequence in standalone or watching mode.

${colors.white("Usage:")}

   ajsfwbuild [-h -v -d [level] -r -w -c {config} -s {solution}] [project]

   -h, --help       shows this help page

   -v, --versions   shows versions of all build tools found in the solution build config

   -m, --manage     magange solution using gui

   -d, --debug      enable logging (errors are logged all the time)
   -d level         set logging verbosity
   --debug level    higher number = higher log verbostiy
                    if no level is specified, 0 will be used

   -r, --reset      cleans outputs for all configured projects

   -w, --watch      start in watch mode (all plugins are watching for changes)

   -c, --config     configuration of the build (usually this will be 'debug' or 'release')
                    added to the name of the config file for particular tool (i.e. tsconfig.<config>.json)

   -s, --solution   path to solution (ajsfwbuild.<config>.json should exist inside of the folder)
                    if no path to solution is specified the current working folder will be used

   project          project to be cleaned, built or watched
                    projects the target projects depends on are included automatically
                    if no project is specified the first project from configuration file will be used

${colors.white("Configuration file:")}

   ABuild configuration file is searched on the solution path. If no -s arument is used the default file
   name (.abuild.json") will be used.

${colors.white("Configuration file description:")}

   "options": {                Options section

      srcDirName: "<path>"     Path to folder where projects with source code can be located
                               Default = .src

      resDirName: "<path>"     Path to folder where projects with resources can be located
                               Default = .src

      buildDirName: "<path>"   Path to folder used for particular build steps
                               Default = .build
   },

   "projects": {               Configuration of particular project build chain

      "<project_name>": {      Name of the project being configured

         "depends": [ <dep> ], List of projects the project depends on

         "tools": [ <tool> ],  List of tools to be used to build the project (in writen order)
                               tool name must be unique but can contain extension (i.e. sync.ts, sync.dts)
                               tool name is used as identifier of config name (see bellow)
                               in previous example, 2 sync tools will be runned, each with different config

         "config": {           Explicit configuration of the tool used for the project
            "<tool>": { ... }, Cofiguration of the build tool related to the project
            ...                Full tool name is used in this case
         }
      }
   }


${colors.white("Standard build chains:")}

   Code to be runned in NodeJS runtime environment (i.e. tests):
      ets -> babel -> sync

   Code to be runned in browser:
      ets -> babel -> ajsfwpack


${colors.white("Tool-chain plugins:")}
`);

export function usage(toolHelp: string[]) {
   console.log(usg);
   console.log(colors.gray(toolHelp.join(process.platform === "win32" ? "\r\n" : "\n")));
}