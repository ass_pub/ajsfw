/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as colors from "colors";

/**
 * Current flag specifying if the debugging is enabled or not (default true)
 */
let _enabled = true;

/**
 * Current value of logging level (default = 0)
 */
let _debugLevel = 0;

/**
 * Start time to be used to determine the log message time since log module is loaded
 */
const startTime = Date.now();


/**
 * Adds spaces to end of the string to fulfill requirement on total number of characters
 * @param s String to which spaces will be added
 * @param totalChars Total number of characters the string has to contain
 */
function addSpacesAfter(s: string, totalChars: number): string {
   let out: string = s;
   while (out.length < totalChars) out += " ";
   return out;
}


/**
 * Adds spaces to beginning of the string to fulfill requirement on total number of characters
 * @param s String to which spaces will be added
 * @param totalChars Total number of characters the string has to contain
 */
function addSpacesBefore(s: string, totalChars: number): string {
   let out = s;
   while (out.length < totalChars) out = " " + out;
   return out;
}


/**
 * Converts miliseconds to time in HH:MM:SS:MSC format
 * @param ms Time in miliseconds to be converted
 */
function msToTime(ms: number): string {
   return new Date(ms).toISOString().slice(11, -1);
}


/**
 * Logs a message to the console
 * @param color Color to be used to display the message (usually white, yellow, red)
 * @param severity Severity of the log message (based on the current log level it will be shown or not)
 * @param moduleName Name of the module or class where the log request was generated
 * @param message Message to be displayed
 */
function log(color?: colors.Color, severity?: string, moduleName?: string, message?: string): void {

   if (color && severity && moduleName && message) {
      const msgTime = msToTime(Date.now() - startTime);
      const sev = addSpacesAfter(severity, 7);
      const pid = addSpacesBefore(process.pid.toString(), 5);
      const mod = addSpacesAfter(moduleName, 20);

      const match = message.match(/'.*?'/g);

      if (match) {
         for (const m of match) {
            const mr = m.replace(/'/g, "");
            message = message.replace(m, color.cyan(mr));
         }
      }

      console.log(color(`${msgTime} ${pid} ${sev} ${mod} ${message}`));
   } else {
      console.log("");
   }

}


/**
 * Enables or disables logging completely
 * Logging is enabled by default
 * @param value Specifies if the logging will be enabled or disabled
 */
export function enabled(value: boolean): void {
   _enabled = value;
}


/**
 * Sets the debug level
 * Default logging level is 0
 * @param value Debug level in range from 0 to x
 */
export function debugLevel(value: number): void {
   if (value < 0) return;
   _debugLevel = value;
}


/**
 * Logs an information message
 * @param module Name of the class or module where log event occured (usually this.constructor.name)
 * @param level Debug or verbosity level (message will be shown only if debug level is set to same or higher value)
 * @param message Message to be shown. Text in single quotes will be highlighted (i.e. "exmaple 'log' message")
 */
export function info(module?: string, level: number = 0, message?: string): void {

   if (!_enabled || level > _debugLevel) return;

   if (module && message) {
      log(colors.white, "INFO", module, message);
   } else {
      log();
   }

}


/**
 * Logs an warning message
 * @param module Name of the class or module where log event occured (usually this.constructor.name)
 * @param message Message to be shown. Text in single quotes will be highlighted (i.e. "exmaple 'log' message")
 */
export function warn(module: string, message: string): void {
   if (!_enabled) return;
   log(colors.yellow, "WARNING", module, message);
}


/**
 * Logs an error message
 * @param module Name of the class or module where log event occured (usually this.constructor.name)
 * @param message Message to be shown. Text in single quotes will be highlighted (i.e. "exmaple 'log' message")
 */
export function error(module: string, message: string): void {
   log(colors.red, "ERROR", module, message);
}
