/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { CmdLineArgs } from "./CmdLineArgs";

function parse(argv: string[], args: CmdLineArgs): void {

   let arg: number = 2;

   while (arg < argv.length) {

      switch (argv[arg]) {

         case "-h":
         case"--help":
            args.help = true;
            break;

         case "-v":
         case "-version":
            args.version = true;
            break;

         case "-d":
         case "--debug":
            args.debug = true;
            if (arg < argv.length - 1 && !argv[arg + 1].startsWith("-")) {
               const debugLevel = parseInt(argv[arg + 1]);
               if (!isNaN(debugLevel)) {
                  args.debugLevel = debugLevel;
                  arg++;
               }
            }
            break;

         case "-m":
         case "--manage":
            args.manage = true;
            break;

         case "-r":
         case "--reset":
            args.clean = true;
            break;

         case "-w":
         case "--watch":
            args.watch = true;
            break;

         case "-c":
         case "--config":
            if (arg < argv.length - 1) {
               arg++;
            } else {
               args.cmdLineParsingErrorCode = 1;
               args.cmdLineParsingError = "Name of the build configuration expected";
               break;
            }
            if (!argv[arg].startsWith("-")) {
               args.buildConfigName = argv[arg];
            } else {
               args.cmdLineParsingErrorCode = 2;
               args.cmdLineParsingError = "Name of the build configuration expected but argument obtained";
            }
            break;

         case "-s":
         case "--solution":
            if (arg < argv.length - 1) {
               arg++;
            } else {
               args.cmdLineParsingErrorCode = 3;
               args.cmdLineParsingError = "Path to the solution expected";
               break;
            }
            if (!argv[arg].startsWith("-")) {
               args.solution = argv[arg];
            } else {
               args.cmdLineParsingErrorCode = 4;
               args.cmdLineParsingError = "Path to the solution expected but argument obtained";
            }
            break;

         default:
            if (!argv[arg].startsWith("-")) {
               if (args.project && args.project !== "") {

               } else {
                  args.project = argv[arg];
                  args.cmdLineParsingError = `Single project can be specified only`;
               }
            } else {
               args.cmdLineParsingErrorCode = 6;
               args.cmdLineParsingError = `Invalid argument '${argv[arg]}'`;
            }
            break;

      }

      if (args.cmdLineParsingErrorCode !== 0) break;

      arg++;
   }

}

export function parseCmdLine(argv: string[]): CmdLineArgs {

   const args: CmdLineArgs = {
      help: false,
      version: false,
      debug: false,
      debugLevel: 0,
      manage: false,
      clean: false,
      watch: false,
      buildConfigName: "",
      solution: "",
      project: "",
      cmdLineParsingErrorCode: 0,
      cmdLineParsingError: ""
   };

   parse(argv, args);

   return args;

}