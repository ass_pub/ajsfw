/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as path from "path";
import * as diag from "../diag";
import * as json from "../json";

import { Project } from "../project";
import { ProjectConfig } from "../project";
import { SolutionConfig } from "./SolutionConfig";

export interface ProjectsMap {
   [projectName: string]: Project;
}

export interface BuildOrder {
   [projectName: string]: Project[];
}

export class Solution {

   private __initialized: boolean;

   private __name: string;
   public get name(): string { return this.__name; }

   private __path: string;
   public get path(): string { return this.__path; }

   private __srcDirName: string;
   public get srcDirName(): string { return this.__srcDirName; }

   private __buildDirName: string;
   public get buildDirName(): string { return this.__buildDirName; }

   private __projects: ProjectsMap;
   public get projects(): ProjectsMap { return this.__projects; }

   private __defaultProjectName: string;
   public get defaultProjectName(): string { return this.__defaultProjectName; }

   constructor (solutionPath: string) {
      this.__initialized = false;
      this.__name = path.basename(solutionPath);
      this.__path = solutionPath;
      this.__srcDirName = ".src";
      this.__buildDirName = ".build";
      this.__projects = {};
      this.__defaultProjectName = "";
   }

   public init(): boolean {

      if (this.__initialized) return true;

      const solutionConfig = this.__loadConfig();
      if (solutionConfig === null) return false;

      this.__diagSolutionInfo(solutionConfig);

      if (!this.__configure(solutionConfig)) return false;
      if (!this.__createProjects(solutionConfig)) return false;

      this.__initialized = true;
      return true;
   }

   /**
    * Loads solution configuration from solutionPath and returns it
    * If something fails, null is returned
    */
   private __loadConfig(): SolutionConfig {

      diag.info(this.constructor.name, 1, "Loading solution configuration");
      const solutionConfigFileName = path.join(this.__path, ".abuild.json");

      diag.info(this.constructor.name, 1, `   File name: '${solutionConfigFileName}'`);

      // check if config exists
      if (!fs.existsSync(solutionConfigFileName)) {
         diag.error(this.constructor.name, "Solution configuration file not found. Nothing to do!");
         return null;
      }

      // load solution config
      let solutionConfigContent: Buffer;

      try {
         solutionConfigContent = fs.readFileSync(solutionConfigFileName);
      } catch (e) {
         diag.error(this.constructor.name, e);
         return null;
      }

      // parse json and eventualy grab more detailed info about error in JSON file
      let solutionConfig: SolutionConfig;

      try {
         const solutionConfigNoComments = json.stripComments(solutionConfigContent.toString());
         solutionConfig = JSON.parse(solutionConfigNoComments);
      } catch (e) {
         diag.error(this.constructor.name, e.toString());
         if (e.message) {
            diag.error(this.constructor.name, json.errorInfo(solutionConfigContent, e.message));
         }
         return null;
      }

      if (!this.__validateConfig(solutionConfig)) return null;

      diag.info(this.constructor.name, 1, "Solution configuration file loaded successfully.");

      return solutionConfig;

   }

   private __validateConfig(solutionConfig: SolutionConfig): boolean {

      const allowedOptions = [ "options", "projects" ];

      for (const k in solutionConfig) {
         if (allowedOptions.indexOf(k) === -1) {
            diag.error(this.constructor.name, `Invalid solution configuration section '${k}'.`);
            return false;
         }
      }

      if (solutionConfig.options) {

         const allowedOptions = [ "srcDirName", "buildDirName" ];

         for (const k in solutionConfig.options) {
            if (allowedOptions.indexOf(k) === -1) {
               diag.error(this.constructor.name, `Invalid solution configuration option '${k}'.`);
               return false;
            }
         }

      }

      if (!solutionConfig.projects || typeof solutionConfig.projects !== "object") {
         diag.error(this.constructor.name, "No projects configured. Nothing to do!");
         return false;
      }

      const projectNames = Object.getOwnPropertyNames(solutionConfig.projects);
      if (projectNames.length === 0) {
         diag.error(this.constructor.name, "No projects configured. Nothing to do!");
         return false;
      }

      return true;

   }

   private __configure(solutionConfig: SolutionConfig): boolean {

      this.__srcDirName = this.__srcDirName || solutionConfig.options.srcDirName;
      this.__buildDirName = this.__buildDirName || solutionConfig.options.buildDirName;

      const projectNames = Object.getOwnPropertyNames(solutionConfig.projects);
      this.__defaultProjectName = projectNames[0];

      return true;
   }

   private __createProjects(solutionConfig: SolutionConfig): boolean {

      let creationOrder: string[] = Object.getOwnPropertyNames(solutionConfig.projects);

      creationOrder = creationOrder.sort(
         (a: string, b: string): number => {

            const pa = solutionConfig.projects[a];
            const pb = solutionConfig.projects[b];

            pa.depends = pa.depends || [];
            pb.depends = pb.depends || [];

            if (pa.depends.indexOf(b) !== -1) return 1;
            if (pb.depends.indexOf(a) !== -1) return -1;
            if (pa.depends.length === 0 && pb.depends.length > 0) return -1;
            if (pb.depends.length === 0 && pa.depends.length > 0) return 1;

            return 0;
         }
      );

      for (const projectName of creationOrder) {
         if (!this.__createProject(projectName, solutionConfig.projects[projectName])) return false;
      }

      return true;

   }

   private __createProject(projectName: string, projectConfig: ProjectConfig): boolean {

      if (this.__projects.hasOwnProperty(projectName)) {
         diag.error(this.constructor.name, `Duplicate project found in solution file: ${projectName}`);
         return false;
      }

      const dependsOn: Project[] = [];
      if (projectConfig.depends && projectConfig.depends instanceof Array) {
         for (const depends of projectConfig.depends) {
            if (!this.__projects.hasOwnProperty(depends)) {
               diag.error(this.constructor.name, `Project dependancy is missing: '${depends}'. Check config for circular dependencies.`);
               return false;
            }
            dependsOn.push(this.__projects[depends]);
         }
      }

      diag.info(this.constructor.name, 1, `Creating project '${projectName}'`);

      const project = new Project(projectName, projectConfig, this, dependsOn);
      if (!project.init()) return false;

      this.__projects[projectName]  = project;

      return true;
   }

   private __diagSolutionInfo(solutionConfig: SolutionConfig): void {

      diag.info(this.constructor.name, 2, `   Solution name:        '${this.__name}'`);
      diag.info(this.constructor.name, 2, `   Solution path:        '${this.__path}'`);
      diag.info(this.constructor.name, 2, `   Sources:              '${this.__srcDirName}'`);
      diag.info(this.constructor.name, 2, `   Build:                '${this.__buildDirName}'`);
      diag.info(this.constructor.name, 2, `   Default project name: '${Object.getOwnPropertyNames(solutionConfig.projects)[0]}'`);
      diag.info(this.constructor.name, 2, `   Projects:             '${Object.getOwnPropertyNames(solutionConfig.projects).join(", ")}'`);

   }

}