/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as path from "path";
import * as diag from "../diag";
import * as json from "../json";

import api from "../../api";

import { CmdLineArgs } from "../cmdline/CmdLineArgs";
import { ABuildConfig  } from "./SolutionConfig.old";
import { ABuildToolConfig } from "../tools/ABuildToolConfig";
import { ABuildToolsInfo } from "../tools/ABuildToolsInfo";
import { ABuildToolSuper } from "../tools/ABuildToolSuper";
import { FileWatcher } from "../fwatch";
import { getTools } from "../tools";

export class Solution {

   private __cmdLineArgs: CmdLineArgs;
   public get cmdLineArgs(): CmdLineArgs { return this.__cmdLineArgs; }

   private __solutionConfig: ABuildConfig;
   public get solutionConfig(): ABuildConfig { return this.__solutionConfig; }

   private __solutionPath: string;
   public get solutionPath(): string { return this.__solutionPath; }

   private __srcDirName: string;
   public get srcPath(): string { return this.__srcDirName; }

   private __buildDirName: string;
   public get buildPath(): string { return this.__buildDirName; }

   private __projects: string[];
   public get projects(): string[] { return this.__projects; }

   private __toolList: ABuildToolsInfo;

   private __fileWatcher: FileWatcher;

   constructor(cmdLineArgs: CmdLineArgs) {

      this.__cmdLineArgs = cmdLineArgs;

      this.__solutionConfig = {
         config: {
            srcDirName: ".src",
            buildDirName: ".build"
         },
         projects: {}
      };

      this.__projects = [];

      this.__fileWatcher = new FileWatcher();

      this.__toolList = getTools();

   }

   /**
    * Initializes solution
    * Resolves paths and loads solition config if exists
    */
   public init(): boolean {

      // resolve solution path
      const spath = path.normalize(this.__cmdLineArgs.solution);
      if (path.isAbsolute(spath)) this.__solutionPath = spath;
      this.__solutionPath = path.resolve(process.cwd(), spath);

      diag.info(this.constructor.name, `Solution path: '${this.__solutionPath}'`);

      // load solution config if found
      if (!this.__loadSolutionConfig()) return false;

      // resolve paths
      this.__srcDirName = this.__solutionConfig.config.srcDirName;
      this.__buildDirName = this.__solutionConfig.config.buildDirName;

      this.__printSolutionInfo();

      return true;

   }

   /**
    * Starts required operation on the solution
    */
   public run(): boolean {

      // if watch and reset options are selected report error and exit
      if (this.__cmdLineArgs.clean && this.__cmdLineArgs.watch) {
         diag.error(this.constructor.name, "Reset (clean) and watch arguments can't be used at the same time");
         return false;
      }

      // prepare the list of projects to operate with
      let scope: string[] = [];
      if (this.__cmdLineArgs.project) scope.push(this.__cmdLineArgs.project);
      if (scope.length === 0) scope.push(this.__projects[0]);

      // add all nesessary projects and sort them to required build order
      scope = this.__buildOrder(scope);

      // check if projects exists in the solution config file
      for (const project of scope) {
         if (this.__projects.indexOf(project) === -1) {
            diag.error(this.constructor.name, `Required project '${project}' does not exist in config file`);
            return false;
         }
      }

      // clean or build

      if (this.__cmdLineArgs.clean) return this.__op("clean", scope);
      if (this.__cmdLineArgs.watch) return this.__op("watch", scope);
      return this.__op("build", scope);

   }

   /**
    * Performs the clean operation for all projects
    * @param scope List of projects to be cleaned
    */
   private __op(operation: "clean" | "build" | "watch", scope: string[]): boolean {

      diag.info(this.constructor.name, `Starting '${operation}' operation...`);
      diag.info(this.constructor.name, `Projects in scope:   '${scope.join(", ")}'`);

      for (const p of scope) {

         if (!this.__solutionConfig.projects.hasOwnProperty(p)) {
            diag.error(this.constructor.name, `Project '${p}' configuration not found!`);
            return false;
         }

         const project = this.__solutionConfig.projects[p];

         for (const t of project.tools) {

            const toolName = t.indexOf(".") === -1 ? t : t.substr(0, t.indexOf("."));
            const toolConfigName = t.indexOf(".") === -1 ? t : t.substr(t.indexOf(".") + 1);

            diag.info(this.constructor.name, `Performing '${operation}' operation on project '${p}' using the '${toolName}' tool ${toolConfigName !== toolName ? `using config: '${toolConfigName}'` : ""}`);

            if (!this.__toolList.hasOwnProperty(toolName)) {
               diag.error(this.constructor.name, `   Project '${p}' is configured to use unresolvable tool '${toolName}'`);
               return false;
            }

            const config: ABuildToolConfig = {
               solutionPath: this.__solutionPath,
               srcDirName: this.__srcDirName,
               buildDirName: this.__buildDirName,
               buildConfig: this.__cmdLineArgs.config,
               projectName: p,
               projectScope: scope,
               topLevelProjectName: scope[scope.length - 1],
            };

            if (this.__solutionConfig.projects[p].config && this.__solutionConfig.projects[p].config.hasOwnProperty(t)) {
               for (const k in this.__solutionConfig.projects[p].config[t]) {
                  (<any>config)[k] = (<any>this.__solutionConfig.projects[p].config[t])[k];
               }
            }

            const tool = new this.__toolList[toolName].ctor(api, config);
            let result: boolean;

            switch (operation) {

               case "clean":
                  result = tool.clean();
                  break;

               case "build":
                  result = tool.build();
                  break;

               case "watch":
                  result = tool.watch();
                  break;

            }

            if (!result) return false;

         }


      }

      return true;

   }

   /**
    * Prepares necessary paths and loads the solution config file from the disk if exists
    * @returns true if the file is loaded successfully, otherwise returns false
    */
   private __loadSolutionConfig(): boolean {

      diag.info(this.constructor.name, "Loading solution configuration");
      const scfn = path.join(this.__solutionPath, ".abuild.json");

      diag.info(this.constructor.name, `File name: '${scfn}'`);

      // check if config exists
      if (!fs.existsSync(scfn)) {
         diag.warn(this.constructor.name, "Solution configuration file not found. Nothing to do!");
         return false;
      }

      // load solution config
      let scc: Buffer;

      try {
         scc = fs.readFileSync(scfn);
      } catch (e) {
         diag.error(this.constructor.name, e);
         return false;
      }

      // parse config and eventualy grab more detailed info about error in JSON file
      let cfg: ABuildConfig;

      try {
         const sccnc = json.stripComments(scc.toString());
         cfg = JSON.parse(sccnc);
      } catch (e) {
         diag.error(this.constructor.name, e);
         if (e.message) {
            diag.error(this.constructor.name, json.errorInfo(scc, e.message));
         }
         return false;
      }

      this.__mergeConfig(this.__solutionConfig, cfg);

      // extract project names
      for (const project in this.__solutionConfig.projects) {
         this.__projects.push(project);
      }

      diag.info(this.constructor.name, "Solution configuration file loaded successfully.");

      return true;

   }

   /**
    * Merges source object to destination
    * Destionation properties are replaced with source properties if exist
    * @param dst Destination object
    * @param src Source object
    */
   private __mergeConfig(dst: any, src: any): void {

      for (const key in src) {

         if (typeof src[key] === "object" && !(src[key] instanceof Array)) {
            dst[key] = {};
            this.__mergeConfig(dst[key], src[key]);
         } else {
            dst[key] = src[key];
         }

      }
   }

   /**
    * Prints information about solution to the console
    */
   private __printSolutionInfo(): void {

      diag.info(this.constructor.name, `Sources:             '${this.__srcDirName}'`);
      diag.info(this.constructor.name, `Build:               '${this.__buildDirName}'`);
      diag.info(this.constructor.name, `Projects in config:  '${this.__projects.join(", ")}'`);

   }

   /**
    * Buids the ordered list of projects according to project dependencies
    * @param scope List of projects to be build
    *
    * @param orderedScope Used internally by the function in recurse, caller should not set this field
    */
   private __buildOrder(scope: string[], orderedScope: string[] = []): string[] {

      for (const project of scope) {

         if (orderedScope.indexOf(project) !== -1) continue;
         if (!this.__solutionConfig.projects.hasOwnProperty(project)) continue;
         if (!this.__solutionConfig.projects[project].hasOwnProperty("depends")) continue;
         if (!(this.__solutionConfig.projects[project].depends instanceof Array)) continue;

         for (const dep of this.__solutionConfig.projects[project].depends) {
            if (dep === project) {
               diag.warn(this.constructor.name, `Project '${project}' can't depend on itself!`);
               continue;
            }
            this.__buildOrder([ dep ], orderedScope);
         }

         if (orderedScope.indexOf(project) === -1) orderedScope.push(project);
      }

      return orderedScope;

   }


}