/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import api from "../../api";

import * as diag from "../diag";

import { Solution } from "../solution";
import { ABuildTool } from "../tools";
import { ProjectConfig } from "./ProjectConfig";

export class Project {

   private __name: string;
   public get name(): string { return this.__name; }

   private __solution: Solution;
   public get solution(): Solution { return this.__solution; }

   private __projectConfig: ProjectConfig;
   public get projectConfig(): ProjectConfig { return this.__projectConfig; }

   private __dependsOn: Project[];
   public get dependsOn(): Project[] { return this.__dependsOn; }

   private __tools: ABuildTool[];
   public get tools(): ABuildTool[] { return this.__tools; }

   constructor (name: string, projectConfig: ProjectConfig, solution: Solution, dependsOn: Project[]) {
      this.__name = name;
      this.__projectConfig = projectConfig;
      this.__solution = solution;
      this.__dependsOn = dependsOn;
      this.__tools = [];
   }

   public init(): boolean {

      if (!this.__validateConfig()) return false;

      this.__diagInfo();

      const tools: ABuildTool[] = this.__createTools();
      if (tools === null) return false;
      this.__tools = tools;

      return true;
   }

   public clean(): boolean {
      for (const tool of this.__tools) {
         if (!tool.clean()) return false;
      }
      return true;
   }

   public build(buildConfig: string): boolean {
      for (const tool of this.__tools) {
         if (!tool.build(buildConfig)) return false;
      }
      return true;
   }

   public watch(buildConfig: string): boolean {
      for (const tool of this.__tools) {
         if (!tool.watch(buildConfig)) return false;
      }
      return true;
   }

   private __validateConfig(): boolean {

      const allowedOptions = [ "depends", "tools", "config" ];
      for (const k in this.__projectConfig) {
         if (allowedOptions.indexOf(k) === -1) {
            diag.error(this.constructor.name, `Invalid project configuration option '${k}'.`);
            return false;
         }
      }

      if (!this.__projectConfig.tools || !(this.__projectConfig.tools instanceof Array) || this.__projectConfig.tools.length === 0) {
         diag.error(this.constructor.name, `Project '${this.__name}' has no build tool chain defined.`);
         return false;
      }

      return true;

   }

   private __createTools(): ABuildTool[] {

      const tools: ABuildTool[] = [];

      for (const tool of this.__projectConfig.tools) {

         let toolName: string;
         const configName: string = tool;

         if (tool.indexOf(".") !== -1) {
            toolName = tool.substr(0, tool.indexOf("."));
         } else {
            toolName = tool;
         }

         let toolConfig: any;
         if (this.__projectConfig.config && this.__projectConfig.config.hasOwnProperty(configName)) {
            toolConfig = this.__projectConfig.config[configName];
         } else {
            toolConfig = {};

            if (toolName !== configName) {
               diag.warn(this.constructor.name, `   Tool '${configName}' has no configuration defined. Default configuration will be used`);
            }
         }

         diag.info(this.constructor.name, 1, `   Creating tool '${toolName}' using config '${configName}'`);

         const toolObj: ABuildTool = this.__createTool(toolName, toolConfig);
         if (toolObj === null) return null;
         tools.push(toolObj);

      }

      return tools;

   }

   private __createTool(toolName: string, toolConfig: object): ABuildTool {

      const toolObj = api.aBuildToolManager.createTool(toolName, this, toolConfig);

      return toolObj;

   }


   private __diagInfo(): void {
      diag.info(this.constructor.name, 2, `   Depends: '${this.__projectConfig.depends.join(", ") || "no depependent"}'`);
      diag.info(this.constructor.name, 2, `   Tools:   '${this.__projectConfig.tools.join(", ")}'`);

   }

}
