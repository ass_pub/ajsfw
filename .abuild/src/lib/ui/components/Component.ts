/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import terminal from "../terminal";

import { AcceleratorMap } from "./AcceleratorMap";
import { Color, Rect, Point } from "../common";
import { ColorTextBuffer, ColorChar } from "../textbuffer";
import { BorderStyle } from "../common/BorderStyle";
import { setColors } from "./Theme";
import { EventDispatcher } from "../../events";

export class Component {

   protected _parent: Component;
   public get parent(): Component { return this._parent; }

   protected _children: Component[];
   public get children(): Component[] { return this._children; }

   protected _visible: boolean;
   public get visible(): boolean { return this._visible; }
   public set visible(value: boolean) { this.__setVisibility(value); }

   protected _enabled: boolean;
   public get enabled(): boolean { return this._enabled; }
   public set enabled(value: boolean) { this.__setEnabled(value); }

   protected _hasFocus: boolean;
   public get hasFocus(): boolean { return this._hasFocus; }

   protected _left: number;
   public get left(): number { return this._left; }
   public set left(value: number) { this._left = value; this.__resized(); }

   protected _top: number;
   public get top(): number { return this._top; }
   public set top(value: number) { this._top = value; this.__resized(); }

   protected _width: number;
   public get width(): number { return this._width; }
   public set width(value: number) {  this._width = value; this.__resized(); }

   protected _height: number;
   public get height(): number { return this._height; }
   public set height(value: number) {  this._height = value; this.__resized(); }

   protected _themeClass: string;
   public get themeClass(): string { return this._themeClass; }
   public set themeClass(value: string) {
      this._themeClass = value;
      setColors(this);
      for (const child of this._children) setColors(child);
   }

   protected _background: Color;
   public get background(): Color { return this._background !== Color.inherit ? this._background : this._getInheritedColor("background"); }
   public set background(value: Color) { this._background = value; this.repaint(true); }

   protected _color: Color;
   public get color(): Color { return this._color !== Color.inherit ? this._color : this._getInheritedColor("color"); }
   public set color(value: Color) {  this._color = value; this.repaint(true); }

   protected _disabledBackground: Color;
   public get disabledBackground(): Color  { return this._disabledBackground !== Color.inherit ? this._disabledBackground : this._getInheritedColor("disabledBackground"); }
   public set disabledBackground(value: Color) {  this._disabledBackground = value; this.repaint(true); }

   protected _disabledColor: Color;
   public get disabledColor(): Color { return this._disabledColor !== Color.inherit ? this._disabledColor : this._getInheritedColor("disabledColor"); }
   public set disabledColor(value: Color) {  this._disabledColor = value; this.repaint(true); }

   protected _border: BorderStyle;
   public get border(): BorderStyle { return this._border; }
   public set border(value: BorderStyle) {  this._border = value; this.repaint(true); }

   protected _borderColor: Color;
   public get borderColor(): Color { return this._borderColor !== Color.inherit ? this._borderColor : this._getInheritedColor("border"); }
   public set borderColor(value: Color) {  this._borderColor = value; this.repaint(true); }

   protected _acceleratorColor: Color;
   public get acceleratorColor(): Color { return this._acceleratorColor !== Color.inherit ? this._acceleratorColor : this._getInheritedColor("accelerator"); }
   public set acceleratorColor(value: Color) {  this._acceleratorColor = value; this.repaint(true); }

   protected _caption: string;
   public get caption(): string { return this._caption; }
   public set caption(value: string) {  this._caption = value; this.repaint(); }

   protected _scrollableX: boolean;
   public get scrollableX(): boolean { return this._scrollableX; }
   public set scrollableX(value: boolean) { this._scrollableX = value; this.repaint(true); }

   protected _scrollableY: boolean;
   public get scrollableY(): boolean { return this._scrollableY; }
   public set scrollableY(value: boolean) { this._scrollableY = value; this.repaint(true); }

   public set scrollable(value: boolean) { this._scrollableX = value; this._scrollableY = value; this.repaint(true); }

   protected _canFocus: boolean;
   public get canFocus(): boolean { return this._canFocus; }
   public set canFocus(value: boolean) { this._canFocus = value; }

   protected _scrollX: number;
   public get scrollX(): number { return this._scrollX; }

   protected _scrollY: number;
   public get scrollY(): number { return this._scrollY; }

   private __onResize: EventDispatcher<void>;
   public get onResize(): EventDispatcher<void> { return this.__onResize; }

   private __onParentResize: EventDispatcher<void>;
   public get onParentResize(): EventDispatcher<void> { return this.__onParentResize; }

   private __onKeyPress: EventDispatcher<string>;
   public get onKeyPress(): EventDispatcher<string> { return this.__onKeyPress; }

   private __onFocus: EventDispatcher<void>;
   public get onFocus(): EventDispatcher<void> { return this.__onFocus; }

   private __onFocusLost: EventDispatcher<void>;
   public get onFocusLost(): EventDispatcher<void> { return this.__onFocusLost; }

   private __onAccelerator: EventDispatcher<void>;
   public get onAccelerator(): EventDispatcher<void> { return this.__onAccelerator; }

   public set setOnFocus(value: () => void) { this.__onFocus.addListener(() => { value(); }); }
   public set setOnFocusLost(value: () => void) { this.__onFocusLost.addListener(() => { value(); }); }

   protected _buffer: ColorTextBuffer;

   protected _initialized: boolean;
   protected _rendered: boolean;

   private __repaintScheduled: boolean;
   private __renderScheduled: boolean;

   private __focusedChild: Component;

   private __accelerators: AcceleratorMap;

   constructor(parent?: Component) {

      this._initialized = false;
      this._rendered = false;

      this.__repaintScheduled = false;
      this.__renderScheduled = false;

      this._parent = parent || null;

      this._children = [];
      this.__accelerators = {};

      this.__focusedChild = null;

      this._visible = true;
      this._enabled = true;
      this._hasFocus = false;

      this._left = 0;
      this._top = 0;
      this._width = 0;
      this._height = 0;

      this._themeClass = null;
      this._background = parent ? Color.inherit : Color.white;
      this._color = parent ? Color.inherit : Color.black;
      this._disabledBackground = parent ? Color.inherit : Color.blue;
      this._disabledColor = parent ? Color.inherit : Color.black;
      this._border = BorderStyle.none;
      this._borderColor = parent ? Color.inherit : Color.white;
      this._acceleratorColor = parent ? Color.inherit : Color.yellow;

      this._caption = "";
      this._scrollableX = false;
      this._scrollableY = false;
      this._canFocus = false;

      this._scrollX = 0;
      this._scrollY = 0;

      this.__onResize = new EventDispatcher();
      this.__onResize.addListener((sender: any) => { this._onResize(sender); });

      this.__onParentResize = new EventDispatcher();
      this.__onParentResize.addListener((sender: any) => { this._onParentResize(sender); });

      this.__onKeyPress = new EventDispatcher();
      this.__onKeyPress.addListener((sender: any, event: string) => { this._onKeyPress(sender, event); });

      this.__onFocus = new EventDispatcher();
      this.__onFocus.addListener((sender: any) => { this._onFocus(sender); });

      this.__onFocusLost = new EventDispatcher();
      this.__onFocusLost.addListener((sender: any) => { this._onFocusLost(sender); });

      this.__onAccelerator = new EventDispatcher();
      this.__onAccelerator.addListener((sender: any) => { this._onAccelerator(sender); });

   }

   public init(): void {

      if (this._initialized) return;

      setColors(this);
      this._initializeComponent();

      this._buffer = new ColorTextBuffer(this._width, this._height);
      this._initialized = true;

   }

   public setFocus() {
      if (!this._visible) return;
      if (!this._enabled) return;
      if (!this._canFocus) return;
      if (!this._parent) return;
      this._parent.__focusChild(this);
   }

   public repaint(repaintChildren: boolean = false): void {

      if (!this._visible) return;
      if (this.__repaintScheduled) return;

      this.__repaintScheduled = true;

      setTimeout(() => {
         this._repaint();
         if (repaintChildren) {
            for (const child of this.children) {
               child._repaint();
            }
         }
      }, 0);

   }

   public scrollTo(x: number, y: number) {
      this._scrollX = x;
      this._scrollY = y;
      this._repaint();
   }

   public scrollToVisibleArea() {

      const parent = this._parent;
      if (!parent) return;

      const posX = this._left - parent.scrollX;
      const posY = this._top - parent.scrollY;
      const pwidth = parent._width - (parent.border !== BorderStyle.none ? 2 : 0) - (parent.scrollableY ? 1 : 0);
      const pheight = parent._height - (parent.border !== BorderStyle.none ? 2 : 0) - (parent.scrollableX ? 1 : 0);

      if (parent._scrollableX) {
         if (posX < 0) parent.scrollTo(this._left, parent._scrollY);
         if (posX >= pwidth) parent.scrollTo(this._left + pwidth - 1, parent._scrollY);
      }

      if (parent.scrollableY) {
         if (posY < 0) parent.scrollTo(parent._scrollX, this._top);
         if (posY >= pheight) parent.scrollTo(parent._scrollX,  this._top - pheight + 1);
      }

   }

   public createComponent(component: typeof Component): Component {
      const child = new component(this);
      child.init();
      this._children.push(child);
      return child;
   }

   public deleteComponent(component: Component): void {

      if (this.__focusedChild === component) {
         this.__focusNext();
      }

      const ci = this._children.indexOf(component);
      if (ci !== -1) {
         this._children[ci].cleanup();
         this._children.splice(ci, 1);
         this._repaint();
      }

   }

   public assignAccelerator(char: string, component: Component): void {
      const accel = char.toLowerCase();
      for (const k in this.__accelerators) {
         if (this.__accelerators[k] === component && accel !== k) delete this.__accelerators[k];
      }
      this.__accelerators[accel] = component;
   }

   public showDialog(component: typeof Component): Component {

      let root: Component = this;
      while (root.parent !== null) root = root.parent;

      if (root) {
         const dialog = root.createComponent(component);
         dialog.onParentResize.dispatch(this);
         dialog.setFocus();
         return dialog;
      }

      return null;
   }

   public cleanup() {

      for (const child of this._children) {
         child.cleanup();
      }

      this.__onResize.removeAllListeners();
      this.__onParentResize.removeAllListeners();
      this.__onKeyPress.removeAllListeners();
      this.__onFocus.removeAllListeners();
      this.__onFocusLost.removeAllListeners();
      this.__onAccelerator.removeAllListeners();

   }

   protected _initializeComponent() {
   }

   protected _onChildTabIndexChanged(child: Component, index: number): number {
      return index;
   }

   protected _onResize(sender: any): void {
   }

   protected _onParentResize(sender: any) {
   }

   protected _onFocus(sender: any) {
   }

   protected _onFocusLost(sender: any) {
   }

   protected _onAccelerator(sender: any) {
   }

   private __resized() {

      if (!this._initialized) return;

      this.__onResize.dispatch(this);

      if (this._buffer.width !== this.width || this._buffer.height !== this._height) {
         this._buffer.resize(this._width, this._height, false);

         for (const ch of this._children) {
            ch.onParentResize.dispatch(this);
         }

         this.repaint();

      }

   }

   protected _onKeyPress(sender: any, key: string): boolean {

      if (!this._initialized) return false;

      if (this.__focusedChild) {
         if (this.__focusedChild._onKeyPress(sender, key)) return true;
      }

      for (const k in this.__accelerators) {
         if (key === "alt-" + k) this.__accelerators[k].__onAccelerator.dispatch(this);
      }

      switch (key) {

         case "tab":
            this.__focusNext();
            return true;

         case "shift-tab":
            this.__focusPrev();
            return true;

         case "up":

            if (this.__focusedChild && (<any>(this.__focusedChild)).group !== undefined) {
               this.__focusPrevInGroup((<any>(this.__focusedChild)).group);
               return true;
            } else {
               if (this.__focusedChild) return this.__focusedChild._onKeyPress(sender, key);
            }

            return false;

         case "down":
            if (this.__focusedChild && (<any>(this.__focusedChild)).group !== undefined) {
                this.__focusNextInGroup((<any>(this.__focusedChild)).group);
                return true;
            } else {
               if (this.__focusedChild) return this.__focusedChild._onKeyPress(sender, key);
            }
            break;
      }

      return false;

   }

   protected _repaint(): void {

      if (!this._initialized) return;
      if (!this._visible) return;

      const windowRect = new Rect(0, 0, this._width, this._height);

      this._buffer.clippingRect = windowRect;
      this._buffer.fillRect(
         windowRect,
         new ColorChar(
            this._enabled ? this.background : this.disabledBackground,
            this._enabled ? this.color : this.disabledColor,
            " "
         )
      );

      this._buffer.background = this._enabled ? this.background : this.disabledBackground;

      if (this._border !== BorderStyle.none) {

         this._buffer.color = this._enabled ?  this.borderColor : this.disabledColor;
         this.__drawBorder();

         if (this._caption) {
            this._buffer.background = this.background;
            this._buffer.color = this.color;
            this.__printCaption();
         }

         this._buffer.clippingRect.left++;
         this._buffer.clippingRect.top++;
         this._buffer.clippingRect.width -= 2;
         this._buffer.clippingRect.height -= 2;

      }

      if (this._scrollableX) {
         this.__drawHorizontalScrollbar();
         this._buffer.clippingRect.height--;
      }

      if (this._scrollableY) {
         this.__drawVerticalScrollBar();
         this._buffer.clippingRect.width--;
      }

      this.__scheduleRender();

      this.__repaintScheduled = false;

   }

   protected _getInheritedColor(bgfg: "background" | "color" | "disabledBackground" | "disabledColor" | "border" | "accelerator"): Color {
      let parent = this._parent;
      while (parent) {
         if (bgfg === "background" && parent._background !== Color.inherit) return parent._background;
         if (bgfg === "color" && parent._color !== Color.inherit) return parent._color;
         if (bgfg === "disabledBackground" && parent._disabledBackground !== Color.inherit) return parent._disabledBackground;
         if (bgfg === "disabledColor" && parent._disabledColor !== Color.inherit) return parent._disabledColor;
         if (bgfg === "border" && parent._borderColor !== Color.inherit) return parent._borderColor;
         if (bgfg === "accelerator" && parent._acceleratorColor !== Color.inherit) return parent._acceleratorColor;
         parent = parent._parent;
      }
   }

   protected _clientToScreen(x: number, y: number): Point {

      const cts = new Point(x, y);

      let __this: Component = this;

      while (__this) {

         cts.x += __this.left;
         cts.y += __this.top;

         if (__this._border !== BorderStyle.none) {
            cts.x++;
            cts.y++;
         }

         __this = __this._parent;
      }

      return cts;
   }

   private __drawBorder() {

      if (this._width === 0 || this._height === 0) return;

      let bchars;
      if (this._border === BorderStyle.single) {
         bchars = "┌┐└┘│─";
      } else {
         bchars = "╔╗╚╝║═";
      }

      this._buffer.printAt(0, 0, bchars[0]);
      this._buffer.printAt(this._width - 1, 0, bchars[1]);
      this._buffer.printAt(0, this._height - 1, bchars[2]);
      this._buffer.printAt(this._width - 1, this._height - 1, bchars[3]);


      for (let i = 1; i < this._width - 1; i++) {
         this._buffer.printAt(i, 0, bchars[5]);
         this._buffer.printAt(i, this._height - 1, bchars[5]);
      }

      for (let i = 1; i < this._height - 1; i++) {
         this._buffer.printAt(0, i, bchars[4]);
         this._buffer.printAt(this._width - 1, i, bchars[4]);
      }

   }

   private __printCaption() {

      const cap = ` ${this._caption} `;
      this._buffer.printAt(~~(this._width / 2) - ~~(cap.length / 2), 0, cap);

   }

   private __drawVerticalScrollBar() {

      const scrollBarTop = this._border !== BorderStyle.none ? 1 : 0;
      const scrollBarLeft = this._width  - (this._border !== BorderStyle.none ? 1 : 0) - 1;
      const scrollBarHeight = this._height - (this._border !== BorderStyle.none ? 2 : 0) + (this._scrollableX ? 1 : 0);

      let max = scrollBarHeight;
      for (const child of this._children) {
         if (child.top + child.height > max) max = child.top + child.height;
      }

      let scrollerHeight = (scrollBarHeight / max) * scrollBarHeight;
      if (scrollerHeight < 1) scrollerHeight = 1;

      const scrollerTop = Math.ceil((scrollBarHeight / max) * this._scrollY);

      for (let i = scrollBarTop; i < scrollBarTop + scrollBarHeight; i++) {
         this._buffer.printAt(scrollBarLeft, i, "░");
      }

      for (let i = scrollBarTop + scrollerTop; i < scrollBarTop + scrollerTop + Math.floor(scrollerHeight); i++) {
         this._buffer.printAt(scrollBarLeft, i, "▓");
      }

   }

   private __drawHorizontalScrollbar() {

      const scrollBarTop = this._height - (this._border !== BorderStyle.none ? 1 : 0) - 1;
      const scrollBarLeft = this._border !== BorderStyle.none ? 1 : 0;
      const scrollBarWidth = this._width - (this._border !== BorderStyle.none ? 2 : 0) + (this._scrollableY ? 0 : 1);

      for (let i = scrollBarLeft; i < scrollBarWidth; i++) {
         this._buffer.printAt(i, scrollBarTop, "░");
      }

   }

   private __setVisibility(value: boolean) {

      if (this._visible === value) return;

      if (!this._visible) this._rendered = false;
      this._visible = value;

      if (this.parent) {
         this._parent.repaint(true);
      } else {
         terminal.output.clear();
      }

      if (this._hasFocus && !this._visible) {
         if (this._parent) this._parent.__focusNext();
         if (this.parent.__focusedChild === this) this.parent.__focusedChild = null;
      }

   }

   private __setEnabled(value: boolean) {

      if (this._enabled === value) return;

      this._enabled = value;

      if (this._hasFocus) {
         if (this._parent) this._parent.__focusNext();
      } else {
         this.repaint();
      }

   }

   private __scheduleRender() {

      if (!this._initialized) return;

      if (!this.visible) return;
      if (this.__renderScheduled) return;
      if (this._parent && !this._parent._rendered) return;

      let parent = this._parent;
      while (parent) {
         if (!parent._initialized || !parent._visible) return;
         parent = parent._parent;
      }

      this.__renderScheduled = true;
      setTimeout(() => { this._render(); }, 0);
   }

   protected _render() {

      let parent = this._parent;
      let offsetX = 0;
      let offsetY = 0;

      const clipRect: Rect = new Rect(this._left, this._top, this._width, this._height);

      while (parent) {

         const inc = parent._border !== BorderStyle.none ? 1 : 0;
         const dec = 2 * inc;

         const parentClipRect = new Rect(
            parent._left + inc,
            parent._top + inc,
            parent._width - dec,
            parent._height - dec
         );

         if (parent.scrollableY) parentClipRect.width--;
         if (parent.scrollableX) parentClipRect.height--;

         offsetX += parentClipRect.left - parent.scrollX;
         offsetY += parentClipRect.top - parent.scrollY;

         clipRect.left += parentClipRect.left - parent.scrollX;
         clipRect.top += parentClipRect.top - parent.scrollY;

         clipRect.clip(parentClipRect);

         parent = parent._parent;
      }

      this._buffer.render(this._left + offsetX, this._top + offsetY, clipRect);

      this._rendered = true;

      for (const child of this._children) {
         if (!child.hasFocus && child._visible) child._render();
      }

      if (this.__focusedChild && this.__focusedChild._visible) {
         this.__focusedChild._render();
      }

      this.__renderScheduled = false;

   }

   private __focusNext() {

      if (!this.__focusedChild) {
         for (const child of this._children) {
            if (child._visible && child._canFocus && child._enabled) {
               this.__focusChild(child);
               return;
            }
         }
         return;
      }

      const i = this._children.indexOf(this.__focusedChild);

      for (let j = i + 1; j < this._children.length; j++) {
         if (this._children[j]._visible && this._children[j]._canFocus && this._children[j]._enabled && !this.__isSameGroup(this.__focusedChild, this._children[j])) {
            this.__focusChild(this._children[j]);
            return;
         }
      }

      for (let j = 0; j < i; j++) {
         if (this._children[j]._visible && this._children[j]._canFocus && this._children[j]._enabled && !this.__isSameGroup(this.__focusedChild, this._children[j])) {
            this.__focusChild(this._children[j]);
            return;
         }
      }

   }

   private __focusPrev() {

      if (!this.__focusedChild) {
         for (const child of this._children) {
            if (child._visible && child._canFocus && child._enabled) {
               this.__focusChild(child);
               return;
            }
         }
         return;
      }

      const i = this._children.indexOf(this.__focusedChild);

      for (let j = i - 1; j >= 0 ; j--) {
         if (this._children[j]._visible && this._children[j]._canFocus && this._children[j]._enabled && !this.__isSameGroup(this.__focusedChild, this._children[j])) {
            if ((<any>(this._children[j])).group !== undefined) {
               this.__focusFirstInGroup((<any>(this._children[j])).group);
               return;
            } else {
               this.__focusChild(this._children[j]);
            }
            return;
         }
      }

      for (let j = this._children.length - 1; j > i; j--) {
         if (this._children[j]._visible && this._children[j]._canFocus && this._children[j]._enabled && !this.__isSameGroup(this.__focusedChild, this._children[j])) {
            if ((<any>(this._children[j])).group !== undefined) {
               this.__focusFirstInGroup((<any>(this._children[j])).group);
               return;
            } else {
               this.__focusChild(this._children[j]);
            }
            return;
         }
      }

   }

   private __isSameGroup(component1: any, component2: any): boolean {
      if (component1.group === undefined || component2.group === undefined) return false;
      return component1.group === component2.group;
   }

   private __focusFirstInGroup(group: number) {

      for (let k = 0; k < this._children.length; k++) {
         if ((<any>(this._children[k])).group === group) {
            this.__focusChild(this._children[k]);
            return;
         }
      }

   }

   private __focusNextInGroup(group: number) {

      if (!this.__focusedChild) return;

      const i = this._children.indexOf(this.__focusedChild);

      for (let j = i + 1; j < this._children.length; j++) {
         if (this._children[j]._visible && this._children[j]._canFocus && this._children[j]._enabled && this.__isSameGroup(this.__focusedChild, this._children[j])) {
            this.__focusChild(this._children[j]);
            return;
         }
      }

      for (let j = 0; j < i; j++) {
         if (this._children[j]._visible && this._children[j]._canFocus && this._children[j]._enabled && this.__isSameGroup(this.__focusedChild, this._children[j])) {
            this.__focusChild(this._children[j]);
            return;
         }
      }

   }

   private __focusPrevInGroup(group: number) {
      if (!this.__focusedChild) return;

      const i = this._children.indexOf(this.__focusedChild);

      for (let j = i - 1; j >= 0 ; j--) {
         if (this._children[j]._visible && this._children[j]._canFocus && this._children[j]._enabled && this.__isSameGroup(this.__focusedChild, this._children[j])) {
            this.__focusChild(this._children[j]);
            return;
         }
      }

      for (let j = this._children.length - 1; j > i; j--) {
         if (this._children[j]._visible && this._children[j]._canFocus && this._children[j]._enabled && this.__isSameGroup(this.__focusedChild, this._children[j])) {
            this.__focusChild(this._children[j]);
            return;
         }
      }

   }

   private __focusChild(child: Component) {

      if (this.__focusedChild === child) return;

      if (this.__focusedChild) {
         this.__focusedChild._hasFocus = false;
         this.__focusedChild.__onFocusLost.dispatch(this);
      }

      this.__focusedChild = child;

      if (this.__focusedChild) {
         this.__focusedChild._hasFocus = true;
         this.__focusedChild.__onFocus.dispatch(this);
      }

   }

}