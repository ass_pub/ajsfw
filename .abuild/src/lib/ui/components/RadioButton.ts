/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Component } from "./Component";
import { EventDispatcher } from "../../events";

export class RadioButton extends Component {

   private __onChange: EventDispatcher<void>;
   public get onChange(): EventDispatcher<void> { return this.__onChange; }

   private _group: number;
   public get group(): number { return this._group; }
   public set group(value: number) { this._group = value; }

   protected _checked: boolean;
   public get checked(): boolean { return this._checked; }
   public set checked(value: boolean) {
      if (this._checked !== value) {

         if (value) {
            for (const child of this._parent.children) {
               if (child instanceof RadioButton && child.group === this._group && child !== this) {
                  child.checked = false;
               }
            }
         }

         this._checked = value;
         this.repaint();
         this.onChange.dispatch(this);
      }
   }

   protected _text: string;
   public get text(): string { return this._text; }
   public set text(value: string) {
      this._text = value;
      const acl = this._text.indexOf("&");
      if (acl !== -1 && acl < this._text.length - 1 && this._parent) {
         this._parent.assignAccelerator(this._text[acl + 1], this);
      }
      this.repaint();
   }

   public cleanup() {

      super.cleanup();
      this.__onChange.removeAllListeners();

   }

   protected _initializeComponent() {

      this.__onChange = new EventDispatcher();

      this._group = 0;
      this._checked = false;
      this._text = "Radio button";
      this._height = 1;
      this._canFocus = true;

   }

   protected _repaint() {

      super._repaint();

      if (this._enabled) {
         this._buffer.background = this.background;
         this._buffer.color = this.color;
      } else {
         this._buffer.background = this.disabledBackground;
         this._buffer.color = this.disabledColor;
      }

      this._buffer.printAt(1, 0, `<${this._checked ? "■" : " "}> ${this._text} `);

   }

   protected _onKeyPress(sender: any, key: string): boolean {

      switch (key) {

         case " ":
            if (this.checked) break;
            this.checked = true;
            return true;

      }

      return false;

   }

}