/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import terminal from "../terminal";

import { Component } from "./Component";
import { EventDispatcher } from "../../events";

export class Button extends Component {

   protected _down: boolean;

   private __onPress: EventDispatcher<void>;
   public get onPress(): EventDispatcher<void> { return this.__onPress; }

   private __topChar: string;
   private __botChar: string;

   protected _text: string;
   public get text(): string { return this._text; }
   public set text(value: string) {
      this._text = value;
      const acl = this._text.indexOf("&");
      if (acl !== -1 && acl < this._text.length - 1 && this._parent) {
         this._parent.assignAccelerator(this._text[acl + 1], this);
      }
      this.repaint();
   }

   public cleanup() {
      super.cleanup();
      this.__onPress.removeAllListeners();
   }

   protected _initializeComponent() {

      if (process.env["TERM"] === "linux") {
         this.__topChar = " ";
         this.__botChar = " ";
      } else {
         this.__topChar = "▄";
         this.__botChar = "▀";
      }


      this.__onPress = new EventDispatcher();

      this._down = false;

      this._text = "Button";
      this._height = 3;
      this._canFocus = true;

   }

   protected _repaint() {

      super._repaint();

      this._buffer.background = this._parent.background;

      if (this._enabled) {
         this._buffer.color = this.background;
      } else {
         this._buffer.color = this.disabledBackground;
      }

      for (let i = 0; i < this._width; i++) {
         this._buffer.printAt(i, 0, this.__topChar);
         this._buffer.printAt(i, 2, this.__botChar);
      }

      if (this._enabled) {
         this._buffer.background = this.background;
         this._buffer.color = this.color;
      } else {
         this._buffer.background = this.disabledBackground;
         this._buffer.color = this.disabledColor;
      }

      this._buffer.printAt(~~(this._width / 2) - ~~(this._text.length / 2), 1, this.acceleratorColor, this._text);

   }

   protected _onAccelerator(): void {
      this.setFocus();
      terminal.input.simulateKeyPress(Buffer.from([ 13 ]));
   }

   protected _onKeyPress(sender: any, key: string): boolean {

      switch (key) {
         case " ":
         case "enter":

            if (this._down) return;
            this._down = true;

            let i = 0;
            const interval = setInterval(() => {
               if (i > 1) {
                  this._down = false;
                  clearInterval(interval);
                  this.onPress.dispatch(this);
                  return;
               }
               const c = this._color;
               this.color = this.background;
               this.background = c;
               i++;
            }, 50);

            return true;
      }

      return false;

   }

}