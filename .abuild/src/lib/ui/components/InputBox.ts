/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Component } from "./Component";
import terminal from "../terminal";
import { EventDispatcher } from "../../events";

export class InputBox extends Component {

   private __onChange: EventDispatcher<void>;
   public get onChange(): EventDispatcher<void> { return this.__onChange; }

   private __cursorPos: number;
   private __firstChar: number;

   protected _text: string;
   public get text(): string { return this._text; }
   public set text(value: string) {
      this._text = value;
      this.__firstChar = this._text.length - this._width + 1;
      if (this.__firstChar < 0) this.__firstChar = 0;
      this.__cursorPos = this._text.length;
      this.onChange.dispatch(this);
      this.repaint();
   }

   public cleanup() {
      super.cleanup();
      this.__onChange.removeAllListeners();
   }

   protected _initializeComponent() {

      this.__onChange = new EventDispatcher();

      this.__cursorPos = 0;
      this.__firstChar = 0;

      this._text = "";
      this._height = 1;
      this._canFocus = true;

   }

   protected _onFocus() {
      this.repaint();
   }

   protected _onFocusLost() {
      this.repaint();
   }

   protected _repaint() {

      super._repaint();

      if (this._enabled) {
         this._buffer.background = this.background;
         this._buffer.color = this.color;
      } else {
         this._buffer.background = this.disabledBackground;
         this._buffer.color = this.disabledColor;
      }

      let empty = "";
      while (empty.length < this._width) empty += " ";

      this._buffer.printAt(0, 0, empty);

      let text = this._text;

      if (this._hasFocus) {
         text = text.substr(this.__firstChar, this.width);
      } else {
         text = text.substr(0, this.width);
      }

      this._buffer.printAt(0, 0, text);

   }

   protected _render() {

      terminal.output.hideCursor();

      super._render();

      if (!this._hasFocus) return;

      const cursorScreenPos = this._clientToScreen(0, 0);

      let x = cursorScreenPos.x + this.__cursorPos - this.__firstChar;
      if (x > cursorScreenPos.x + this._width - 1) x = cursorScreenPos.x + this._width - 1;

      terminal.output.moveto(x, cursorScreenPos.y);

      if (this.hasFocus) {
         terminal.output.showCursor();
      }

   }

   protected _onKeyPress(sender: any, key: string): boolean {

      const stopChars = " /\\";

      switch (key) {

         case "home":
            this.__cursorPos = 0;
            this.__firstChar = 0;
            this._repaint();
            return true;

         case "end":
            this.__cursorPos = this._text.length;
            this.__firstChar = this.__cursorPos - this._width + 1;
            if (this.__firstChar < 0) this.__firstChar = 0;
            this._repaint();
            return true;

         case "left":
            this.__cursorLeft();
            this.repaint();
            return true;

         case "right":
            this.__cursorRight();
            this.repaint();
            return true;

         case "ctrl-left":

            if (stopChars.indexOf(this._text[this.__cursorPos - 1]) !== -1) {
               while (this.__cursorPos > 0 && stopChars.indexOf(this._text[this.__cursorPos - 1]) !== -1) {
                  this.__cursorLeft();
               }
            }

            while (this.__cursorPos > 0 && stopChars.indexOf(this._text[this.__cursorPos - 1]) === -1) {
               this.__cursorLeft();
            }

            this.repaint();
            return true;

         case "ctrl-right":

            while (this.__cursorPos < this._text.length && stopChars.indexOf(this._text[this.__cursorPos]) === -1) {
               this.__cursorRight();
            }

            while (this.__cursorPos < this._text.length && stopChars.indexOf(this._text[this.__cursorPos]) !== -1) {
               this.__cursorRight();
            }

            this.repaint();
            return true;

         case "backspace":
            if (this.__cursorPos > 0) {
               this._text = this._text.substr(0, this.__cursorPos - 1) + this._text.substr(this.__cursorPos);
               this.__cursorLeft();
               this.__onChange.dispatch(this);
               this.repaint();
            }
            return true;

         case "delete":
            if (this.__cursorPos < this._text.length) {
               this._text = this._text.substr(0, this.__cursorPos) + this._text.substr(this.__cursorPos + 1);
               this.__onChange.dispatch(this);
               this.repaint();
            }
            return true;

         default:
            if (key.length === 1 && key.charCodeAt(0) >= 32) {
               this._text = this._text.substr(0, this.__cursorPos) + key + this._text.substr(this.__cursorPos);
               this.__cursorRight();
               this.__onChange.dispatch(this);
               this._repaint();
               return true;
            }
            return false;

      }

   }

   private __cursorLeft() {

      this.__cursorPos--;
      if (this.__cursorPos < this.__firstChar) {
         this.__firstChar--;
         if (this.__firstChar < 0) {
            this.__firstChar = 0;
            this.__cursorPos = 0;
         }
      }

   }

   private __cursorRight() {

      this.__cursorPos++;
      if (this.__cursorPos > this._text.length) {
         this.__cursorPos = this._text.length;
      }
      if (this.__cursorPos - this.__firstChar >= this._width) {
         this.__firstChar++;
         if (this.__firstChar + this._width > this._text.length) {
            this.__firstChar = this._text.length - this._width + 1;
            this.__cursorPos = this.__firstChar + this._width - 1;
         }
      }

   }

}