/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Color } from "../common";
import { Component } from "./Component";
import { BorderStyle } from "../common/BorderStyle";
import { ListItem } from "./ListItem";
import { EventDispatcher } from "../../events";

export class List extends Component {

   private __selectionChanged: EventDispatcher<void>;
   public get selectionChanged(): EventDispatcher<void> { return this.__selectionChanged; }

   protected _items: ListItem[];
   public get items(): ListItem[] { return this.items; }

   protected _selectedIndex: number;
   public get selectedIndex(): number { return this._selectedIndex; }
   public set selectedIndex(value: number) { this._selectItemByIndex(value); }

   protected _selectedItem: ListItem;
   public get selectedItem(): ListItem { return this._selectedItem; }
   public set selectedItem(value: ListItem) { this._selectItemByItem(value); }

   protected _selectedItemBackground: Color;
   public get selectedItemBackground(): Color { return this._selectedItemBackground; }
   public set selectedItemBackground(value: Color) { this._selectedItemBackground = value; this.repaint(); }

   protected _selectedItemColor: Color;
   public get selectedItemColor(): Color { return this._selectedItemColor; }
   public set selectedItemColor(value: Color) { this._selectedItemColor = value; this.repaint(); }

   public cleanup() {

      super.cleanup();
      this._items.length = 0;
      this.__selectionChanged.removeAllListeners();

   }

   public addItem(text: string) {

      const item: ListItem = <ListItem>this.createComponent(ListItem);
      this._items.push(item);

      item.text = text;
      item.selected = this._items.length === 1;
      item.top = this._items.length - 1;
      item.width = this.width - (this._border !== BorderStyle.none ? 2 : 0)  - (this._scrollableY ? 1 : 0);

      if (item.selected) {
         this._selectedIndex = this._items.length - 1;
         this._selectedItem = item;
         this.__selectionChanged.dispatch(this);
      }

      this.repaint();

   }

   protected _initializeComponent() {

      this.__selectionChanged = new EventDispatcher();

      this._items = [];
      this._selectedIndex = -1;
      this._selectedItem = null;

   }

   protected _selectItemByIndex(index: number) {

      let i = index;
      if (i === this._selectedIndex) return;

      if (i < 0) i = this._items.length > 0 ? 0 : -1;
      if (i > this._items.length - 1) i = this._items.length - 1;

      if (this._selectedItem) this._selectedItem.selected = false;

      this._selectedIndex = i;
      if (i === -1) {
         this._selectedItem = null;
         this.__selectionChanged.dispatch(this);
         return;
      }

      this._selectedItem = this._items[i];
      this._selectedItem.selected = true;
      this.__selectionChanged.dispatch(this);

   }

   protected _selectItemByItem(item: ListItem) {

      const index = this._items.indexOf(item);
      if (index === -1 || this._selectedItem === item) return;
      if (this._selectedItem) this._selectedItem.selected = false;
      this._selectedIndex = index;
      this._selectedItem = item;
      this._selectedItem.selected = true;
      this.__selectionChanged.dispatch(this);

   }

   protected _onKeyPress(sender: any, key: string): boolean {

      let height: number;

      switch (key) {
         case "up":
            if (this._selectedIndex > 0) this.selectedIndex = this._selectedIndex - 1;
            return true;

         case "down":
            if (this._selectedIndex < this._items.length - 1) this.selectedIndex = this._selectedIndex + 1;
            return true;

         case "pgup":
            height = this._height - (this.border !== BorderStyle.none ? 2 : 0) - (this.scrollableX ? 1 : 0);
            this.selectedIndex = this._selectedIndex - height;
            return true;

         case "pgdown":
            height = this._height - (this.border !== BorderStyle.none ? 2 : 0) - (this.scrollableX ? 1 : 0);
            this.selectedIndex = this._selectedIndex + height;
            return true;

         case "home":
            this.selectedIndex = 0;
            return true;

         case "end":
            this.selectedIndex = this._items.length - 1;
            return true;
      }

      return false;

   }

}