/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Component } from "./Component";
import { Color } from "../common";
import { List } from "./List";

export class ListItem extends Component {

   protected _selectedBackground: Color;
   public get selectedBackground(): Color { return this._selectedBackground; }
   public set selectedBackground(value: Color) { this._selectedBackground = value; this.repaint(); }

   protected _selectedColor: Color;
   public get selectedColor(): Color { return this._selectedBackground; }
   public set selectedColor(value: Color) { this._selectedColor = value; this.repaint(); }

   protected _text: string;
   public get text(): string { return this._text; }
   public set text(value: string) { this._text = value; this.repaint(); }

   protected _selected: boolean;
   public get selected(): boolean { return this._selected; }
   public set selected(value: boolean) { this._selected = value; if (this._selected) this.scrollToVisibleArea(); this.repaint(); }

   constructor(parent?: Component) {

      if (!parent || !(parent instanceof List)) throw "List item muse be created as child of List!";

      super(parent);

      this._selectedBackground = Color.inherit;
      this._selectedColor = Color.inherit;
      this._text = "";
      this._selected = false;
      this._height = 1;
      this._width = this.parent ? parent.width : 10;

   }

   protected _initializeComponent() {

      super._initializeComponent();
      this._text = "Selectable item";
      this._selected = false;

   }

   protected _repaint() {

      super._repaint();

      if (this.selected) {
         if (this._parent && !this._parent.enabled) {
            this._buffer.background = this.disabledColor;
            this._buffer.color = this.disabledBackground;
         } else {
            this._buffer.background = this._selectedBackground === Color.inherit ? (<List>this._parent).selectedItemBackground : this._selectedBackground;
            this._buffer.color = this._selectedColor === Color.inherit ? (<List>this._parent).selectedItemColor : this._selectedColor;
         }
      } else {
         if (this._parent && !this._parent.enabled) {
            this._buffer.color = this.disabledColor;
            this._buffer.background = this.disabledBackground;
         } else {
            this._buffer.color = this.color;
            this._buffer.background = this.background;
         }
      }

      this._buffer.clear();

      this._buffer.printAt(0, 0, this._text);

   }


}