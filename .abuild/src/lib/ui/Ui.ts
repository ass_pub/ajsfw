/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import terminal from "./terminal";

import { setTheme } from "./components/Theme";
import { Component } from "./components";
import { EventDispatcher } from "../events";

export class Ui {

   private __view: Component;

   private __onKey: EventDispatcher<string>;
   public get onKey(): EventDispatcher<string> { return this.__onKey; }

   public __onResize: EventDispatcher<{ width: number, height: number }>;
   public get onResize(): EventDispatcher<{ width: number, height: number }> { return this.__onResize; }

   constructor() {
      this.__view = null;
      this.__onKey = new EventDispatcher();
      this.__onResize = new EventDispatcher();
   }

   public init(theme: any, root: Component): void {

      terminal.output.addOnResizeListener(
         (width: number, height: number): void => {
            this.__onResize.dispatch(null, { width, height });
         }
      );

      terminal.input.addOnKeypressListener(
         (key: string): void => {
            this.__onKey.dispatch(null, key);
         }
      );

      terminal.input.addOnExitListener(() => { this.__onExit(); });

      setTheme(theme);

      this.__view = root;
      this.__view.init();

   }

   public run(): void {

      terminal.output.enterAltScreen();

      terminal.output.moveto(0, 0);

      terminal.output.hideCursor();
      terminal.input.run();
   }

   private __onExit(): void {

      this.__view.visible = false;

      setTimeout(() => {
         this.__view.cleanup();
         terminal.output.exitAltScreen();
         terminal.output.showCursor();
      }, 0);

   }

}
