/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Rect } from "./Rect";

/**
 * 2D point information and operations
 */
export class Point {

   /**
    * Stores X coordinate of the 2D point
    */
   public x: number;

   /**
    * Stores Y coordinate of the 2D point
    */
   public y: number;

   /**
    * Creates and initializes the 2D point object
    * @param x X coordinate of the 2D point
    * @param y Y coordinate of the 2D point
    */
   constructor (x: number, y: number) {
      this.x = x;
      this.y = y;
   }

   /**
    * Checks if the point is inside of the given rectangle
    * @param rect Rectangle to be checked
    */
   public inside(rect: Rect): boolean {
      return this.x >= rect.left && this.x < rect.left + rect.width && this.y >= rect.top && this.y < rect.top + rect.height;
   }

}
