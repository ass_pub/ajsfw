/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

/**
 * Rectangle information and operations
 */
export class Rect {

   public left: number;
   public top: number;
   public width: number;
   public height: number;

   constructor (left: number, top: number, width: number, height: number) {
      this.left = left;
      this.top = top;
      this.width = width;
      this.height = height;
   }

   /**
    * Clips the current rectangle to not overflow the passed rectangle
    * @param rect Rectangle to be used to clip the current rectangle
    */
   public clip(rect: Rect) {

      if (this.left > rect.left + rect.width - 1) { this.width = 0; }
      if (this.top > rect.top + rect.height - 1) { this.height = 0; }

      if (rect.left > this.left) { this.width -= rect.left - this.left; this.left = rect.left; }
      if (rect.top > this.top) { this.height -= rect.top - this.top; this.top = rect.top; }
      if (rect.width < this.width) { this.width = rect.width; }
      if (rect.height < this.height) { this.height = rect.height; }

      if (this.left + this.width > rect.left + rect.width) this.width = rect.width - this.left + 1;
      if (this.top + this.height > rect.top + rect.height) this.height = rect.height - this.top + 1;

   }

}
