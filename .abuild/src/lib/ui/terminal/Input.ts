/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

export interface KeypressEventListener {
   (key: string): void;
}

export interface ExitEventListener {
   (): void;
}

export class Input {

   private __keyPressListeners: KeypressEventListener[];
   private __exitListeners: ExitEventListener[];

   constructor() {
      this.__keyPressListeners = [];
      this.__exitListeners = [];
   }

   public addOnKeypressListener(listener: KeypressEventListener) {
      if (this.__keyPressListeners.indexOf(listener) !== -1) return;
      this.__keyPressListeners.push(listener);
   }

   public addOnExitListener(listener: ExitEventListener) {
      if (this.__exitListeners.indexOf(listener) !== -1) return;
      this.__exitListeners.push(listener);
   }

   public run() {
      process.stdin.setRawMode(true);
      process.stdin.addListener("data", (data: Buffer): void => {
         this.__processKeyPress(data);
      });
   }

   public simulateKeyPress(data: Buffer) {
      this.__processKeyPress(data);
   }

   private __processKeyPress(data: Buffer): void {

      let pos = 0;

      while (pos < data.length) {

         const d0 = data.readUInt8(pos);
         pos++;

         let keyName: string;

         switch (d0) {

            case 0x03:

               for (const listener of this.__exitListeners) {
                  listener();
               }

               setTimeout(() => {
                  process.exit();
               }, 0);

               return;

            case 0x1b:
               const buf = data.subarray(pos - 1);
               const seqInfo = this.__getEscSeqInfo(buf);

               // unknow sequence -> throw rest of the buffer
               if (seqInfo.length === 0) return;

               keyName = seqInfo.sequenceName;
               pos += seqInfo.length - 1;
               break;

            default:
               keyName = this.__getKeyName(d0);
               break;

         }

         for (const listener of this.__keyPressListeners) {
            listener(keyName);
         }

      }

   }

   private __getKeyName(keyCode: number): string {

      switch (keyCode) {
         case 0x08: return "backspace";
         case 0x7F: return "backspace";
         case 0x09: return "tab";
         case 0x0d: return "enter";
         case 0x1b: return "esc";

         default:
            if (keyCode > 31 && keyCode < 127) return String.fromCharCode(keyCode);

            // console.log(keyCode.toString());
            return "unknown";

      }

   }

   private __getEscSeqInfo(escSequence: Buffer): { sequenceName: string, length: number } {

      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x09]))) return { sequenceName: "shift-tab", length: 2 };

      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x4f, 0x44]))) return { sequenceName: "ctrl-left", length: 3 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x4f, 0x43]))) return { sequenceName: "ctrl-right", length: 3 };

      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x5a]))) return { sequenceName: "shift-tab", length: 3 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x41]))) return { sequenceName: "up", length: 3 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x42]))) return { sequenceName: "down", length: 3 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x43]))) return { sequenceName: "right", length: 3 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x44]))) return { sequenceName: "left", length: 3 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x46]))) return { sequenceName: "end", length: 3 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x48]))) return { sequenceName: "home", length: 3 };

      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x35, 0x7e]))) return { sequenceName: "pgup", length: 4 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x36, 0x7e]))) return { sequenceName: "pgdown", length: 4 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x31, 0x7e]))) return { sequenceName: "home", length: 4 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x34, 0x7e]))) return { sequenceName: "end", length: 4 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x32, 0x7e]))) return { sequenceName: "ins", length: 4 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x33, 0x7e]))) return { sequenceName: "delete", length: 4 };

      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x31, 0x3b, 0x35, 0x44]))) return { sequenceName: "ctrl-left", length: 6 };
      if (!Buffer.compare(escSequence, Buffer.from([0x1b, 0x5b, 0x31, 0x3b, 0x35, 0x43]))) return { sequenceName: "ctrl-right", length: 6 };

      if (escSequence.length > 1 && escSequence[1] >= 97 && escSequence[1] <= 122) {
         return { sequenceName: "alt-" + String.fromCharCode(escSequence[1]), length: 2 };
      }

      if (escSequence.length === 1) {
         return { sequenceName: "esc", length: 1 };
      }

      /*for (const b of escSequence) {
         console.log(b);
      }*/

      return { sequenceName: "unknown", length: 0 };

   }

}