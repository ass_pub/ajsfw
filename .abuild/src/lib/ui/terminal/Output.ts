/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Color } from "../common/Color";

export class Output {

   private __onResizeListeners: ((width: number, height: number) => void)[] = [];

   private __background: Color;
   public get background(): Color { return this.__background; }
   public set background(value: Color) { this.__background = value; }

   private __color: Color;
   public get color(): Color { return this.__color; }
   public set color(value: Color) { this.__color = value; }

   private __width: number;
   public get width(): number { return this.__width; }

   private __height: number;
   public get height(): number { return this.__height; }

   private __resized: boolean;

   constructor() {

      this.__onResizeListeners = [];
      this.__background = Color.black;
      this.__color = Color.white;
      this.__width = process.stdout.columns;
      this.__height = process.stdout.rows;

      this.__resized = false;

      process.stdout.addListener("resize", () => {

         this.__width = process.stdout.columns;
         this.__height = process.stdout.rows;

         if (this.__resized) return;
         this.__resized = true;

         setTimeout(() => {
            for (const listener of this.__onResizeListeners) {
               listener(this.__width, this.__height);
            }
            this.__resized = false;
         }, 10);

      });

   }

   public addOnResizeListener(listener: (width: number, height: number) => void) {
      if (this.__onResizeListeners.indexOf(listener) !== -1) return;
      this.__onResizeListeners.push(listener);
   }

   public enterAltScreen() {
      process.stdout.write("\x1b[?1049h");
   }

   public exitAltScreen() {
      process.stdout.write("\x1b[?1049l");
   }

   public hideCursor() {
      process.stdout.write("\x1b[?25l");
   }

   public showCursor() {
      process.stdout.write("\x1b[?25h");
   }

   public clear() {

      let s = "";

      for (let i = 0; i < this.__width; i++) {
         s += " ";
      }

      for (let i = 0; i < this.__height; i++) {
         this.moveto(0, i + 0);
         process.stdout.write(`\x1b[${this.__background + 40}m${s}`);
      }

   }

   public moveto(x: number, y: number) {
      process.stdout.write(`\x1b[${y + 1};${x + 1}H`);
   }

   public printAt(x: number, y: number, text: string) {
      this.moveto(x + 1, y + 1);
      process.stdout.write(`\x1b[${this.__background + 40};${this.__color + 30}m${text}`);
   }

}
