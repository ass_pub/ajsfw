/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

export class ColorChar {

   private __background: number;
   public get background(): number { return this.__background; }

   private __foreground: number;
   public get foreground(): number { return this.__foreground; }

   private __character: number;
   public get character(): string { return String.fromCharCode(this.__character); }

   public static fromBigInt(colorChar: bigint): ColorChar {
      const bk = Number((colorChar & BigInt(0x0000FF0000000000)) >> BigInt(40));
      const fg = Number((colorChar & BigInt(0x000000FF00000000)) >> BigInt(32));
      const ch = Number(colorChar & BigInt(0xFFFFFFFF));
      return new ColorChar(bk, fg, String.fromCharCode(ch), true);
   }

   constructor(background: number, foreground: number, character: string, direct: boolean = false) {
      if (direct) {
         this.__background = background;
         this.__foreground = foreground;
      } else {
         this.__background = background + (background < 10 ? 40 : 90);
         this.__foreground = foreground + (foreground < 10 ? 30 : 80);
      }
      this.__character = character ? character.codePointAt(0) : 32;
   }

   public toBigInt64(): bigint {
      return BigInt(this.__background) << BigInt(40) |
             BigInt(this.__foreground) << BigInt(32) |
             BigInt(this.__character);
   }

}
