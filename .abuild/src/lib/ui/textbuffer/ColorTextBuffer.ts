/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { Rect } from "../common/Rect";
import { Point } from "../common/Point";

import { ColorChar } from "./ColorChar";
import { Color } from "../common";

export class ColorTextBuffer {

   // buffer stores colored text data in the following represenation:
   // /     unused    \  fgcolor  bkcolor /           UTF-8 char            \
   // xxxxxxxx xxxxxxxx ffffffff bbbbbbbb uuuuuuuu uuuuuuuu uuuuuuuu uuuuuuuu
   private __buffer: BigInt64Array;

   private __width: number;
   public get width(): number { return this.__width; }
   public set width(value: number) { this.resize(value, this.__height); }

   private __height: number;
   public get height(): number { return this.__height; }
   public set height(value: number) { this.resize(this.__width, value); }

   private __background: number;
   public get background(): number { return this.__background; }
   public set background(value: number) { this.__background = value; }

   private __color: number;
   public get color(): number { return this.__color; }
   public set color(value: number) { this.__color = value; }

   private __clippingRect: Rect;
   public get clippingRect(): Rect { return this.__clippingRect; }
   public set clippingRect(value: Rect) { this.__setClippingRect(value); }

   private __position: Point;

   constructor(width: number, height: number) {

      this.__width = width;
      this.__height = height;
      this.__background = Color.black;
      this.__color = Color.white;
      this.__position = new Point(0, 0);
      this.__clippingRect = new Rect(0, 0, width, height);

      this.__buffer = new BigInt64Array(width * height);
      this.fillRect(new Rect(0, 0, width, height), new ColorChar(Color.black, Color.white, " "));

   }

   public resize(width: number, height: number, keepContents: boolean = true) {

      const newBuffer = new BigInt64Array(width * height);

      if (!keepContents) {
         this.__buffer = newBuffer;
         this.__width = width;
         this.__height = height;
         this.fillRect(new Rect(0, 0, width, height), new ColorChar(0, 0, " "));
         return;
      }

   }

   public clear() {
      this.fillRect(new Rect(0, 0, this.__width, this.__height), new ColorChar(this.__background, this.__color, " "));
   }

   public fillRect(rect: Rect, colorChar: ColorChar) {
      for (let y = rect.top; y < rect.height; y++) {
         if (y < 0 || y >= this.__height) continue;
         for (let x = rect.left; x < rect.width; x++) {
            if (x < 0 || x >= this.__width) continue;
            this.__buffer[y * this.__width + x] = colorChar.toBigInt64();
         }
      }
   }

   public setPos(left: number, top: number) {
      if (left < 0 || left >= this.__width) return;
      if (top < 0 || top >= this.__height) return;
      this.__position = new Point(left, top);
   }

   public print(text: string) {

      for (const t of text) {

         if (this.__position.inside(this.__clippingRect)) {
            const colorChar = new ColorChar(this.__background, this.__color, t);
            this.__buffer[this.__position.y * this.__width + this.__position.x] = colorChar.toBigInt64();
         }

         this.__position.x ++;
      }

   }

   public printAt(left: number, top: number, text: string): void;
   public printAt(left: number, top: number, accelColor: Color, text: string): void;
   public printAt(left: number, top: number, accelColorText: Color | string, text?: string): void {
      this.setPos(left, top);
      if (typeof accelColorText === "string") {
         this.print(accelColorText);
      } else {
         if (text.indexOf("&") === -1) {
            this.print(text);
         } else {
            for (let i = 0; i < text.length; i++) {
               if (text[i] === "&") {
                  const tmp = this.__color;
                  this.__color = accelColorText;
                  i++;
                  this.print(text[i]);
                  this.color = tmp;
               } else {
                  this.print(text[i]);
               }
            }
         }
      }
   }

   public copyRect(src: ColorTextBuffer, dst: ColorTextBuffer, srcRect: Rect, dstPos: Point) {

      if (srcRect.left > src.width) return;
      if (srcRect.top > src.height) return;
      if (srcRect.left + srcRect.left < 0) return;
      if (srcRect.top + srcRect.height < 0) return;

      const sRect = new Rect(srcRect.left, srcRect.top, srcRect.width, srcRect.height);
      if (sRect.left < 0) { sRect.width += sRect.left; sRect.left = 0; }
      if (sRect.top < 0) { sRect.height += sRect.top; sRect.top = 0; }
      if (sRect.left + sRect.width > src.width) { sRect.width = src.width - sRect.left; }
      if (sRect.top + sRect.height > src.height) { sRect.height = src.height - sRect.top; }

      const dRect = new Rect(0, 0, dst.width, dst.height);

      this.__copyRect(src.__buffer, src.__width, dst.__buffer, dst.__width, sRect, dRect, dstPos);

   }

   public render(x: number, y: number, clippingRect: Rect) {

      let o = "";
      let lch: ColorChar;

      let cRect: Rect;

      if (clippingRect.width === -1 && clippingRect.height === -1) {
         cRect = new Rect(clippingRect.left, clippingRect.top, process.stdout.columns - clippingRect.left, process.stdout.rows - clippingRect.top);
      } else {
         cRect = clippingRect;
      }

      if (cRect.left < 0) { cRect.width += cRect.left; cRect.left = 0; }
      if (cRect.top < 0) { cRect.height += cRect.top; cRect.top = 0; }
      if (cRect.left + cRect.width > process.stdout.columns) cRect.width = process.stdout.columns - cRect.left;
      if (cRect.top + cRect.height > process.stdout.rows) cRect.height = process.stdout.rows - cRect.top;

      if (cRect.width <= 0 || cRect.height <= 0) return;

      const left = x < cRect.left ? cRect.left : x;

      for (let j = 0; j < this.__height; j++) {

         if (y + j < cRect.top) continue;
         if (y + j >= cRect.top + cRect.height) break;
         if (x >= cRect.left + cRect.width) break;

         o += `\x1b[${y + j + 1};${left + 1}H`;

         lch = null;

         for (let i = 0; i < this.__width; i++) {

            if (x + i < cRect.left) continue;
            if (x + i >= cRect.left + cRect.width) break;

            const ch = ColorChar.fromBigInt(this.__buffer[j * this.__width + i]);
            const bk = (ch.background).toString();
            const fg = (ch.foreground).toString();

            if (lch && lch.background === ch.background && lch.foreground === ch.foreground) {
               o += ch.character;
            } else {
               o += `\x1b[${bk};${fg}m${ch.character}`;
            }

            lch = ch;

         }

      }

      process.stdout.write(`${o}\x1b[0m`);

   }

   private __setClippingRect(rect: Rect) {

      const cRect = new Rect(rect.left, rect.top, rect.width, rect.height);

      if (cRect.left < 0) { cRect.width += cRect.left; cRect.left = 0; }
      if (cRect.top < 0) { cRect.height += cRect.top; cRect.top = 0; }
      if (cRect.left + cRect.width > this.__width) { cRect.width = this.__width - cRect.left; }
      if (cRect.top + cRect.height > this.__height) { cRect.height = this.__height - cRect.top; }

      this.__clippingRect = cRect;

   }

   private __copyRect(src: BigInt64Array, srcRowLength: number, dst: BigInt64Array, dstRowLength: number, srcRect: Rect, dstRect: Rect, dstPos: Point) {

      for (let y = 0; y < srcRect.height; y++) {

         const dy = dstPos.y + y;
         if (dy + dstPos.y < dstRect.top) continue;
         if (dy + dstPos.y >= dstRect.top + dstRect.height) continue;

         for (let x = 0; x < srcRect.width; x++) {

            const dx = dstPos.x + x;
            if (dx + dstPos.x < dstRect.left) continue;
            if (dx + dstPos.x >= dstRect.left + dstRect.width) continue;

            dst[dy * dstRowLength + dx] = src[(y + srcRect.top) * srcRowLength + (x + srcRect.left)];

         }
      }
   }


}