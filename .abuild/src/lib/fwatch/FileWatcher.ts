/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as path from "path";

import * as diag from "../diag";

import { FileEventInfo } from "./FileEventInfo";
import { WFSWatcher } from "./WFSWatcher";
import { WatchedFiles } from "./WatchedFiles";
import { FileEventListener } from "./FileEventListener";
import { FileType } from "./FileType";

interface GroupByListener {
   listener: FileEventListener;
   events: FileEventInfo[];
}

interface ForcedEventFileInfo {
   [fileName: string]: fs.Stats;
}

/**
 * Simple File Watcher implementation (Windows, OSX, Linux)
 *
 * - implements feature of event aggreagtion
 *    - same events on the same file fired in configured timeframe are aggregated to single event
 * - implements collection of events within defined time frame from first event occured
 *    - usually it is necessary to wait for multiple events before file processing can be done by build tools
 *    - this prevents multiple calls to be performed on the build tool when multiple files are modified
 * Although directories can be watched and basically internally only directories are watched, events are fired
 * only in case the file to throw the event is registered
 *
 * FileWatcher is using functionalities of NodeJS for file system watching so same caveats applies
 */
export class FileWatcher {

   /**
    * Defines for how long after the first FS event captured are other events collected and aggregated before the final event is thrown
    */
   private __FIRE_EVENT_DELAY: number;

   /**
    * Defines a time frame for aggragation of same events occured on same files
    */
   private __EVENT_AGGREGATION_TIME: number;

   /**
    * Holds list of files being watched and maps listeners and watcher to them
    */
   private __watchedFiles: WatchedFiles;

   /**
    * Holds list of directories being watched and maps listeners and watcher to them
    */
   private __watchedDirs: WatchedFiles;

   /**
    * List of aggregated events
    */
   private __aggregated: FileEventInfo[];

   /**
    * Indicator that first event if bunch occured and currently is running collection of other events during __FIRE_EVENT_DELAY time
    */
   private __waitingNextEvents: boolean;

   /**
    * Information about files collected before forced eevent is trhown
    * Normal events are ignored if the stats of the file are the same as stored in this map
    */
   private __forcedEventFileInfo: ForcedEventFileInfo;

   constructor() {

      this.__EVENT_AGGREGATION_TIME = 100;
      this.__FIRE_EVENT_DELAY = 1000;

      this.__watchedFiles = {};
      this.__watchedDirs = {};
      this.__aggregated = [];
      this.__waitingNextEvents = false;
      this.__forcedEventFileInfo = {};
   }

   /**
    * Starts watching given files and direcories
    * At least one root directory for watching should be specified
    * @param filesAndDirs Files and directories to be watched
    * @param listener Listener to be called when file or directory change occurs
    * @param recursive Specifies if complete directory structures starting by passed directories will be watched
    */
   public watch(filesAndDirs: string[], listener: FileEventListener, recursive: boolean = false): boolean {

      const dirs: string[] = [];
      const files: string[] = [];

      // separate files and directories
      for (const fd of filesAndDirs) {
         if (!fs.existsSync(fd)) continue;
         const stat = fs.statSync(fd);
         if (stat.isDirectory()) dirs.push(fd);
         if (stat.isFile()) files.push(fd);
      }

      // extract directories and add them to dirs if not exist already
      // store dirs to be watched to global list ans assign listeners to them
      for (const f of files) {

         const dir = path.dirname(f);
         if (dirs.indexOf(dir) === -1) dirs.push(dir);

         if (this.__watchedFiles.hasOwnProperty(f)) {
            if (this.__watchedFiles[f].listeners.indexOf(listener) === -1) {
               this.__watchedFiles[f].listeners.push(listener);
            }
         } else {
            this.__watchedFiles[f] = {
               listeners: [ listener ]
            };
         }
      }

      // collect complete directory structure for dirs to be watched
      let wDirs: string[];

      if (recursive) {
         wDirs = [];
         for (const d of dirs) {
            this.__findSubDirs(d, wDirs);
         }
      } else {
         wDirs = dirs;
      }

      // store dirs to be watched to global list and assign listeners to them
      for (const d of wDirs) {
         if (this.__watchedDirs.hasOwnProperty(d)) {
            if (this.__watchedDirs[d].listeners.indexOf(listener) === -1) {
               this.__watchedDirs[d].listeners.push(listener);
            }
         } else {
            this.__watchedDirs[d] = {
               listeners: [ listener ]
            };
         }
      }

      // watch collected dirs for changes
      return this.__watchDirs(wDirs);
   }

   /**
    * Forces processing of files passed as parameter. Next event for each file
    * will be ignored if stats of the file will be the same
    * This method can be used to avoid file event aggregation and waiting delays
    * @param files Files for which the event will be forced. Normalized paths are expected
    */
   public forceChangeEvents(files: string[]): void {

      const groupByListener: GroupByListener[] = [];

      for (const file of files) {
         this.__forcedEventFileInfo[file] = fs.statSync(file);

         const eventInfo: FileEventInfo = {
            eventName: "change",
            fileType: FileType.File,
            fileName: file,
            timestamp: Date.now(),
            stats: this.__forcedEventFileInfo[file]
         };

         if (this.__watchedFiles.hasOwnProperty(file)) {
            diag.info(this.constructor.name, 4, `Forcing event: '${eventInfo.eventName}', '${eventInfo.fileName}'`);
            this.__groupByListener(this.__watchedFiles[file].listeners, eventInfo, groupByListener);
         }
      }

      for (const gpl of groupByListener) {
         gpl.listener(gpl.events);
      }

   }

   /**
    * Unwatches file or directory being watched and releases held resources
    * @param fileOrDirectory File or directory to be unwatched
    */
   public unwatch(fileOrDirectory: string): void {
   }

   /**
    * Collect list of subdirectories recursively
    * @param dir Directory to be scanned
    * @param dirs Array to be filled with discovered sirectories
    */
   private __findSubDirs(dir: string, dirs: string[]): void {

      if (dirs.indexOf(dir) === -1) dirs.push(dir);

      const dc = fs.readdirSync(dir);
      for (const fd of dc) {
         const fullFn = path.join(dir, fd);
         const stat = fs.statSync(fullFn);
         if (stat.isDirectory()) this.__findSubDirs(fullFn, dirs);
      }

   }

   /**
    * Starts watching on all passed directories if they are not watched already
    * @param dirs List of directories to be watched
    */
   private __watchDirs(dirs: string[]): boolean {

      try {
         for (const d of dirs) {
            if (!this.__watchedDirs[d] || !this.__watchedDirs[d].watcher) this.__watchDir(d);
         }

      } catch (e) {
         diag.error(this.constructor.name, `Failed to start file watcher: '${e}'`);
         return false;
      }
   }

   /**
    * Uses NodeJS File watcher to create watcher on passed directory
    * Caller must take care of thrown exceptions
    * @param dir Directory to be watched
    * @throws Exceptions fom NodeJS fs.watch
    */
   private __watchDir(dir: string): void {

      // Create watcher object
      const watcher: WFSWatcher = <WFSWatcher>fs.watch(
         dir,
         {
            encoding: "utf-8",
            persistent: true,
            recursive: false
         },
      );

      // Extend watcher object with necessary information
      watcher.directory = dir;
      watcher.fileWatcher = this;

      // Assign event listeners
      this.__assignWatcherEventHanfdlers(watcher);

      // Store watcher to __watchedDirs
      this.__watchedDirs[dir].watcher = watcher;

   }

   /**
    * Event handlers for the given watcher
    * @param watcher Watcher to be assigned with event handlers
    */
   private __assignWatcherEventHanfdlers(watcher: WFSWatcher) {

      watcher.on(
         "change", function(event: string, fd: string) {
            this.fileWatcher.__fileChanged(this, event, path.join(watcher.directory, fd));
         }
      );

      watcher.on(
         "error", function(error: Error) {

            // Workaround: errors occurs on deletion of empty folders created before wather is started
            if (!fs.existsSync(watcher.directory)) {
               this.fileWatcher.__aggregate("unlink", this.directory, FileType.Directory);
               return;
            }

            // Otherwise, real error occured
            diag.warn(this.constructor.name, `File watcher '${watcher.directory}' error: '${error.message}'`);
         }
      );

   }

   /**
    * Handles change events fired by file system watchers
    * Normalizes events thrown by different OS
    * Event processing is queued to end of the JS task queue
    * @param watcher WFSWatcher which fired the even
    * @param event Event fired
    * @param fileName Name of the file or directory on which the change occured
    */
   private __fileChanged(watcher: WFSWatcher, event: string, fileName: string): void {

      setTimeout(() => {

         // check if the file / directory still exist and if not, aggreate the "unlink" event and unwatch it if possible
         if (!fs.existsSync(fileName)) {
            this.__aggregate(watcher, "unlink", fileName, FileType.Unknown, undefined);
            return;
         }

         const stats = fs.statSync(fileName);
         const type: FileType = stats.isDirectory() ? FileType.Directory : stats.isFile() ? FileType.File : FileType.Unknown;

         // only files and directories are handled (not devices, links or other file types)
         if (type === FileType.Unknown) return;

         // normalize event names
         if (event === "rename") event = "create";

         // if a directory was created, add it to watch list and assign same listeners as parent folder
         if (event === "create" && type === FileType.Directory) {
            if (!this.__watchedDirs.hasOwnProperty(fileName)) {
               this.__watchedDirs[fileName] = {
                  listeners: this.__watchedDirs[watcher.directory].listeners.slice(),
               };
            }
            this.__watchDir(fileName);
         }

         // aggregate the event
         this.__aggregate(watcher, event, fileName, type, stats);

      }, 0);

   }

   /**
    * Aggregates same events on the same file or directory for defined time to minimize number of events thrown per file
    * @param eventName Name of the event occured
    * @param fileName Name of the file on which the event occured
    * @param fileType Type of the file
    */
   private __aggregate(watcher: WFSWatcher, eventName: string, fileName: string, fileType: FileType, stats: fs.Stats): void {

      // setup info for event aggreation
      const eventInfo: FileEventInfo = {
         timestamp: Date.now(),
         eventName,
         fileName,
         fileType,
         stats
      };

      // check if event on the same file occured already and discard it if so
      if (this.__isAggregated(eventInfo)) return;

      diag.info(this.constructor.name, 4, `New event: '${eventInfo.eventName}', '${eventInfo.fileName}', aggregated '${this.__aggregated.length}'`);

      // otherwise store it to aggreaged event list
      this.__aggregated.push(eventInfo);

      // and if it is the first event after all previous events were fire
      // schedule firing of the final event after defined time
      if (!this.__waitingNextEvents) {
         this.__waitingNextEvents = true;
         setTimeout(() => { this.__fireEvents(watcher); }, this.__FIRE_EVENT_DELAY);
      }

   }

   /**
    * Checks if the event on the same file occured peviously within the defined time frame
    * @param event Event descriptor
    */
   private __isAggregated(event: FileEventInfo): boolean {

      for (const e of this.__aggregated) {

         if (
            e.eventName === event.eventName &&
            e.fileName === event.fileName &&
            e.fileType === event.fileType &&
            event.timestamp - e.timestamp < this.__EVENT_AGGREGATION_TIME
         ) return true;

      }

      return false;

   }

   /**
    * Fires aggreated events
    */
   private __fireEvents(watcher: WFSWatcher): void {

      diag.info(this.constructor.name, 3, `Firing events...`);

      const currentTime = Date.now();
      const groupByListener: GroupByListener[] = [];

      while (this.__aggregated.length > 0) {

         // Work only on events occured before current time
         if (this.__aggregated[0].timestamp >= currentTime) break;

         // fiFO event from aggragated list
         const e = this.__aggregated.shift();

         // if file is in forced event list, remove it. if current stats are the same, don't fire the event
         if (this.__forcedEventFileInfo.hasOwnProperty(e.fileName)) {
            const forcedStats = this.__forcedEventFileInfo[e.fileName];
            delete this.__forcedEventFileInfo[e.fileName];

            if (
               forcedStats.mtime.getTime() === e.stats.mtime.getTime() &&
               forcedStats.ctime.getTime() === e.stats.ctime.getTime()
            ) {
               diag.info(this.constructor.name, 4, `Ignoring previously fired forced events for the file '${e.fileName}'.`);
               continue;
            }

         }

         // check if there is file or directory being watched and add its listener to grouped list
         // also, if new file is created fire the event
         if (e.fileType === FileType.File && (this.__watchedFiles.hasOwnProperty(e.fileName) || e.eventName === "create")) {
            if (e.eventName === "create") {
               this.__groupByListener(this.__watchedDirs[watcher.directory].listeners, e, groupByListener);
            } else {
               this.__groupByListener(this.__watchedFiles[e.fileName].listeners, e, groupByListener);
            }
         }

         if (e.fileType !== FileType.File) {
            for (const d in this.__watchedDirs) {
               if (e.fileName.startsWith(d)) {
                  this.__groupByListener(this.__watchedDirs[d].listeners, e, groupByListener);
                  break;
               }
            }
         }

      }

      // fire grouped events
      let statListeners = 0;
      let statEvents = 0;
      for (const gpl of groupByListener) {
         statListeners++;
         statEvents += gpl.events.length;
         gpl.listener(gpl.events);
      }

      diag.info(this.constructor.name, 3, `Firing of '${statEvents}' event/s for '${statListeners}' listener/s done.`);

      // if there are still aggregated events in the queue, schedule firing them out
      if (this.__aggregated.length > 0) {
         setTimeout(() => { this.__fireEvents(watcher); }, this.__FIRE_EVENT_DELAY);
      } else {
         this.__waitingNextEvents = false;
      }

   }

   /**
    * Groups events occured by listeners assigned to files or directories on which event occured
    * @param listeners List of listeners assigned to signle file or directory
    * @param eventInfo Event descriptor (info about the event occured)
    * @param groupByListener List of listeners to be extended with events to be passed to them
    */
   private __groupByListener(listeners: FileEventListener[], eventInfo: FileEventInfo, groupByListener: GroupByListener[]) {

      for (const l of listeners) {

         let found = false;

         for (const gpl of groupByListener) {
            if (gpl.listener === l) {
               found = true;
               gpl.events.push(eventInfo);
               break;
            }
         }

         if (!found) {
            groupByListener.push({ listener: l, events: [ eventInfo ] });
         }

      }

   }

}