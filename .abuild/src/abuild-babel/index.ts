/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as path from "path";
import * as colors from "colors";
import api from "../api";

const usage =
`   ${colors.cyan("abuild-babel")}

      Standard Babel with plugins -> Transpiles the ESNEXT to required ES target
      (usually that one supported by Node.js or targetted browser set)


      - configurable per project by babel.config.<config>.json

      - root project babel.config is used when multiple (dependant) projects are built

      - ajsfwbuild babel plugin source / target / paths are used
        (configurable through the "config": { "babel" : { "src" : "...", "dst": "..." } } })

      - default paths are src = "./.build/<project_name>/ts" and "./.build/<project_name>/babel"

`;

interface ABuiltToolEtsConfig {
   sourceDirName: string;
   targetDirName: string;
   projectSrcPath: string;
   projectTgtPath: string;
}

module.exports = class ABuildBabel extends api.ABuildToolSuper<ABuiltToolEtsConfig> {

   public static readonly versions: string[] = [
      "abuild-babel 1.0.0",
      "babel x"
   ];

   public static readonly usage = usage;

   protected _configure(): void {

      this._config.sourceDirName = this._config.sourceDirName || "ts";
      this._config.targetDirName = this._config.targetDirName || "babel";

      this._config.projectSrcPath = this._config.projectSrcPath || "<solutionPath>/<buildDirName>/<projectName>/<sourceDirName>";
      this._config.projectTgtPath = this._config.projectTgtPath || "<solutionPath>/<buildDirName>/<projectName>/<targetDirName>";

      this._config.projectSrcPath = this._patchPath(this._config.projectSrcPath);
      this._config.projectTgtPath = this._patchPath(this._config.projectTgtPath);

   }

   protected _clean(): boolean {

      try {

         if (fs.existsSync(this._config.projectTgtPath)) {
            api.diag.info(this.constructor.name, 1, `Cleaning '${this._config.projectTgtPath}'`);
            this._rmDirRecursive(this._config.projectTgtPath);
         }

      } catch (e) {

         api.diag.error(this.constructor.name, `Failed to clean: ${e}`);
         return false;

      }

      return true;
   }

};
