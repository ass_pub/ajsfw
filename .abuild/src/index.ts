/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import api from "./api";
import * as diag from "./lib/diag";
import * as path from "path";

import { parseCmdLine } from "./lib/cmdline";
import { CmdLineArgs } from "./lib/cmdline/CmdLineArgs";
import { ABuildToolCtor } from "./lib/tools";
import { ABuildToolManager } from "./lib/tools";
import { Solution } from "./lib/solution";
import { FileWatcher } from "./lib/fwatch";

import * as manager from "./manager";

export function main(argv: string[]) {

   const cmdLineArgs: CmdLineArgs = parseCmdLine(argv);

   if (!cmdLineArgs.help && !cmdLineArgs.manage) {
      diag.enabled(cmdLineArgs.debug);
      diag.debugLevel(cmdLineArgs.debugLevel);
   }

   if ((cmdLineArgs.debug || cmdLineArgs.help) && !cmdLineArgs.manage) {
      diag.header();
   }

   // initialize tool manager and load available tools
   api.aBuildToolManager = new ABuildToolManager();
   api.aBuildToolManager.init();

   // cmd line arg - help
   if (cmdLineArgs.help) {
      diag.usage(collectToolsUsage());
   }

   // cmd line arg - parsing errors
   if (cmdLineArgs.cmdLineParsingErrorCode !== 0) {

      if (cmdLineArgs.cmdLineParsingErrorCode !== 0) {
         diag.error("MAIN", `Error parsing the command line arguments: ${cmdLineArgs.cmdLineParsingError}`);
      }
      process.exit(cmdLineArgs.cmdLineParsingErrorCode);

   }

   // cmd line arg - exit on help -> help is shown in both cases - if help is reuested or if arguments parsing error occur
   if (cmdLineArgs.help) {
      process.exit(0);
   }

   // cmd line arg - version
   if (cmdLineArgs.version) {
      printVersions();
      process.exit(0);
   }

   if (cmdLineArgs.manage) {
      manager.manage();
      return;
   }

   api.fileWatcher =  new FileWatcher();

   const solution = new Solution(getSolutionPath(cmdLineArgs.solution));
   if (!solution.init()) process.exit(10);

   const projectName = cmdLineArgs.project || solution.defaultProjectName;
   if (!solution.projects.hasOwnProperty(projectName)) {
      diag.error("MAIN", `Project '${projectName}' not found in configuration file`);
      process.exit(20);
   }

   const buildConfig: string = cmdLineArgs.buildConfigName;

   let result: boolean;

   if (cmdLineArgs.clean) {
      result = runTask("clean", solution, projectName);
   } else  if (cmdLineArgs.watch) {
      result = runTask("watch", solution, projectName, buildConfig);
   } else {
      result = runTask("build", solution, projectName, buildConfig);
   }

   if (!result) process.exit(30);

}

function runTask(taskName: "clean" | "build" | "watch", solution: Solution, projectName: string, buildConfig: string = ""): boolean {

   for (const project of solution.projects[projectName].dependsOn) {
      if (!runTask(taskName, solution, project.name, buildConfig)) return false;
   }


   diag.info("MAIN", 0, `Starting task '${taskName}' on project '${projectName}' ${buildConfig ? `configuration '${buildConfig}'` : ""}`);

   const project = solution.projects[projectName];

   switch (taskName) {

      case "clean":

         return project.clean();

      case "build":

         if (!buildConfig) {
            diag.warn("MAIN", "Build configuration (-c option) not specified! Default config files will be used. 'No Inputs' or other errors may occur!");
         } else {
            diag.info("MAIN", 1, `Build configuration name '${buildConfig || "not specified"}'`);
         }

         return project.build(buildConfig);

      case "watch":

         if (!buildConfig) {
            diag.warn("MAIN", "Build configuration (-c option) not specified! Default config files will be used. 'No Inputs' or other errors may occur!");
         } else {
            diag.info("MAIN", 1, `Build configuration name '${buildConfig || "not specified"}'`);
         }

         return project.watch(buildConfig);
   }

   return false;
}


function getSolutionPath(confPath: string): string {

   if (confPath) {
      if (path.isAbsolute(confPath)) {
         return confPath;
      } else {
         return path.resolve(process.cwd(), confPath);
      }
   } else {
      return process.cwd();
   }

}

function collectToolsUsage(): string[] {

   const usage: string[] = [];

   for (const tool in api.aBuildToolManager.tools) {
      if (api.aBuildToolManager.tools[tool].ctor.usage) {
         usage.push(api.aBuildToolManager.tools[tool].ctor.usage);
      }
   }

   return usage;

}

function printVersions() {

   const tools = api.aBuildToolManager.tools;
   const ver: string[] = [ `   abuild-core ${api.version}` ];

   for (const tool in tools) {

      try {
         const t: ABuildToolCtor = tools[tool].ctor;
         ver.push(`   ${t.versions[0]}`);
         for (let i = 1; i < t.versions.length; i++) {
            ver.push(`      ${t.versions[i]}`);
         }
      } catch (e) {
         diag.error("MAIN", `Failed to load the build tool '${tool}'`);
      }

   }

   console.log("");
   for (const v of ver) console.log(v);

}

main(process.argv);