/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as path from "path";
import * as colors from "colors";

import api from "../api";

const usage =
`   ${colors.cyan("abuild-sync")}

      Folder synchronization plugin -> Synchronizes one root folder to another
      (i.e. build code to node_modules)


      - ajsfwbuild sync plugin source / target / paths are used
        (configurable through the "config": { "ets" : { "src" : "...", "dst": "..." } } })

      - default paths are src = "./.build/<project_name>/babel" and "./node_modules/<project_name>"

`;

interface ABuiltToolEtsConfig {
   sourceDirName: string;
   targetDirName: string;
   projectSrcPath: string;
   projectTgtPath: string;
}

module.exports = class ABuildSync extends api.ABuildToolSuper<ABuiltToolEtsConfig> {

   public static readonly versions: string[] = [
      "abuild-sync 1.0.0"
   ];

   public static readonly usage = usage;


   protected _configure(): void {

      this._config.sourceDirName = this._config.sourceDirName || "babel";
      this._config.targetDirName = this._config.targetDirName || "node_modules";

      this._config.projectSrcPath = this._config.projectSrcPath || "<solutionPath>/<buildDirName>/<projectName>/<sourceDirName>";
      this._config.projectTgtPath = this._config.projectTgtPath || "<solutionPath>/<targetDirName>/<projectName>";

      this._config.projectSrcPath = this._patchPath(this._config.projectSrcPath);
      this._config.projectTgtPath = this._patchPath(this._config.projectTgtPath);

   }


   protected _clean(): boolean {

      try {

         if (fs.existsSync(this._config.projectTgtPath)) {
            api.diag.info(this.constructor.name, 1, `Cleaning '${this._config.projectTgtPath}'`);
            this._rmDirRecursive(this._config.projectTgtPath);
         }

      } catch (e) {

         api.diag.warn(this.constructor.name, `Failed to clean: '${e}'`);
         return true;

      }

      return true;
   }


   protected _build(): boolean {
      api.diag.info(this.constructor.name, 1, `Syncing directory '${this._config.projectSrcPath}' to '${this._config.projectTgtPath}'`);
      return this.__dirSync(this._config.projectSrcPath, this._config.projectTgtPath);
   }


   protected _watch(): boolean {

      const srcFileList: string[] = [ this._config.projectSrcPath ];

      api.diag.info(this.constructor.name, 1, `Syncing directory '${this._config.projectSrcPath}' to '${this._config.projectTgtPath}'`);
      if (!this.__dirSync(this._config.projectSrcPath, this._config.projectTgtPath, srcFileList)) return false;

      api.diag.info(this.constructor.name, 1, `Directory '${this._config.projectSrcPath}' will be monitored`);

      api.fileWatcher.watch(
         srcFileList,
         (eventInfo: api.FileEventInfo[]) => {
            this.__filesChanged(eventInfo);
         }
      );

      return true;

   }


   private __filesChanged(eventInfo: api.FileEventInfo[]): void {

      const forceEvents: string[] = [];

      for (const event of eventInfo) {

         const relPath = event.fileName.substr(this._config.projectSrcPath.length);
         const tgtPath = path.join(this._config.projectTgtPath, relPath);

         switch (event.eventName) {
            case "create":
            case "change":

               if (event.fileType === api.FileType.File) {
                  api.diag.info(this.constructor.name, 4, `Copying fie '${event.fileName}' to '${tgtPath}'`);
                  fs.copyFileSync(event.fileName, tgtPath);
                  forceEvents.push(tgtPath);
               }

               if (event.fileType === api.FileType.Directory) {
               }

               break;

            case "unlink":
               break;
         }

      }

      if (forceEvents.length > 0) api.fileWatcher.forceChangeEvents(forceEvents);
   }


   private __dirSync(src: string, dst: string, srcFileList: string[] = []): boolean {

      if (!fs.existsSync(src) || !fs.statSync(src).isDirectory) {
         api.diag.error(this.constructor.name, `Source directory '${this._config.projectSrcPath}' not found or is not directory!`);
         return false;
      }

      if (fs.existsSync(dst) && !fs.statSync(src).isDirectory) {
         api.diag.error(this.constructor.name, `Destination '${this._config.projectSrcPath}' is not directory!`);
         return false;
      }

      if (!fs.existsSync(dst)) {
         try {
            fs.mkdirSync(dst, { recursive: true });
            if (srcFileList.indexOf(src) === -1) srcFileList.push(src);
         } catch (e) {
            api.diag.error(this.constructor.name, `Unable to create the target directory: '${e}'`);
            return false;
         }
      }

      const srcFiles = fs.readdirSync(src);

      for (const fileName of srcFiles) {

         const srcFn = path.join(src, fileName);
         const dstFn = path.join(dst, fileName);

         let stats: fs.Stats;

         try {
            stats = fs.statSync(srcFn);
         } catch (e) {
            api.diag.error(this.constructor.name, `Failed to stat file '${srcFn}': ${e}`);
            return false;
         }

         if (stats.isDirectory()) {
            if (!this.__dirSync(srcFn, dstFn, srcFileList)) return false;
            continue;
         }

         if (stats.isFile()) {
            try {
               fs.copyFileSync(srcFn, dstFn);
               if (srcFileList.indexOf(srcFn) === -1) srcFileList.push(srcFn);
            } catch (e) {
               api.diag.error(this.constructor.name, `Failed to copy file '${srcFn}' to ${dstFn}: ${e}`);
               return false;
            }
            continue;
         }

         api.diag.warn(this.constructor.name, `File '${path.join(src, fileName)}' is not a file or directory!`);
      }

      return true;

   }

};
