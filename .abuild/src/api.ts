/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as _diag from "./lib/diag";
import * as _json from "./lib/json";

// exported types
import { ABuildTool } from "./lib/tools/ABuildTool";
import { Project } from "./lib/project";
import { Solution } from "./lib/solution";
import { FileEventInfo } from "./lib/fwatch";
import { EventListener } from  "./lib/events";

// exported types and instances
import { version as _version } from "./version";
import { ABuildToolManager as _ABuildToolManager } from "./lib/tools/ABuildToolManager";
import { ABuildToolSuper as _ABuildToolSuper } from "./lib/tools/ABuildToolSuper";
import { FileType as _FileType } from "./lib/fwatch";
import { FileWatcher as _FileWatcher } from "./lib/fwatch";
import { EventDispatcher as _EventDispatcher } from "./lib/events";

declare namespace api {

   export { ABuildTool };
   export { FileEventInfo };
   export { Project };
   export { Solution };
   export { EventListener };
   export { _EventDispatcher as TEventDispatcher };

}

namespace api {

   export const version = _version;

   export const ABuildToolManager: typeof _ABuildToolManager = _ABuildToolManager;
   export const ABuildToolSuper: typeof _ABuildToolSuper = _ABuildToolSuper;
   export const FileWatcher: typeof _FileWatcher = _FileWatcher;
   export const FileType: typeof _FileType = _FileType;
   export const EventDispatcher: typeof _EventDispatcher = _EventDispatcher;

   export const diag = {
      info: _diag.info,
      warn: _diag.warn,
      error: _diag.error,
   };

   export const json = {
      stripComments: _json.stripComments,
      errorInfo: _json.errorInfo
   };

   export let fileWatcher: _FileWatcher = null;
   export let aBuildToolManager: _ABuildToolManager = null;

}

export default api;