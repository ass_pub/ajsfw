/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

//
// Currently, ES6 imports and ES next dynamic imports are transformed to the
// commonjs "require" as there is no ECMA Script standard for the script loader.
// For various reasons related to the Ajs Framework and legacy browser support,
// custom dependency loader must be supported by the browser prior it will be
// possible to use es6+ imports directly.
//
// In this class, dependencies are detected only and ES6+ imports are fully
// supported. Scripts are transformed in later phases of the packaging chain.
//

import * as estree from "estree";
import * as path from "path";
import * as fs from "fs";
import { EsDep } from "./EsDep";

// inject accorn dynamic import support (parser)
const acorn = require("acorn-dynamic-import").default;

// inject accorn dynamic import support (walker)
const walk = require("acorn-dynamic-import/lib/walk").default;

/**
 * EcmaScript Dependency Analyzer
 * Searches for dependencies on another modules
 * Currently, CommonJS and esnext modules are supported (static and dynamic)
 */
export class EsDeps {

    protected _baseUrl: string;
    protected _acornOptions: acorn.Options;

    /**
     * Creates and configures the es dependency analyzer
     * @param baseUrl Base url to be used to load files from
     * @param acronOptions Acron parser options
     */
    constructor(baseUrl: string, acronOptions: acorn.Options) {
        this._baseUrl = baseUrl;
        this._acornOptions = acronOptions;
    }

    /**
     * Returns list of dependencies collected from the file
     * @param url Relative url to baseUrl of the file relative to the ulr base directory
     */
    public getDeps(url: string): EsDep[] {

        const rootPath: string = path.resolve(this._baseUrl, url);
        const content: string = fs.readFileSync(rootPath).toString();
        const program: estree.Program = acorn.parse(content, this._acornOptions);
        const deps: EsDep[] = [];

        walk.ancestor(program, {
            "CallExpression": (cex: estree.CallExpression, ancestors: estree.Node[]) => {
                this._processRequire(cex, ancestors, deps);
            },
            "Import": (node: estree.Node, ancestors: estree.Node[]) => {
                this._processImport(node, ancestors, deps);
            },
            "ImportDeclaration": (idec: estree.ImportDeclaration, ancestors: estree.Node[]) => {
                this._processImportDeclaration(idec, ancestors, deps);
            }
        }, walk.base);

        return deps;
    }

    /**
     * Searches for require identifier and a call statement then processes it
     * @param cex Call expression to be checked for require and eventually processed
     * @param ancestors Node ancestors
     * @param deps Array to which the eventually found dependency information will be pushed
     */
    protected _processRequire(cex: estree.CallExpression, ancestors: estree.Node[], deps: EsDep[]): void {

        // look just for CallExpression with the require identifier
        if (cex.callee.type !== "Identifier" || (<estree.Identifier>cex.callee).name !== "require") return;

        // exactly 1 grgument must be used within the require CEx
        if (cex.arguments.length !== 1) return;

        // search for first block element in ancestors. If block statement is found, the dynamic import is used
        let isDynamic = false;
        for (let i = ancestors.length - 1; i >= 0; i--) {
            if (ancestors[i].type === "BlockStatement") {
                isDynamic = true;
                break;
            }
        }

        // also, when the require call argument contains anything else than [string] literal it is a dynamic import
        if (!isDynamic && cex.arguments[0].type !== "Literal") {
            isDynamic = true;
        }

        // push info about the discovered dependency
        deps.push({
            id: isDynamic ? undefined : <string>(<estree.Literal>cex.arguments[0]).value,
            filename: undefined,
            dynamic: isDynamic,
            callExpression: cex,
            ancestors
        });

    }

    /**
     * Processes the import declaration (currently not supported as there is no ECMA standard for loaders)
     * TODO: IMPLEMENT WHEN ECMA LOADERS ARE STANDARDIZED
     * @param idec Import declaration to be processed
     * @param ancestors Node ancestors
     * @param deps Array to which the eventually found dependency information will be pushed
     */
    protected _processImportDeclaration(idec: estree.ImportDeclaration, ancestors: estree.Node[], deps: EsDep[]): void {

        deps.push({
            id: <string>idec.source.value,
            filename: undefined,
            dynamic: false,
            importDeclaration: idec,
            ancestors
        });

    }

    /**
     * Processes the import (dynamic) node
     * TODO: IMPLEMENT WHEN ECMA LOADERS ARE STANDARDIZED
     * @param i Import node to be processed
     * @param ancestors Node ancestors
     * @param deps Array to which the eventually found dependency information will be pushed
     */
    protected _processImport(i: estree.Node, ancestors: estree.Node[], deps: EsDep[]): void {

        deps.push({
            id: undefined,
            filename: undefined,
            dynamic: true,
            import: i,
            ancestors
        });

    }

}
