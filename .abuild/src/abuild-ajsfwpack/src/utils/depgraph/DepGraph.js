"use strict";
/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
exports.__esModule = true;
var fs = require("fs");
var path = require("path");
var colors = require("colors");
var estools_1 = require("../estools");
var Module = require("module");
var DepGraph = /** @class */ (function () {
    function DepGraph(baseUrl) {
        this._baseUrl = baseUrl;
        this._esDeps = new estools_1.EsDeps(baseUrl, estools_1.acornconfig.options);
    }
    /**
     * Builds a module graph for the given root module
     * @param rootModuleFileName
     */
    DepGraph.prototype.graph = function (rootModuleFileName) {
        this._modules = [];
        var rootModule = this._resolve(rootModuleFileName);
        this._modules.push(rootModule);
        this._getDeps(rootModule);
        return this._modules;
    };
    DepGraph.prototype._getDeps = function (mod) {
        var deps = this._esDeps.getDeps(mod.filename);
        for (var _i = 0, deps_1 = deps; _i < deps_1.length; _i++) {
            var dep = deps_1[_i];
            if (dep.dynamic)
                continue;
            var m = this._resolve(dep.id, mod);
            mod.requires.push(m);
            var emi = this._indexOfModuleByFilename(m.filename);
            if (emi === -1) {
                this._modules.push(m);
                if (m.filename.endsWith("package.json")) {
                    var filename = this._getPackageJsonMain(m.filename);
                    m.code = filename;
                    if (fs.existsSync(filename) && this._indexOfModuleByFilename(filename) === -1) {
                        m = { filename: filename, dependent: mod, references: [mod], requires: [] };
                        this._modules.push(m);
                        this._getDeps(m);
                    }
                }
                else {
                    this._getDeps(m);
                }
            }
            else {
                this._modules[emi].references.push(m.dependent);
            }
        }
    };
    DepGraph.prototype._indexOfModuleByFilename = function (filename) {
        for (var i = 0; i < this._modules.length; i++) {
            if (this._modules[i].filename === filename)
                return i;
        }
        return -1;
    };
    DepGraph.prototype._resolve = function (id, dependent) {
        var filename;
        // resolve root module
        if (!dependent) {
            filename = this._tryFiles(this._baseUrl, id);
            if (fs.existsSync(filename)) {
                if (dependent) {
                    return { filename: filename, dependent: dependent, references: [dependent], requires: [] };
                }
                else {
                    return { filename: filename, dependent: dependent, references: [], requires: [] };
                }
            }
            throw "Failed to resolve root module '" + filename + "'";
        }
        // try to locate module on relative path to parent
        if (id.startsWith("./") || id.startsWith("../")) {
            filename = this._tryFiles(path.dirname(dependent.filename), id);
            if (fs.existsSync(filename)) {
                return { filename: filename, dependent: dependent, references: [dependent], requires: [] };
            }
            // try to locate module on baseUrl or node_modules up to the fs root
        }
        else {
            // firstly, try to locate file on base url
            filename = this._tryFiles(this._baseUrl, id);
            if (fs.existsSync(filename)) {
                return { filename: filename, dependent: dependent, references: [dependent], requires: [] };
            }
            // go up the fs dirs and try to locate module in node_modules folder
            var currentPath = this._baseUrl;
            while (true) {
                var nodeModules = path.resolve(currentPath, "./node_modules");
                filename = this._tryFiles(nodeModules, id);
                if (fs.existsSync(filename)) {
                    return { filename: filename, dependent: dependent, references: [dependent], requires: [] };
                }
                if (currentPath === "/")
                    break;
                currentPath = path.resolve(currentPath, "..");
            }
        }
        throw colors.red("Failed to resolve '" + id + "' (parent module: '" + dependent.filename + "')");
    };
    // tries to locate the file on the passed path or according to defined rules
    DepGraph.prototype._tryFiles = function (baseUrl, filename) {
        var tests = [
            filename,
            filename + ".js",
            filename + "/package.json",
            filename + "/index.js",
            filename + filename.substr(filename.lastIndexOf("/")) + ".js"
        ];
        for (var i = 0; i < tests.length; i++) {
            var filename_1 = path.resolve(baseUrl, tests[i]);
            if (fs.existsSync(filename_1) && !fs.statSync(filename_1).isDirectory()) {
                return filename_1;
            }
        }
    };
    // tries to resolve main module defined in the package.json
    DepGraph.prototype._getPackageJsonMain = function (filename) {
        var base = path.dirname(filename);
        if (!fs.existsSync(filename)) {
            return undefined;
        }
        var f = fs.readFileSync(filename).toString();
        var pkg = JSON.parse(f);
        if (pkg.main) {
            var main = path.join(base, pkg.main);
            if (fs.existsSync(main))
                return main;
        }
        return undefined;
    };
    return DepGraph;
}());
exports.DepGraph = DepGraph;
