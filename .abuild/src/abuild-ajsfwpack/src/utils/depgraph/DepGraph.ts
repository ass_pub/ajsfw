/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as path from "path";
import * as colors from "colors";
import { acornconfig, EsDeps } from "../estools";

const Module = require("module");

export interface Module {
    filename: string;
    dependent: Module;
    references: Module[];
    requires: Module[];
    code?: string;
}

export class DepGraph {

    protected _esDeps: EsDeps;
    protected _baseUrl: string;
    protected _modules: Module[];

    constructor (baseUrl: string) {
        this._baseUrl = baseUrl;
        this._esDeps = new EsDeps(baseUrl, acornconfig.options);
    }

    /**
     * Builds a module graph for the given root module
     * @param rootModuleFileName
     */
    public graph(rootModuleFileName: string): Module[] {

        this._modules = [];
        const rootModule: Module = this._resolve(rootModuleFileName);
        this._modules.push(rootModule);
        this._getDeps(rootModule);

        return this._modules;

    }

    protected _getDeps(mod: Module): void {

        const deps = this._esDeps.getDeps(mod.filename);

        for (const dep of deps) {
            if (dep.dynamic) continue;

            let m = this._resolve(dep.id, mod);
            mod.requires.push(m);

            const emi = this._indexOfModuleByFilename(m.filename);

            if (emi === -1) {
                this._modules.push(m);

                if (m.filename.endsWith("package.json")) {

                    const filename = this._getPackageJsonMain(m.filename);
                    m.code = filename;

                    if (fs.existsSync(filename) && this._indexOfModuleByFilename(filename) === -1) {
                        m = { filename, dependent: mod, references: [ mod ], requires: [] };
                        this._modules.push(m);
                        this._getDeps(m);
                    }

                } else {
                    this._getDeps(m);
                }
            } else {
                this._modules[emi].references.push(m.dependent);
            }
        }

    }

    protected _indexOfModuleByFilename(filename: string): number {
        for (let i = 0; i < this._modules.length; i++) {
            if (this._modules[i].filename === filename) return i;
        }
        return -1;
    }

    protected _resolve(id: string, dependent?: Module): Module {

        let filename: string;

        // resolve root module
        if (!dependent) {

            filename = this._tryFiles(this._baseUrl, id);

            if (fs.existsSync(filename)) {
                if (dependent) {
                    return { filename, dependent, references: [ dependent ], requires: [] };
                } else {
                    return { filename, dependent, references: [], requires: [] };
                }

            }
            throw `Failed to resolve root module '${filename}'`;
        }

        // try to locate module on relative path to parent
        if (id.startsWith("./") || id.startsWith("../")) {

            filename = this._tryFiles(path.dirname(dependent.filename), id);

            if (fs.existsSync(filename)) {
                return { filename, dependent, references: [ dependent ], requires: [] };
            }

        // try to locate module on baseUrl or node_modules up to the fs root
        } else {

            // firstly, try to locate file on base url
            filename = this._tryFiles(this._baseUrl, id);
            if (fs.existsSync(filename)) {
                return { filename, dependent, references: [ dependent ], requires: [] };
            }

            // go up the fs dirs and try to locate module in node_modules folder
            let currentPath = this._baseUrl;
            while (true) {
                const nodeModules = path.resolve(currentPath, "./node_modules");
                filename = this._tryFiles(nodeModules, id);

                if (fs.existsSync(filename)) {
                    return { filename, dependent, references: [ dependent ], requires: [] };
                }

                if (currentPath === "/") break;

                currentPath = path.resolve(currentPath, "..");
            }


        }

        throw colors.red(`Failed to resolve '${id}' (parent module: '${dependent.filename}')`);

    }

    // tries to locate the file on the passed path or according to defined rules
    protected _tryFiles(baseUrl: string, filename: string) {

        const tests: string[] = [
            filename,
            filename + ".js",
            filename + "/package.json",
            filename + "/index.js",
            filename + filename.substr(filename.lastIndexOf("/")) + ".js"
        ];
        for (let i = 0; i < tests.length; i++) {
            let filename = path.resolve(baseUrl, tests[i]);
            if (fs.existsSync(filename) && !fs.statSync(filename).isDirectory()) {
                return filename;
            }
        }

    }

    // tries to resolve main module defined in the package.json
    protected _getPackageJsonMain(filename: string): string {

        const base = path.dirname(filename);

        if (!fs.existsSync(filename)) {
            return undefined;
        }

        const f = fs.readFileSync(filename).toString();
        const pkg = JSON.parse(f);

        if (pkg.main) {
            const main = path.join(base, pkg.main);
            if (fs.existsSync(main)) return main;
        }

        return undefined;
    }

}
