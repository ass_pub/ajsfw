/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as colors from "colors";

export { Diag } from "./Diag";

/** Holds name of the log file */
let logFile: string;

function fillSpaces(text: string): string {
    return (`[${text}]                          `).substr(0, 25);
}

/**
 * Logs an info record to the screen and if log file is configured to the log file too
 * @param component Name of the component
 * @param message Info message
 */
export function info(component: string, message: string): void {
    const output = `INFO    ${fillSpaces(component)} ${message}`;
    console.log(colors.white(output));
    if (logFile) fs.writeFileSync(logFile, output, "a");
}

/**
 * Logs an info record to the screen and if log file is configured to the log file too
 * @param component Name of the component
 * @param message Info message
 */
export function warning(component: string, message: string): void {
    const output = `WARNING ${fillSpaces(component)} ${message}`;
    console.log(colors.yellow(output));
    if (logFile) fs.writeFileSync(logFile, output, "a");
}

/**
 * Logs an info record to the screen and if log file is configured to the log file too
 * @param component Name of the component
 * @param message Info message
 */
export function error(component: string, message: string): void {
    const output = `ERROR   ${fillSpaces(component)} ${message}`;
    console.error(colors.red(output));
    if (logFile) fs.writeFileSync(logFile, output, "a");
}

/**
 * Configures file name of the log file to be used to log diagnostic messages
 * @param filename Log file name
 * @param clean Indicates the file is cleaned before logging to it starts
 */
export function useLogFile(filename: string, clean = false): void {
    logFile = filename;
    if (clean) fs.writeFileSync(filename, "");
}
