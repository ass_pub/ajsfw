/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

export const usage = `
Usage:
   ajsfwbuild [-w -q -l <file> -p <path|file> -c <name> -d <defines>]

Main options:

   -h, --help              Prints this help
   -w, --watch             Starts ajsfwbuild in watching mode
   -q, --quiet             Prints INFO, WARNING and ERROR messages only
   -l, --log-file <file>   Logs output messages to the log

Project and workspace:

   -p, --project <path|filename>
      Workspace folder of the project to be built or path to the configuration
      file to be loaded

Build configuration:

   -c, --build-config    Build configuration to be used
      If the environment argument is set to any value (usually debug, test,
      release) this value is used as an extension prefix of each configuration
      file to be used during the build process (i.e.) 'ajsfwbuild.debug.json' or
      'tsconfig.release.json'. If the file with prefixed extension is not found
      the default one is loaded.

Preprocessing and transpilation:

   -d, --define <const1>=<value1>,<const2>=<value2>
      Defines the constant list usefull for pre/post processors
`;
