"use strict";
/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
exports.__esModule = true;
var path = require("path");
var fs = require("fs");
var Config = /** @class */ (function () {
    /**
     * Constructs the Config object ready to load the configuration from the filesystem
     * @param workspaceFolder Optional - the workspace folder (usually passed from the command line)
     */
    function Config(workspaceFolder) {
        this._workspaceFolder = workspaceFolder || process.cwd();
        this._buildFolder = path.resolve(this._workspaceFolder, "./build");
        this._distFolder = path.resolve(this._workspaceFolder, "./js");
        this._plugins = {
            packager: {
                "packages": {
                    "index.js": "index"
                }
            }
        };
    }
    Object.defineProperty(Config.prototype, "workspaceFolder", {
        get: function () { return this._workspaceFolder; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "buildFolder", {
        get: function () { return this._buildFolder; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "distFolder", {
        get: function () { return this._distFolder; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "plugins", {
        get: function () { return this._plugins; },
        enumerable: true,
        configurable: true
    });
    /**
     * Loads the configuration from the file.
     * @param filename Filename of the config file to be loaded (relative to the workspace root)
     */
    Config.prototype.load = function (filename) {
        if (!fs.existsSync(filename) || !fs.statSync(filename))
            throw "Failed to load configuration file '" + filename + "'";
        this._currentFile = filename;
        var file = fs.readFileSync(filename).toString();
        var json = this._stripJSONComments(file);
        var config = JSON.parse(json);
        this._applyConfig(config);
    };
    /**
     * Applies the loaded config to the curent object
     */
    Config.prototype._applyConfig = function (config) {
        for (var key in config) {
            switch (key) {
                case "build":
                    this._buildFolder = config[key];
                    break;
                case "dist":
                    this._distFolder = config[key];
                    break;
                case "plugins":
                    this._applyPluginsConfig(config[key]);
                    break;
                default:
                    throw "Invalid option: '" + key + "'";
            }
        }
    };
    /**
     * Applies plugins configuration to the current object
     * The config is not checked at this time (it should be checked by each plugin when instanced)
     */
    Config.prototype._applyPluginsConfig = function (plugins) {
        for (var plugin in plugins) {
            this._plugins[plugin] = plugins[plugin];
        }
    };
    /**
     * Extends loaded configuration with configuration loaded from the file
     * @param filename Filename of the config to be loaded and extended
     */
    Config.prototype._extend = function (filename) {
    };
    /**
     * Strips comments from the json formatted string
     * @param json Json formatted string to be stripped
     */
    Config.prototype._stripJSONComments = function (json) {
        var pos = 0;
        var inComment1 = false;
        var inComment2 = false;
        var out = "";
        function nextChar() {
            if (pos < json.length - 1)
                return json[pos + 1];
        }
        while (pos < json.length) {
            if (!inComment1 && !inComment2 && json[pos] === "/" && nextChar() === "/") {
                inComment1 = true;
                pos += 2;
            }
            else if (!inComment1 && !inComment2 && json[pos] === "/" && nextChar() === "*") {
                inComment2 = true;
                pos += 2;
            }
            else if (inComment1 && json[pos] === "\n") {
                inComment1 = false;
                out += "\n";
                pos++;
            }
            else if (inComment2 && json[pos] === "*" && nextChar() === "/") {
                inComment2 = false;
                pos += 2;
            }
            else if (!inComment1 && !inComment2) {
                out += json[pos];
                pos++;
            }
            else {
                pos++;
            }
        }
        return out;
    };
    return Config;
}());
exports.Config = Config;
