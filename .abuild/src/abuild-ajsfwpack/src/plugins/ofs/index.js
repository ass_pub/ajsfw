"use strict";
/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
exports.__esModule = true;
var fs_1 = require("fs");
exports.Stats = fs_1.Stats;
var fs_2 = require("fs");
exports.ReadStream = fs_2.ReadStream;
var fs_3 = require("fs");
exports.WriteStream = fs_3.WriteStream;
var fs_4 = require("fs");
exports.rename = fs_4.rename;
var fs_5 = require("fs");
exports.renameSync = fs_5.renameSync;
var fs_6 = require("fs");
exports.truncate = fs_6.truncate;
var fs_7 = require("fs");
exports.truncateSync = fs_7.truncateSync;
var fs_8 = require("fs");
exports.ftruncate = fs_8.ftruncate;
var fs_9 = require("fs");
exports.ftruncateSync = fs_9.ftruncateSync;
var fs_10 = require("fs");
exports.chown = fs_10.chown;
var fs_11 = require("fs");
exports.chownSync = fs_11.chownSync;
var fs_12 = require("fs");
exports.fchown = fs_12.fchown;
var fs_13 = require("fs");
exports.fchownSync = fs_13.fchownSync;
var fs_14 = require("fs");
exports.lchown = fs_14.lchown;
var fs_15 = require("fs");
exports.lchownSync = fs_15.lchownSync;
var fs_16 = require("fs");
exports.chmod = fs_16.chmod;
var fs_17 = require("fs");
exports.fchmod = fs_17.fchmod;
var fs_18 = require("fs");
exports.fchmodSync = fs_18.fchmodSync;
var fs_19 = require("fs");
exports.lchmod = fs_19.lchmod;
var fs_20 = require("fs");
exports.lchmodSync = fs_20.lchmodSync;
var fs_21 = require("fs");
exports.stat = fs_21.stat;
var fs_22 = require("fs");
exports.statSync = fs_22.statSync;
var fs_23 = require("fs");
exports.fstat = fs_23.fstat;
var fs_24 = require("fs");
exports.fstatSync = fs_24.fstatSync;
var fs_25 = require("fs");
exports.lstat = fs_25.lstat;
var fs_26 = require("fs");
exports.lstatSync = fs_26.lstatSync;
var fs_27 = require("fs");
exports.link = fs_27.link;
var fs_28 = require("fs");
exports.linkSync = fs_28.linkSync;
var fs_29 = require("fs");
exports.symlink = fs_29.symlink;
var fs_30 = require("fs");
exports.symlinkSync = fs_30.symlinkSync;
var fs_31 = require("fs");
exports.readlink = fs_31.readlink;
var fs_32 = require("fs");
exports.readlinkSync = fs_32.readlinkSync;
var fs_33 = require("fs");
exports.realpath = fs_33.realpath;
var fs_34 = require("fs");
exports.realpathSync = fs_34.realpathSync;
var fs_35 = require("fs");
exports.unlink = fs_35.unlink;
var fs_36 = require("fs");
exports.unlinkSync = fs_36.unlinkSync;
var fs_37 = require("fs");
exports.rmdir = fs_37.rmdir;
var fs_38 = require("fs");
exports.rmdirSync = fs_38.rmdirSync;
var fs_39 = require("fs");
exports.mkdir = fs_39.mkdir;
var fs_40 = require("fs");
exports.mkdirSync = fs_40.mkdirSync;
var fs_41 = require("fs");
exports.mkdtemp = fs_41.mkdtemp;
var fs_42 = require("fs");
exports.mkdtempSync = fs_42.mkdtempSync;
var fs_43 = require("fs");
exports.readdir = fs_43.readdir;
var fs_44 = require("fs");
exports.readdirSync = fs_44.readdirSync;
var fs_45 = require("fs");
exports.close = fs_45.close;
var fs_46 = require("fs");
exports.closeSync = fs_46.closeSync;
var fs_47 = require("fs");
exports.open = fs_47.open;
var fs_48 = require("fs");
exports.openSync = fs_48.openSync;
var fs_49 = require("fs");
exports.utimes = fs_49.utimes;
var fs_50 = require("fs");
exports.utimesSync = fs_50.utimesSync;
var fs_51 = require("fs");
exports.futimes = fs_51.futimes;
var fs_52 = require("fs");
exports.futimesSync = fs_52.futimesSync;
var fs_53 = require("fs");
exports.fsync = fs_53.fsync;
var fs_54 = require("fs");
exports.fsyncSync = fs_54.fsyncSync;
var fs_55 = require("fs");
exports.write = fs_55.write;
var fs_56 = require("fs");
exports.writeSync = fs_56.writeSync;
var fs_57 = require("fs");
exports.read = fs_57.read;
var fs_58 = require("fs");
exports.readSync = fs_58.readSync;
var fs_59 = require("fs");
exports.readFile = fs_59.readFile;
var fs_60 = require("fs");
exports.readFileSync = fs_60.readFileSync;
var fs_61 = require("fs");
exports.writeFile = fs_61.writeFile;
var fs_62 = require("fs");
exports.writeFileSync = fs_62.writeFileSync;
var fs_63 = require("fs");
exports.appendFile = fs_63.appendFile;
var fs_64 = require("fs");
exports.appendFileSync = fs_64.appendFileSync;
var fs_65 = require("fs");
exports.watchFile = fs_65.watchFile;
var fs_66 = require("fs");
exports.unwatchFile = fs_66.unwatchFile;
var fs_67 = require("fs");
exports.watch = fs_67.watch;
var fs_68 = require("fs");
exports.exists = fs_68.exists;
var fs_69 = require("fs");
exports.existsSync = fs_69.existsSync;
var fs_70 = require("fs");
exports.access = fs_70.access;
var fs_71 = require("fs");
exports.accessSync = fs_71.accessSync;
var fs_72 = require("fs");
exports.createReadStream = fs_72.createReadStream;
var fs_73 = require("fs");
exports.createWriteStream = fs_73.createWriteStream;
var fs_74 = require("fs");
exports.fdatasync = fs_74.fdatasync;
var fs_75 = require("fs");
exports.fdatasyncSync = fs_75.fdatasyncSync;
var fs_76 = require("fs");
exports.copyFile = fs_76.copyFile;
