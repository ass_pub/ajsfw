/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

(function (m, mm) {
    var w = window;
    if (!w._ajs) throw "Main package must be loaded first!";

    function addParent(m, pk, p) {
        for (var k in m) if (k !== "__parent__") {
            if (typeof m[k] !== "string") addParent(m[k], k, m);
            if (p) m.__parent__ = { k: pk, p: p };
        }
    }

    function push(s, d) {
        var p = Object.keys(s);
        for (var i = 0; i < p.length; i++) {
            var k = p[i];
            if (!d.hasOwnProperty(k)) {
                d[k] = s[k];
            } else {
                push(s[k], d[k]);
            }
        }
    }

    push(m, w._ajs.modules);
    addParent(w._ajs.modules);

    return mm;
}());
