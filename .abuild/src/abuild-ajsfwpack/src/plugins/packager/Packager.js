"use strict";
/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
exports.__esModule = true;
var fs = require("fs");
var path = require("path");
var depgraph_1 = require("../../utils/depgraph");
/**
 * AjsBuild packager plugin
 * Used to create packages of modules in dependancy on the configuration
 * First module of the created package is entry module specified in the configuration file
 * The first package created is a common package containing the module resolver code (see loader.js) and all modules common to all packages
 * Other packages includes just the module registration code (see pusher.js)
 * Packages contains just list of modules (id of the module, module code as a string value and the module dependencies list)
 */
var Packager = /** @class */ (function () {
    function Packager(diag, workspaceFolder, buildFolder, distFolder) {
        this._diag = diag;
        this._buildFolder = path.resolve(workspaceFolder, buildFolder);
        this._distFolder = path.resolve(workspaceFolder, distFolder);
        this._packages = {};
    }
    Object.defineProperty(Packager.prototype, "name", {
        get: function () { return "Packager"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Packager.prototype, "copyright", {
        get: function () { return "Copyright (c)2018 Atom Software Studios, MIT"; },
        enumerable: true,
        configurable: true
    });
    Packager.prototype.init = function (config) {
        this._packages = {};
        for (var pkg in config.packages) {
            this._packages[pkg] = {
                mainModule: config.packages[pkg]
            };
        }
    };
    /**
     * Builds output packages based on inputs passed to the constructor
     */
    Packager.prototype.run = function () {
        this._resolvePackagesModules();
        this._loadPackagesModules();
        this._buildPackages();
    };
    /**
     * Executes in watch mode when one of monitored files is changed
     * @param filename Name of the file changed to be porcessed
     */
    Packager.prototype.fileChanged = function (filename) {
    };
    /**
     * Resolves module dependencies and assigns modules to correct packages
     */
    Packager.prototype._resolvePackagesModules = function () {
        // prepare the module depenendcy graph builder
        var dg = new depgraph_1.DepGraph(this._buildFolder);
        // store reference to main package and initialize module arrays
        for (var packageName in this._packages) {
            if (!this._mainPackage)
                this._mainPackage = this._packages[packageName];
            this._packages[packageName].modules = [];
        }
        // build a separate package for each entry module specified
        for (var packageName in this._packages) {
            // prepare package info structure
            var currentPackage = this._packages[packageName];
            // build the dependency graph
            var moduleIdList = dg.graph(currentPackage.mainModule);
            for (var _i = 0, moduleIdList_1 = moduleIdList; _i < moduleIdList_1.length; _i++) {
                var m = moduleIdList_1[_i];
                // if the module is already included in the main pkg, don't include it again to the current package
                // either don't check previously created packages for the module existence as it should not exist there
                if (this._moduleIndexByFilename(this._mainPackage.modules, m.filename) !== -1)
                    continue;
                // if the module is also used in another package (except main), remove it and put to main
                var modMoved = false;
                // walk trough all packages, except main and the current one
                for (var pkgName in this._packages) {
                    var pkg = this._packages[pkgName];
                    if (pkg === currentPackage || pkg === this._mainPackage)
                        continue;
                    // check if the module is contained within the previously created package
                    var modIndex = this._moduleIndexByFilename(pkg.modules, m.filename);
                    // if the module was previously included to another package
                    if (modIndex !== -1) {
                        // move it to main package
                        this._mainPackage.modules.push(pkg.modules[modIndex]);
                        // and remove it from the package where it was previously included
                        pkg.modules.splice(modIndex, 1);
                        // flag it was moved to main so it will not be added to the current package
                        modMoved = true;
                    }
                }
                // if the module was not previously added to any package, add it to the current one
                if (!modMoved) {
                    var newMod = {
                        filename: m.filename,
                        code: m.code || undefined,
                        requires: []
                    };
                    for (var _a = 0, _b = m.requires; _a < _b.length; _a++) {
                        var rm = _b[_a];
                        newMod.requires.push(rm.filename);
                    }
                    currentPackage.modules.push(newMod);
                }
            }
        }
    };
    /**
     * Loads modules code as a string to the package/module
     */
    Packager.prototype._loadPackagesModules = function () {
        // walk through all packages
        for (var packageName in this._packages) {
            var pkg = this._packages[packageName];
            // and for each declared module load its source code
            for (var _i = 0, _a = pkg.modules; _i < _a.length; _i++) {
                var mod = _a[_i];
                try {
                    if (!mod.filename.endsWith("package.json")) {
                        mod.code = fs.readFileSync(mod.filename).toString();
                    }
                }
                catch (e) {
                    throw "Failed to load '" + mod.filename + "':\n   " + e;
                }
            }
        }
    };
    /**
     * Returns index of the module sesrched by the fileanem
     * @param moduleList Module list to be searched in
     * @param filename Absolute path to the module to be searched
     */
    Packager.prototype._moduleIndexByFilename = function (moduleList, filename) {
        for (var i = 0; i < moduleList.length; i++) {
            if (moduleList[i].filename === filename)
                return i;
        }
        return -1;
    };
    /**
     * Builds output packages including source maps if avaialable
     */
    Packager.prototype._buildPackages = function () {
        var loader = fs.readFileSync(path.resolve(__dirname, "loader.js")).toString();
        loader = loader.substr(0, loader.lastIndexOf("));"));
        var pusher = fs.readFileSync(path.resolve(__dirname, "pusher.js")).toString();
        pusher = pusher.substr(0, pusher.lastIndexOf("));"));
        for (var pkgName in this._packages) {
            var content = ""; //"[\n";
            var pkg = this._packages[pkgName];
            var modules = {};
            for (var _i = 0, _a = pkg.modules; _i < _a.length; _i++) {
                var mod = _a[_i];
                var relFilename = path.relative(this._buildFolder, mod.filename);
                if (relFilename.startsWith(".." + path.sep)) {
                    while (relFilename.startsWith(".." + path.sep)) {
                        relFilename = relFilename.substring(3);
                    }
                }
                var dir = modules;
                var segments = relFilename.split(path.sep);
                for (var i = 0; i < segments.length - 1; i++) {
                    if (dir.hasOwnProperty(segments[i])) {
                        dir = dir[segments[i]];
                    }
                    else {
                        dir[segments[i]] = {};
                        dir = dir[segments[i]];
                    }
                }
                var url = "http://localhost:8080/build/" + relFilename.replace(/\\/gm, "/");
                var sourceUrl = "//# sourceURL=" + url.replace(/\\/gm, "/");
                if (mod.filename.endsWith("package.json")) {
                    relFilename = path.relative(this._buildFolder, mod.code);
                    if (relFilename.startsWith(".." + path.sep)) {
                        while (relFilename.startsWith(".." + path.sep)) {
                            relFilename = relFilename.substring(3);
                        }
                    }
                    dir[segments[segments.length - 1]] = relFilename.replace(/\\/gm, "/");
                }
                else {
                    dir[segments[segments.length - 1]] = mod.code + "\n" + sourceUrl;
                }
            }
            content = JSON.stringify(modules, null, 2);
            if (pkg === this._mainPackage) {
                content += "));";
                content = loader + content;
            }
            else {
                content += ", \"" + pkg.mainModule.replace(/\\/gm, "/") + "\"";
                content += "));";
                content = pusher + content;
            }
            var filename = path.resolve(this._distFolder, pkgName);
            fs.writeFileSync(filename, content);
            this._diag.info("packager", "Created package '" + filename + "'. Package size: " + fs.statSync(filename).size.toLocaleString() + " bytes");
        }
    };
    return Packager;
}());
exports.Packager = Packager;
