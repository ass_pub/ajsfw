"use strict";
/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fs = require("fs");
// inject accorn dynamic import support (parser)
const acorn = require("acorn-dynamic-import").default;
// inject accorn dynamic import support (walker)
const walk = require("acorn-dynamic-import/lib/walk").default;
/**
 * EcmaScript Dependency Analyzer
 * Searches for dependencies on another modules
 * Currently, CommonJS and esnext modules are supported (static and dynamic)
 */
class EsDeps {
    /**
     * Creates and configures the es dependency analyzer
     * @param baseUrl Base url to be used to load files from
     * @param acronOptions Acron parser options
     */
    constructor(baseUrl, acronOptions) {
        this._baseUrl = baseUrl;
        this._acornOptions = acronOptions;
    }
    /**
     * Returns list of dependencies collected from the file
     * @param url Relative url to baseUrl of the file relative to the ulr base directory
     */
    getDeps(url) {
        const rootPath = path.resolve(this._baseUrl, url);
        const content = fs.readFileSync(rootPath).toString();
        const program = acorn.parse(content, this._acornOptions);
        const deps = [];
        walk.ancestor(program, {
            "CallExpression": (cex, ancestors) => {
                this._processRequire(cex, ancestors, deps);
            },
            "Import": (node, ancestors) => {
                this._processImport(node, ancestors, deps);
            },
            "ImportDeclaration": (idec, ancestors) => {
                this._processImportDeclaration(idec, ancestors, deps);
            }
        }, walk.base);
        return deps;
    }
    /**
     * Searches for require identifier and a call statement then processes it
     * @param cex Call expression to be checked for require and eventually processed
     * @param ancestors Node ancestors
     * @param deps Array to which the eventually found dependency information will be pushed
     */
    _processRequire(cex, ancestors, deps) {
        // look just for CallExpression with the require identifier
        if (cex.callee.type !== "Identifier" || cex.callee.name !== "require")
            return;
        // exactly 1 grgument must be used within the require CEx
        if (cex.arguments.length !== 1)
            return;
        // search for first block element in ancestors. If block statement is found, the dynamic import is used
        let isDynamic = false;
        for (let i = ancestors.length - 1; i >= 0; i--) {
            if (ancestors[i].type === "BlockStatement") {
                isDynamic = true;
                break;
            }
        }
        // also, when the require call argument contains anything else than [string] literal it is a dynamic import
        if (!isDynamic && cex.arguments[0].type !== "Literal") {
            isDynamic = true;
        }
        // push info about the discovered dependency
        deps.push({
            id: isDynamic ? undefined : cex.arguments[0].value,
            filename: undefined,
            dynamic: isDynamic,
            callExpression: cex,
            ancestors
        });
    }
    /**
     * Processes the import declaration (currently not supported as there is no ECMA standard for loaders)
     * TODO: IMPLEMENT WHEN ECMA LOADERS ARE STANDARDIZED
     * @param idec Import declaration to be processed
     * @param ancestors Node ancestors
     * @param deps Array to which the eventually found dependency information will be pushed
     */
    _processImportDeclaration(idec, ancestors, deps) {
        deps.push({
            id: idec.source.value,
            filename: undefined,
            dynamic: false,
            importDeclaration: idec,
            ancestors
        });
    }
    /**
     * Processes the import (dynamic) node
     * TODO: IMPLEMENT WHEN ECMA LOADERS ARE STANDARDIZED
     * @param i Import node to be processed
     * @param ancestors Node ancestors
     * @param deps Array to which the eventually found dependency information will be pushed
     */
    _processImport(i, ancestors, deps) {
        deps.push({
            id: undefined,
            filename: undefined,
            dynamic: true,
            import: i,
            ancestors
        });
    }
}
exports.EsDeps = EsDeps;
//# sourceMappingURL=EsDeps.js.map