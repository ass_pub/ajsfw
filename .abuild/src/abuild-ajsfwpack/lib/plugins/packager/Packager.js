"use strict";
/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const depgraph_1 = require("../../utils/depgraph");
/**
 * AjsBuild packager plugin
 * Used to create packages of modules in dependancy on the configuration
 * First module of the created package is entry module specified in the configuration file
 * The first package created is a common package containing the module resolver code (see loader.js) and all modules common to all packages
 * Other packages includes just the module registration code (see pusher.js)
 * Packages contains just list of modules (id of the module, module code as a string value and the module dependencies list)
 */
class Packager {
    get name() { return "Packager"; }
    get copyright() { return "Copyright (c)2018 Atom Software Studios, MIT"; }
    constructor(diag, workspaceFolder, buildFolder, distFolder) {
        this._diag = diag;
        this._buildFolder = path.resolve(workspaceFolder, buildFolder);
        this._distFolder = path.resolve(workspaceFolder, distFolder);
        this._packages = {};
    }
    init(config) {
        this._packages = {};
        for (const pkg in config.packages) {
            this._packages[pkg] = {
                mainModule: config.packages[pkg]
            };
        }
    }
    /**
     * Builds output packages based on inputs passed to the constructor
     */
    run() {
        this._resolvePackagesModules();
        this._loadPackagesModules();
        this._buildPackages();
    }
    /**
     * Executes in watch mode when one of monitored files is changed
     * @param filename Name of the file changed to be porcessed
     */
    fileChanged(filename) {
    }
    /**
     * Resolves module dependencies and assigns modules to correct packages
     */
    _resolvePackagesModules() {
        // prepare the module depenendcy graph builder
        const dg = new depgraph_1.DepGraph(this._buildFolder);
        // store reference to main package and initialize module arrays
        for (const packageName in this._packages) {
            if (!this._mainPackage)
                this._mainPackage = this._packages[packageName];
            this._packages[packageName].modules = [];
        }
        // build a separate package for each entry module specified
        for (const packageName in this._packages) {
            // prepare package info structure
            const currentPackage = this._packages[packageName];
            // build the dependency graph
            const moduleIdList = dg.graph(currentPackage.mainModule);
            for (const m of moduleIdList) {
                // if the module is already included in the main pkg, don't include it again to the current package
                // either don't check previously created packages for the module existence as it should not exist there
                if (this._moduleIndexByFilename(this._mainPackage.modules, m.filename) !== -1)
                    continue;
                // if the module is also used in another package (except main), remove it and put to main
                let modMoved = false;
                // walk trough all packages, except main and the current one
                for (const pkgName in this._packages) {
                    const pkg = this._packages[pkgName];
                    if (pkg === currentPackage || pkg === this._mainPackage)
                        continue;
                    // check if the module is contained within the previously created package
                    const modIndex = this._moduleIndexByFilename(pkg.modules, m.filename);
                    // if the module was previously included to another package
                    if (modIndex !== -1) {
                        // move it to main package
                        this._mainPackage.modules.push(pkg.modules[modIndex]);
                        // and remove it from the package where it was previously included
                        pkg.modules.splice(modIndex, 1);
                        // flag it was moved to main so it will not be added to the current package
                        modMoved = true;
                    }
                }
                // if the module was not previously added to any package, add it to the current one
                if (!modMoved) {
                    const newMod = {
                        filename: m.filename,
                        code: m.code || undefined,
                        requires: []
                    };
                    for (const rm of m.requires)
                        newMod.requires.push(rm.filename);
                    currentPackage.modules.push(newMod);
                }
            }
        }
    }
    /**
     * Loads modules code as a string to the package/module
     */
    _loadPackagesModules() {
        // walk through all packages
        for (const packageName in this._packages) {
            const pkg = this._packages[packageName];
            // and for each declared module load its source code
            for (const mod of pkg.modules) {
                try {
                    if (!mod.filename.endsWith("package.json")) {
                        mod.code = fs.readFileSync(mod.filename).toString();
                    }
                }
                catch (e) {
                    throw `Failed to load '${mod.filename}':\n   ${e}`;
                }
            }
        }
    }
    /**
     * Returns index of the module sesrched by the fileanem
     * @param moduleList Module list to be searched in
     * @param filename Absolute path to the module to be searched
     */
    _moduleIndexByFilename(moduleList, filename) {
        for (let i = 0; i < moduleList.length; i++) {
            if (moduleList[i].filename === filename)
                return i;
        }
        return -1;
    }
    /**
     * Builds output packages including source maps if avaialable
     */
    _buildPackages() {
        let loader = fs.readFileSync(path.resolve(__dirname, "loader.js")).toString();
        loader = loader.substr(0, loader.lastIndexOf("));"));
        let pusher = fs.readFileSync(path.resolve(__dirname, "pusher.js")).toString();
        pusher = pusher.substr(0, pusher.lastIndexOf("));"));
        for (const pkgName in this._packages) {
            let content = ""; //"[\n";
            const pkg = this._packages[pkgName];
            const modules = {};
            for (const mod of pkg.modules) {
                let relFilename = path.relative(this._buildFolder, mod.filename);
                if (relFilename.startsWith(".." + path.sep)) {
                    while (relFilename.startsWith(".." + path.sep)) {
                        relFilename = relFilename.substring(3);
                    }
                }
                let dir = modules;
                let segments = relFilename.split(path.sep);
                for (let i = 0; i < segments.length - 1; i++) {
                    if (dir.hasOwnProperty(segments[i])) {
                        dir = dir[segments[i]];
                    }
                    else {
                        dir[segments[i]] = {};
                        dir = dir[segments[i]];
                    }
                }
                const url = "http://localhost:8080/build/" + relFilename.replace(/\\/gm, "/");
                const sourceUrl = "//# sourceURL=" + url.replace(/\\/gm, "/");
                if (mod.filename.endsWith("package.json")) {
                    relFilename = path.relative(this._buildFolder, mod.code);
                    if (relFilename.startsWith(".." + path.sep)) {
                        while (relFilename.startsWith(".." + path.sep)) {
                            relFilename = relFilename.substring(3);
                        }
                    }
                    dir[segments[segments.length - 1]] = relFilename.replace(/\\/gm, "/");
                }
                else {
                    dir[segments[segments.length - 1]] = mod.code + "\n" + sourceUrl;
                }
            }
            content = JSON.stringify(modules, null, 2);
            if (pkg === this._mainPackage) {
                content += "));";
                content = loader + content;
            }
            else {
                content += ", \"" + pkg.mainModule.replace(/\\/gm, "/") + "\"";
                content += "));";
                content = pusher + content;
            }
            const filename = path.resolve(this._distFolder, pkgName);
            fs.writeFileSync(filename, content);
            this._diag.info("packager", `Created package '${filename}'. Package size: ${fs.statSync(filename).size.toLocaleString()} bytes`);
        }
    }
}
exports.Packager = Packager;
//# sourceMappingURL=Packager.js.map