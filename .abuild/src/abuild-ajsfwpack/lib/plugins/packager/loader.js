// The MIT License (MIT), Copyright (c)2018-2019 Atom Software Studios. All rights reserved.
// TODO: Add support for global modules
// TODO: Fix parent folder modules (../baseUrl) -> move to global modules
// Modules definitions ({ id: string, code: string }) are passed automatically
(function (modules) {
    // just to shorten the window, use w variable instead
    var w = window;
    // adds parent folder reference to the folder structure
    function addParent(m, pk, p) {
        for (var k in m)
            if (typeof m[k] !== "string")
                addParent(m[k], k, m);
        if (p)
            m.__parent__ = { k: pk, p: p };
    }
    addParent(modules);
    // _ajs object is stored globally in order to be extendable by lazy loaded packages
    w._ajs = w._ajs || {
        // cache of instantiated modules { id: string, filename: string, exports: keyValue<string,object> }
        cache: {},
        // loaded modules (see definition above)
        modules: modules,
        // require implementation
        require: function (moduleId) {
            // just to shorten the reference
            var ajs = w._ajs;
            // get file name of the module from the parent (current) module and required module id
            var mod = ajs.findModule(moduleId, this.filename);
            // if the filename was not recognized in the module list, throw an error
            if (!mod)
                throw "Can't resolve module '" + moduleId + "'" + (this.filename ? " required by '" + this.filename + "'" : "");
            // if the module was resolved previously (cache[filename]) exists, return its exports
            if (ajs.cache.hasOwnProperty(mod.f))
                return (ajs.cache[mod.f].exports);
            // prepare new module descriptor object and store it to the cache
            var moduleDescriptor = new Object();
            moduleDescriptor.filename = mod.f;
            moduleDescriptor.exports = new Object();
            ajs.cache[mod.f] = moduleDescriptor;
            // prepare the standard module definition function
            var define = function (require, exports, module) {
                eval(mod.c);
            };
            // call the module and executing it will fill exports
            var bound = ajs.require.bind(moduleDescriptor);
            define.call(moduleDescriptor, bound, moduleDescriptor.exports, moduleDescriptor);
            // return exports
            return moduleDescriptor.exports;
        },
        findModule: function (moduleId, dependent) {
            // tries to locate the file on the passed path or according to defined rules
            function test(moduleName, folder) {
                if (folder.__parent__ && folder.__parent__.k === moduleName && folder.hasOwnProperty("index.js"))
                    return "index.js";
                if (folder.hasOwnProperty(moduleName))
                    return moduleName;
                if (folder.hasOwnProperty(moduleName + ".js"))
                    return moduleName + ".js";
                if (folder.hasOwnProperty("package.json"))
                    return "package.json";
            }
            // navigates to the folder of the module
            function navigate(path, folder) {
                // navigate to path relative to dependent module (if any)
                path = path.split("/");
                for (var i = 0; i < path.length; i++) {
                    if (path[i] === ".")
                        continue;
                    if (path[i] === "..") {
                        folder = folder.__parent__.p;
                        continue;
                    }
                    if (folder.hasOwnProperty(path[i]) && typeof folder[path[i]] !== "string")
                        folder = folder[path[i]];
                }
                return folder;
            }
            dependent = dependent || "";
            var folder = w._ajs.modules;
            // relative path
            if (moduleId.substr(0, 2) === "./" || moduleId.substr(0, 3) === "../") {
                folder = navigate(dependent, folder);
            }
            var moduleName = moduleId.split("/");
            moduleName = moduleName[moduleName.length - 1];
            // navigateto folder given by the module ID (absolute form root)
            folder = navigate(moduleId, folder);
            // try to locate module on the given path
            var filename = test(moduleName, folder);
            if (!filename) {
                folder = navigate(moduleId, w._ajs.modules["node_modules"]);
                filename = test(moduleName, folder);
                if (!filename)
                    return;
            }
            if (filename === "package.json") {
                let fn = folder[filename].split("/");
                fn = fn[fn.length - 1];
                folder = navigate(folder[filename], w._ajs.modules["node_modules"]);
                filename = test(fn, folder);
                if (!filename)
                    return;
            }
            var code = folder[filename];
            // obtain full path to the module
            while (folder.__parent__) {
                filename = folder.__parent__.k + "/" + filename;
                folder = folder.__parent__.p;
            }
            return { f: filename, c: code };
        }
        /*        getFileName: function(moduleId, parentModule) {
        
                    // just to shorten ajs and allow better minification
                    var ajs = w._ajs;
        
                    // tries to locate the file on the passed path or according to defined rules
                    function tryPaths(path) {
                        var tests = [
                            path,
                            path + ".js",
                            path + "/index.js",
                            path + path.substr(path.lastIndexOf("/")) + ".js"
                        ];
        
                        for (var i = 0; i < tests.length; i++) {
                            if (ajs.modules.hasOwnProperty(tests[i])) return tests[i];
                        }
                    }
        
                    // check if the passed patch matches one of configured modules directly
                    if (!parentModule && ajs.modules.hasOwnProperty(moduleId)) return moduleId;
        
                    var filename;
        
                    // required module has a relative path from the current module
                    if (moduleId.substr(0, 2) === "./") {
                        filename = tryPaths(parentModule + moduleId.substr(1));
                        if (filename) return filename;
                        parentModule = parentModule.substr(0, parentModule.lastIndexOf("/"));
                        filename = parentModule + moduleId.substr(1);
                        if (filename.substr(0, 1) === "/") filename = "." + filename;
                        return tryPaths(filename);
                        // required module has a relative path from the current module, but upwards
                    } else if (moduleId.substr(0, 3) === "../") {
                        if (parentModule.substr(parentModule.length - 1, 1) === "/") parentModule = parentModule.substr(0, parentModule.length - 1);
                        parentModule = parentModule.substr(0, parentModule.lastIndexOf("/"));
                        filename = tryPaths(parentModule + moduleId.substr(2));
                        if (filename) return filename;
                        parentModule = parentModule.substr(0, parentModule.lastIndexOf("/"));
                        return tryPaths(parentModule + moduleId.substr(2));
                    // required module refers to the core, root or node_modules folders (i.e. path, typescript and so on)
                    } else {
                        filename = tryPaths(moduleId);
                        if (filename) return filename;
                        filename = tryPaths("./" + moduleId);
                        if (filename) return filename;
                        filename = tryPaths("./node_modules/" + moduleId);
                        if (filename) return filename;
                        filename = tryPaths("./../node_modules/" + moduleId);
                        if (filename) return filename;
                    }
                }*/
    };
    // as this loader is just in the first loaded package, get the first package module and require it
    var index = Object.keys(modules)[0];
    w._ajs.require(index.substr(0, index.length - 3));
}());
//# sourceMappingURL=loader.js.map