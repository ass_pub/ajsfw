"use strict";
/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
var usage_1 = require("./usage");
exports.usage = usage_1.usage;
let options;
let cmdArgs;
let currentArg;
function parseCmdLine(args) {
    if (!Array.isArray(args))
        return {};
    options = {};
    cmdArgs = args;
    currentArg = 0;
    while (currentArg < cmdArgs.length) {
        parseArg(cmdArgs[currentArg++]);
    }
    return options;
}
exports.parseCmdLine = parseCmdLine;
function getStringArg(errorMessage) {
    if (currentArg >= cmdArgs.length)
        throw errorMessage;
    if (cmdArgs[currentArg].startsWith("-"))
        throw errorMessage;
    const value = cmdArgs[currentArg];
    currentArg++;
    return value;
}
function parseArg(arg) {
    switch (arg) {
        case "-h":
        case "--help":
            options.help = true;
            break;
        case "-q":
        case "--quiet":
            options.quiet = true;
            break;
        case "-p":
        case "--project":
            options.project = getStringArg("[ajsfwbuild.cmdline]: Path to project folder or to the configuration file expected");
            break;
        case "-c":
        case "--build-config":
            options.buildConfig = getStringArg("[ajsfwbuild.cmdline]: Name of the configuration expected");
            break;
        case "-d":
        case "--define":
            break;
        default:
            throw `Unsupported command line argument '${arg}'`;
    }
}
//# sourceMappingURL=index.js.map