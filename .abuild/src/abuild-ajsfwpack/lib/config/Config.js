"use strict";
/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fs = require("fs");
class Config {
    get workspaceFolder() { return this._workspaceFolder; }
    get buildFolder() { return this._buildFolder; }
    get distFolder() { return this._distFolder; }
    get plugins() { return this._plugins; }
    /**
     * Constructs the Config object ready to load the configuration from the filesystem
     * @param workspaceFolder Optional - the workspace folder (usually passed from the command line)
     */
    constructor(workspaceFolder) {
        this._workspaceFolder = workspaceFolder || process.cwd();
        this._buildFolder = path.resolve(this._workspaceFolder, "./build");
        this._distFolder = path.resolve(this._workspaceFolder, "./js");
        this._plugins = {
            packager: {
                "packages": {
                    "index.js": "index"
                }
            }
        };
    }
    /**
     * Loads the configuration from the file.
     * @param filename Filename of the config file to be loaded (relative to the workspace root)
     */
    load(filename) {
        if (!fs.existsSync(filename) || !fs.statSync(filename))
            throw `Failed to load configuration file '${filename}'`;
        this._currentFile = filename;
        const file = fs.readFileSync(filename).toString();
        const json = this._stripJSONComments(file);
        const config = JSON.parse(json);
        this._applyConfig(config);
    }
    /**
     * Applies the loaded config to the curent object
     */
    _applyConfig(config) {
        for (const key in config) {
            switch (key) {
                case "build":
                    this._buildFolder = config[key];
                    break;
                case "dist":
                    this._distFolder = config[key];
                    break;
                case "plugins":
                    this._applyPluginsConfig(config[key]);
                    break;
                default:
                    throw `Invalid option: '${key}'`;
            }
        }
    }
    /**
     * Applies plugins configuration to the current object
     * The config is not checked at this time (it should be checked by each plugin when instanced)
     */
    _applyPluginsConfig(plugins) {
        for (const plugin in plugins) {
            this._plugins[plugin] = plugins[plugin];
        }
    }
    /**
     * Extends loaded configuration with configuration loaded from the file
     * @param filename Filename of the config to be loaded and extended
     */
    _extend(filename) {
    }
    /**
     * Strips comments from the json formatted string
     * @param json Json formatted string to be stripped
     */
    _stripJSONComments(json) {
        let pos = 0;
        let inComment1 = false;
        let inComment2 = false;
        let out = "";
        function nextChar() {
            if (pos < json.length - 1)
                return json[pos + 1];
        }
        while (pos < json.length) {
            if (!inComment1 && !inComment2 && json[pos] === "/" && nextChar() === "/") {
                inComment1 = true;
                pos += 2;
            }
            else if (!inComment1 && !inComment2 && json[pos] === "/" && nextChar() === "*") {
                inComment2 = true;
                pos += 2;
            }
            else if (inComment1 && json[pos] === "\n") {
                inComment1 = false;
                out += "\n";
                pos++;
            }
            else if (inComment2 && json[pos] === "*" && nextChar() === "/") {
                inComment2 = false;
                pos += 2;
            }
            else if (!inComment1 && !inComment2) {
                out += json[pos];
                pos++;
            }
            else {
                pos++;
            }
        }
        return out;
    }
}
exports.Config = Config;
//# sourceMappingURL=Config.js.map