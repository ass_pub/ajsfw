"use strict";
/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const colors = require("colors");
/** Holds name of the log file */
let logFile;
function fillSpaces(text) {
    return (`[${text}]                          `).substr(0, 25);
}
/**
 * Logs an info record to the screen and if log file is configured to the log file too
 * @param component Name of the component
 * @param message Info message
 */
function info(component, message) {
    const output = `INFO    ${fillSpaces(component)} ${message}`;
    console.log(colors.white(output));
    if (logFile)
        fs.writeFileSync(logFile, output, "a");
}
exports.info = info;
/**
 * Logs an info record to the screen and if log file is configured to the log file too
 * @param component Name of the component
 * @param message Info message
 */
function warning(component, message) {
    const output = `WARNING ${fillSpaces(component)} ${message}`;
    console.log(colors.yellow(output));
    if (logFile)
        fs.writeFileSync(logFile, output, "a");
}
exports.warning = warning;
/**
 * Logs an info record to the screen and if log file is configured to the log file too
 * @param component Name of the component
 * @param message Info message
 */
function error(component, message) {
    const output = `ERROR   ${fillSpaces(component)} ${message}`;
    console.error(colors.red(output));
    if (logFile)
        fs.writeFileSync(logFile, output, "a");
}
exports.error = error;
/**
 * Configures file name of the log file to be used to log diagnostic messages
 * @param filename Log file name
 * @param clean Indicates the file is cleaned before logging to it starts
 */
function useLogFile(filename, clean = false) {
    logFile = filename;
    if (clean)
        fs.writeFileSync(filename, "");
}
exports.useLogFile = useLogFile;
//# sourceMappingURL=index.js.map