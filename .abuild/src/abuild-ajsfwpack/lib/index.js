"use strict";
/*******************************************************************************
The MIT License (MIT)
Copyright (c)2018-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
const perf_hooks_1 = require("perf_hooks");
const buildTime = perf_hooks_1.performance.now();
const colors = require("colors");
const fs = require("fs");
const path = require("path");
const cmdline_1 = require("./cmdline");
const config_1 = require("./config");
const diag = require("./diag");
const utils_1 = require("./utils");
const version = "0.1";
const logComponentName = "ajsfwbuild";
const plugins = [];
let cmdLineOptions;
let workspaceFolder;
let configFilename;
let config;
// app entry point is at the botton of the file and calls main() .-)
function getWorkspaceFolder(options) {
    let pathOrConfig = options.project ? options.project : process.cwd();
    if (!path.isAbsolute(pathOrConfig))
        pathOrConfig = path.resolve(process.cwd(), pathOrConfig);
    if (path.extname(pathOrConfig) !== "")
        pathOrConfig = path.dirname(pathOrConfig);
    return pathOrConfig;
}
function getConfigFileName(workspaceFolder, options) {
    let resolvedPath;
    let searchPath;
    if (options.project) {
        const basepath = path.normalize(options.project);
        if (path.isAbsolute(basepath)) {
            resolvedPath = basepath;
        }
        else {
            const basename = path.basename(basepath);
            resolvedPath = basename === "." || basename === ".." ? workspaceFolder : path.resolve(basename, workspaceFolder);
        }
    }
    else {
        resolvedPath = path.resolve(workspaceFolder, "ajsfwbuild.json");
    }
    if (path.extname(resolvedPath) !== ".json") {
        resolvedPath = path.join(resolvedPath, "ajsfwbuild.json");
    }
    const filename = utils_1.addExtensionPrefix(path.basename(resolvedPath), options.buildConfig);
    const filepath = path.dirname(resolvedPath);
    searchPath = path.resolve(filepath, filename);
    if (fs.existsSync(searchPath) && fs.statSync(searchPath).isFile())
        return searchPath;
    searchPath = path.resolve(filepath, "./buildconfig", filename);
    if (fs.existsSync(searchPath) && fs.statSync(searchPath).isFile())
        return path.join(filepath, "buildconfig", filename);
}
function loadPlugin(pluginName) {
    let pluginModule;
    let pluginCtor;
    const localPlugin = path.resolve(__dirname, "plugins", pluginName);
    if (fs.existsSync(localPlugin) && fs.statSync(localPlugin).isDirectory()) {
        pluginModule = require(localPlugin);
    }
    else {
        try {
            pluginModule = require(pluginName);
        }
        catch (e) {
        }
    }
    if (pluginModule && pluginModule.default) {
        pluginCtor = pluginModule.default;
    }
    return pluginCtor;
}
function loadPlugins() {
    for (const p in config.plugins) {
        const start = perf_hooks_1.performance.now();
        const pluginCtor = loadPlugin(p);
        if (!pluginCtor)
            throw `Plugin '${p}' not found. Review configuration file `;
        if (typeof pluginCtor !== "function")
            throw `'${p}' is not valid ajsfwbuild plugin`;
        const plugin = new pluginCtor(diag, workspaceFolder, config.buildFolder, config.distFolder);
        if (!plugin.name || !plugin.copyright || !plugin.init)
            throw `'${p}' is not valid ajsfwbuild plugin`;
        plugins.push(plugin);
        const time = (perf_hooks_1.performance.now() - start).toFixed(3);
        diag.info(logComponentName, `Plugin '${p}' loaded successfully in ${time} ms`);
        plugin.init(config.plugins[p]);
    }
}
function runPlugins() {
    for (const plugin of plugins) {
        try {
            if (plugin.run)
                plugin.run();
        }
        catch (e) {
            diag.error("plugin." + plugin.name, e);
        }
    }
}
function main(argv) {
    try {
        cmdLineOptions = cmdline_1.parseCmdLine(argv);
        outputHeader();
        if (cmdLineOptions.help) {
            printUsage();
            return -1;
        }
        workspaceFolder = getWorkspaceFolder(cmdLineOptions);
        configFilename = getConfigFileName(workspaceFolder, cmdLineOptions);
        config = new config_1.Config();
        if (configFilename) {
            config.load(path.resolve(workspaceFolder, configFilename));
        }
        else {
            diag.warning(logComponentName, "Configuration file not found. Using defaults.");
        }
        outputConfig(workspaceFolder, configFilename, config);
        loadPlugins();
        runPlugins();
    }
    catch (e) {
        diag.error(logComponentName, e);
        return 1;
    }
    return 0;
}
function outputHeader() {
    if (cmdLineOptions.quiet)
        return;
    console.log();
    console.log(colors.cyan("AjsFwBuild v" + version));
    console.log(colors.cyan("Copyright (c)2018 Atom Software Studios, released under MIT license"));
}
function printUsage() {
    const help = cmdline_1.usage.split("\n");
    for (const line of help) {
        console.log(colors.white(line));
    }
}
function outputConfig(workspaceFolder, configFilename, config) {
    if (cmdLineOptions.quiet)
        return;
    console.log();
    console.log(colors.gray(`Workspace folder: ${workspaceFolder}`));
    console.log(colors.gray(`Config file:      ${configFilename ? configFilename : "not loaded"}`));
    console.log();
    console.log(colors.gray(`Loaded configuration:`));
    console.log(colors.gray(`   Build:         ${config.buildFolder}`));
    console.log(colors.gray(`   Dist:          ${config.distFolder}`));
    console.log();
    console.log(colors.gray(`Pugins configuration:`));
    for (const plugin in config.plugins) {
        console.log(colors.gray(`   ${plugin}:      ${JSON.stringify(config.plugins[plugin])}`));
    }
    console.log();
}
/* Application entry point */
let returnCode = main(process.argv.splice(2));
if (returnCode === 0) {
    const time = (perf_hooks_1.performance.now() - buildTime).toFixed(3);
    diag.info(logComponentName, `Total ajsfwbuild time: ${time} ms`);
    console.log();
}
// return codes less than zero means OK (just directs what to print in the end)
if (returnCode < 0) {
    returnCode = 0;
}
process.exit(returnCode);
//# sourceMappingURL=index.js.map