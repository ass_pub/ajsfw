/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as fs from "fs";
import * as path from "path";
import * as ts from "typescript";
import * as colors from "colors";

import api from "../api";

import { ABuildToolEtsConfig } from "./ABuildEtsConfig";
import { ABuildEtsConfig } from "./ets/ABuildEtsConfig";
import { createCompilerHost } from "./ets/ABuildEtsCompilerHost";
import { ABuildEtsCompiler } from "./ets/ABuildEtsCompiler";

const usage =
`   ${colors.cyan("abuild-ets")}

      Extended TypeScript -> Transpiles the TypeScript code to ESNEXT


      - configurable per project by tsconfig.<config>.json

      - root project tsconfig is used when multiple projects are built

      - ajsfwbuild ets plugin source / target / paths are used
        (configurable through the "config": { "ets" : { "src" : "...", "dst": "..." } } })

      - default paths are src = "./src/<project_name>" and dst = "./.build/<project_name/ts"

`;

export class ABuildEts extends api.ABuildToolSuper<ABuildToolEtsConfig> {

   public static readonly versions: string[] = [
      `abuild-ets 1.0.0`,
      `typescript ${ts.version}`
   ];

   public static readonly usage: string = usage;

   /**
    *
    */
   protected _configure(): void {

      this._config.outputDirName = this._config.outputDirName || "ts";
      this._config.mapFilesOutputDirName = this._config.mapFilesOutputDirName || "map";
      this._config.typesOuptutDirname = this._config.typesOuptutDirname || "@types";

      this._config.projectSrcPath = this._config.projectSrcPath || "<solutionPath>/<srcDirName>/<projectName>";
      this._config.projectOutPath = this._config.projectOutPath || "<solutionPath>/<buildDirName>/<projectName>/<outputDirName>";
      this._config.projectMapOutPath = this._config.projectMapOutPath || "<solutionPath>/<buildDirName>/<projectName>/<mapFilesOutputDirName>";
      this._config.projectTypesOutPath = this._config.projectTypesOutPath || "<solutionPath>/<buildDirName>/<projectName>/<typesOuptutDirname>";

      this._config.projectSrcPath = this._patchPath(this._config.projectSrcPath);
      this._config.projectOutPath = this._patchPath(this._config.projectOutPath);
      this._config.projectMapOutPath = this._patchPath(this._config.projectMapOutPath);
      this._config.projectTypesOutPath = this._patchPath(this._config.projectTypesOutPath);

   }


   /**
    *
    */
   protected _clean(): boolean {

      try {

         if (fs.existsSync(this._config.projectOutPath)) {
            api.diag.info(this.constructor.name, 1, `Cleaning '${this._config.projectOutPath}'`);
            this._rmDirRecursive(this._config.projectOutPath);
         }

         if (fs.existsSync(this._config.projectTypesOutPath)) {
            api.diag.info(this.constructor.name, 1, `Cleaning '${this._config.projectTypesOutPath}'`);
            this._rmDirRecursive(this._config.projectTypesOutPath);
         }

      } catch (e) {
         api.diag.warn(this.constructor.name, `Failed to clean: ${e}`);
         return true;
      }

      return true;
   }


   /**
    *
    */
   protected _build(buildConfig: string): boolean {

      const compiler = this.__setupCompiler(buildConfig);
      if (compiler === null) return false;
      if (!this.__compile(compiler)) return false;

      return true;

   }

   /**
    *
    */
   protected _watch(buildConfig: string): boolean {

      const emittedFiles: string[] = [];

      const compiler = this.__setupCompiler(buildConfig);
      if (compiler === null) return false;

      emittedFiles.length = 0;
      if (!this.__compile(compiler, emittedFiles)) return false;

      const toWatch: string[] = [];

      // add project directory to be monitored
      toWatch.push(this._config.projectSrcPath);

      // watch all config files involved
      for (const configFileName of compiler.config.allConfigFiles) {
         const fn = path.normalize(configFileName);
         if (toWatch.indexOf(fn) === -1) toWatch.push(fn);
      }

      // watch all input files
      for (const fileName of compiler.config.parsedCmdLine.fileNames) {
         const fn = path.normalize(fileName);
         if (toWatch.indexOf(fn) === -1)toWatch.push(fn);
      }

      // watch all references and imports other than those from current project
      for (const sourceFile of compiler.program.getSourceFiles()) {

         // don't watch d.ts files (only those related to imports - see bellow)
         // if (sourceFile.fileName.endsWith("d.ts")) continue;

         // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         // THIS CODE IS NOT TESTED AS REFERENCES WERE NOT REQUIRED TO BE WORKING WHEN TOOL WAS DEVELOPED
         // check all referenced files
         for (const referencedFile of sourceFile.referencedFiles) {

            // check if referenced file is in the project scope (project or projects this project depends on)
            let inScope = false;

            for (const project of this._project.dependsOn) {
               if (referencedFile.fileName.startsWith(project.name)) {
                  inScope = true;
                  break;
               }
            }

            if (!inScope) continue;
            if (referencedFile.fileName.startsWith("./") || referencedFile.fileName.startsWith("../")) continue;

            // !!! this will probably need to do same things as bellow in imports, not needed now !!!
            const fn = path.normalize(referencedFile.fileName);
            if (toWatch.indexOf(fn) === -1) toWatch.push(fn);
         }
         // THIS CODE IS NOT TESTED AS REFERENCES WERE NOT REQUIRED TO BE WORKING WHEN TOOL WAS DEVELOPED
         // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         // check all imported files
         for (const importedFile of (<any>sourceFile).imports) {

            // check if imported file is in the project scope (project or projects this project depends on)
            let inScope = false;

            for (const project of this._project.dependsOn) {
               if (importedFile.text.startsWith(project.name)) {
                  inScope = true;
                  break;
               }
            }

            // don't monitor files out of scope and skip files of current project (added already above)
            if (!inScope) continue;
            if (importedFile.text.startsWith("./") || importedFile.text.startsWith("../")) continue;

            // monitor the root folder of required node_modules module and eventually types
            const fn = path.normalize(importedFile.text);
            const sfn = fn.split(path.sep);
            const typesRootPath = path.join(this._project.solution.path, "node_modules", "@types", sfn[0]);
            const packageRootPath = path.join(this._project.solution.path, "node_modules", sfn[0]);

            if (toWatch.indexOf(typesRootPath) === -1) toWatch.push(typesRootPath);
            if (toWatch.indexOf(packageRootPath) === -1) toWatch.push(packageRootPath);

            // monitor also the file itself
            const typeFileToMonitorTypes = path.join(this._project.solution.path, "node_modules", "@types", fn + ".d.ts");
            const typeFileToMonitorPackages = path.join(this._project.solution.path, "node_modules", fn + ".d.ts");

            if (toWatch.indexOf(typeFileToMonitorTypes) === -1) toWatch.push(typeFileToMonitorTypes);
            if (toWatch.indexOf(typeFileToMonitorPackages) === -1) toWatch.push(typeFileToMonitorPackages);

         }

      }

      api.diag.info(this.constructor.name, 4, `Following source files/directories will be monitored`);
      for (const f of toWatch) {
         api.diag.info(this.constructor.name, 4, `   ${f}`);
      }

      api.fileWatcher.watch(
         toWatch,
         (eventInfo: api.FileEventInfo[]) => {
            this.__filesChanged(eventInfo, compiler, buildConfig);
         }
      );

      return true;

   }


   /**
    *
    * @param eventsInfo
    */
   private __filesChanged(eventsInfo: api.FileEventInfo[], compiler: ABuildEtsCompiler, buildConfig: string): void {

      api.diag.info(this.constructor.name, 2, `File changes detected ('${eventsInfo.length}')`);

      const inputFiles: string[] = [];
      const writtenFiles: string[] = [];

      let configChanged = false;
      let unlinkedFiles = false;
      let createdFiles = false;
      let changedDirs = false;

      for (const eventInfo of eventsInfo) {
         api.diag.info(this.constructor.name, 4, `   '${eventInfo.eventName}' : '${eventInfo.fileName}'`);

         const isConfigFile = compiler.config.allConfigFiles.indexOf(eventInfo.fileName) !== -1;
         if (isConfigFile) configChanged = true;

         if (!isConfigFile && eventInfo.eventName === "change" && inputFiles.indexOf(eventInfo.fileName) === -1) {
            inputFiles.push(eventInfo.fileName);
         }

         if (!isConfigFile && eventInfo.eventName === "unlink") {
            unlinkedFiles = true;
         }

         if (eventInfo.eventName === "create") {
            createdFiles = true;
         }

         if (eventInfo.fileType === api.FileType.Directory && eventInfo.eventName === "change") {
            changedDirs = true;
         }

      }

      if (configChanged) if (!compiler.updateConfigFile()) return;

      let compile = true;

      if (unlinkedFiles || createdFiles || changedDirs) {

         inputFiles.length = 0;

         const config = this.__loadTsConfig(buildConfig);

         let fileListChanged: boolean = config.parsedCmdLine.fileNames.length !== compiler.config.parsedCmdLine.fileNames.length;

         if (!fileListChanged) {
            for (const f of config.parsedCmdLine.fileNames) {
               if (compiler.config.parsedCmdLine.fileNames.indexOf(f) === -1) {
                  fileListChanged = true;
                  break;
               }
            }
         }

         if (fileListChanged) {
            compiler.config.parsedCmdLine.fileNames = [];
            for (const f of config.parsedCmdLine.fileNames) {
               compiler.config.parsedCmdLine.fileNames.push(f);
            }
         }

         compile = fileListChanged;

      }

      if (compile) {
         this.__compile(compiler, inputFiles, writtenFiles);
         api.fileWatcher.forceChangeEvents(writtenFiles);
      }

   }


   /**
    *
    */
   private __setupCompiler(buildConfig: string): ABuildEtsCompiler {

      const tsconfig = this.__loadTsConfig(buildConfig);
      if (!tsconfig) return null;

      const compilerHost = createCompilerHost(tsconfig.parsedCmdLine.options);

      return new ABuildEtsCompiler(compilerHost, tsconfig);

   }

   private __loadTsConfig(buildConfig: string): ABuildEtsConfig {

      let tsConfigFileName: string;

      if (buildConfig) {
         tsConfigFileName = path.join(this._config.projectSrcPath, `tsconfig.${buildConfig}.json`);
      } else {
         tsConfigFileName = path.join(this._config.projectSrcPath, "tsconfig.json");
      }

      const tsconfig = new ABuildEtsConfig(this._project, this._config);
      if (!tsconfig.updateConfigFile(tsConfigFileName)) return null;

      return tsconfig;

   }

   /**
    *
    * @param compiler
    */
   private __compile(compiler: ABuildEtsCompiler, inputFiles: string[] = [], writtenFiles: string[] = []): boolean {

      api.diag.info(this.constructor.name, 0, `Starting '${this._project.name}' project compilation using TypeScript v. '${ts.version}'`);
      const result = compiler.compile(true, inputFiles && inputFiles.length > 0 ? inputFiles : undefined);

      let errors = 0;
      let warns = 0;
      let sugs = 0;

      for (const d of result.preEmitDiagnostics) {
         if (d.category === ts.DiagnosticCategory.Error) errors++;
         if (d.category === ts.DiagnosticCategory.Warning) warns++;
         if (d.category === ts.DiagnosticCategory.Suggestion) sugs++;
      }

      for (const d of result.emitResult.diagnostics) {
         if (d.category === ts.DiagnosticCategory.Error) errors++;
         if (d.category === ts.DiagnosticCategory.Warning) warns++;
         if (d.category === ts.DiagnosticCategory.Suggestion) sugs++;
      }

      if (result.writtenFiles) {
         for (const f of result.writtenFiles) {
            const fn = path.normalize(f);
            if (writtenFiles.indexOf(fn) === -1) writtenFiles.push(fn);
         }
      }

      api.diag.info(this.constructor.name, 0, `Compilation finished with '${errors}' error/s, '${warns}' warning/s and '${sugs}' suggestion/s. Total '${writtenFiles.length}' file/s written.`);

      if (result.preEmitDiagnostics.length === 0 && result.emitResult.diagnostics.length === 0) return true;

      this.__logDiagnostics(result.preEmitDiagnostics);
      this.__logDiagnostics(result.emitResult.diagnostics);

      return errors === 0;

   }


   /**
    *
    * @param diagnostics
    */
   private __logDiagnostics(diagnostics: readonly ts.Diagnostic[]): void {

      let i = 0;
      for (const d of diagnostics) {
         let diagStr = ts.formatDiagnostic(
            d,
            {
               getCurrentDirectory: (): string => { return ""; },
               getCanonicalFileName: (fileName: string) => { return fileName; },
               getNewLine: () => { return "\n"; }
            }
         );
         if (diagStr.endsWith("\r\n")) diagStr = diagStr.substr(0, diagStr.length - 2);
         if (diagStr.endsWith("\n")) diagStr = diagStr.substr(0, diagStr.length - 1);
         api.diag.error(this.constructor.name, diagStr);
         i++;
         if (i > 10) break;
      }

   }

}
