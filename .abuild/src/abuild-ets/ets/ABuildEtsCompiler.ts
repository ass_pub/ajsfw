/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as path from "path";

import * as ts from "typescript";
import * as diag from "../../lib/diag";
import { ABuildEtsConfig } from "./ABuildEtsConfig";
import { ACompilerHost } from "./ABuildEtsCompilerHost";
import { createCompilerHost } from "./ABuildEtsCompilerHost";

interface CompilationResult {
   preEmitDiagnostics: readonly ts.Diagnostic[];
   emitResult: ts.EmitResult;
   writtenFiles: string[];
}

/**
 * Incremental TypeScript compiler implementation
 */
export class ABuildEtsCompiler {

   /**
    * Holds extended TypeScript configuration loaded form tsconfig file(s)
    */
   private __config: ABuildEtsConfig;
   public get config(): ABuildEtsConfig { return this.__config; }
   public set config(config: ABuildEtsConfig) {
      this.__config = config;
      this.__program = undefined;
   }

   /**
    * Stores a compiler host
    */
   private __host: ACompilerHost;

   /**
    * Stores instance of program when the initial compilation is done
    */
   private __program: ts.Program;
   public get program(): ts.Program { return this.__program; }


   constructor(compilerHost: ACompilerHost, config: ABuildEtsConfig) {
      this.__config = config;
      this.__host = compilerHost;
      this.__program = undefined;
   }


   /**
    * Initial or updated config file is used to create the compiler host
    * It must be called prior compilation or in case of incremental compilation after every config file(s) change.
    */
   public updateConfigFile(): boolean {
      if (!this.__config.updateConfigFile()) return false;
      try {
         this.__host = createCompilerHost(this.__config.parsedCmdLine.options);
      } catch  (e) {
         diag.error(this.constructor.name, `Failed to update config file: '${e}'`);
         return false;
      }
      this.__program = undefined;
      return true;
   }


   /**
    * Compiles a TypeScript project. Can be used also for incremental compilation.
    * @param emit Specifies if files shouls be emitted after compilation
    * @param fileNames List of files to be compiled. Only necessary or changed files should be passed.
    */
   public compile(emit: boolean = false, fileNames?: string[]): CompilationResult {

      if (!this.__host) throw "updateConfigFile must be called prior the compilation can be started!";

      // prepare list of files written to the disk
      // emit info returned from TSC does not need to contain complete list of files so compiler host is used
      const writtenFiles: string[] = [];

      // hook to file write event
      this.__host.onFileWritten.addListener((host: ACompilerHost, fileName: string) => {
         writtenFiles.push(fileName);
      });

      // during incremental compilation it is necessary to identify all dependent files in order to be possible to perform full type checking of changed files
      if (this.__program && (<any>this.__program).getRefFileMap && fileNames instanceof Array && fileNames.length > 0) {
         this.__includeDependents(fileNames);
      }

      // standard TSC API - compile program
      this.__program = ts.createProgram(
         this.__config.parsedCmdLine.fileNames,
         this.__config.parsedCmdLine.options,
         this.__host,
         this.__program,
         this.__config.parsedCmdLine.errors
      );

      // collect only compiled files matching input files and their dependednts

      const sourceFiles: ts.SourceFile[] = [];

      if (fileNames && fileNames instanceof Array) {
         for (const fileName of fileNames) {
            const sourceFile = this.__program.getSourceFile(fileName);
            if (sourceFile) sourceFiles.push(sourceFile);
         }
      }

      // prepare diag info array and emit result

      const preEmitDiagnostics: ts.Diagnostic[] = [];
      let emitResult: { emitSkipped: boolean, diagnostics: ts.Diagnostic[], emittedFiles?: string[] };


      // if some copiled files matches to input files
      if (sourceFiles.length > 0) {

         // collected diag just for files in interrest.
         for (const sf of sourceFiles) {
            const pED = ts.getPreEmitDiagnostics(this.__program, sf);
            for (const d of pED) {
               preEmitDiagnostics.push(d);
            }
         }

         // emit files, collect emit diag and list of files

         if (emit) {

            emitResult = {
               emitSkipped: false,
               diagnostics: [],
               emittedFiles: []
            };

            for (const sf of sourceFiles) {

               const er = this.__program.emit(sf);

               if (er.diagnostics && er.diagnostics instanceof Array) {
                  for (const d of er.diagnostics) emitResult.diagnostics.push(d);
               }

               if (er.emittedFiles && er.emittedFiles instanceof Array) {
                  for (const f of er.emittedFiles) emitResult.emittedFiles.push(f);
               } else {
                  emitResult.emittedFiles.push(sf.fileName);
               }

            }
         }

      } else {

         // if no input files matched just collect pre-emit diagnostics for the whole project

         const pED = ts.getPreEmitDiagnostics(this.__program);
         for (const d of pED) {
            preEmitDiagnostics.push(d);
         }

         emitResult = emit ? <any>this.__program.emit() : { emitSkipped: true, diagnostics: [] };

      }

      return { preEmitDiagnostics, emitResult, writtenFiles };

   }


   /**
    * Include dependent files
    * This method is using non-published TypeScript API methods and possibly can stop working with any future TypeScript version
    * @param fileNames List of file names on which other files depends
    */
   private __includeDependents(fileNames: string[]): void {

      const tmp = [];
      const map = (<any>this.__program).getRefFileMap(); // non ts-api published

      for (const f of fileNames) {

         // ref file map is using / in path, even on windows
         let fn = f.replace(/\\/g, "/");

         // ref file map stores files in lower case if useCaseSensitiveFileNames returns false
         if (!this.__host.useCaseSensitiveFileNames()) fn = fn.toLocaleLowerCase();

         // get dependents
         const refs = map.get(fn);
         if (!refs) continue;

         // for each dependent get its full file name with correct letter case and slashes
         for (const ref of refs) {
            const srcFile = this.__program.getSourceFileByPath(ref.file);
            if (srcFile) tmp.push(path.normalize(srcFile.fileName));
         }

      }

      // extend passed fileNames with obtained information
      for (const f of tmp) {
         if (fileNames.indexOf(f) === -1) fileNames.push(f);
      }

   }

}