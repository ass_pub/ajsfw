/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import api from "../../api";

import * as fs from "fs";
import * as path from "path";
import * as ts from "typescript";

import { ABuildToolEtsConfig } from "../ABuildEtsConfig";

export class ABuildEtsConfig {


   private __project: api.Project;

   private __etsConfig: ABuildToolEtsConfig;

   private __rootConfigPath: string;

   private __allConfigFiles: string[];
   public get allConfigFiles(): string[] { return this.__allConfigFiles; }

   private __parsedCmdLine: ts.ParsedCommandLine;
   public get parsedCmdLine(): ts.ParsedCommandLine { return this.__parsedCmdLine; }

   /**
    * Constructs the wrapper for typescript configuration file loader
    * @param rootDir Root dir usually = solution root dir
    */
   constructor(project: api.Project, etsConfig: ABuildToolEtsConfig) {

      this.__project = project;

      this.__etsConfig = etsConfig;

      this.__rootConfigPath = "";

      this.__allConfigFiles = [];

      this.__parsedCmdLine = {
         errors: [],
         fileNames: [],
         options: {},
         projectReferences: [],
         typeAcquisition: {},
         raw: {},
         wildcardDirectories: {},
         compileOnSave: false
      };

   }

   /**
    * Should be called all the time when the configuration file is changed
    * @param fileName Main typescript compiler configuration file to be loaded
    */
   public updateConfigFile(fileName?: string): boolean {

      fileName = fileName || this.__rootConfigPath;
      if (!fileName) {
         api.diag.error(this.constructor.name, `Failed to update config file, no config file exist'`);
         return false;
      }

      // if the file tsconfig.<buildConfig>.json not exist, try tsconfig.json
      if (!fileName.endsWith("tsconfig.json")) {
         if (!fs.existsSync(fileName)) {
            api.diag.warn(this.constructor.name, `Config '${fileName}' not found, trying tsconfig.json`);
            fileName = path.join(path.dirname(fileName), "tsconfig.json");
         }
      }

      api.diag.info(this.constructor.name, 1, `Updating root configuration file '${fileName}'`);

      // loads and parses the configuration file
      this.__rootConfigPath = fileName;
      if (!this.__loadConfig()) return false;

      // check for errors
      if (this.__parsedCmdLine.errors.length !== 0) {
         const pn = path.dirname(fileName.replace(/\\/g, "/"));
         const sp = this.__project.solution.path.replace(/\\/g, "/");
         for (const err of this.__parsedCmdLine.errors) {
            api.diag.error(this.constructor.name, err.messageText.toString().replace(sp, pn));
         }
         return false;
      }

      // stores paths to all config files extended by the root (used when watch mode is enabled to monitor config files changes)
      this.__allConfigFiles = [];
      if (!this.__findAllConfigFiles(this.__rootConfigPath)) return false;

      api.diag.info(this.constructor.name, 1, `Configuration updated successfully`);

      this.__configDiag();

      return true;

   }

   /**
    * Prints configuration diagnostic
    */
   private __configDiag(): void {

      api.diag.info(this.constructor.name, 2, `   Main config file:      '${this.__rootConfigPath}'`);
      api.diag.info(this.constructor.name, 2, `   Extends:               '${this.__allConfigFiles.length - 1}' config file/s`);

      for (let i = 1; i < this.__allConfigFiles.length; i++) {
         api.diag.info(this.constructor.name, 2, `   Extended config file:  '${path.relative(path.dirname(this.__rootConfigPath), this.__allConfigFiles[i])}'`);
      }

      api.diag.info(this.constructor.name, 2, `   Options:`);

      const preventDisplay = [ "configFile", "configFilePath" ];
      for (const k in this.__parsedCmdLine.options) {

         let text: string = "";

         switch (k) {
            case "moduleResolution":
               text = `${ts.ModuleResolutionKind[this.__parsedCmdLine.options[k]]}`;
               break;
            case "module":
               text = `${ts.ModuleKind[this.__parsedCmdLine.options[k]]}`;
               break;
            case "target":
               text = `${ts.ScriptTarget[this.__parsedCmdLine.options[k]]}`;
               break;
            default:
               if (preventDisplay.indexOf(k) === -1) {
                  text = `${this.__parsedCmdLine.options[k]}`;
               }
         }

         if (text !== "") {
            api.diag.info(this.constructor.name, 2, `      ${(k + ":                         ").substr(0, 25)}'${text}'`);
         }
      }

      const sfiles = this.__parsedCmdLine.fileNames.filter(
         (value: string, index: number) => { return !value.endsWith(".d.ts"); }
      );

      const tfiles = this.__parsedCmdLine.fileNames.filter(
         (value: string, index: number) => { return value.endsWith(".d.ts"); }
      );

      api.diag.info(this.constructor.name, 2, `   '${sfiles.length}' source files will be included in the build process:`);

      for (const f of sfiles) {
         api.diag.info(this.constructor.name, 4, `      ${f}`);
      }

      api.diag.info(this.constructor.name, 2, `   '${tfiles.length}' type files will be included in the build process:`);

      for (const f of tfiles) {
         api.diag.info(this.constructor.name, 4, `      ${f}`);
      }

   }

   /**
    * Loads a configuration file using the standard TypeScript API functions while paths are patched according
    * to the Solution and Project locations
    */
   private __loadConfig(): boolean {

      api.diag.info(this.constructor.name, 1, `Loading TypeScript configuration`);

      let unrecoverableOccured = false;

      // configure parse config host
      const host: ts.ParseConfigFileHost = {

         // current directory is always solution path
         getCurrentDirectory: (): string => {
            return this.__project.solution.path;
         },

         // some diagnostics
         onUnRecoverableConfigFileDiagnostic: (diagnostic: ts.Diagnostic): void => {
            api.diag.error(this.constructor.name, diagnostic.messageText.toString());
            unrecoverableOccured = true;
         },

         // read config and patch paths to fit solution and project
         readFile: (configPath: string): string => {
            return this.__readConfigAndPatch(configPath);
         },

         // use standard ts.sys functions for the rest
         useCaseSensitiveFileNames: ts.sys.useCaseSensitiveFileNames,
         readDirectory: ts.sys.readDirectory,
         fileExists: ts.sys.fileExists
       };

       // get the configuration for tsc - this path must point to the solution root dir, the real file is read from project path (see __readConfigAndPatch)
       const configPath = path.join(this.__project.solution.path, path.basename(this.__rootConfigPath));
       this.__parsedCmdLine = ts.getParsedCommandLineOfConfigFile(configPath, undefined, host);

       // patch options according the solution and project settings
       return !unrecoverableOccured;

   }

   /**
    * Check if the config file extends another config file and tries to resolve and find all extended configs
    * @param configPath Path to config file to be checked if it extends another config file
    */
   private __findAllConfigFiles(configPath: string): boolean {

      if (!fs.existsSync(configPath)) {
         api.diag.error(this.constructor.name, `Config file '${configPath}' not found`);
         return false;
      }

      this.__allConfigFiles.push(configPath);

      if (configPath !== this.__rootConfigPath) {
         api.diag.info(this.constructor.name, 2, `Found config file '${path.relative(path.dirname(this.__rootConfigPath), configPath)}'`);
      }

      let configExtends: string = "";

      try {
         const cfgContent = fs.readFileSync(configPath);
         const jsonText = api.json.stripComments(cfgContent.toString());
         const jsonJs = JSON.parse(jsonText);
         if (!jsonJs.hasOwnProperty("extends")) return true;
         configExtends = jsonJs.extends;
         api.diag.info(this.constructor.name, 2, `Config '${path.basename(configPath)}' extends '${path.basename(configExtends)}'`);
      } catch (e) {
         api.diag.error(this.constructor.name, `Failed to search for extended files: ${e.message}`);
         return false;
      }

      const extpath = path.isAbsolute(configExtends) ? configExtends : path.resolve(path.dirname(configPath), configExtends);

      return this.__findAllConfigFiles(extpath);

   }

   /**
    * Reads a tsconfig file from disk and patches paths (baseUrl, rootDir, outDir, declarationDir)
    * @param configPath path to the config file to be loaded from the disk
    */
   private __readConfigAndPatch(configPath: string): string {

      configPath = path.join(this.__etsConfig.projectSrcPath, path.basename(configPath));

      if (!fs.existsSync(configPath)) return undefined;

      const cfgContent = fs.readFileSync(configPath);
      const jsonText = api.json.stripComments(cfgContent.toString());
      const jsonJs = JSON.parse(jsonText);
      this.__patchConfig(jsonJs);
      return JSON.stringify(jsonJs);
   }

   /**
    * Patches the tsconfig to fit abuild paths
    * @param jsonJs config object to be patched
    */
   private __patchConfig(jsonJs: any): void {

      const relProjectSrc = "./" + path.relative(this.__project.solution.path, this.__etsConfig.projectSrcPath);

      // compile on save is not supported
      if (jsonJs.hasOwnProperty("compileOnSave")) {
         delete jsonJs.compileOnSave;
      }

      // references are not supported
      if (jsonJs.hasOwnProperty("references")) {
         delete jsonJs.references;
      }

      // fix include paths to match the project directory
      if (jsonJs.hasOwnProperty("include") && jsonJs.include instanceof Array) {
         for (let i = 0 ; i < jsonJs.include.length; i++) {
            jsonJs.include[i] = path.normalize(`${relProjectSrc}/${jsonJs.include[i]}`);
         }
      }

      // fix exclude paths to match the project directory
      if (jsonJs.hasOwnProperty("exclude") && jsonJs.exclude instanceof Array) {
         for (let i = 0 ; i < jsonJs.exclude.length; i++) {
            jsonJs.exclude[i] = path.normalize(`${relProjectSrc}/${jsonJs.exclude[i]}`);
         }
      }

      // remove unsupported compiler options and fix or add necessary paths
      if (!jsonJs.hasOwnProperty("compilerOptions")) jsonJs.compilerOptions = {};

      jsonJs.compilerOptions.rootDir = path.relative(this.__project.solution.path, this.__etsConfig.projectSrcPath);
      jsonJs.compilerOptions.baseUrl = path.relative(this.__project.solution.path, path.join(this.__project.solution.path, this.__project.solution.srcDirName));
      jsonJs.compilerOptions.outDir = path.relative(this.__project.solution.path, this.__etsConfig.projectOutPath);

      if (jsonJs.compilerOptions.sourceMap) {
         jsonJs.compilerOptions.sourceRoot = path.relative(this.__project.solution.path, this.__etsConfig.projectSrcPath);
      }

      if (jsonJs.compilerOptions.declaration || jsonJs.compilerOptions.composite) {
         jsonJs.compilerOptions.declarationDir = this.__etsConfig.projectTypesOutPath;
      }

      // jsonJs.outFile			projectTarget + filename
      // jsonJs.declarationDir		projectTarget + @types


   }

}
