/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as ts from "typescript";

interface Defines {
   [name: string]: number | string | boolean;
}

interface NodeToDelete {
   parent: ts.Node;
   node: ts.Node;

}

enum CommentType {
   Inline = 0,
   Block = 1
}

export class ABuildEtsPreprocessor {

   private __defines: Defines = {};
   private __currentSource: ts.SourceFile = undefined;

   private __evalDirectives: boolean[];
   private __lastParsedDirectiveEndPos: number;
   private __nodesToDelete: NodeToDelete[];

   constructor(defines?: Defines) {
      this.__defines = defines || {};
      this.__defines["TRUE"] = true;
      this.__defines["FALSE"] = false;
      this.__currentSource = undefined;
   }

   public preprocessSourceFile(sourceFile: ts.SourceFile): void {

      return;

      this.__currentSource = sourceFile;
      this.__evalDirectives = [ true ];
      this.__lastParsedDirectiveEndPos = 0;
      this.__nodesToDelete = [];

      ts.forEachChild(
         this.__currentSource,
         (node: ts.Node) => {
            this.__processNode(node, sourceFile);
         }
      );

      for (const ntd of this.__nodesToDelete) {
         this.__deleteNode(ntd.parent, ntd.node);
      }

   }

   private __processNode(node: ts.Node, parent: ts.Node): void {

      const nodeText = this.__currentSource.text.substr(node.pos, node.end - node.pos);

      /*console.log(`Processing node: ${ ts.SyntaxKind[node.kind] } ::: ${nodeText.replace(/\n/g, " ")}`);
      console.log("");*/

      this.__parseDirectives(nodeText, node.pos);

      if (!this.__evalDirectives[this.__evalDirectives.length - 1]) {
         this.__nodesToDelete.push({ parent, node });
      } else {
         if (node.kind === ts.SyntaxKind.Identifier) {
            const iName = nodeText.trim();
            if (this.__defines.hasOwnProperty(iName)) {
               if (typeof(this.__defines[iName]) === "string") {
                  const newNode = ts.createStringLiteral(this.__defines[iName].toString());
                  newNode.pos = node.pos;
                  this.__replaceNode(parent, node, newNode);
               }
               if (typeof(this.__defines[iName]) === "number") {
                  const newNode = ts.createNumericLiteral(this.__defines[iName].toString());
                  newNode.pos = node.pos;
                  this.__replaceNode(parent, node, newNode);
               }
               if (typeof(this.__defines[iName]) === "boolean") {
                  const newNode = ts.createNode(this.__defines[iName] ? ts.SyntaxKind.TrueKeyword : ts.SyntaxKind.FalseKeyword, node.pos, -1);
                  this.__replaceNode(parent, node, newNode);
               }
            }
         }

         ts.forEachChild(
            node,
            (child: ts.Node) => {
               this.__processNode(child, node);
            }
         );
      }
   }

   private __deleteReferencedFile(fileName: string): void {
      let i = 0;
      while (i < this.__currentSource.referencedFiles.length) {
         if (this.__currentSource.referencedFiles[i].fileName === fileName) {
            (<any[]>this.__currentSource.referencedFiles).splice(i, 1);
         } else {
            i++;
         }
      }
   }

   private __deleteTypeReferenceDirective(fileName: string): void {
      let i = 0;
      while (i < this.__currentSource.typeReferenceDirectives.length) {
         if (this.__currentSource.typeReferenceDirectives[i].fileName === fileName) {
            (<any[]>this.__currentSource.typeReferenceDirectives).splice(i, 1);
         } else {
            i++;
         }
      }
   }

   private __deleteLibReferenceDirective(fileName: string): void {
      let i = 0;
      while (i < this.__currentSource.libReferenceDirectives.length) {
         if (this.__currentSource.libReferenceDirectives[i].fileName === fileName) {
            (<any[]>this.__currentSource.libReferenceDirectives).splice(i, 1);
         } else {
            i++;
         }
      }
   }


   private __deleteNode(parent: ts.Node, node: ts.Node): void {

      for (const key in parent) {
         if ((<any>parent)[key] instanceof Array) {
            for (let i = 0; i < (<any>parent)[key].length; i++) {
               if ((<any>parent)[key][i] === node) {
                  (<any>parent)[key].splice(i, 1);
                  return;
               }
            }
         } else if ((<any>parent)[key] === node) {
            delete (<any>parent)[key];
            return;
         }
      }

      this.__releaseNodeTree(node);

   }

   private __replaceNode(parent: ts.Node, oldNode: ts.Node, newNode: ts.Node): void {
      for (const key in parent) {
         if ((<any>parent)[key] instanceof Array) {
            for (let i = 0; i < (<any>parent)[key].length; i++) {
               if ((<any>parent)[key][i] === oldNode) {
                  (<any>parent)[key][i] = newNode;
                  return;
               }
            }
         } else if ((<any>parent)[key] === oldNode) {
            (<any>parent)[key] = newNode;
            return;
         }
      }

      this.__releaseNodeTree(oldNode);

   }

   private __releaseNodeTree(node: ts.Node): void {
   }


   private __parseDirectives(text: string, nodePos: number): void {

      let pos: number = 0;
      let inComment: boolean = false;
      let blockCounter: number = 0;
      let commentType: CommentType = CommentType.Inline;
      let readingDirective: boolean = false;
      let directive: string = "";

      while (pos < text.length) {

         if (!inComment && text.substr(pos, 3) === "///") {

            let tsd = "";
            pos += 3;
            while (text[pos] !== "\n" && text[pos] !== "\r") {
               tsd += text[pos];
               pos++;
            }

            if (!this.__evalDirectives[this.__evalDirectives.length - 1]) {

               tsd = tsd.trim();

               let rxa: RegExpMatchArray;

               rxa = tsd.match(/<reference\s*path[\s?]*=[\s?]*"(.*)"[\s?]*\/>/);
               if (rxa && rxa.length === 2) { this.__deleteReferencedFile(rxa[1]); }
               rxa = tsd.match(/<reference\s*types[\s?]*=[\s?]*"(.*)"[\s?]*\/>/);
               if (rxa && rxa.length === 2) { this.__deleteTypeReferenceDirective(rxa[1]); }
               rxa = tsd.match(/<reference\s*lib[\s?]*=[\s?]*"(.*)"[\s?]*\/>/);
               if (rxa && rxa.length === 2) { this.__deleteLibReferenceDirective(rxa[1]); }

            }

         }

         if (!inComment && text.substr(pos, 2) === "//") {
            inComment = true;
            commentType = CommentType.Inline;
            pos++;
         }

         if (!inComment && text.substr(pos, 2) === "/*") {
            inComment = true;
            commentType = CommentType.Block;
            pos++;
         }

         if (!inComment && text[pos] === "{") {
            blockCounter++;
         }

         if (!inComment && text[pos] === "}") {
            blockCounter--;
         }

         if (inComment && commentType === CommentType.Inline && (text[pos] === "\r" || text[pos] === "\n")) {
            inComment = false;
         }

         if (inComment && commentType === CommentType.Block && text.substr(pos, 2) === "*/") {
            inComment = false;
         }

         if (!blockCounter && inComment && commentType === CommentType.Inline && text[pos] === "#" && nodePos + pos >= this.__lastParsedDirectiveEndPos) {
            directive = "";
            readingDirective = true;
            pos++;
         }

         if (readingDirective) {
            if (text[pos] === "\r" || text[pos] === "\n") {
               readingDirective = false;
               this.__lastParsedDirectiveEndPos = nodePos + pos;
               this.__parseDirective(directive);
            } else {
               directive += text[pos];
            }
         }

         pos++;

      }

   }

   private __parseDirective(directive: string): void {

      interface Directives {
         [key: string]: (expression?: string) => void;
      }

      const directives: Directives = {
         "define\\s(.*)$": (expression: string) => {
            if (this.__evalDirectives[this.__evalDirectives.length - 1]) {
               this.__evalDefineExpression(expression);
            }
         },

         "if\\s(.*)$": (expression: string) => {
            if (this.__evalDirectives[this.__evalDirectives.length - 1]) {
               this.__evalDirectives.push(this.__evalIfExpression(expression));
            } else {
               this.__evalDirectives.push(false);
            }
         },

         "else\s*$": () => {
            if (this.__evalDirectives.length === 0 || this.__evalDirectives[this.__evalDirectives.length - 2]) {
               this.__evalDirectives[this.__evalDirectives.length - 1] = !this.__evalDirectives[this.__evalDirectives.length - 1];
            }
         },

         "endif\s*$": () => {
            if (this.__evalDirectives.length > 1) {
               this.__evalDirectives.pop();
            } else {
               console.error("Missing if directive!");
            }
         }
      };

      for (const dk in directives) {
         const rx = new RegExp(dk);
         const rxa = directive.match(rx);
         if (rxa) {
            const expr = rxa.length === 2 ? rxa[1] : undefined;
            directives[dk](expr);
         }
      }

   }

   private __tokenizeExpression(expression: string): any[] {

      const tokenized: any[] = [];

      let pos = 0;
      let token = "";
      let inString = false;

      function addToken() {
         if (token !== "") {
            tokenized.push(token);
            token = "";
         }
      }

      while (pos < expression.length) {

         const ch: string = expression[pos];
         const ch2: string = expression.substr(pos, 2);

         // space, tab
         if (!inString && /\s/.test(ch)) {
            addToken();
         }

         // double char operators <= >= == != && ||
         else if (!inString && /\<\=|\>\=|\=\=|\!\=|\&\&|\|\|/.test(ch2)) {
            addToken();
            tokenized.push(ch2);
            pos++;
         }


         // single char operators and expression characters
         else if (!inString && /\+|\-|\*|\/|\\|\^|\~|\!|\%|\&|\(|\)|\[|\]/.test(ch)) {
            addToken();
            tokenized.push(ch);
         }

         else if (!inString && ch === "\"") {
            inString = true;
            token += ch;
         }

         else if (inString && ch === "\"") {
            inString = false;
            token += ch;
         }

         else {
            token += ch;
         }

         pos++;

      }

      addToken();

      return tokenized;

   }

   private __evalDefineExpression(expression: string): void {

      const expr: any[] = this.__tokenizeExpression(expression);

      if (expr.length < 1) {
         console.error("Invalid define expression");
      }

      if (this.__defines.hasOwnProperty(expr[0])) {
         console.log(`Warning: Preprocessor variable ${expr[0]} defined already`);
      }

      const key = expr[0];
      const value = true;

      if (expr.length > 1) {

         for (let i = 1; i < expr.length; i++) {
            if (this.__defines.hasOwnProperty(expr[i])) {
               if (typeof(this.__defines[expr[i]]) === "string") {
                  expr[i] = `"${this.__defines[expr[i]]}"`;
               } else {
                  expr[i] = this.__defines[expr[i]];
               }
            }
         }

         expr.splice(0, 1);
         const evalExpr = "value = " + expr.join(" ");

         try {
            eval(evalExpr);
         } catch (e) {
            console.error(e);
         }

      }

      this.__defines[key] = value;

   }

   private __evalIfExpression(expression: string): boolean {

      const expr: any[] = this.__tokenizeExpression(expression);

      // tslint:disable-next-line: prefer-const
      let value: boolean;

      if (expr.length > 0) {

         for (let i = 0; i < expr.length; i++) {
            if (this.__defines.hasOwnProperty(expr[i])) {
               if (typeof(this.__defines[expr[i]]) === "string") {
                  expr[i] = `"${this.__defines[expr[i]]}"`;
               } else {
                  expr[i] = this.__defines[expr[i]].toString();
               }
            }
         }

         const evalExpr = "value = " + expr.join(" ");

         try {
            eval(evalExpr);
         } catch (e) {
            console.error(e);
         }

      }

      return value;

   }

}




