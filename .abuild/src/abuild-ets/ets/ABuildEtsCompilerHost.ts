/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as ts from "typescript";

import api from "../../api";
import { ABuildEtsPreprocessor } from "./ABuildEtsPreprocessor";

export interface ACompilerHost extends ts.CompilerHost {
   onFileWritten: api.TEventDispatcher<string>;
}

export function createCompilerHost(options: ts.CompilerOptions): ACompilerHost {

   const onFileWritten = new api.EventDispatcher<string>();
   const preprocessor = new ABuildEtsPreprocessor();

   let origWriteFile: ts.WriteFileCallback;

   function getSourceFile(fileName: string, languageVersion: ts.ScriptTarget, onError?: (message: string) => void, shouldCreateNewSourceFile?: boolean): ts.SourceFile | undefined {

      // console.log(`Creating source file '${fileName}'`);
      const sourceText = ts.sys.readFile(fileName);
      const sourceFile = sourceText !== undefined ? ts.createSourceFile(fileName, sourceText, languageVersion) : undefined;

      if (sourceFile && !sourceFile.isDeclarationFile) {
         preprocessor.preprocessSourceFile(sourceFile);
      }

      return sourceFile;

   }

   function writeFile(fileName: string, data: string, writeByteOrderMark: boolean, onError?: (message: string) => void, sourceFiles?: readonly ts.SourceFile[]): void {
      origWriteFile(fileName, data, writeByteOrderMark, onError, sourceFiles);
      onFileWritten.dispatch(this, fileName);
   }

   const compilerHost = <ACompilerHost>ts.createCompilerHost(options, false);
   origWriteFile = compilerHost.writeFile;

   compilerHost.getSourceFile = getSourceFile;
   compilerHost.writeFile = writeFile;
   compilerHost.onFileWritten = onFileWritten;

   return compilerHost;

}
