/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as components from "../../lib/ui/components";

export class InputDialog extends components.Component {

   private __label: components.Label;
   private __input: components.InputBox;
   private __buttonOk: components.Button;
   private __buttonCancel: components.Button;

   public set label(value: string) { this.__label.text = value; }
   public set input(value: string) { this.__input.text = value; }
   public get input(): string { return this.__input.text; }

   public _onParentResize() {
      this._left = ~~(this._parent.width / 2) - ~~(this.width / 2) - 1;
      this._top = ~~(this._parent.height / 2) - ~~(this.height / 2) - 1;
      this._repaint();
   }

   protected _initializeComponent() {

      this.themeClass = "dialog";

      this.caption = "";
      this.width = 60;
      this.height = 9;
      this.scrollable = false;
      this.canFocus = true;

      this.__label = <components.Label>this.createComponent(components.Label);
      this.__label.left = 1;
      this.__label.top = 1;
      this.__label.width = this.width - 4;
      this.__label.text = "Label:";
      this.__label.onAccelerator.addListener(() => {
         this.__input.setFocus();
      });

      this.__input = <components.InputBox>this.createComponent(components.InputBox);
      this.__input.top = 2;
      this.__input.left = 1;
      this.__input.width = this.width - 4;
      this.__input.text = "";
      this.__input.setFocus();

      this.__buttonOk = <components.Button>this.createComponent(components.Button);
      this.__buttonOk.left = 16;
      this.__buttonOk.top = 4;
      this.__buttonOk.width = 12;
      this.__buttonOk.text = "&OK";
      this.__buttonOk.onPress.addListener(() => {
         this._parent.deleteComponent(this);
      });

      this.__buttonCancel = <components.Button>this.createComponent(components.Button);
      this.__buttonCancel.left = 30;
      this.__buttonCancel.top = 4;
      this.__buttonCancel.width = 12;
      this.__buttonCancel.text = "&Cancel";
      this.__buttonCancel.enabled = true;
      this.__buttonCancel.onPress.addListener(() => {
         this._parent.deleteComponent(this);
      });

   }

   protected _onKeyPress(sender: any, key: string): boolean {

      if (super._onKeyPress(sender, key)) return true;

   }

}