/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import * as components from "../../lib/ui/components";
import terminal from "../../lib/ui/terminal";

export class Root extends components.Component {

   private __copyright: components.Label;
   private __license: components.Label;

   private __content: components.Component;

   public setContent(value: typeof components.Component): components.Component {
      this.__content = this.createComponent(value);
      this.__content.setFocus();
      this.__content.onParentResize.dispatch(this);
      return this.__content;
   }

   protected _initializeComponent() {

      this.caption = "ABuild Solution Manager";

      this.__copyright = <components.Label>this.createComponent(components.Label);
      this.__copyright.text = "Copyright ©2020 Atom Software Studios";
      this.__copyright.width = this.__copyright.text.length;

      this.__license = <components.Label>this.createComponent(components.Label);
      this.__license.text = "Released under MIT license";
      this.__license.width = this.__license.text.length;

      this._width = terminal.output.width;
      this._height = terminal.output.height;

      this.__content = null;

      this._onResize();

   }

   protected _onResize() {

      this.__copyright.top = this.height - 4;
      this.__copyright.left = ~~(this.width / 2) - ~~(this.__copyright.width / 2) - 1;

      this.__license.top = this.height - 3;
      this.__license.left = ~~(this.width / 2) - ~~(this.__license.width / 2) - 1;

      if (this.__content !== null) {
         this.__content.onParentResize.dispatch(this);
      }
   }

}