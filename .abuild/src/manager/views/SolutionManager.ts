/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import terminal from "../../lib/ui/terminal";

import * as components from "../../lib/ui/components";
import { InputDialog } from "./InputDialog";

export class SolutionManager extends components.Component {

   private __labelSolutionName: components.Label;
   private __inputSolutionName: components.InputBox;

   private __labelSrcDirName: components.Label;
   private __inputSrcDirName: components.InputBox;

   private __labelBuildDirName: components.Label;
   private __inputBuildDirName: components.InputBox;

   private __labelProjects: components.Label;
   private __listProjects: components.List;

   private __buttonAddProject: components.Button;
   private __buttonEditProject: components.Button;
   private __buttonDelProject: components.Button;

   private __buttonOk: components.Button;
   private __buttonCancel: components.Button;

   private __escPressed: boolean;

   public _onParentResize() {
      this._left = ~~(this._parent.width / 2) - ~~(this.width / 2) - 1;
      this._top = ~~(this._parent.height / 2) - ~~(this.height / 2) - 1;
      this._repaint();
   }

   protected _initializeComponent() {

      this.__escPressed = false;

      this.caption = "Solution configuration";
      this.width = 54;
      this.height = 26;
      this.scrollable = false;
      this.canFocus = true;

      this.__labelSolutionName = <components.Label>this.createComponent(components.Label);
      this.__labelSolutionName.left = 1;
      this.__labelSolutionName.top = 1;
      this.__labelSolutionName.width = 14;
      this.__labelSolutionName.text = "&Solution name:";
      this.__labelSolutionName.onAccelerator.addListener((sender: any) => { this.__inputSolutionName.setFocus(); });

      this.__inputSolutionName = <components.InputBox>this.createComponent(components.InputBox);
      this.__inputSolutionName.top = 1;
      this.__inputSolutionName.left = 25;
      this.__inputSolutionName.width = this.width - 25 - 3;
      this.__inputSolutionName.text = "ajsfw";
      this.__inputSolutionName.enabled = false;

      this.__labelSrcDirName = <components.Label>this.createComponent(components.Label);
      this.__labelSrcDirName.top = 3;
      this.__labelSrcDirName.left = 1;
      this.__labelSrcDirName.width = 23;
      this.__labelSrcDirName.text = "Sou&rces directory name:";
      this.__labelSrcDirName.onAccelerator.addListener((sender: any) => { this.__inputSrcDirName.setFocus(); });

      this.__inputSrcDirName = <components.InputBox>this.createComponent(components.InputBox);
      this.__inputSrcDirName.top = 3;
      this.__inputSrcDirName.left = 25;
      this.__inputSrcDirName.width = this.width - 25 - 3;
      this.__inputSrcDirName.text = ".src";
      this.__inputSrcDirName.setFocus();

      this.__labelBuildDirName = <components.Label>this.createComponent(components.Label);
      this.__labelBuildDirName.top = 5;
      this.__labelBuildDirName.left = 1;
      this.__labelBuildDirName.width = 21;
      this.__labelBuildDirName.text = "&Build directory name:";
      this.__labelBuildDirName.onAccelerator.addListener((sender: any) => { this.__inputBuildDirName.setFocus(); });

      this.__inputBuildDirName = <components.InputBox>this.createComponent(components.InputBox);
      this.__inputBuildDirName.top = 5;
      this.__inputBuildDirName.left = 25;
      this.__inputBuildDirName.width = this.width - 25 - 3;
      this.__inputBuildDirName.text = ".build";

      this.__labelProjects = <components.Label>this.createComponent(components.Label);
      this.__labelProjects.top = 7;
      this.__labelProjects.left = 1;
      this.__labelProjects.width = 9;
      this.__labelProjects.text = "&Projects:";
      this.__labelProjects.onAccelerator.addListener((sender: any) => { this.__listProjects.setFocus(); });

      this.__listProjects = <components.List>this.createComponent(components.List);
      this.__listProjects.left = 1;
      this.__listProjects.top = 8;
      this.__listProjects.width = this.width - 4;
      this.__listProjects.height = 10;
      this.__listProjects.canFocus = true;

      this.__listProjects.addItem("Project 1");
      this.__listProjects.addItem("Project 2");
      this.__listProjects.addItem("Project 3");

      this.__buttonAddProject = <components.Button>this.createComponent(components.Button);
      this.__buttonAddProject.top = 18;
      this.__buttonAddProject.left = 1;
      this.__buttonAddProject.width = 16;
      this.__buttonAddProject.text = "&Add project";

      this.__buttonEditProject = <components.Button>this.createComponent(components.Button);
      this.__buttonEditProject.top = 18;
      this.__buttonEditProject.left = 18;
      this.__buttonEditProject.width = 16;
      this.__buttonEditProject.text = "&Edit project";

      this.__buttonDelProject = <components.Button>this.createComponent(components.Button);
      this.__buttonDelProject.top = 18;
      this.__buttonDelProject.left = 35;
      this.__buttonDelProject.width = 16;
      this.__buttonDelProject.text = "&Delete project";

      this.__buttonOk = <components.Button>this.createComponent(components.Button);
      this.__buttonOk.left = 13;
      this.__buttonOk.top = 21;
      this.__buttonOk.width = 12;
      this.__buttonOk.text = "&OK";
      this.__buttonOk.onPress.addListener(() => {
         const idialog: InputDialog = <InputDialog>this.showDialog(InputDialog);
         idialog.caption = "Test input dialog";
         idialog.label = "&Test";
         idialog.input = "Text";
      });

      this.__buttonCancel = <components.Button>this.createComponent(components.Button);
      this.__buttonCancel.left = 27;
      this.__buttonCancel.top = 21;
      this.__buttonCancel.width = 12;
      this.__buttonCancel.text = "&Cancel";
      this.__buttonCancel.enabled = true;
      this.__buttonCancel.onPress.addListener(() => {
         terminal.input.simulateKeyPress(Buffer.from("\x03", "utf8"));
      });

   }

   protected _onKeyPress(sender: any, key: string): boolean {

      if (super._onKeyPress(sender, key)) return true;

      if (key === "esc") {
         if (this.__escPressed) {
            terminal.input.simulateKeyPress(Buffer.from("\x03", "utf8"));
         } else {
            this.__escPressed = true;
         }
      } else {
         this.__escPressed = false;
      }

   }

}