/* *****************************************************************************
The MIT License (MIT)
Copyright (c)2016-2019 Atom Software Studios. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/

import { readFileSync } from "fs";

import { Color } from "../../lib/ui/common";
import { BorderStyle } from "../../lib/ui/components";

const colors = {
   screenBackground: Color.black,
   screenColor: Color.white,

   background: Color.blue,
   color: Color.white,
   controlBackground: Color.white,
   controlColor: Color.black,
   disabledBackground: Color.cyan,
   disabledColor: Color.blue,
   focusedBackground: Color.cyan,
   focusedColor: Color.black,
   focusedSelectedItemBackground: Color.blue,
   focusedSelectedItemColor: Color.white,
   acceleratorColor: Color.yellow,
   borderStyle: BorderStyle.double,

   dialogBorderStyle: BorderStyle.double,
   dialogBorderColor: Color.black,
   dialogBackground: Color.white,
   dialogColor: Color.black,
   dialogControlBackground: Color.blue,
   dialogControlColor: Color.white,
   dialogDisabledBackground: Color.cyan,
   dialogDisabledColor: Color.blue,
   dialogFocusedBackground: Color.cyan,
   dialogFocusedColor: Color.black,
   dialogAcceleratorColor: Color.yellow,
};

// Windows is using bit different color tones for the console so make it similar
if (process.platform === "win32") {
   colors.disabledBackground = Color.cyan,
   colors.disabledColor = Color.blue;
   colors.focusedBackground = Color.brightCyan;
   colors.dialogFocusedBackground = Color.brightCyan;
}

if (process.platform === "linux") {

   // Linux, but WSL (windows) console (as windows above)
   if (readFileSync("/proc/version").toString().toLowerCase().indexOf("microsoft")) {
      colors.disabledBackground = Color.cyan,
      colors.disabledColor = Color.blue;
      colors.focusedBackground = Color.brightCyan;
      colors.dialogFocusedBackground = Color.brightCyan;
   }

   // Double buffer is not supported on default linux terminal (it is in dependancy of font, but better to bet it is not)
   if (process.env["TERM"] === "linux") {
      colors.borderStyle = BorderStyle.single;
      colors.dialogBorderStyle = BorderStyle.single;
   }

}

export const theme = {

   default: {

      background: colors.background,
      color: colors.color,
      disabledBackground: colors.disabledBackground,
      disabledColor: colors.disabledColor,
      border: colors.borderStyle,
      acceleratorColor: colors.acceleratorColor,

      Root: {
         background: colors.screenBackground,
         color: colors.screenColor,
         border: BorderStyle.single
      },

      Label: {
      },

      CheckBox: {
         background: Color.inherit,
         color: Color.inherit,
         disabledBackground: colors.disabledBackground,
         disabledColor: colors.disabledColor,

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocus: function() {
            this.background = colors.focusedBackground;
            this.color = colors.focusedColor;
         },

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocusLost: function() {
            this.background = Color.inherit;
            this.color = Color.inherit;
         }

      },

      RadioButton: {
         background: Color.inherit,
         color: Color.inherit,
         disabledBackground: colors.disabledBackground,
         disabledColor: colors.disabledColor,

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocus: function() {
            this.background = colors.focusedBackground;
            this.color = colors.focusedColor;
         },

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocusLost: function() {
            this.background = Color.inherit;
            this.color = Color.inherit;
         }

      },

      Button: {
         background: colors.controlBackground,
         color: colors.controlColor,

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocus: function() {
            this.background = colors.focusedBackground;
            this.color = colors.focusedColor;
         },

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocusLost: function() {
            this.background = colors.controlBackground;
            this.color = colors.controlColor;
         }

      },

      InputBox: {

         background: colors.controlBackground,
         color: colors.controlColor,

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocus: function() {
            this.background = colors.focusedBackground;
            this.color = colors.focusedColor;
         },

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocusLost: function() {
            this.background = colors.controlBackground;
            this.color = colors.controlColor;
         }

      },

      List: {
         background: colors.controlBackground,
         color: colors.controlColor,
         selectedItemBackground: colors.focusedBackground,
         selectedItemColor: colors.focusedColor,
         border: BorderStyle.single,
         borderColor: Color.blue,

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocus: function() {
            this.background = colors.focusedBackground;
            this.color = colors.focusedColor;
            this.selectedItemBackground = colors.focusedSelectedItemBackground;
            this.selectedItemColor = colors.focusedSelectedItemColor;
         },

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocusLost: function() {
            this.background = colors.controlBackground;
            this.color = colors.controlColor;
            this.selectedItemBackground =  colors.focusedBackground;
            this.selectedItemColor = colors.focusedColor;
         }
      },

      ListItem: {
      }

   },

   dialog: {

      background: colors.dialogBackground,
      color: colors.dialogColor,
      disabledBackground: colors.dialogDisabledBackground,
      disabledColor: colors.dialogDisabledColor,
      borderColor: colors.dialogBorderColor,
      border: colors.dialogBorderStyle,
      acceleratorColor: colors.dialogAcceleratorColor,

      Label: {
      },

      Button: {
         background: colors.dialogControlBackground,
         color: colors.dialogControlColor,

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocus: function() {
            this.background = colors.dialogFocusedBackground;
            this.color = colors.dialogFocusedColor;
         },

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocusLost: function() {
            this.background = colors.dialogControlBackground;
            this.color = colors.dialogControlColor;
         }

      },

      InputBox: {

         background: colors.dialogControlBackground,
         color: colors.dialogControlColor,

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocus: function() {
            this.background = colors.dialogFocusedBackground;
            this.color = colors.dialogFocusedColor;
         },

         // tslint:disable-next-line: object-literal-shorthand
         setOnFocusLost: function() {
            this.background = colors.dialogControlBackground;
            this.color = colors.dialogControlColor;
         }

      },


   }


};
